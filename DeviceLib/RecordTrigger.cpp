#include "RecordTrigger.h"
#include "SystemManager.h"
#include <iostream>
#include "TriggerManager.h"

namespace DeviceLib
{

	RecordTrigger::RecordTrigger()
		: m_EventType(BEGIN)
		, m_ArmCondition(0)
		, m_RecordCondition(0)
		, m_Armed(false)
		, m_Triggered(false)
		, m_Done(false)
	{
	}

	RecordTrigger::~RecordTrigger()
	{
	}

	void RecordTrigger::ArmTrigger()
	{
		m_Armed = true;
		m_Done = false;
	}

	/// IDeviceDataUpdateListener implementation.
	void RecordTrigger::UpdateEntryData(DeviceData* data)
	{
		if (!m_Triggered)
		{
			long long nArmTime = 0;

			/*
			m_Armed = !m_Armed && m_ArmCondition == 0 ? true : m_Armed;

			if (!m_Armed && m_ArmCondition)
			{
				if (m_ArmCondition->CheckTriggerCondition())
				{
					m_Armed = true;
				}
				else
				{
					nArmTime = m_ArmCondition->GetTriggerNanoSecond();
				}
			}
			*/
			if (m_Armed && m_RecordCondition)
			{
				bool bTrigger = false;
				long long nTriggerTime = 0;
				if (m_RecordCondition->CheckTriggerCondition())
				{
					nTriggerTime = m_RecordCondition->GetTriggerNanoSecond();
					if (nArmTime <= nTriggerTime)
					{
						bTrigger = true;
					}
					else
					{
						bTrigger = false;
					}
				}

				if (bTrigger && !m_Triggered)
				{
					SystemManager::GetInstance()->SetRecording(true);
					for (size_t i = 0; i < m_TriggerListeners.size(); ++i)
					{
						m_TriggerListeners[i]->PerformTriggerEvent(nTriggerTime);
					}
					m_Armed = false;
					m_Triggered = true;
				}
			}
		}
	}

	void RecordTrigger::UpdateExitData(DeviceData* data)
	{
		if (m_Triggered && !m_Done)
		{
			if (TriggerManager::GetInstance()->RecordFinished())
			{
				SystemManager::GetInstance()->SetRecording(false);
				m_Done = true;
			}
		}
	}

	bool RecordTrigger::StartListening()
	{
		m_Armed = false;
		m_Triggered = false;
		return true;
	}

	void RecordTrigger::StopListening()
	{
		m_Armed = false;
		m_Triggered = false;
	}

	/// ITrigger implementation.
	void RecordTrigger::SetTriggerArmCondition(ITriggerCondition* condition)
	{
		if (condition)
			m_ArmCondition = condition;
	}

	void RecordTrigger::SetTriggerCondition(ITriggerCondition* condition)
	{
		if (condition)
			m_RecordCondition = condition;
	}

	void RecordTrigger::AddTriggerListener(ITriggerListener* listener)
	{
		if (listener)
			m_TriggerListeners.push_back(listener);
	}

	void RecordTrigger::Reset()
	{
		m_TriggerListeners.clear();
		m_Triggered = false;
		m_Armed = false;
		m_Done = false;
		m_ArmCondition = m_RecordCondition = 0;
	}

}