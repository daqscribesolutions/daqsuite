#ifndef CHANNELMONITOR_H
#define CHANNELMONITOR_H

#include "ITrigger.h"
#include "boost/thread.hpp"

namespace DeviceLib
{

	class ChannelInfo;

	/// ChannelMonitor implements the time based trigger.
	class RecordSizeMonitor : public ITriggerCondition
	{

	public:
		/// The full constructor
		RecordSizeMonitor();

		/// The destructor
		~RecordSizeMonitor();

		/// ITriggerCondition implementation.
		virtual long long GetTriggerNanoSecond();

		virtual bool CheckTriggerCondition();

		virtual void SetChannel(ChannelInfo*, int) {}

		virtual void Reset() { m_CurrentDataSize = 0; }

		/// Stop terminates the record.
		void Stop() { m_bStop = true; }

	protected:

		/// The flag for the forced stop.
		bool m_bStop;

		/// The time when the condition is met in nanosecond.
		long long m_TriggerNanoSecond;

		/// The searching index to support searching whole channel after the condition is met once.
		long long m_CurrentDataSize;

	};

	/// ChannelMonitor implements the time based trigger.
	class ChannelMonitor :	public ITriggerCondition
	{

	public:
		/// The full constructor
		ChannelMonitor(ChannelInfo* channel, int chidx, long long triggerTime = 0);

		/// The destructor
		~ChannelMonitor();

		/// ITriggerCondition implementation.
		virtual long long GetTriggerNanoSecond();

		virtual bool CheckTriggerCondition();

		virtual void SetChannel(ChannelInfo* channel, int idx);
		
		/// Reset sets the search data index to the beginning of the data.
		virtual void Reset() { m_CurrentDataSize = 0; }

	protected:

		/// The channel pointer to be monitored.
		ChannelInfo* m_pChannel;

		/// The time when the condition is met in nanosecond.
		long long m_TriggerNanoSecond;

		/// The searching index to support searching whole channel after the condition is met once.
		long long m_CurrentDataSize;

		/// The channel idex.
		int m_ChIdx;

	};

	/// MonitorDataType decides the data to monitor.
	enum MonitorDataType { MILLIVOLT, EUVALUE };

	/// MonitorLimitType decides the type of limit.
	enum MonitorLimitType { UPPER, LOWER };

	/// LimitChannelMonitor implements logic to check channel data under or over the limit.
	class LimitChannelMonitor : public ChannelMonitor
	{

	public:
		/// The full constructor
		LimitChannelMonitor(ChannelInfo* channel, int chidx, double dlimit, MonitorLimitType limitType, MonitorDataType dataType);

		/// The destructor
		~LimitChannelMonitor();

		/// ITriggerCondition implementation.
		virtual long long GetTriggerNanoSecond();

		virtual bool CheckTriggerCondition();

	protected:
		
		/// The limit value.
		double m_Limit;

		/// The limit type.
		MonitorLimitType m_LimitType;

		/// The data type.
		MonitorDataType m_DataType;

	};

	/// EdgeChannelMonitor implements logic to check channel data falling under or rising over the limit.
	class EdgeChannelMonitor : public LimitChannelMonitor
	{

	public:
		/// The full constructor
		EdgeChannelMonitor(ChannelInfo* channel, int chidx, double dlimit, MonitorLimitType limitType, MonitorDataType dataType);

		/// The destructor
		~EdgeChannelMonitor();

		/// ITriggerCondition implementation.
		virtual long long GetTriggerNanoSecond();

		virtual bool CheckTriggerCondition();
		
	private:

		/// The value to check continuous edge.
		double m_lastValue;

		/// The flag to validity of the last value.
		bool m_first;

	};
}

#endif