#ifndef TRIGGERMANAGER_H
#define TRIGGERMANAGER_H

#include <vector>
#include "DeviceDataRecorder.h"
#include "ChannelMonitor.h"
#include <ctime>

namespace DeviceLib
{
	class RecordTrigger;
	class IDevice;
	class ITriggerCondition;
	enum TriggerControlType { NONE, TIME, LIMIT, EDGE };

	/**
	 * TriggerManager manages the triggers.
	 */
	class TriggerManager
	{

	public:
		/**
		 * GetInstance supports the singleton.
		 * @return The TriggerManager pointer.
		 */
		static TriggerManager* GetInstance();

		/**
		 * TriggerBegin triggers the record beginning with command.
		 */
		void TriggerBegin();

		/**
		 * TriggerEnd triggers the record ending with command.
		 */
		void TriggerEnd();

		/**
		 * ArmTrigger sets the trigger to check the trigger condition.
		 */
		void ArmTrigger();

		/// The accessor for the trigger control type.
		TriggerControlType GetTriggerControlType() { return m_CurrentTriggerType; }

		/// The setter for the trigger control type.
		void SetTriggerControlType(TriggerControlType controltype) { m_CurrentTriggerType = controltype; }

		/// The getter for the recording data speed.
		double GetRecordSpeed();

		/// The getter for the recording data sample (peak) speed.
		double GetRecordSampleSpeed();
		
		/// The getter for the recording data size.
		double GetTotalRecordedSize();

		/// The getter for the condition trigger limit.
		double GetLimit() { return m_limit; }

		/// The setter for the condition trigger limit.
		void SetLimit(double limit) { m_limit = limit; }

		/// The getter for the pre trigger time.
		double GetPreTrigger() { return m_PreTrigger; }

		/// The setter for the pre trigger time.
		void SetPreTrigger(double val) { m_PreTrigger = std::min(2.0, std::max(0.0, val)); }

		/// The getter for the condition trigger channel.
		int GetTriggerChannelIndex() { return m_TriggerChannel; }

		/// The setter for the condition trigger channel.
		void SetTriggerChannelIndex(int channel);


		/// The getter for the condition trigger limit type.
		MonitorLimitType GetTriggerLimitType() { return m_limitType; }

		/// The setter for the condition trigger limit type.
		void SetTriggerLimitType(MonitorLimitType val) { m_limitType = val; }

		/// The getter for the condition trigger value type.
		MonitorDataType GetTriggerLimitValueType() { return m_limitDataType; }

		/// The setter for the condition trigger value type.
		void SetTriggerLimitValueType(MonitorDataType val) { m_limitDataType = val; }

		/// RecordFinished checks the recorders whether they are finished or not. 
		/// If all recorders are finished, return true, otherwise return false.
		bool RecordFinished();

		/**
		 * SetCommandTrigger sets the record trigger with command.
		 */
		void SetCommandTrigger();

		/// The getter for the schedule time for trigger.
		tm GetTriggerScheduleTime() { return m_ScheduleTime; }

		/// The setter for the schedule time for trigger.
		void SetTriggerScheduleTime(tm val) { m_ScheduleTime = val; }

		/// The accessor for the schedule trigger.
		bool GetScheduleTrigger();

		/// The accessor for the snapshot flag.
		bool GetSnapshot(){ return m_bSnapshot; }

		void SetSnapshot(bool bval) { m_bSnapshot = bval; }

		bool IsArmed();

		/// The number of snapshot to keep track of snapshot.
		int nSnapshot;

		/// ResetTrigger removes the trigger.
		void ResetTrigger();

	protected:

		/**
		 * SetTimeTrigger sets the record trigger with time based trigger.
		 */
		void SetTimeTrigger();

		/**
		 * SetLimitTrigger sets the record trigger with upper limit based trigger.
		 */
		void SetLimitTrigger(bool bEdge);


	private:

		/// ClearRecorders frees the memory and removes the recorders.
		void ClearRecorders();

		/// The process to set trigger channel.
		void SetChannelInfo();

		/// The trigger type for record.
		TriggerControlType m_CurrentTriggerType;

		/// The limit for the condition trigger.
		double m_limit;
		
		/// The pre trigger time to record in second.
		double m_PreTrigger;

		/// The condition type.
		MonitorLimitType m_limitType;
		
		/// The condition data type.
		MonitorDataType m_limitDataType;

		/// The channel number for the condition trigger.
		int m_TriggerChannel;

		int m_DeviceChannelIndex;

		ChannelInfo* m_DeviceChannel;

		RecordTrigger* m_RecordBegin;

		ITriggerCondition* m_BeginCondition;

		std::vector<DeviceDataRecorder*> m_Recorders;

		tm m_ScheduleTime;

		tm m_Now;

		bool m_bSnapshot;
		
		static TriggerManager* m_Instance;

		TriggerManager();

		~TriggerManager();

		void Clear();

	};

}
#endif