#include "DeviceDataRecorder.h"
#include "DeviceDataManager.h"
#include "DeviceRecordSetting.h"
#include <ctime>
#include <algorithm>
#include <iostream>
#include <sstream>
#define INCLUDE_LDSF
#include "ldsf.h"
#include "logger.h"
#include "SystemManager.h"
#include "TriggerManager.h"
#include "DeviceDataReader.h"
#include "DataConverter.h"
#include "IADCDevice.h"
#include <boost/filesystem.hpp>

#ifdef WIN32
#pragma warning (disable : 4996)
#endif

namespace DeviceLib
{

	DeviceDataRecorder::DeviceDataRecorder(const char* filename, DeviceDataManager* mgr, bool bSnapshot)
		: m_RecordDataSize(0)
		, m_NumberOfChannels(0)
		, m_RecordDuration(0)
		, m_SampleRate(1000)
		, m_InputMilliVolt(1000)
		, m_TriggerTimeNanoSecond(0)
		, m_Status(STOP)
		, m_Recording(false)
		, m_bFinished(false)
		, m_Manager(mgr)
		, m_Format(FOUR)
		, m_RecordTime(0)
		, m_bSnapshot(bSnapshot)
		, m_Snapshot(0)
	{
		if (filename)
		{
			m_FileName = filename;
		}
	}

	DeviceDataRecorder::~DeviceDataRecorder()
	{
		if (m_Snapshot) delete m_Snapshot;
		m_Snapshot = 0;
	}

	bool DeviceDataRecorder::IsReady()
	{
		return !m_bFinished;
	}

	void DeviceDataRecorder::UpdateExitData(DeviceData* data)
	{
		if (data && m_Status == RUN)
		{
			m_Format = data->GetFormat();
			m_NumberOfChannels = data->GetNumberOfChannels();
			m_SampleRate = data->GetSampleRatePerSecond();
			m_InputMilliVolt = data->GetInputMilliVolt();
			m_Recording = true;
			double duration = TriggerManager::GetInstance()->GetPreTrigger() 
				+ DeviceRecordSetting::GetInstance()->GetRecordDuration() / 1000000000.0;
			long long record = (long long)(m_Format * m_NumberOfChannels * m_SampleRate * duration);
			if (m_RecordDataSize == 0)
			{
				delete[] m_Snapshot;
				m_Snapshot = new char[(size_t)record];
				m_SpeedMonitor.Reset();
			}
			long long begin = data->GetStartNanoSecond();
			long long end = data->GetStartNanoSecond() + data->GetDurationNanoSecond();
			long long saveBeginTime = std::max((long long)0, m_TriggerTimeNanoSecond 
				- (long long)(TriggerManager::GetInstance()->GetPreTrigger() * 1000000000));
			if (end < saveBeginTime)
			{
				m_Recording = false;
				return;
			}
			if (m_RecordDuration != 0 && begin > saveBeginTime + m_RecordDuration)
			{
				if (!m_bFinished) Close();
				m_Recording = false;
				return;
			}
			begin = std::max(begin, saveBeginTime) - data->GetStartNanoSecond();
			if (m_RecordDuration != 0)
			{
				end = std::min(end, saveBeginTime + m_RecordDuration) - data->GetStartNanoSecond();
			}
			else
			{
				end -= data->GetStartNanoSecond();
			}
			long long databegin = data->GetADCCountDataSizeOfNanoSecond(begin);
			long long datasize = data->GetADCCountDataSizeOfNanoSecond(end - begin);
			if (datasize > 0)
			{
				if (m_OutStream.good())
				{
					m_OutStream.write(data->GetADCCountData() + databegin, datasize);
				}
				if (m_bSnapshot && record >= m_RecordDataSize + datasize)
				{
					memcpy(m_Snapshot + (size_t)m_RecordDataSize, data->GetADCCountData() + databegin, (size_t)datasize);
				}
				m_SpeedMonitor.UpdateData(datasize);
				m_SpeedMonitor.EndSample();
				m_SpeedMonitor.BeginSample();
				m_RecordDataSize += datasize;
			}
			if (data->GetADCCountDataSizeOfNanoSecond(m_RecordDataSize) >= m_RecordDuration)
			{
				if (!m_bFinished) Close();
				m_Recording = false;
				return;
			}
			m_Recording = false;
		}
	}

	void DeviceDataRecorder::UpdateEntryData(DeviceData* data)
	{
	}

	void DeviceDataRecorder::Open()
	{
		m_mutex.lock();
		if (m_OutStream.is_open()) m_OutStream.close();
		time(&m_RecordTime);
		for (int i = 0; i < 512; ++i)
		{
			m_Header[i] = ' ';
		}
		if (!m_bSnapshot)
		{
			if (m_FileName != "")
			{
				m_OutStream.open(m_FileName.c_str(), std::ios::binary);
				m_OutStream.write(m_Header, 512);
			}
		}
		m_bFinished = false;
		m_mutex.unlock();
	}

	void DeviceDataRecorder::Close()
	{
		m_mutex.lock();
		if (m_FileName != "" && m_OutStream.is_open())
		{
			m_OutStream.seekp(0);
			GetHeader();
			m_OutStream.write(m_Header, 512);
			m_OutStream.close();
		}
		if (m_bSnapshot && m_RecordDataSize > 0)
		{
			long long pretrigger = (long long)(TriggerManager::GetInstance()->GetPreTrigger() * 1000000000);
			long long duration = pretrigger + DeviceRecordSetting::GetInstance()->GetRecordDuration();
			DeviceData *data = new DeviceData(-pretrigger, duration, (long)m_SampleRate, m_NumberOfChannels, m_InputMilliVolt, m_Format);
			memcpy(data->GetADCCountData(), m_Snapshot, (size_t)m_RecordDataSize);
			m_Manager->AddSnapshotData(data);
		}
		m_RecordDataSize = 0;
		m_bFinished = true;
		m_mutex.unlock();
	}

	bool DeviceDataRecorder::StartListening()
	{
		m_Status = RUN;
		return true;
	}

	void DeviceDataRecorder::StopListening()
	{
		m_Status = STOP;
		if (!m_bFinished) Close();
	}

	void DeviceDataRecorder::GetHeader()
	{
		tm* t = localtime(&m_RecordTime);

		sprintf(m_Header, "%d Bytes Recorded:Run Number: 0:Buffer Size: 512:Record Rate:%.1f:Header File##Channel Count:%d:Stream Type:%d:Record Start Date YYYYMMDD:%d%.2d%.2d:Record Start Time HHMMSS:%.2d%.2d%.2d:",
			(int)m_RecordDataSize, m_SampleRate, m_NumberOfChannels, (m_Format == TWO ? 4 : 5), t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
		auto triggersetting = TriggerManager::GetInstance();
		std::string header = m_Header;
		header += "trigger used:" + std::to_string((long long)triggersetting->GetTriggerControlType());
		header += ":trigger channel:" + std::to_string((long long)triggersetting->GetTriggerChannelIndex());
		header += ":trigger type:" + std::to_string((long long)triggersetting->GetTriggerLimitType());
		header += ":trigger level:" + std::to_string((long double)(triggersetting->GetLimit()));
		header += ":trigger unit:" + std::to_string((long long)(triggersetting->GetTriggerLimitValueType()));
		header += ":Input voltage range(milivolt):" + std::to_string((long double)(m_InputMilliVolt));
		strcpy(m_Header, header.c_str());
	}

	void DeviceDataRecorder::PerformTriggerEvent(long long triggertime)
	{
		m_TriggerTimeNanoSecond = triggertime;
		m_RecordDuration = DeviceRecordSetting::GetInstance()->GetRecordDuration()
			+ (long long)(TriggerManager::GetInstance()->GetPreTrigger() * 1000000000);
		StartListening();
		Open();
	}

	void DeviceDataRecorder::StopTriggerEvent(long long triggertime)
	{
		while (m_Recording)
		{
		}
		if (!m_bFinished) Close();
		m_RecordDuration = 0;
		m_RecordDataSize = 0;
		StopListening();
	}

	void DeviceDataRecorder::SetRecordNanosecond(long long duration)
	{
		m_RecordDuration = duration;
	}

	void DeviceDataRecorder::Convert()
	{
		RECORD_TYPE record = SystemManager::GetInstance()->GetRecordType();
		if (record == LDSF_FILE || record == RAWFS_LDSF)
		{
			std::string filename = m_FileName;
			m_FileName = "";
			if (!m_bFinished) Close();
			ConvertToLDSF(filename, m_Manager->GetActiveChannels());
			if (record == LDSF_FILE)
			{
				try
				{
					boost::filesystem::remove(filename.c_str());
				}
				catch (std::exception& ex)
				{
					Logger::GetInstance()->Log(ex.what());
				}
			}
		}
	}

	void DeviceDataRecorder::ConvertToLDSF(const std::string filename, const std::vector<ChannelInfo*>& channels)
	{
		DeviceDataReader reader(filename.c_str());
		if (reader.IsOpen())
		{
			boost::filesystem::path dir(filename);
			dir = dir.replace_extension("ldsf");
			if (boost::filesystem::exists(dir))
			{
				try
				{
					boost::filesystem::remove(dir);
				}
				catch (std::exception& ex)
				{
					Logger::GetInstance()->Log(ex.what());
				}
			}
#ifdef INCLUDE_LDSF
            auto data = reader.GetDeviceData();
            auto manager = SystemManager::GetInstance();
            std::vector<std::vector<double> > EUValues;
            DataConverter dataconverter(data, 0, false);
            std::shared_ptr<LDSF::FileWriter> m_LDSFWriter(new LDSF::FileWriter(LDSF::toWString(dir.string())));
			std::wstring desc = LDSF::toWString(DeviceRecordSetting::GetInstance()->GetRecordName()) + L" "
				+ LDSF::toWString(DeviceRecordSetting::GetInstance()->GetRecordNumber() - 1);
			m_LDSFWriter->setApplication(desc);
			const int MAX_NUM_VALUES = 1000;
			double step = 1.0 / data->GetSampleRatePerSecond();
			if (m_LDSFWriter && channels.size() > 0)
			{
				LDSF::INT8 count = 0;
				LDSF::Unit timeUnit(LDSF::Quantity(L"time", false, 0, 0, 1, 0, 0, 0, 0, 0), 1, 0.0, 1.0, L"s");
				LDSF::EquidistantDataType timeType(0.0, step);
				std::wstring timeName = L"Second";
				LDSF::DomainWriter domain = m_LDSFWriter->createDomain(timeName,
					timeType,
					timeUnit,
					-1);
				domain.setSession(desc);
				for (size_t chanIdx = 0;
					chanIdx < static_cast<size_t>(channels.size());
					++chanIdx)
				{
					auto& channelinfo = channels[chanIdx];
					std::wstring channelName = LDSF::toWString(channelinfo->Name);
					std::wstring EUName = LDSF::toWString(channelinfo->EUName);
					LDSF::Unit channelUnit(LDSF::Quantity(EUName, false, 1, 1, -2, 0, 0, 0, 0, 0), 1.0, 0.0, 1.0, EUName);
					LDSF::NumericDataType<LDSF::FLOAT8> channelType;
					LDSF::ChannelWriter channel = domain.createChannel(channelName,
						channelType,
						channelUnit);
					channel.setPhysicalId(L"Physical" + LDSF::toWString(count));
					channel.setPrimaryId(channelName);
					channel.setSecondaryId(L"Secondary" + LDSF::toWString(count));
					channel.setChannelGroup(static_cast<LDSF::ChannelGroupValues>(count % LDSF::FIRST_UNKNOWN_CHANNELGROUPVALUE));
					channel.setChannelId(chanIdx);
					++count;
				}
				LDSF::Unit xUnit(LDSF::Quantity(L"Time", false, 1, 1, -2, 0, 0, 0, 0, 0), 1.0, 0.0, 1.0, L"Second");
				while (true)
				{
					dataconverter.ConvertADCtoMilivoltAndEUValue(EUValues, channels);
					size_t ndata = (size_t)(data->GetDataByteSize() / data->GetFormat() / channels.size());
					size_t nFrames = (size_t)ceil((double)ndata / MAX_NUM_VALUES);
					for (size_t fi = 0; fi < nFrames; ++fi)
					{
						size_t nframedata = MAX_NUM_VALUES;
						size_t currentdata = fi * MAX_NUM_VALUES;
						if (currentdata + MAX_NUM_VALUES > ndata)
						{
							nframedata = ndata - currentdata;
						}
						if (nframedata <= 0) continue;

						LDSF::ChannelWriterVec channelsv;
						domain.getDataChannels(channelsv);
						size_t channelIdx = 0;
						LDSF::ChannelWriterVec::iterator chanEnd = channelsv.end();
						for (LDSF::ChannelWriterVec::iterator chanIt = channelsv.begin();
							chanIt != chanEnd;
							++chanIt, ++channelIdx)
						{
							auto& val = EUValues[channelIdx];
							LDSF::NumericSliceWriter<LDSF::FLOAT8> slice =
								chanIt->getSlice().getLossyUnitConversionSlice<LDSF::FLOAT8>(
								chanIt->getUnit());
							LDSF::SliceIterator sliceEnd = slice.end();
							size_t nv = fi * MAX_NUM_VALUES;
							for (LDSF::SliceIterator sliceIt = slice.begin();
								((nv < nframedata) && sliceIt < sliceEnd);
								++sliceIt)
							{
								if (nv >= (size_t)val.size()) break;
								slice.setValue(val[nv], sliceIt);
								++nv;
							}
							slice.release();
						}
					}
					if (reader.HasMoreData())
					{
						reader.ReadNext();
					}
					else
					{
						break;
					}
				}
			}
			m_LDSFWriter->close();
#endif
		}
	}
}
