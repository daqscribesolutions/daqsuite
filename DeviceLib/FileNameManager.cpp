#include "FileNameManager.h"
#include "SystemManager.h"
#include "DeviceRecordSetting.h"
#include "TriggerManager.h"
#include "IDevice.h"

#ifdef WIN32
#pragma warning(disable : 4996)
#endif

namespace DeviceLib
{
	std::string NameItem::GetItem()
	{
		std::string name;
		auto devices = SystemManager::GetInstance()->GetDevices();
		auto t = TriggerManager::GetInstance()->GetTriggerScheduleTime();
		std::stringstream str;
		switch (itemType)
		{
		case DeviceLib::PLAN_NAME:
			name = DeviceRecordSetting::GetInstance()->GetRecordName();
			break;

		case DeviceLib::DEVICE_NAME:
			if (devices.size() > 0)
			{
				name = devices.front()->GetDeviceInfo();
			}
			return name;
			break;
		case DeviceLib::RECORD_NUMBER:
			name = std::to_string((long long)DeviceRecordSetting::GetInstance()->GetRecordNumber());
			break;
		case DeviceLib::RECORD_TIME:
			str << t.tm_hour << "_" << t.tm_min << "_" << t.tm_sec;
			name = str.str();
			break;
		case DeviceLib::RECORD_DATE:
			str << t.tm_year + 1900 << "_" << t.tm_mon + 1 << "_" << t.tm_mday;
			name = str.str();
			break;
		case DeviceLib::USER_DEFINED:
			name = item;
			break;
		default:
			break;
		}
		return name;
	}

	FileNameManager* FileNameManager::m_Instance = 0;

	FileNameManager* FileNameManager::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new FileNameManager();
		}
		return m_Instance;
	}

	std::string FileNameManager::GetFileName(int index)
	{
		std::string name;
		for (int i = 0; i < (int)m_items.size(); ++i)
		{
			auto item = m_items[i];
			name += item.GetItem();
		}
		name += "_" + std::to_string((long long)index);
		return name;
	}

	FileNameManager::FileNameManager()
	{
		m_items.resize(5);
	}

	FileNameManager::~FileNameManager()
	{
	}
}