#ifdef _WIN32
#pragma warning(disable : 4267)
#pragma warning(disable : 4996)
#pragma warning(disable : 4100)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#endif
#endif

#include "logger.h"
#include "SystemManager.h"
#include "TriggerManager.h"
#include "DeviceRecordSetting.h"
#include "FileNameManager.h"
#include "ClientDevice.h"
#include "ServerDevice.h"
#include "VirtualDevice.h"
#include "DataConverter.h"
#include "DeviceDataRecorder.h"
#include "StorageManager.h"

/** 
 * \mainpage DaqScribe Device Library (DeviceLib) 
 *
 * \section intro_sec Introduction
 *
 * DeviceLib is the library to support digital data recorder including data monitoring and recording
 * for ADC devices and network cards. Recording engine supports rawfs file format for ADC devices and 
 * pcap file format for network cards.
 * 
 * \section adc_sec Features for ADC(Analog to Digital Converter) devices
 * -# The virtual device to emulate the device with sine signal in 50 per second frequency.
 * -# The device data manager to convert, receive and send the device data based on duration.
 * -# The TCP connection to send/receive the device data.
 * -# The recording engine to save in the raw data file format (.rawfs). The input voltage is added in the header.
 * -# The basic triggers for any event based on the device data both voltage and engineering unit data.
 *  - The limit trigger (upper/lower).
 *  - The crossing limit trigger (upward/downward).
 *
 * \section network_sec Features for Network cards
 * -# The recording and replaying for pcap file format for the generic network cards and Napatech 10G cards.
 *
 * All rights reserved to DaqScribe Solution. LLC
 */