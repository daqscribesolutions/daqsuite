#ifndef FILECONVERTMANAGER_H
#define FILECONVERTMANAGER_H

#include <queue>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#ifdef WIN32
#pragma warning(disable : 4100)
#endif

namespace DeviceLib
{
	class DeviceDataRecorder;

	/**
 	 */
	class FileConvertManager
	{
	public:

		static FileConvertManager * GetInstance();

		void Start();

		void AddRecorder(DeviceDataRecorder* recorder);

		void Stop();

		void Convert();

		void Clear();

		bool IsEmpty() { return m_Recorders.empty(); }

	private:

		/// The thread to run the device.
		boost::thread* m_Thread;

		/**
		 * FileConvertRunner is the functor that implements the virtual device thread logic.
		 */
		struct FileConvertRunner
		{
			/**
			 * FileConvertRunner is the full constructor.
			 * @param pDevice The file convert manager pointer to be run. set.
			 */
			FileConvertRunner(FileConvertManager* pManager) : m_pConverManager(pManager), m_bRunning(true) {}

			/**
			 * operator() is the implementation of the file convert manager running logic.
			 */
			void operator()()
			{
				if (m_pConverManager)
				{
					while (m_bRunning)
					{
						m_pConverManager->Convert();
					}
				}
			}

			/**
			 * Reset makes to start the thread.
			 * @return The thread is set to run continually.
			 */
			void Reset() { m_bRunning = true; }

			/**
			 * Stop stops the thread.
			 * @return The thread is stopped.
			 */
			void Stop() { m_bRunning = false; }

		private:

			bool m_bRunning;

			FileConvertManager* m_pConverManager;
		};

		/// The functor to implement the logic of running the device.
		FileConvertRunner* m_Runner;
	
		std::queue<DeviceDataRecorder*> m_Recorders;

		static FileConvertManager * instance;

		FileConvertManager();


	};

}

#endif