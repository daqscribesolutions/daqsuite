#include "ChannelInfo.h"
#include <cstdlib>
#include "DeviceData.h"
#include "IADCDevice.h"

namespace DeviceLib
{

	ChannelInfo::ChannelInfo(const std::string& name, IADCDevice* pdev, double sensitivity, double offset, long long durationNanoSecond, long sampleratepersecond)
		: m_Sensitivity(sensitivity)
		, Name(name)
		, m_pDevice(pdev)
		, m_Offset(offset)
		, m_EUSensitivity(sensitivity)
		, m_EUOffset(offset)
		, m_DurationNanoSecond(0)
		, m_SampleRatePerSecond(0)
		, m_DataSize(0)
		, m_bActive(true)
		, EUName("mV")
		, ActualDuration(0)
		, ActualDataSize(0)
		, MilivoltDataMin(0)
		, MilivoltDataMax(0)
		, EUDataMin(0)
		, EUDataMax(0)
		, Average(0)
		, RMS(0)
		, EUAverage(0)
		, EURMS(0)
		, m_SnapshotData(0)
		, pOrigin(0)
	{
		Init(durationNanoSecond, sampleratepersecond);
	}

	ChannelInfo::~ChannelInfo()
	{
		ClearData();
	}

	double ChannelInfo::GetSensitivity()
	{
		return m_Sensitivity;
	}

	void ChannelInfo::SetSensitivity(double value)
	{
		m_Sensitivity = value;
	}

	double ChannelInfo::GetOffset()
	{
		return m_Offset;
	}

	void ChannelInfo::SetOffset(double value)
	{
		m_Offset = value;
	}

	double ChannelInfo::GetEUSensitivity()
	{
		return m_EUSensitivity;
	}

	void ChannelInfo::SetEUSensitivity(double value)
	{
		m_EUSensitivity = value;
	}

	double ChannelInfo::GetEUOffset()
	{
		return m_EUOffset;
	}

	void ChannelInfo::SetEUOffset(double value)
	{
		m_EUOffset = value;
	}

	void ChannelInfo::Init(long long duration, long sampleratepersecond)
	{
		if (m_DurationNanoSecond != duration ||  m_SampleRatePerSecond != sampleratepersecond)
		{
			m_DurationNanoSecond = duration;
			m_SampleRatePerSecond = sampleratepersecond;
			m_DataSize = static_cast<long long>(m_DurationNanoSecond * m_SampleRatePerSecond / 1000000000.0);
		}
	}

	void ChannelInfo::SetSnapShotData(DeviceData* data)
	{ 
		m_SnapshotData = data;
		m_DurationNanoSecond = data->GetDurationNanoSecond();
		m_StartNanoSecond = data->GetStartNanoSecond();
		m_SampleRatePerSecond = data->GetSampleRatePerSecond();
		m_DataSize = static_cast<long long>(m_DurationNanoSecond * m_SampleRatePerSecond / 1000000000.0);
	}

	void ChannelInfo::ClearData()
	{
	}

	long ChannelInfo::GetSampleRatePerSecond()
	{
		return m_SampleRatePerSecond;
	}

	void ChannelInfo::SetSampleRatePerSecond(long value)
	{
		m_SampleRatePerSecond = value;
	}

	long long ChannelInfo::GetStartNanoSecond()
	{
		return m_StartNanoSecond;
	}

	void ChannelInfo::SetStartNanoSecond(long long value)
	{
		m_StartNanoSecond = value;
	}

	long long ChannelInfo::GetDurationNanoSecond()
	{
		return m_DurationNanoSecond;
	}

	void ChannelInfo::SetDurationNanoSecond(long long value)
	{
		m_DurationNanoSecond = value;
	}

	void ChannelInfo::SetActive(bool value)
	{
		m_bActive = value;
	}

	void ChannelInfo::AddUserData(int idx)
	{
		auto temps = UserData;
		UserData.clear();
		if (idx < 0)
		{
			UserData.push_back(ChannelUserData());
		}
		for (int i = 0; i < (int)temps.size(); ++i)
		{
			if (idx == i)
			{
				UserData.push_back(ChannelUserData());
			}
			UserData.push_back(temps[i]);
		}
		if (idx == (int)temps.size())
		{
			UserData.push_back(ChannelUserData());
		}
	}

	void ChannelInfo::RemoveUserData(int idx)
	{
		auto temps = UserData;
		UserData.clear();

		for (int i = 0; i < (int)temps.size(); ++i)
		{
			if (idx != i)
			{
				UserData.push_back(temps[i]);
			}
		}
	}

	DeviceData* ChannelInfo::GetData()
	{
		return m_pDevice->GetRenderData(); 
	}

	int ChannelInfo::GetDeviceId() 
	{ 
		return (m_pDevice ? m_pDevice->GetID() : 0); 
	}
}
