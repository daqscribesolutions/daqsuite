#ifndef DEVICERECORDSETTING_H
#define DEVICERECORDSETTING_H

#include <string>
#ifdef max
#undef max
#endif

#include <algorithm>

namespace DeviceLib
{
	/**
	 * DeviceRecordSetting stores the setting for the recording such as the trigger information and the recording path.
	 */
	class DeviceRecordSetting
	{

	public:

		/// GetInstance supports singleton architecture.
		static DeviceRecordSetting* GetInstance();

		/**
		 * GetRecordFileName for the device index.
		 * @return The file name to record the device with the index id.
		 */
		std::string GetRecordFileName(size_t id);
		
		/// The getter for record durataion.
		long long GetRecordStartTime();

		/// The setter for record duration.
		void SetRecordStartTime(long long recordStart);

		/// The getter for record durataion.
		long long GetRecordDuration();

		/// The setter for record duration.
		void SetRecordDuration(long long recordDuration);

		/// The getter for relay durataion.
		long long GetRelayStartTime();

		/// The setter for relay duration.
		void SetRelayStartTime(long long relayStart);

		/// The getter for relay durataion.
		long long GetRelayDuration();

		/// The setter for relay duration.
		void SetRelayDuration(long long relayDuration);

		/// The getter for record path.
		const std::string& GetRecordPath() { return m_RecordPath; }

		/// The setter for record path.
		void SetRecordPath(const std::string& path) { m_RecordPath = path; }

		/// The getter for record section name.
		const std::string& GetRecordName() { return m_RecordName; }

		/// The setter for record section name.
		void SetRecordName(const std::string& deviceName) { m_RecordName = deviceName; }

		/// The getter for record number.
		int GetRecordNumber() { return m_RecordNumber; }

		/// The setter for record number.
		void SetRecordNumber(int recordNumber) { m_RecordNumber = recordNumber; }

		///  IncrementRecordNumber increases the record number by one.
		void IncrementRecordNumber() { ++m_RecordNumber; }

		/// The getter for record times to perform.
		int GetRecordTimes() { return m_RecordTimes; }

		/// The setter for record times to perform.
		void SetRecordTimes(int recordTimes) { m_RecordTimes = std::max(recordTimes, (int)1); }

	private:

		/// The directory to store the device data.
		std::string m_RecordPath;
		
		/// The record name for the file name.
		std::string m_RecordName;

		/// The record number for the file name.
		int m_RecordNumber;

		/// The record times.
		int m_RecordTimes;

		/// The record beginning time.
		long long m_RecordBeginTime;

		/// The record duration in nano second.
		long long m_RecordDuration;

		/// The relay beginning time.
		long long m_RelayBeginTime;

		/// The relay duration in nano second.
		long long m_RelayDuration;

		/// The single instance to support singleton.
		static DeviceRecordSetting* m_Instance;

		DeviceRecordSetting();

		~DeviceRecordSetting();

	};

}

#endif