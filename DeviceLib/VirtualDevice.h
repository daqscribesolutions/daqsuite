#ifndef VIRTUALDEVICE_H
#define VIRTUALDEVICE_H

#include "IDevice.h"
#include "IADCDevice.h"
#include "DeviceDataManager.h"
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>

#ifdef max
#undef max
#endif

namespace DeviceLib
{
	const int PACKET_HEADER_SIZE = 24;

	/**
	 * VirtualDevice is the example implementation of IDevice.
	 */
	class VirtualDevice : public IADCDevice
	{

	public:

		/**
		 * VirtualDevice is the full constructor.
		 * @param numberOfChannels The number of channels of the device to be set.
		 * @param format The format of the device to be set.
		 * @return The virtual device is set to be used.
		 */
		VirtualDevice(int numberOfChannels = 32, DATA_FORMAT format = TWO);

		virtual ~VirtualDevice();

		/// IDevice implementation
		virtual const char* GetDeviceInfo();

		virtual std::vector<unsigned int> GetAvailableChannelNumbers();

		virtual void SetChannelNumber(unsigned int nchannels);

		virtual long GetDeviceMaximumSampleRatePerSecond();

		virtual long GetDeviceMinimumSampleRatePerSecond();

		virtual std::vector<int> GetDeviceAvailableInputMilliVoltages();

		virtual int GetDeviceInputMilliVoltage();

		virtual void SetDeviceInputMilliVoltage(int voltage);

		virtual bool Initialize();

		virtual bool Start();

		virtual bool Stop();

		virtual long GetSampleRatePerSecond();

		virtual void SetSampleRatePerSecond(long rate);

		virtual std::vector<ChannelInfo*>& GetChannels();

		virtual std::vector<ChannelInfo*>& GetSnapshotChannels();

		virtual std::shared_ptr<IDeviceDataManager> GetDeviceDataManager() { return m_DataManager; }

		virtual void SetDeviceDataManager(const std::shared_ptr<IDeviceDataManager>& manager);
		
		virtual DATA_FORMAT GetDeviceDataFormat();

		virtual int GetID() { return ID; }

		virtual void SetID(int id) { ID = id; }

		virtual void AddChannelUserData(int i);

		virtual void RemoveChannelUserData(int i);

		/// Render data accessor.
		virtual DeviceData* GetRenderData() { return m_RenderData; }

		virtual DeviceData* GetMonitorData();

		/**
		 * Update calls to listners to update the data.
		 * @return The listeners for the data update are notified.
		 */
		virtual void Update();

		/**
		 * ReadData reads rawfs data from a file and stores in device data.
		 * @param filename The rawfs file name.
		 */
		void ReadData(const char* filename);

		/// The accessor for device datas.
		std::vector<DeviceData*> GetDeviceData() { return m_DeviceDatas; }

		/// InitSnapshot initializes snapshot channels.
		void InitSnapshot();

	protected:

		/**
		 * ClearDeviceDatas releases the memory used for the device. It should not be called by users.
		 * @return The memory used for the device is cleared.
		 */
		void ClearDeviceDatas();

		/// Maximum sample rate per second that the device can support.
		long m_MaximumSampleRatePerSecond;

		/// Minimum sample rate per second that the device can support.
		long m_MinimumSampleRatePerSecond;

		/// Sample rate per second to be used for monitering and recording.
		long m_SampleRatePerSecond;

		/// The number of channels to be used for the device.
		unsigned int m_NumberOfChannels;

		/// Input voltage index for the current innput voltage.
		unsigned int m_InputVoltageIndex;

		/// The data manager to handle the device data to be processed and displayed.
		std::shared_ptr<IDeviceDataManager> m_DataManager;

		/// Device data storage.
		std::vector<DeviceData*> m_DeviceDatas;

		/// Render data
		DeviceData* m_RenderData;

		/// The voltages that supported by the device.
		std::vector<int> m_Voltages;

		/// Device information storage.
		std::string m_DeviceInfo;

		/// Data format and the data size for each ADC data.
		DATA_FORMAT m_Format;

		/// Run time in nano second.
		long long m_RunNanoSecond;

		/// Data duration for channel.
		long long m_KeepingDuration;

		/// The channels data storage. The device is responsible to handle the channels.
		std::vector<ChannelInfo*> m_Channels;

		/// The channels data for snapshot.
		std::vector<ChannelInfo*> m_SnapshotChannels;

		/// The thread to run the device.
		boost::thread* m_Thread;

		/**
		 * DeviceRunner is the functor that implements the virtual device thread logic.
		 */
		struct DeviceRunner
		{
			/**
			 * DeviceRunner is the full constructor.
			 * @param pDevice The device pointer to be run. set.
			 * @return The virtual device is set to be used.
			 */
			DeviceRunner(VirtualDevice* pDevice) : m_pDevice(pDevice), m_bRunning(true) {}

			/**
			 * operator() is the implementation of the device running logic.
			 * @return The device is updated per 50 milisecond.
			 */
			void operator()()
			{
				if (m_pDevice)
				{
					while (m_bRunning)
					{
						m_pDevice->Update();
					}
				}
			}

			/**
			 * Reset makes to start the thread.
			 * @return The device is set to run continually.
			 */
			void Reset() { m_bRunning = true; }

			/**
			 * Stop stops the thread.
			 * @return The device is stopped.
			 */
			void Stop() { m_bRunning = false; }

		private:

			bool m_bRunning;
			VirtualDevice* m_pDevice;
		};

		/// The buffer size between client/server devices.
		int PACKET_SIZE;

		/// The functor to implement the logic of running the device.
		DeviceRunner* m_Runner;

		/// The hardware sample rate.
		int m_HardwardSampleRate;

		/// The id.
		int ID;

		/// The avaiable channel numbers.
		std::vector<unsigned int> m_AvailableChannels;
	};

}

#endif