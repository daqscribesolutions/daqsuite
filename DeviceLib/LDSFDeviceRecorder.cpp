#include "LDSFDeviceRecorder.h"
#include <ctime>
#include <algorithm>

namespace DeviceLib
{
	LDSFDeviceRecorder::LDSFDeviceRecorder(const char* filename) : DeviceDataRecorder(filename)
	{
	}

	LDSFDeviceRecorder::~LDSFDeviceRecorder()
	{
	}

	bool LDSFDeviceRecorder::IsReady()
	{
		return true;
	}
	void LDSFDeviceRecorder::Open()
	{
		time(&m_RecordTime);


	}

	void LDSFDeviceRecorder::Close()
	{
		if (IsReady())
		{
		}
	}

	void LDSFDeviceRecorder::UpdateExitData(DeviceData* data)
	{
		if (data && IsReady())
		{
			m_Format = data->GetFormat();
			m_NumberOfChannels = data->GetNumberOfChannels();
			m_SampleRate = data->GetSampleRatePerSecond();
			m_InputMilliVolt = data->GetInputMilliVolt();

			long long begin = data->GetStartNanoSecond();
			long long end = data->GetStartNanoSecond() + data->GetDurationNanoSecond();

			if (end < m_TriggerTimeNanoSecond)
			{
				return;
			}

			if (m_RecordDuration != 0 && begin > m_TriggerTimeNanoSecond + m_RecordDuration)
			{
				Close();
				return;
			}

			begin = std::max(begin, m_TriggerTimeNanoSecond) - data->GetStartNanoSecond();
			if (m_RecordDuration != 0)
			{
				end = std::min(end, m_TriggerTimeNanoSecond + m_RecordDuration) - data->GetStartNanoSecond();
			}
			else
			{
				end -= data->GetStartNanoSecond();
			}

			long long databegin = data->GetADCCountDataSizeOfNanoSecond(begin);
			long long datasize = data->GetADCCountDataSizeOfNanoSecond(end - begin);

			if (m_RecordDuration != 0 && datasize <= 0)
			{
				Close();
				return;
			}


			m_RecordDataSize += datasize;
		}
	}

}