SOURCES += ChannelInfo.cpp \
    ChannelMonitor.cpp \
    ClientDevice.cpp \
    CalibrationIO.cpp \
    FileNameManager.cpp \
    FileConvertManager.cpp \
    StorageManager.cpp \
    logger.cpp \
    DataSpeedMonitor.cpp \
    DataConverter.cpp \
    DeviceData.cpp \
    DeviceDataManager.cpp \
    DeviceDataReader.cpp \
    DeviceDataRecorder.cpp \
    DeviceRecordSetting.cpp \
    RecordTrigger.cpp \
    ServerDevice.cpp \
    SystemManager.cpp \
    TriggerManager.cpp \
    VirtualDevice.cpp

HEADERS += ChannelInfo.h \
    ChannelMonitor.h \
    ClientDevice.h \
    CalibrationIO.h \
    FileNameManager.h \
    FileConvertManager.h \
    StorageManager.h \
    logger.h \
    DataSpeedMonitor.h \
    DataConverter.h \
    DeviceData.h \
    DeviceDataManager.h \
    DeviceDataReader.h \
    DeviceDataRecorder.h \
    DeviceLib.h \
    DeviceRecordSetting.h \
    IADCDevice.h \
    IDevice.h \
    IDeviceDataManager.h \
    ITrigger.h \
    RecordTrigger.h \
    ServerDevice.h \
    SystemManager.h \
    TriggerManager.h \
    VirtualDevice.h
