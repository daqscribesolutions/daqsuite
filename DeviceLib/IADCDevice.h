#ifndef IADCDEVICE_H
#define IADCDEVICE_H

#include "IDevice.h"
#include "IDeviceDataManager.h"

namespace DeviceLib
{
	class IDeviceDataManager;
	class ChannelInfo;

	/**
	 * IADCDevice is the interface for ADC aqusition devices. 
	 */
	class IADCDevice : public IDevice
	{

	public:
		/// Virtual destructor.
		virtual ~IADCDevice() {}

		/**
		 * GetAvailableChannelNumbers sets the maximum number of channels.
		 * @return The device's maximum number of the channels.
		 */
		virtual std::vector<unsigned int> GetAvailableChannelNumbers() = 0;

		/**
		 * SetChannelNumber sets the maximum number of channels.
		 * @param nchannels The number of the channels to be set.
		 */
		virtual void SetChannelNumber(unsigned int nchannels) = 0;

		/**
		 * GetDeviceMaximumSampleRate provides the device's maximum sample rate per second.
		 * @return The device's maximum sample rate of the device per second.
		 */
		virtual long GetDeviceMaximumSampleRatePerSecond() = 0;

		/**
		 * GetDeviceMinimumSampleRate provides the device's minimum sample rate per second.
		 * @return The minimum sample rate of the device per second.
		 */
		virtual long GetDeviceMinimumSampleRatePerSecond() = 0;

		/**
		 * GetDeviceAvailableInputMilliVoltages provides the device's available milli-voltages.
		 * @return The device minimum sample rate of the device.
		 */
		virtual std::vector<int> GetDeviceAvailableInputMilliVoltages() = 0;

		/**
		 * GetDeviceInputMilliVoltage provides the device's voltage peak to peak value (+-).
		 * @return The device's voltage.
		 */
		virtual int GetDeviceInputMilliVoltage() = 0;

		/**
		 * SetDeviceInputMilliVoltage sets the device's voltage peak to peak value (+-).
		 * @return The device's voltage is set.
		 */
		virtual void SetDeviceInputMilliVoltage(int voltage) = 0;

		/**
		 * GetSampleRatePerSecond provides the sample rates of the data.
		 * @return The sample rate per second is returned.
		 */
		virtual long GetSampleRatePerSecond() = 0;

		/**
		 * SetSampleRatePerSecond sets the sample rates of the data.
		 * @param rate The sample rates of the data to be set.
		 * @return The sample rate per second is set.
		 */
		virtual void SetSampleRatePerSecond(long rate) = 0;

		/**
		 * GetChannels gets the channels in the device.
		 * @return The channels in the device are returned.
		 */
		virtual std::vector<ChannelInfo*>& GetChannels() = 0;

		/**
		 * GetSnapshotChannels gets the snapshot channels in the device.
		 * @return The snapshot channels in the device are returned.
		 */
		virtual std::vector<ChannelInfo*>& GetSnapshotChannels() = 0;

		/**
		 * AddChannelUserData adda user data for channels.
		 * @param idx The location to add user data.
		 */
		virtual void AddChannelUserData(int idx) = 0;

		/**
		 * RemoveChannelUserData removes user data for channels.
		 * @param idx The location to be removed.
		 */
		virtual void RemoveChannelUserData(int idx) = 0;

		/// Render data accessor.
		virtual DeviceData* GetRenderData() = 0;

		/// Render data accessor.
		virtual DeviceData* GetMonitorData() = 0;

		virtual int GetID() = 0;

		virtual void SetID(int id) = 0;
	};
}

#endif
