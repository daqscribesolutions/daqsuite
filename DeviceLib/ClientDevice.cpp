#include "DeviceLib.h"
#include "ClientDevice.h"
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include "SystemManager.h"
#include "NetworkManager.h"
namespace DeviceLib
{
	
	/**
	 * ClientDeviceRunner is the functor that implements the client device thread logic.
	 */
	struct ClientDevice::ClientDeviceRunner
	{
		/**
		 * ClientDeviceRunner is the full constructor.
		 * @param pDevice The device pointer to be run. set.
		 * @return The virtual device is set to be used.
		 */
		ClientDeviceRunner(ClientDevice* pDevice) : m_pDevice(pDevice), m_bRunning(true) {}

		/**
		 * operator() is the implementation of the device running logic.
		 * @return The device is updated per 50 milisecond.
		 */
		void operator()()
		{
			if (m_pDevice)
			{
				m_pDevice->Connect();
			}
		}

		/**
		 * Reset makes to start the thread.
		 * @return The device is set to run continually.
		 */
		void Reset() { m_bRunning = true; }

		/**
		 * Stop stops the thread.
		 * @return The device is stopped.
		 */
		void Stop() { m_bRunning = false; }

		/**
		 * IsRunning is the getter for the running state.
		 * @return True if the device is running otherwise return false.
		 */
		bool IsRunning() { return m_bRunning; }

	private:

		ClientDevice* m_pDevice;

		bool m_bRunning;

	};

	ClientDevice::ClientDevice(const std::shared_ptr<IDevice>& pDevice, const std::string& host, int port)
		: m_Host(host)
		, m_Sender(NetworkManager::GetInstance()->GetIOService())
		, m_bHeaderSent(false)
		, m_Port(port)
		, m_Connected(false)
		, m_pDevice(pDevice)
		, m_NIC("192.168.0.109")
	{
		m_UDPData.resize(1024);
	}

	ClientDevice::~ClientDevice()
	{
		Disconnect();
	}

	void ClientDevice::SetHost(const std::string& host)
	{
		if (host != m_Host)
		{
			if (m_NetworkRunner)
			{
				m_NetworkRunner->Stop();
				Disconnect();
				m_Host = host;
				m_NetworkRunner->Reset();
			}
			else
			{
				m_Host = host;
			}		
		}
	}

	void ClientDevice::SetNICHost(const std::string& nic)
	{
		if (nic != m_NIC)
		{
			if (m_NetworkRunner)
			{
				m_NetworkRunner->Stop();
				Disconnect();
				m_NIC = nic;
				m_NetworkRunner->Reset();
			}
			else
			{
				m_NIC = nic;
			}
		}
	}

	bool ClientDevice::IsConnected()
	{
		return m_Connected;
	}

	bool ClientDevice::Connect()
	{
		if (!IsConnected())
		{
			m_bHeaderSent = false;
			try
			{
				if (m_Sender.is_open())
				{
					m_Sender.close();
				}
				const int BufferSize = SystemManager::GetInstance()->GetNetworkBufferSize();
				m_UDPData.resize(BufferSize);
				PACKET_SIZE = BufferSize - PACKET_HEADER_SIZE;
				m_ReceiverEndPoint = tcp::endpoint(boost::asio::ip::address::from_string(m_Host), m_Port);
				m_Sender.connect(m_ReceiverEndPoint);
				m_Connected = true;
			}
			catch (std::exception& e)
			{
				m_Connected = false;
				std::cerr << "ClientDevice::Connect : " << e.what() << std::endl;
			}
		}
		return IsConnected();
	}

	bool ClientDevice::Disconnect()
	{
		m_Sender.close();
		m_Connected = false;
		return !IsConnected();
	}

	void ClientDevice::UpdateEntryData(DeviceData* data)
	{
	}

	void ClientDevice::SetPort(int port)
	{
		if (m_NetworkRunner)
		{
			Disconnect();
			m_Port = port;
		}
		else
		{
			m_Port = port;
		}
	}

	void ClientDevice::UpdateExitData(DeviceData* data)
	{
		if (IsConnected())
		{
			try
			{
				*((int*)&m_UDPData[0]) = (int)data->GetDurationNanoSecond();
				*((int*)&m_UDPData[4]) = (int)data->GetSampleRatePerSecond();
				*((int*)&m_UDPData[8]) = (int)data->GetNumberOfChannels();
				*((int*)&m_UDPData[12]) = (int)data->GetInputMilliVolt();
				*((int*)&m_UDPData[16]) = (int)data->GetFormat();
				long long nDurationData = data->GetDataSize() * data->GetFormat();
				int i = 0;
				for (long long nIndex = 0; nIndex < nDurationData; nIndex += PACKET_SIZE)
				{
					long length = PACKET_SIZE;
					if (length > nDurationData - nIndex)
					{
						length = (long)(nDurationData - nIndex);
					}
					*((int*)&m_UDPData[20]) = i++;
					memcpy(&m_UDPData[PACKET_HEADER_SIZE], data->GetADCCountData() + nIndex, length);
					boost::asio::write(m_Sender, boost::asio::buffer(m_UDPData, PACKET_SIZE + PACKET_HEADER_SIZE));
				}
			}
			catch (std::exception& e)
			{
				std::cerr << "ClientDevice::UpdateExitData : " << e.what() << std::endl;
			}
		}
	}

	bool ClientDevice::StartListening()
	{
		if (m_NetworkRunner && m_NetworkRunner->IsRunning()) return true;

		m_NetworkRunner = std::unique_ptr<ClientDeviceRunner>(new ClientDeviceRunner(this));
		m_NetworkRunner->Reset();
		m_NetworkThread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_NetworkRunner)));
		return true;
	}

	void ClientDevice::StopListening()
	{
		Disconnect();
		m_NetworkRunner->Stop();
	}

}