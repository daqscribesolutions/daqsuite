#include "DeviceRecordSetting.h"
#include <boost/filesystem.hpp>
#include <sstream>
#include <algorithm>
#include "StorageManager.h"
#include "FileNameManager.h"
#include "TriggerManager.h"

namespace DeviceLib
{
	DeviceRecordSetting* DeviceRecordSetting::m_Instance = 0;

	DeviceRecordSetting* DeviceRecordSetting::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new DeviceRecordSetting();
		}
		return m_Instance;
	}

	std::string DeviceRecordSetting::GetRecordFileName(size_t id)
	{
		StorageManager::CreatePath(m_RecordPath + "\\" + m_RecordName);
		auto n = FileNameManager::GetInstance()->GetFileName((int)id);
		return m_RecordPath + "\\" + m_RecordName + "\\" + n;
	}

	DeviceRecordSetting::DeviceRecordSetting()
		: m_RecordPath("C:\\Simple Record")
		, m_RecordName("Record")
		, m_RecordDuration(1000000000)
		, m_RecordNumber(1)
		, m_RecordBeginTime(0)
		, m_RelayDuration(1000000000)
		, m_RelayBeginTime(0)
		, m_RecordTimes(1)
	{
	}

	DeviceRecordSetting::~DeviceRecordSetting()
	{
	}

	long long DeviceRecordSetting::GetRecordStartTime()
	{
		return m_RecordBeginTime;
	}

	void DeviceRecordSetting::SetRecordStartTime(long long recordStart)
	{
		m_RecordBeginTime = recordStart;
	}

	long long DeviceRecordSetting::GetRecordDuration()
	{
		long long res = m_RecordDuration;
		if (TriggerManager::GetInstance()->GetSnapshot())
		{
			res = std::min((long long)1000000000, res);
		}
		return res;
	}

	void DeviceRecordSetting::SetRecordDuration(long long recordDuration)
	{
		m_RecordDuration = std::max((long long)100000000, recordDuration);
	}

	long long DeviceRecordSetting::GetRelayStartTime()
	{
		return m_RelayBeginTime;
	}

	void DeviceRecordSetting::SetRelayStartTime(long long relayStart)
	{
		m_RelayBeginTime = relayStart;
	}

	long long DeviceRecordSetting::GetRelayDuration()
	{
		return m_RelayDuration;
	}

	void DeviceRecordSetting::SetRelayDuration(long long relayDuration)
	{
		m_RelayDuration = relayDuration;
	}

}
