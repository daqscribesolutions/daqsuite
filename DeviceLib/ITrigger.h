#ifndef ITRIGGER_H
#define ITRIGGER_H

namespace DeviceLib
{

	class ChannelInfo;

	/**
	 * ITriggerCondition is the interface to perform checking trigger event condition.
	 */
	class ITriggerCondition
	{

	public:

		/// Virtual destructor.
		virtual ~ITriggerCondition() {}

		/**
		 * CheckTriggerCondition checks if the triggering condition is met.
		 * @return True if the condition is met and the trigger time is set.
		 */
		virtual bool CheckTriggerCondition() = 0;

		/**
		 * GetTriggerNanoSecond returns the trigger time in nanosecond.
		 * @return The trigger time.
		 */
		virtual long long GetTriggerNanoSecond() = 0;

		/**
		 * SetChannel sets the trigger channel.
		 * @param channel The trigger channel to monitor.
		 * @param idx The trigger channel index.
		 */
		virtual void SetChannel(ChannelInfo* channel, int idx) = 0;

		/**
		* Reset the condition.
		*/
		virtual void Reset() = 0;
	};
	
	/**
	 * ITriggerListener is the interface to perform operation following the trigger event.
	 */
	class ITriggerListener
	{

	public:
		/// Virtual destructor.
		virtual ~ITriggerListener() {}

		/**
		 * PerformTriggerEvent performs the custom procedure for the trigger event.
		 * @param triggertime The time of event in nanosecond.
		 * @return The custom procedure is performed for the trigger event.
		 */
		virtual void PerformTriggerEvent(long long triggertime) = 0;

		/**
		 * StopTriggerEvent stops the custom procedure for the trigger event.
		 * @param triggertime The time to stop in nanosecond.
		 * @return The custom procedure is stopped.
		 */
		virtual void StopTriggerEvent(long long triggertime) = 0;

	};

	/**
	 * ITrigger is the interface to mediate the trigger conditions and trigger listeners.
	 */
	class ITrigger
	{

	public:
		/// Virtual destructor.
		virtual ~ITrigger() {}

		/**
		 * SetTriggerArmCondition sets a trigger arm condition to check to generate trigger events.
		 * @param condition The trigger arm condition to be set.
		 * @return The trigger arm condition is added.
		 */
		virtual void SetTriggerArmCondition(ITriggerCondition* condition) = 0;

		/**
		 * SetTriggerCondition sets a trigger condition to check to generate trigger events.
		 * @param condition The trigger condition to be set.
		 * @return The trigger condition is added.
		 */
		virtual void SetTriggerCondition(ITriggerCondition* condition) = 0;

		/**
		 * GetTriggerNanoSecond adds the trigger time in nanosecond.
		 * @param listener The trigger listener to be added.
		 * @return The trigger listener is added.
		 */
		virtual void AddTriggerListener(ITriggerListener* listener) = 0;

	};
}

#endif