#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <boost/asio.hpp>
#include <boost/array.hpp>
using boost::asio::ip::tcp;

namespace DeviceLib
{

	class NetworkManager
	{

	public:

		static NetworkManager* GetInstance();

		boost::asio::io_service& GetIOService() { return io_service; }

	private:

		boost::asio::io_service io_service;

		NetworkManager();

		~NetworkManager();

		static NetworkManager* instance;

	};

}
#endif