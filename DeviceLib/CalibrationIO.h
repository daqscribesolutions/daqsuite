#ifndef CALIBRATIONIO_H
#define CALIBRATIONIO_H
#include <string>
#include <vector>

namespace DeviceLib
{
	class ChannelInfo;

	/**
	 * CalibrationIO provides the funtionality to load calibration file.
	 */
	class CalibrationIO
	{

	public:

		static void LoadCalibration(const std::string& filename, std::vector<ChannelInfo*> channels);

	private:

		CalibrationIO();
		
		~CalibrationIO();
	
	};

}

#endif