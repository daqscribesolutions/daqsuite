#ifndef LDSFDEVICEDATARECORDER_H
#define LDSFDEVICEDATARECORDER_H

#include "DeviceDataRecorder.h"

namespace DeviceLib
{

	class LDSFDeviceRecorder : public DeviceDataRecorder
	{

	public:
		/**
		 * LDSFDeviceRecorder is the full constructor.
		 * @param filename The name of file to be written.
		 * @return the recorder is ready to be used.
		 */
		LDSFDeviceRecorder(const char* filename);

		virtual ~LDSFDeviceRecorder();

		/// IsReady returns the state of the file.
		bool IsReady();

		/// Open the file stream.
		void Open();

		/// Close the file stream.
		void Close();

		/// IDeviceDataUpdateListener implementation
		virtual void UpdateEntryData(DeviceData* data);

		virtual void UpdateExitData(DeviceData* data);

	};

}

#endif