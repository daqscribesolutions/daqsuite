#ifndef DEVICEDATAMANAGER_H
#define DEVICEDATAMANAGER_H

#include "IDeviceDataManager.h"
#include "DataSpeedMonitor.h"
#include <deque>

namespace DeviceLib
{
	class IADCDevice;

	/**
	 * DeviceDataManager is the implementation for the interface IDeviceDataManager to handle one device.
	 * It collects data from a device and feeds the data for update to other listeners.
	 */
	class DeviceDataManager : public IDeviceDataManager
	{

	public:

		/**
		 * DeviceDataManager is the default constructor.
		 * @param pDevice The pointer of the device to be managed.
		 */
		DeviceDataManager(IADCDevice* pDevice);

		virtual ~DeviceDataManager();

		/// IDeviceDataManager Implementation
		virtual bool Initialize();

		virtual long GetManagedDataDuration();
		
		virtual void SetManagedDataDuration(long duration);

		virtual long GetManagedSampleRatePerSecond();

		virtual void SetManagedSampleRatePerSecond(long samplerate);

		virtual void AddData(DeviceData* data);
		
		virtual void AddUpdateDataListener(IDeviceDataUpdateListener* listener);

		virtual void RemoveUpdateDataListener(IDeviceDataUpdateListener* listener);

		virtual double GetDataProcessedSecond();

		virtual double GetCurrentSpeed() { return m_SpeedMonitor.GetCurrentSpeed(); }

		virtual double GetCurrentSampleSpeed() { return m_SpeedMonitor.GetDataSampleSpeed(); }

		virtual std::vector<ChannelInfo*>& GetActiveChannels() { return m_ActiveChannels; }

		void AddSnapshotData(DeviceData* data);

		virtual void BeginSample() { m_SpeedMonitor.BeginSample(); }

		virtual void EndSample() { m_SpeedMonitor.EndSample(); }

		virtual DeviceData* GetCurrentData();

	protected:

		/// SetActiveChannels gets the active channels from the device.
		void SetActiveChannels();

		/// The total data duration to manage.
		long m_ManagedDataDuration;		

		/// The sample rate per second to the managed data.
		long m_ManagedSampleRatePerSecond;

		/// The device data storage. The data will be stored while it is processed.
		std::deque<DeviceData*> m_ManagedDatas;

		/// The device data update listeners which will get the updated data.
		std::vector<IDeviceDataUpdateListener*> m_UpdateDataListeners;	

		/// The active channels for the device.
		std::vector<ChannelInfo*> m_ActiveChannels;

		/// The data source device.
		IADCDevice* m_pDevice;

		/// The time of the amount of the processed data.
		long long m_DataProcessedNanoSecond;

		/// The speed monitor.
		DataSpeedMonitor m_SpeedMonitor;

		std::vector<DeviceData*> m_SnapshotDatas;

	};

}
#endif