#ifndef IDEVICE_H
#define IDEVICE_H

#include <vector>
#include <memory>
#include "IDeviceDataManager.h"

namespace DeviceLib
{
	class IDeviceDataManager;
	class ChannelInfo;

	/**
	 * IDevice is the interface of board to get digital data. The specific setting is hidden from users.
	 */
	class IDevice
	{

	public:

		/// Virtual destructor.
		virtual ~IDevice() {}

		/**
		 * GetDeviceInfo provides the device information.
		 * @return The information of the device.
		 */		
		virtual const char* GetDeviceInfo() = 0;

		/**
		 * Initialize initialize the device to be able to run.
		 * @return True if the device is initialized, otherwise return false.
		 */
		virtual bool Initialize() = 0;

		/**
		 * Start begins to collect data from the device.
		 * @return True if the device started successfuly, otherwise return false.
		 */
		virtual bool Start() = 0;

		/**
		 * Stop finishes collecting data from the device.
		 * @return True if the device started successfuly, otherwise return false.
		 */
		virtual bool Stop() = 0;

		/**
		 * GetDeviceDataManager gets IDeviceDataManager to handle the device data.
		 * @return The IDeviceDataManager is returned.
		 */
		virtual std::shared_ptr<IDeviceDataManager> GetDeviceDataManager() = 0;

		/**
		 * SetDeviceDataManager sets IDeviceDataManager to handle the device data.
		 * @param manager The IDeviceDataManager pointer to be added.
		 * @return The IDeviceDataManager is added.
		 */
		virtual void SetDeviceDataManager(const std::shared_ptr<IDeviceDataManager>& manager) = 0;

		/**
		 * GetDeviceDataFormat gets the data format of the device.
		 * @return The data format of the device.
		 */
		virtual DATA_FORMAT GetDeviceDataFormat() = 0;

		/// The accessor for the unique id;
		virtual int GetID() = 0;

		/// The setter for the unique id;
		virtual void SetID(int id) = 0;
	};

}
#endif
