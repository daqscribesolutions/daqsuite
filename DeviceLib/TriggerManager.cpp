#include "TriggerManager.h"
#include "RecordTrigger.h"
#include "SystemManager.h"
#include "IDevice.h"
#include "IADCDevice.h"
#include "ChannelMonitor.h"
#include "DeviceRecordSetting.h"
#include <algorithm>
#include "DeviceDataManager.h"
#include "FileConvertManager.h"
#ifdef WIN32
#pragma warning(disable : 4996)
#endif

namespace DeviceLib
{
	class CommandTrigger : public ITriggerCondition
	{

	public:

		CommandTrigger() : m_TriggerTime(-1) {}

		void Trigger(long long triggerTime)
		{
			m_TriggerTime = triggerTime;
		}

		virtual void SetChannel(ChannelInfo*, int) {}

		/// ITriggerCondition implementation.
		virtual long long GetTriggerNanoSecond()
		{
			return m_TriggerTime;
		}

		virtual bool CheckTriggerCondition()
		{
			return true;
		}

		virtual void Reset()
		{
			m_TriggerTime = SystemManager::GetInstance()->GetSystemRunningTime();
		}

	private:

		long long m_TriggerTime;

	};

	TriggerManager* TriggerManager::m_Instance = 0;

	TriggerManager* TriggerManager::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new TriggerManager();
		}
		return m_Instance;
	}

	TriggerManager::TriggerManager()
		: m_RecordBegin(0)
		, m_BeginCondition(0)
		, m_CurrentTriggerType(NONE)
		, m_TriggerChannel(0)
		, m_limit(0)
		, m_limitType(UPPER)
		, m_limitDataType(EUVALUE)
		, m_PreTrigger(0)
		, m_bSnapshot(true)
		, nSnapshot(0)
	{
		time_t now;
		time(&now); 
		m_Now = m_ScheduleTime = *localtime(&now);
		m_RecordBegin = new RecordTrigger();
	}

	TriggerManager::~TriggerManager()
	{
		Clear();
	}

	void TriggerManager::ResetTrigger()
	{
		ClearRecorders();
		if (m_BeginCondition) delete m_BeginCondition;
		m_BeginCondition = 0;
		m_RecordBegin->Reset();
	}

	void TriggerManager::SetTimeTrigger()
	{
		ResetTrigger();
		m_RecordBegin->Reset();

		DeviceRecordSetting * recordSetting = DeviceRecordSetting::GetInstance();
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		ChannelInfo * channel = 0;
		for (size_t i = 0; i < devices.size(); ++i)
		{
			std::shared_ptr<IDevice> pDevice = devices[i];
			if (!pDevice) continue;
			IADCDevice* pADC = dynamic_cast<IADCDevice*>(pDevice.get());
			auto & channels = pADC->GetChannels();
			if (channel == 0)
			{
				for (int j = 0; j < (int)channels.size(); ++j)
				{ 
					if (channels[j]->GetActive())
					{
						channel = channels[j];
						break;
					}
				}
			}
			std::shared_ptr<IDeviceDataManager> pDataManager = pDevice->GetDeviceDataManager();
			if (!pDataManager) continue;
			pDataManager->AddUpdateDataListener(m_RecordBegin);
			DeviceDataManager* datamanager = dynamic_cast<DeviceDataManager*>(pDataManager.get());
			DeviceDataRecorder* recorder = new DeviceDataRecorder((recordSetting->GetRecordFileName(i) + ".rawfs").c_str(), datamanager, m_bSnapshot);
			pDataManager->AddUpdateDataListener(recorder);
			m_RecordBegin->AddTriggerListener(recorder);
			m_Recorders.push_back(recorder);
		}
		m_BeginCondition = new ChannelMonitor(channel, 0, DeviceRecordSetting::GetInstance()->GetRecordStartTime());
		m_RecordBegin->SetTriggerCondition(m_BeginCondition);
	}

	void TriggerManager::SetLimitTrigger(bool bEdge)
	{
		ResetTrigger();
		m_RecordBegin->Reset();

		DeviceRecordSetting * recordSetting = DeviceRecordSetting::GetInstance();
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		int channelidx = 0;
		int chidx = 0;
		ChannelInfo * channel = 0;
		for (size_t i = 0; i < devices.size(); ++i)
		{
			std::shared_ptr<IDevice> pDevice = devices[i];
			if (!pDevice) continue;
			IADCDevice* pADC = dynamic_cast<IADCDevice*>(pDevice.get());
			auto & channels = pADC->GetChannels();

			if (channel == 0 && m_TriggerChannel < channelidx + (int)channels.size())
			{
				channel = channels[m_TriggerChannel - channelidx];
				chidx = m_TriggerChannel - channelidx;
			}
			channelidx += (int)channels.size();
			std::shared_ptr<IDeviceDataManager> pDataManager = pDevice->GetDeviceDataManager();
			if (!pDataManager) continue;
			pDataManager->AddUpdateDataListener(m_RecordBegin);
			DeviceDataManager* datamanager = dynamic_cast<DeviceDataManager*>(pDataManager.get());
			DeviceDataRecorder* recorder = new DeviceDataRecorder((recordSetting->GetRecordFileName(i) + ".rawfs").c_str(), datamanager, m_bSnapshot);
			pDataManager->AddUpdateDataListener(recorder);
			m_RecordBegin->AddTriggerListener(recorder);
			m_Recorders.push_back(recorder);
		}
		if (bEdge)
		{
			m_BeginCondition = new EdgeChannelMonitor(channel, chidx, m_limit, m_limitType, m_limitDataType);
		}
		else
		{
			m_BeginCondition = new LimitChannelMonitor(channel, chidx, m_limit, m_limitType, m_limitDataType);
		}

		m_RecordBegin->SetTriggerCondition(m_BeginCondition);

		m_RecordBegin->StartListening();
	}

	void TriggerManager::SetCommandTrigger()
	{
		ResetTrigger();
		m_RecordBegin->Reset();
		m_RecordBegin->ArmTrigger();

		DeviceRecordSetting * recordSetting = DeviceRecordSetting::GetInstance();
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		for (size_t i = 0; i < devices.size(); ++i)
		{
			std::shared_ptr<IDevice> pDevice = devices[i];
			if (!pDevice) continue;
			std::shared_ptr<IDeviceDataManager> pDataManager = pDevice->GetDeviceDataManager();
			if (!pDataManager) continue;
			pDataManager->AddUpdateDataListener(m_RecordBegin);
			DeviceDataManager* datamanager = dynamic_cast<DeviceDataManager*>(pDataManager.get());
			DeviceDataRecorder* recorder = new DeviceDataRecorder((recordSetting->GetRecordFileName(i) + ".rawfs").c_str(), datamanager, m_bSnapshot);
			pDataManager->AddUpdateDataListener(recorder);
			m_RecordBegin->AddTriggerListener(recorder);
			m_Recorders.push_back(recorder);
		}
		CommandTrigger* cm = new CommandTrigger();
		cm->Trigger(SystemManager::GetInstance()->GetSystemRunningTime());
		m_BeginCondition = cm;
		m_RecordBegin->SetTriggerCondition(m_BeginCondition);
	}

	void TriggerManager::SetChannelInfo()
	{
		if (m_CurrentTriggerType == LIMIT || m_CurrentTriggerType == EDGE)
		{
			const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
			int channelidx = 0;
			int chidx = 0;
			ChannelInfo * ch = 0;
			for (size_t i = 0; i < devices.size(); ++i)
			{
				std::shared_ptr<IDevice> pDevice = devices[i];
				if (!pDevice) continue;
				IADCDevice* pADC = dynamic_cast<IADCDevice*>(pDevice.get());
				auto & channels = pADC->GetChannels();

				if (ch == 0 && m_TriggerChannel < channelidx + (int)channels.size())
				{
					ch = channels[m_TriggerChannel - channelidx];
					chidx = m_TriggerChannel - channelidx;
				}
				channelidx += (int)channels.size();
			}

			m_DeviceChannel = ch;
			m_DeviceChannelIndex = chidx;
			if (m_DeviceChannel && m_BeginCondition)
			{
				m_BeginCondition->SetChannel(m_DeviceChannel, chidx);
			}
		}
	}

	void TriggerManager::SetTriggerChannelIndex(int channel)
	{ 
		m_TriggerChannel = channel;
		SetChannelInfo();
	}

	void TriggerManager::TriggerBegin()
	{
		if (m_CurrentTriggerType == TIME)
		{
			SetTimeTrigger();
		}
		else if (m_CurrentTriggerType == LIMIT)
		{
			SetLimitTrigger(false);
		}
		else if (m_CurrentTriggerType == EDGE)
		{
			SetLimitTrigger(true);
		}
	}

	void TriggerManager::TriggerEnd()
	{

	}

	void TriggerManager::ClearRecorders()
	{
		struct RecorderClearFunctor
		{
			void operator()(DeviceDataRecorder*& recorder)
			{
				FileConvertManager::GetInstance()->AddRecorder(recorder);
				if (recorder)
				{
					const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
					for (size_t i = 0; i < devices.size(); ++i)
					{
						std::shared_ptr<IDevice> pDevice = devices[i];
						if (!pDevice) continue;
						std::shared_ptr<IDeviceDataManager> pDataManager = pDevice->GetDeviceDataManager();
						if (pDataManager) pDataManager->RemoveUpdateDataListener(recorder);
					}
				}
			}
		};
		std::for_each(m_Recorders.begin(), m_Recorders.end(), RecorderClearFunctor());
		m_Recorders.clear();
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		for (size_t i = 0; i < devices.size(); ++i)
		{
			std::shared_ptr<IDevice> pDevice = devices[i];
			if (!pDevice) continue;
			std::shared_ptr<IDeviceDataManager> pDataManager = pDevice->GetDeviceDataManager();
			if (!pDataManager) continue;
			pDataManager->RemoveUpdateDataListener(m_RecordBegin);
		}
		delete m_RecordBegin;
		m_RecordBegin = new RecordTrigger();
	}

	void TriggerManager::Clear()
	{
		if (m_RecordBegin) delete m_RecordBegin;
		if (m_BeginCondition) delete m_BeginCondition;
		ClearRecorders();
		FileConvertManager::GetInstance()->Clear();
	}

	bool TriggerManager::IsArmed()
	{
		return m_RecordBegin->IsArmed();
	}

	void TriggerManager::ArmTrigger()
	{
		m_RecordBegin->ArmTrigger();
	}

	double TriggerManager::GetRecordSpeed()
	{
		double speed = 0;
		for (size_t i = 0; i < m_Recorders.size(); ++i)
		{
			speed += m_Recorders[i]->GetCurrentSpeed();
		}
		return speed;
	}

	double TriggerManager::GetRecordSampleSpeed()
	{
		double speed = 0;
		for (size_t i = 0; i < m_Recorders.size(); ++i)
		{
			speed += m_Recorders[i]->GetCurrentSampleSpeed();
		}
		return speed;
	}

	double TriggerManager::GetTotalRecordedSize()
	{
		double recordedsize = 0;
		for (size_t i = 0; i < m_Recorders.size(); ++i)
		{
			recordedsize += m_Recorders[i]->GetTotalDataBytes() / 1000000.0;
		}
		return recordedsize;
	}

	bool TriggerManager::RecordFinished()
	{
		bool finished = true;
		for (size_t i = 0; i < m_Recorders.size(); ++i)
		{
			if (!m_Recorders[i]->IsFinished())
			{
				finished = false;
				break;
			}
		}
		return finished;
	}

	bool TriggerManager::GetScheduleTrigger()
	{
		time_t now;
		time(&now);
		m_Now = *localtime(&now);
		double diff = difftime(mktime(&m_ScheduleTime), mktime(&m_Now));
		return 0 >= diff;
	}
}