#include "NetworkManager.h"

namespace DeviceLib
{
	NetworkManager* NetworkManager::instance = 0;

	NetworkManager* NetworkManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new NetworkManager();
		}
		return instance;
	}

	NetworkManager::NetworkManager()
	{
	}

	NetworkManager::~NetworkManager()
	{
	}

}