#ifndef RECORDTRIGGER_H
#define RECORDTRIGGER_H

#include "ITrigger.h"
#include "IDeviceDataManager.h"

namespace DeviceLib
{
	/**
	 * RecordTrigger is the implementation of trigger to set recording.
	 */
	class RecordTrigger : public ITrigger, public IDeviceDataUpdateListener
	{

	public:
		/// EventType decides the event type to trigger.
		enum EventType { BEGIN, END };

		/// The full constructor.
		RecordTrigger();

		~RecordTrigger();

		/// IDeviceDataUpdateListener implementation.
		virtual void UpdateEntryData(DeviceData* data);

		virtual void UpdateExitData(DeviceData* data);

		virtual bool StartListening();

		virtual void StopListening();

		/// ITrigger implementation.
		virtual void SetTriggerArmCondition(ITriggerCondition* condition);

		virtual void SetTriggerCondition(ITriggerCondition* condition);

		virtual void AddTriggerListener(ITriggerListener* listener);

		/// Reset clears the trigger listeners.
		void Reset();

		/// ArmTrigger set the trigger armed.
		void ArmTrigger();

		/// IsTriggered gets the triggered flag.
		bool IsTriggered() { return m_Triggered; }

		/// Accessor to get the armed flag.
		bool IsArmed() { return m_Armed; }

	protected:

		/// Condition for arm to trigger recording.
		ITriggerCondition* m_ArmCondition;

		/// Condition for recording.
		ITriggerCondition* m_RecordCondition;

		/// Trigger listeners for recording.
		std::vector<ITriggerListener*> m_TriggerListeners;

		/// Event type. Internally to keep track of the record.
		EventType m_EventType;

		/// Flag for arm conditions status.
		bool m_Armed;

		/// Flag for the status of whether a trigger happened or not.
		bool m_Triggered;

		bool m_Done;

	};
}

#endif