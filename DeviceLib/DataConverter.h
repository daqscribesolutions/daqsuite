#ifndef DATACONVERTER_H
#define DATACONVERTER_H

#include "DeviceData.h"

namespace DeviceLib
{
	class IADCDevice;

	/**
	 * DataConverter provides the data conversion implementation for the different format.
	 */
	struct DataConverter
	{
		/**
		 * DataConverter is the full constructor.
		 * @param data The device data for the input/output ADC data for conversion.
		 * @param pDevice The device pointer for the input/output channels.
		 * @param bSnapshot The flag for the snapshot channel.
		 * @return The data conversion is ready.
		 */
		DataConverter(DeviceData* data, IADCDevice* pDevice, bool bSnapshot);

		/**
		 * ConvertADCtoMilivoltAndEUValue is an implementation of conversion from ADC to Milivolt and EU (Engineering Unit) value.
		 */
		void ConvertADCtoMilivoltAndEUValue();

		/**
		 * ConvertADCtoMilivoltAndEUValue is an implementation of conversion from ADC to EU (Engineering Unit) value and store in EUValues.
		 */
		void ConvertADCtoMilivoltAndEUValue(std::vector<std::vector<double> >& EUValues, const std::vector<ChannelInfo*>& channels);

		/**
		 * SetSineSignal sets the initial data with sine signal.  It is used to generate data for the virtual device.
		 */
		void SetSineSignal();

		/**
		 * Multiply multiplys the val to the initial data.  It is used to change data for the virtual device.
		 */
		void Multiply(double val);

		/**
		 * PrintMilivolt displays the voltage value of channels in the console.
		 */
		void PrintMilivolt();

		/**
		 * PrintADCCount displays the ADC value of channels in the console.
		 */
		void PrintADCCount();

	private: 

		bool m_bSnapshot;		/// The flag for snapshot.

		DeviceData* m_DeviceData; /// The device data for raw data.

		IADCDevice* m_pDevice;		/// The device pointer for the voltage and EU value data for channels.

	};
}
#endif