#include "DeviceLib.h"
#include "ServerDevice.h"
#include "SystemManager.h"
#include <algorithm>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
using boost::asio::ip::tcp;
#include <iostream>
#include <fstream>
#include "NetworkManager.h"

namespace DeviceLib
{

	/**
	 * ServerDeviceRunner is the functor that implements the server device thread logic.
	 */
	struct ServerDevice::ServerDeviceRunner
	{
		/**
		 * ServerDeviceRunner is the full constructor.
		 * @param pDevice The device pointer to be run.
		 * @return The virtual device is set to be used.
		 */
		ServerDeviceRunner(ServerDevice* pDevice) : m_pDevice(pDevice), m_bRunning(true) {}

		~ServerDeviceRunner() { m_pDevice = 0; m_bRunning = false; }
		/**
		 * operator() is the implementation of the device running logic.
		 * @return The device is updated.
		 */
		void operator()()
		{
			if (m_pDevice)
			{
				while (m_bRunning)
				{
					try
					{
						if (!m_pDevice->IsConnected())
						{
							m_pDevice->Connect();
						}
						else
						{
							m_pDevice->ReceiveData();
						}
					}
					catch (std::exception&)
					{
					}
					boost::this_thread::sleep(boost::posix_time::milliseconds(1)); 
				}
			}
		}

		/**
		 * Reset makes to start the thread.
		 * @return The device is set to run continually.
		 */
		void Reset() { m_bRunning = true; }

		/**
		 * Stop stops the thread.
		 * @return The device is stopped.
		 */
		void Stop() { m_bRunning = false; }

		/**
		 * IsRunning is the getter for the running state.
		 * @return True if the device is running otherwise return false.
		 */
		bool IsRunning() { return m_bRunning; }
	private:

		ServerDevice* m_pDevice;
		bool m_bRunning;

	};

	ServerDevice::ServerDevice(int port)
		: VirtualDevice(32, TWO)
		, m_Connected(false)
		, m_Receiver(NetworkManager::GetInstance()->GetIOService(), tcp::endpoint(tcp::v4(), m_Port))
		, m_CurrentNumberOfPackets(0)
		, m_NumberOfPackets(4)
		, m_Port(port)
		, m_NIC("192.168.0.109")
		, m_Socket(NetworkManager::GetInstance()->GetIOService())
		, m_CurrentDataSize(0)
		, m_MissingPackets(0)
		, m_Index(-1)
	{
		m_NetworkRunner = std::unique_ptr<ServerDeviceRunner>(new ServerDeviceRunner(this));
		m_NetworkThread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_NetworkRunner)));
		m_UDPData.resize(1024);
		m_DeviceInfo = "Receiver 1.0";
		if (m_Runner) delete m_Runner;
		m_Runner = 0;
		if (m_NetworkRunner)
		{
			m_NetworkRunner->Stop();
		}
	}

	ServerDevice::~ServerDevice()
	{
		if (m_NetworkRunner)
		{
			m_NetworkRunner->Stop();
		}
	}

	bool ServerDevice::Initialize()
	{
		if (m_DataManager)
		{
			m_DataManager->Initialize();
		}
		return true;
	}

	void ServerDevice::SetPort(int port)
	{ 
		if (IsConnected())
		{
			m_NetworkRunner->Stop();
			Disconnect();
			m_Port = port;
			m_NetworkRunner->Reset();
		}
		else
		{
			m_Port = port;
		}
	}

	bool ServerDevice::Start()
	{
		if (m_NetworkRunner && m_NetworkRunner->IsRunning()) return true;
		m_NetworkRunner.release();
		m_NetworkThread.release();
		m_NetworkRunner = std::unique_ptr<ServerDeviceRunner>(new ServerDeviceRunner(this));
		m_NetworkThread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_NetworkRunner)));
		m_NetworkRunner->Reset();
		return true;
	}

	bool ServerDevice::Stop()
	{
		m_NetworkRunner->Stop();
		Disconnect();
		m_CurrentNumberOfPackets = 0;
		m_RunNanoSecond = 0;
		m_MissingPackets = 0;
		m_Index = -1;
		m_NetworkRunner->Stop();
		return true;
	}

	void ServerDevice::Update()
	{
		if (m_DataManager && !m_DeviceDatas.empty())
		{
			m_DeviceDatas.front()->SetStartNanoSecond(m_RunNanoSecond);
			m_RunNanoSecond += m_DeviceDatas.front()->GetDurationNanoSecond();
			m_DataManager->AddData(m_DeviceDatas.front());
			m_DataManager->EndSample();
			m_DataManager->BeginSample();
		}
	}

	bool ServerDevice::IsConnected()
	{
		return m_Connected;
	}

	bool ServerDevice::Connect()
	{
		if (!IsConnected())
		{
			try
			{
				const int BufferSize = SystemManager::GetInstance()->GetNetworkBufferSize();
				m_UDPData.resize(BufferSize);
				PACKET_SIZE = BufferSize - PACKET_HEADER_SIZE;
				m_Receiver.accept(m_Socket);
				m_Connected = true;
			}
			catch (std::exception& e)
			{
				std::cout << e.what() << std::endl;
				m_Connected = false;
			}
		}
		return IsConnected();
	}

	bool ServerDevice::Disconnect()
	{
		m_Receiver.close();
		m_Connected = false;
		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();
		m_Index = -1;
		return !IsConnected();
	}

	void ServerDevice::HandleReceive(size_t length)
	{
		if (length != SystemManager::GetInstance()->GetNetworkBufferSize()) return;
		int sampleRatePerSecond = *((int*)&m_UDPData[4]);
		int numberOfChannels = *((int*)&m_UDPData[8]);
		DATA_FORMAT format = (DATA_FORMAT)*((int*)&m_UDPData[16]);
		int idx = *((int*)&m_UDPData[20]);
		int duration = *((int*)&m_UDPData[0]);
		int InputMilliVoltage = *((int*)&m_UDPData[12]);
		if (sampleRatePerSecond < 0 || idx < 0 || duration < 0 || InputMilliVoltage < 0 || InputMilliVoltage > 100000)
			return;

		if (m_Index != -1)
		{
			if (idx != m_Index + 1 && idx != 0)
			{
				++m_MissingPackets;
			}
		}
		else
		{
			m_CurrentNumberOfPackets = idx;
		}
		m_Index = idx;
		SetDeviceInputMilliVoltage(*((int*)&m_UDPData[12]));
		if (sampleRatePerSecond == 0) return;
		if (m_DeviceDatas.size() > 0)
		{
			DeviceData* data = m_DeviceDatas.front();
			if (data->GetFormat() != format || data->GetDurationNanoSecond() != duration
				|| data->GetNumberOfChannels() != numberOfChannels || data->GetSampleRatePerSecond() != sampleRatePerSecond)
			{
				delete data;
				m_DeviceDatas.clear();
			}
		}
		if (m_DeviceDatas.empty())
		{
			m_SampleRatePerSecond = sampleRatePerSecond;
			m_NumberOfChannels = numberOfChannels;
			m_Format = format;
			if (m_Channels.size() > m_NumberOfChannels)
			{
				for (size_t i = m_Channels.size() - 1; i > m_NumberOfChannels - 1 && i > 0; --i)
				{
					delete m_Channels[i];
					m_Channels.pop_back();
				}
			}
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();
			for (size_t i = m_Channels.size(); i < m_NumberOfChannels; ++i)
			{
				ChannelInfo* info = new ChannelInfo("Channel " + std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(info);
			}

			for (size_t i = 0; i < m_NumberOfChannels; ++i)
			{
				m_Channels[i]->Init(duration, m_SampleRatePerSecond);
			}

			if (m_DataManager)
			{
				m_DataManager->Initialize();
				m_DataManager->SetManagedSampleRatePerSecond(m_SampleRatePerSecond);
			}
			DeviceData* data = new DeviceData((*(int*)&m_UDPData[0]), duration, m_SampleRatePerSecond, m_NumberOfChannels, GetDeviceInputMilliVoltage(), m_Format);
			m_DeviceDatas.push_back(data);
			long long datasize = data->GetADCCountDataSizeOfNanoSecond(duration);
			m_NumberOfPackets = static_cast<long>(ceil(datasize / (double)PACKET_SIZE));
		}
		if (!m_DeviceDatas.empty())
		{
			DeviceData* data = m_DeviceDatas.front();
			long length = (long)PACKET_SIZE;
			long pre_size = (long)idx * PACKET_SIZE;
			long byteSize = (long)data->GetDataByteSize();
			if (length > (byteSize - pre_size))
			{
				length = (byteSize - pre_size);
			}
			if (length > 0)
			{
				memcpy(data->GetADCCountData() + pre_size, &m_UDPData[PACKET_HEADER_SIZE], length);
			}
			if (idx == m_NumberOfPackets - 1)
			{
				Update();
				m_CurrentNumberOfPackets = m_NumberOfPackets - 1;
			}
		}
		++m_CurrentNumberOfPackets;
	}

	void ServerDevice::ReceiveData()
	{
		boost::system::error_code error;
		int length = m_Socket.read_some(boost::asio::buffer(m_UDPData), error);
		if (!error)
		{
			HandleReceive(length);
		}
	}

	void ServerDevice::SetNICHost(const std::string& nic)
	{
		if (nic != m_NIC)
		{
			if (m_NetworkRunner)
			{
				m_NetworkRunner->Stop();
				Disconnect();
				m_NIC = nic;
				m_NetworkRunner->Reset();
			}
			else
			{
				m_NIC = nic;
			}
		}
	}

}