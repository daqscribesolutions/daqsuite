#include "CalibrationIO.h"
#include <fstream>
#include <algorithm>
#include "ChannelInfo.h"

namespace DeviceLib
{
	std::vector<char> HexToBytes(const std::string& hex)
	{
		std::vector<char> bytes;
		for (unsigned int i = 0; i < hex.length(); i += 2) 
		{
			std::string byteString = hex.substr(i, 2);
			char byte = (char)strtol(byteString.c_str(), NULL, 16);
			bytes.push_back(byte);
		}
		return bytes;
	}

	struct Tokenizer
	{
		std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
		{
			size_t current;
			size_t next = -1;
			std::vector<std::string> columns;
			int nColumns = 0;
			do
			{
				current = next + 1;
				next = line.find_first_of(delimiters, current);
				std::string field = line.substr(current, next - current);
				columns.push_back(field);
				++nColumns;
			} while (next != std::string::npos);
			return columns;
		}
	};

	void CalibrationIO::LoadCalibration(const std::string& filename, std::vector<ChannelInfo*> channels)
	{
		std::ifstream fin(filename.c_str());
		if (fin.good())
		{
			Tokenizer tok;
			std::string line;
			bool bFound = false;
			while (getline(fin, line))
			{
				if (line.find("Voltage Calibration Data") != std::string::npos)
				{
					bFound = true;
					break;
				}
			}
			int nCount = 0;
			if (bFound)
			{
				while (getline(fin, line))
				{
					if (line.find("Channel Count") != std::string::npos)
					{
						std::vector<std::string> fields = tok.GetFields(line, ":");
						if (fields.size() == 2)
						{
							std::vector<std::string> items = tok.GetFields(fields[1], ",");
							std::string item;
							for (int i = 0; i < (int)items.size(); ++i)
							{
								item += items[i];
							}
							std::vector<char> val = HexToBytes(item);
							nCount = *(int*)&val[0];
						}
						break;
					}
				}
			}
			for (int i = 0; i < nCount && i < (int)channels.size(); ++i)
			{
				auto& channel = channels[i];
				getline(fin, line);
				std::vector<std::string> fields = tok.GetFields(line, ":");
				if (fields.size() == 2)
				{
					std::vector<std::string> items = tok.GetFields(fields[1], ",");
					std::string item;
					for (int i = 0; i < (int)items.size(); ++i)
					{
						item += items[i];
					}
					std::vector<char> val = HexToBytes(item);
					channel->SetSensitivity(*(double*)&val[0]);
				}
				getline(fin, line);
				fields = tok.GetFields(line, ":");
				if (fields.size() == 2)
				{
					std::vector<std::string> items = tok.GetFields(fields[1], ",");
					std::string item;
					for (int i = 0; i < (int)items.size(); ++i)
					{
						item += items[i];
					}
					std::vector<char> val = HexToBytes(item);
					channel->SetOffset(*(double*)&val[0]);
				}
				getline(fin, line);
			}
			bFound = false;
			while (getline(fin, line))
			{
				if (line.find("EU Calibration Data") != std::string::npos)
				{
					bFound = true;
					break;
				}
			}
			nCount = 0;
			if (bFound)
			{
				while (getline(fin, line))
				{
					if (line.find("Channel Count") != std::string::npos)
					{
						std::vector<std::string> fields = tok.GetFields(line, ":");
						if (fields.size() == 2)
						{
							std::vector<std::string> items = tok.GetFields(fields[1], ",");
							std::string item;
							for (int i = 0; i < (int)items.size(); ++i)
							{
								item += items[i];
							}
							std::vector<char> val = HexToBytes(item);
							nCount = *(int*)&val[0];
						}
						break;
					}
				}
			}
			for (int i = 0; i < nCount && i < (int)channels.size(); ++i)
			{
				auto& channel = channels[i];
				getline(fin, line);
				std::vector<std::string> fields = tok.GetFields(line, ":");
				if (fields.size() == 2)
				{
					std::vector<std::string> items = tok.GetFields(fields[1], ",");
					std::string item;
					for (int i = 0; i < (int)items.size(); ++i)
					{
						item += items[i];
					}
					std::vector<char> val = HexToBytes(item);
					channel->SetEUSensitivity(*(double*)&val[0]);
				}
				getline(fin, line);
				fields = tok.GetFields(line, ":");
				if (fields.size() == 2)
				{
					std::vector<std::string> items = tok.GetFields(fields[1], ",");
					std::string item;
					for (int i = 0; i < (int)items.size(); ++i)
					{
						item += items[i];
					}
					std::vector<char> val = HexToBytes(item);
					channel->SetEUOffset(*(double*)&val[0]);
				}
				getline(fin, line);
			}
		}
	}

	CalibrationIO::CalibrationIO()
	{
	}

	CalibrationIO::~CalibrationIO()
	{
	}

}
