#ifndef SERVERDEVICE_H
#define SERVERDEVICE_H

#include "VirtualDevice.h"
#include <boost/asio.hpp>
#include <boost/array.hpp>

using boost::asio::ip::tcp;

namespace DeviceLib
{
	/**
	 * ServerDevice receives the device data from the client device and provides the data to the device data update listeners.
	 */
	class ServerDevice : public VirtualDevice
	{

	public:
		/**
		 * The full constroctor
		 * @param port The port to receive data from the client device.
		 */
		ServerDevice(int port);

		virtual ~ServerDevice();

		virtual bool Initialize();

		virtual bool Start();

		virtual bool Stop();

		virtual void Update();

		/**
		 * IsConnected checks the status of the network stream and return the status of connection.
		 * @return True if the connection to the server is established, otherwise false.
		 */
		bool IsConnected();

		/**
		 * Connect open the socket to wait connections using TCP.
		 * @return True if the connection to the server is established, otherwise false.
		 */
		bool Connect();

		/**
		 * Disconnect close the socket the server using TCP.
		 * @return True if the connection to the server is terminated, otherwise false.
		 */
		bool Disconnect();

		/**
		 * ReceiveData receive one device data unit from the client using TCP.
		 * @return The device data is stored in .
		 */
		void ReceiveData();

		/**
		 * GetNICHost is the getter for the network card to bind.
		 * @return The network card to bind information.
		 */
		const std::string& GetNICHost() { return m_NIC; }

		/**
		 * SetNICHost is the setter for the network card to bind.
		 * @param host The network card to bind information to be connected.
		 */
		void SetNICHost(const std::string& nic);

		/// The getter for TCP port.
		int GetPort() { return m_Port; }

		/// The setter for TCP port.
		void SetPort(int port);

		/// The getter for the running state.
		bool IsRunning() { return m_IsRunning; }

		/// The getter for the missing packets.
		long long GetMissingPackets() { return m_MissingPackets; }

	private:

		/**
		 * HandleReceive receives the device data unit from the client using TCP.
		 * @param length The length of received data.
		 */
		void HandleReceive(size_t length);

		/// Port to connect TCP.
		int m_Port;

		/// The nic information.
		std::string m_NIC;

		/// The number of packets for one device data.
		long m_NumberOfPackets;

		/// The current number of the received packets.using boost::asio::ip::tcp;
		long m_CurrentNumberOfPackets;

		/// The receiver socket object.
		tcp::acceptor m_Receiver;

		tcp::socket m_Socket;

		/// The data buffer to be used for TCP connection.
		std::vector<char> m_UDPData;

		int m_CurrentDataSize;

		/// The flag for the TCP connection state.
		bool m_Connected;

		struct ServerDeviceRunner;

		/// The network connection handling functor.
		std::unique_ptr<ServerDeviceRunner> m_NetworkRunner;

		/// The thread to run the device.
		std::unique_ptr<boost::thread> m_NetworkThread;

		/// The flag for the running state.
		bool m_IsRunning;

		/// The number of missing packets.
		long long m_MissingPackets;

		/// The number of packets to check missing packets.
		int m_Index;

	};

}

#endif