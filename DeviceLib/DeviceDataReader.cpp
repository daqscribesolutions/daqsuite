#include "DeviceDataReader.h"
#include <fstream>

namespace DeviceLib
{
	struct Tokenizer
	{
		std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
		{
			size_t current;
			size_t next = -1;
			std::vector<std::string> columns;
			int nColumns = 0;
			do
			{
				current = next + 1;
				next = line.find_first_of(delimiters, current);
				columns.push_back(line.substr(current, next - current));
				++nColumns;
			} while (next != std::string::npos);
			return columns;
		}
	};

	DeviceDataReader::DeviceDataReader(const std::string& filename)
		: m_fin(filename, std::ios::binary)
		, m_data(0)
		, m_nbytes(0)
		, m_dataduration(0)
		, m_currentByte(0)
	{
		if (m_fin.good())
		{
			m_fin.read(header, 512);
			header[511] = '\0';
			std::string line = header;
			Tokenizer token;
			std::vector<std::string> elem = token.GetFields(line, ":");
			int nElements = (int)elem.size();
			if (nElements < 14)
			{
				return;
			}
			std::vector<std::string> elem1 = token.GetFields(elem[0], " ");
			m_nbytes = atoi(elem1[0].c_str());
			long nRate = (long)atof(elem[6].c_str());
			int nChannel = atoi(elem[8].c_str());
			long long nFormat = ((long long)atoi(elem[10].c_str()) == 4 ? 2 : 4);
            //long long nDate = (long long)atoi(elem[12].c_str());
            //long long nStartTime = (long long)atoi(elem[14].c_str());
			int nInputVoltage = nElements > 26 ? atoi(elem[26].c_str()) : 2500;
			m_nframes = m_nbytes / (nChannel * nFormat);
			m_dataduration = (long long)((double)m_nframes / (nChannel * nFormat / nRate) * 1000000000);
            long long du = (long long)(500.0 / nRate * 1000000000);
			m_data = new DeviceData(0, du, nRate, nChannel, nInputVoltage, nFormat == 4 ? FOUR : TWO);
			ReadNext();
		}
	}

	DeviceDataReader::~DeviceDataReader()
	{
		delete m_data;
		m_fin.close();
	}

	void DeviceDataReader::ReadNext()
	{
		if (m_fin.good())
		{
			if (m_currentByte < m_nbytes)
			{
				if (m_currentByte + m_data->GetDataByteSize() < m_nbytes)
				{
					m_fin.read(m_data->GetADCCountData(), (size_t)m_data->GetDataByteSize());
					m_currentByte += m_data->GetDataByteSize();
				}
				else
				{
					long long ds = m_nbytes - m_currentByte;
					long long b = DeviceData::GetCloseADCCountDataSizeAndNanoSecond(ds, m_data->GetSampleRatePerSecond()
						, m_data->GetNumberOfChannels(), m_data->GetFormat());
					m_data->SetDurationNanoSecond(b);
					m_data->InitData();
					m_fin.read(m_data->GetADCCountData(), (size_t)m_data->GetDataByteSize());
					m_currentByte += m_data->GetDataByteSize();
				}
			}
		}
	}

	void DeviceDataReader::Reset()
	{
		m_fin.seekg(512);
		ReadNext();
	}

	std::vector<DeviceData*> DeviceDataReader::ReadDatas(const char* filename)
	{
		std::vector<DeviceData*> datas;
		char header[512];
		std::ifstream fin(filename, std::ios::binary);
		if (fin.good())
		{
			fin.read(header, 512);
			header[511] = '\0';
			long long nBytes = 0;
			std::string line = header;
			Tokenizer token;
			std::vector<std::string> elem = token.GetFields(line, ":");
			int nElements = (int)elem.size();
			if (nElements < 14)
			{
				return datas;
			}
			std::vector<std::string> elem1 = token.GetFields(elem[0], " ");
			nBytes = atoi(elem1[0].c_str());
			long nRate = (long)atof(elem[6].c_str());
			int nChannel = atoi(elem[8].c_str());
			long long nFormat = ((long long)atoi(elem[10].c_str()) == 4 ? 2 : 4);
            //long long nDate = (long long)atoi(elem[12].c_str());
            //long long nStartTime = (long long)atoi(elem[14].c_str());
			int nInputVoltage = nElements > 26 ? atoi(elem[26].c_str()) : 2500;
			long long nFrames = nBytes / (nChannel * nFormat);
			const int MAX_FRAMES = 1048576;
			long long nData = (long long)ceil(nFrames / (double)MAX_FRAMES);
			long long currenttime = 0;
			for (long long i = 0; i < nData; ++i)
			{
				long long ncurrent = MAX_FRAMES;
				if (i == nData - 1)
				{
					ncurrent = nFrames % MAX_FRAMES;
				}
				long long nNanoDuration = (long long)(((double)ncurrent / nRate) * 1000000000.0);
				DeviceData* data = new DeviceData(currenttime, nNanoDuration, nRate, nChannel, nInputVoltage, nFormat == 4 ? FOUR : TWO);
				currenttime += nNanoDuration;
				fin.read(data->GetADCCountData(), (size_t)data->GetDataByteSize());
				datas.push_back(data);
			}
		}
		fin.close();

		return datas;
	}

	double DeviceDataReader::GetDataDuration(const char* filename)
	{
		double dataduration = 0;
		char header[512];
		std::ifstream fin(filename, std::ios::binary);
		if (fin.good())
		{
			fin.read(header, 512);
			header[511] = '\0';
			long long nBytes = 0;
			std::string line = header;
			Tokenizer token;
			std::vector<std::string> elem = token.GetFields(line, ":");
			int nElements = (int)elem.size();
			if (nElements < 14)
			{
				return dataduration;
			}
			std::vector<std::string> elem1 = token.GetFields(elem[0], " ");
			nBytes = atoi(elem1[0].c_str());
			long nRate = (long)atof(elem[6].c_str());
			int nChannel = atoi(elem[8].c_str());
			long long nFormat = ((long long)atoi(elem[10].c_str()) == 4 ? 2 : 4);
			dataduration = (double)nBytes / (nChannel * nFormat / nRate);
		}
		fin.close();

		return dataduration;
	}
}
