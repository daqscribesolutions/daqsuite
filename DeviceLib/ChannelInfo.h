#ifndef CHANNELINFO_H
#define CHANNELINFO_H
#include <string>
#include <vector>

namespace DeviceLib
{

	struct ChannelUserData
	{
		ChannelUserData() : name("User Data") {}
		std::string name;
		std::string value;
	};
	class IADCDevice;
	class DeviceData;
	/**
	 * ChannelInfo holds the information of the channel callibration and the data from the board for display and store.
	 */
	class ChannelInfo
	{

	public:

		/**
		 * ChannelInfo is the full constructor.
		 * @param name The name of the channel.
		 * @param pdev The device pointer.
		 * @param sensitivity The sensitivity value for the channel.
		 * @param offset The offset value for the channel.
		 * @param duration The duration of the data for the channel.
		 * @param sampleratepersecond The sample rate per second of the channel.
		 * @return The channel info is set and ready to use.
		 */
		ChannelInfo(const std::string& name, IADCDevice* pdev, double sensitivity = 1, double offset = 0, long long duration = 50000, long sampleratepersecond = 1000);
		
		~ChannelInfo();

		/// The getter for the sensitivity.
		double GetSensitivity(); 

		/// The setter for the sensitivity.
		void SetSensitivity(double value);	

		/// The getter for the offset.
		double GetOffset();		

		/// The setter for the offset.
		void SetOffset(double value);

		/// The getter for the EU sensitivity.
		double GetEUSensitivity();

		/// The setter for the EU sensitivity.
		void SetEUSensitivity(double value);

		/// The getter for the EU offset.
		double GetEUOffset();

		/// The setter for the EU offset.
		void SetEUOffset(double value);

		/**
		 * Init sets the memory space.
		 * @param duration The duration of the data for the channel.
		 * @param sampleratepersecond The sample rate per second of the channel.
		 * @return The channel info is set and ready to use.
		 */
		void Init(long long duration, long sampleratepersecond); 

		/// The getter for the start time.
		long long GetStartNanoSecond();

		/// The setter for the start time.
		void SetStartNanoSecond(long long value);

		/// The getter for the sample rate.
		long GetSampleRatePerSecond();	

		/// The setter for the sample rate.
		void SetSampleRatePerSecond(long value);	

		/// The getter for the duration.
		long long GetDurationNanoSecond();	

		/// The setter for the duration.
		void SetDurationNanoSecond(long long value);	

		/// The getter for the active flag.
		bool GetActive() const { return m_bActive; }	

		/// The setter for the active flag.
		void SetActive(bool value);	

		/// The getter for the datasize.
		long long GetDataSize() { return m_DataSize; }
		
		/// The data setter.
		void SetSnapShotData(DeviceData* data);
		
		/// The data getter
		DeviceData* GetData();

		DeviceData* GetSnapShotData() { return m_SnapshotData; }
		/// The name of the channel to distinguish
		std::string Name;

		/// The description of the channel
		std::string Description;

		/// The EU unit name.
		std::string EUName;

		double MilivoltDataMin;	/// Minimum value of the channel Milivolt data.

		double MilivoltDataMax; /// Maximum value of the channel Milivolt data.

		double EUDataMin;		/// Minimum value of the channel EU data.

		double EUDataMax;		/// Maximum value of the channel EU data.

		double Average;			/// Average.

		double RMS;				/// RMS.

		double EUAverage;		/// EUAverage.

		double EURMS;			/// EURMS.

		long long ActualDuration; /// Actual duration of the data.

		long long ActualDataSize; /// Actual data size of the data.

		std::vector<ChannelUserData> UserData;

		void AddUserData(int idx);

		void RemoveUserData(int idx);

		int GetDeviceId();

		ChannelInfo* pOrigin;

	private:
		
		/// ClearData frees the memory space for the channel.
		void ClearData();

		long long m_DataSize;

		long long m_StartNanoSecond;

		long long m_DurationNanoSecond; /// Duration in nanosecond.
		
		long m_SampleRatePerSecond;	/// Sample rate per second of the channel.

		IADCDevice* m_pDevice;

		DeviceData* m_SnapshotData;

		double m_EUSensitivity;	/// EUSensitivity.

		double m_EUOffset;		/// EUOffset.

		double m_Sensitivity;	/// Sensitivity.

		double m_Offset;		/// Offset.

		bool m_bActive;			/// Active flag.

	};

}
#endif

