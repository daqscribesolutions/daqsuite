TARGET = DeviceLib
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++11

include(DeviceLib.pri)


INCLUDEPATH += $$PWD/../../boost_1_59_0
DEPENDPATH += $$PWD/../../boost_1_59_0
