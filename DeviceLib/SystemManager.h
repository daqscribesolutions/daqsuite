#ifndef SYSTEMMANAGER_H
#define SYSTEMMANAGER_H

#include <vector>
#include <memory>
#include <boost/property_tree/ptree.hpp>
#include <boost/thread.hpp>
namespace DeviceLib
{
	class IDevice;
	class IDeviceDataManager;
	class IDeviceDataUpdateListener;
	class ClientDevice;
	enum SYSTEM_STATUS { STOPPED, MONITORING, RECORDING, READYTOSTOP };
	enum RECORD_TYPE { NO_FILES, RAWFS, LDSF_FILE, RAWFS_LDSF};
	/**
	 * IDeviceFactor is the interface fo DeviceFactory to add the new devices.
	 */
	class IDeviceFactory
	{
	public:
		~IDeviceFactory() {}
		virtual void GetDevices(std::vector<IDevice*>& devices) = 0;
	};

	/**
	 * SystemManager manages the boards and the data managers.
	 */
	class SystemManager
	{

	public:

		/**
		 * GetInstance supports the singleton.
		 * @return The SystemManager pointer.
		 */
		static SystemManager* GetInstance();

		/**
		 * StartMonitor begins to monitor the data from the active boards.
		 * @return True if monitoring is done successfuly, otherwise return false.
		 */
		bool StartMonitor();

		/**
		 * StartRecord begins to store the data from the active boards.
		 * @return True if recording is started successfuly, otherwise return false.
		 */
		bool StartRecord();

		/**
		 * ArmTrigger sets the trigger to check the trigger condition.
		 */
		void ArmTrigger();

		/**
		 * StopMonitor ends monitoring the data from the active boards.
		 * @return Monitoring is ended.
		 */
		void StopMonitor();

		/**
		 * StopRecord ends storing the data from the active boards.
		 * @return Recording is ended.
		 */
		void StopRecord();

		/**
		 * GetDevices returns all devices available in the system.
		 * @return All devices available in the system.
		 */		
		const std::vector<std::shared_ptr<IDevice>>& GetDevices() { return m_Devices; }

		/**
		 * GetErrorMessage returns the error message.
		 * @return The error message is returned.
		 */		
		const char* GetErrorMessage();

		/**
		 * SetErrorMessage sets the error message.
		 * @param message The error message to set.
		 * @return The error message is set for the detail information.
		 */		
		void SetErrorMessage(const char* message);

		/**
		 * AddClientDevice adds a client device to the given host.
		 * @param host The server of TCP connection.
		 * @param pDevice The device to send the data to the host.
		 * @param port The TCP port.
		 */		
		void AddClientDevice(const std::string& host, const std::shared_ptr<IDevice>& pDevice, int port);

		/**
		 * AddServerDevice adds a server device to the receive data.
		 * @param pDevice The client device pointer to be removed.
		 */
		void RemoveClientDevice(std::shared_ptr<ClientDevice>& pDevice);

		/**
		 * AddDevices add devices from the factory to the system.
		 * @param factory The device factory for the different devices.
		 */		
		void AddDevices(IDeviceFactory& factory);

		/// The accessor for the client devices.
		std::vector<std::shared_ptr<ClientDevice>> GetClientDevices();

		/**
		 * AddServerDevice adds a server device to the receive data.
		 * @param port The TCP port.
		 */
		void AddServerDevice(int port);

		/**
		 * AddVirtualDevice adds a virtual device to emulate.
		 */		
		void AddVirtualDevice();

		/**
		 * RemoveDevice removes a device from the system.
		 * @param pDevice The device pointer to be removed.
		 */		
		void RemoveDevice(std::shared_ptr<IDevice>& pDevice);

		/**
		 * RemoveDeviceDataManager removes a device data manager from the system.
		 * @param pDeviceDataManager The device data manager pointer to be removed.
		 */		
		void RemoveDeviceDataManager(std::shared_ptr<IDeviceDataManager>& pDeviceDataManager);

		/**
		 * RemoveDataUpdateListener removes a device data listener from the system.
		 * @param pDeviceDataListener The device data listener pointer to be removed.
		 */		
		void RemoveDataUpdateListener(IDeviceDataUpdateListener* pDeviceDataListener);

		/**
		 * AddDataUpdateListener removes a device data listener from the system.
		 * @param pDevice The device pointer to be listened.
		 * @param pDeviceDataListener The device data listener pointer to be added.
		 */		
		void AddDataUpdateListener(const std::shared_ptr<IDevice>& pDevice, IDeviceDataUpdateListener* pDeviceDataListener);

		/// The getter for the running time.
		long long GetSystemRunningTime();

		/// The setter for recording status.
		void SetRecording(bool bRecording);

		/// The getter for the status.
		SYSTEM_STATUS GetStatus() { return m_Status; }

		/// The getter for the monitoring data speed.
		double GetMonitorSpeed();

		/// The getter for the monitoring data peak speed.
		double GetMonitorSampleSpeed();

		/**
		 * ReadSystemInfo loads the saved system information.
		 * @param pt The xml file node.
		 */		
		void ReadSystemInfo(const boost::property_tree::ptree& pt);

		/**
		 * WriteSystemInfo saves the system information.
		 * @param pt The xml file node.
		 */		
		void WriteSystemInfo(boost::property_tree::ptree& pt);

		/**
		 * LoadCalibration saves the system information.
		 * @param calFile The calibration file name to be loaded.
		 * @param pDevice The device pointer for calibration information.
		 */
		void LoadCalibration(const std::string& calFile, const std::shared_ptr<IDevice>& pDevice);

		/// The accessor for the network buffer size to be used in client/server network.
		int GetNetworkBufferSize() { return m_NetworkBufferSize; }

		/// The setter for the network buffer size to be used in client/server network.
		void SetNetworkBufferSize(int bufferSize) { m_NetworkBufferSize = bufferSize; }

		/**
		 * ConnectServerClient connects server and clients.
		 * @return The connected stats of clients/servers will be updated.
		 */
		void ConnectServerClient();

		/**
		 * DisconnectServerClient connects server and clients.
		 * @return The connected stats of clients/servers will be updated.
		 */
		void DisconnectServerClient();

		/// Clear releases resources.
		void Clear();

		/// Removes all the devices in the system. (client/server)
		void ClearDevices();

		/// The accessor for the flag to load GS board.
		bool GetLoadGSFlag() { return m_LoadGSBoard; }

		/// The setter for the flag to load GS board.
		void SetLoadGSFlag(bool flag) { m_LoadGSBoard = flag; }

		/// The accessor for the flag to load BlackBurn board.
		bool GetLoadBlackBurnBoardFlag() { return m_LoadBlackBurnBoard; }

		/// The setter for the flag to load BlackBurn board.
		void SetLoadBlackBurnBoardFlag(bool flag) { m_LoadBlackBurnBoard = flag; }

		/// The accessor for the flag to load ICS645B board.
		bool GetLoadICS645BBoardFlag() { return m_LoadICS645BBoard; }

		/// The setter for the flag to load ICS645B board.
		void SetLoadICS645BBoardFlag(bool flag) { m_LoadICS645BBoard = flag; }

		/// The accessor for the Post Record Status.
		SYSTEM_STATUS GetPostRecordStatus() { return m_PostRecordStatus; }

		/// The setter for the Post Record Status.
		void SetPostRecordStatus(SYSTEM_STATUS flag) { m_PostRecordStatus = flag; }

		/// The accessor for the Record Type.
		RECORD_TYPE GetRecordType() { return m_RecordType; }

		/// The setter for the Record Type.
		void SetRecordType(RECORD_TYPE flag) { m_RecordType = flag; }
		
		/// GetChannelDuration gets the current duration in second in channel.
		double GetChannelDuration();

		/// GetInputMilivoltage gets the current input voltage.
		int GetInputMilivoltage();

		/// SetInputMilivoltage sets the current input voltage.
		void SetInputMilivoltage(int voltage);

		/// GetSampleRate gets the current sample rate in the devices.
		long GetSampleRate();

		/// SetSampleRate gets the current sample rate in the devices.
		void SetSampleRate(long samplerate);

		/// GetSampleSize gets the current sample size in the devices.
		int GetSampleSize();

		/// GetRecordDataSize in MB.
		double GetRecordDataSize();

		/// GetRecordProgress in percentage.
		int GetRecordProgres();

		/// Get the state of the record.
		bool RecordExist();

		/**
		 * Wait idles the process for a given micro second.
		 * @param millisecond The millisecond to wait.
		 */		
		static void Wait(int millisecond);

		/**
		 * The function to control user data for channels.
		 */
		void AddChannelUserData(int idx);

		void RemoveChannelUserData(int idx);

		int GetCurrentChannelCount();

		void CheckStatusForStop();

		int GetProgress() { return m_progress; }

		void SetProgress(int prog) { m_progress = prog; }

	private:

		boost::mutex mutex;

		SystemManager();

		~SystemManager();

		static SystemManager* m_Instance;

		std::vector<std::shared_ptr<IDevice>> m_Devices;

		std::vector<std::shared_ptr<IDeviceDataManager>> m_DeviceDataManagers;

		std::vector<IDeviceDataUpdateListener*> m_DeviceDataListeners;

		std::vector<std::shared_ptr<IDevice>> m_FileDevices;

		std::vector<std::shared_ptr<IDeviceDataManager>> m_DeviceFileDataManagers;

		std::vector<std::shared_ptr<IDeviceDataUpdateListener>> m_DeviceFileDataListeners;

		std::vector<std::shared_ptr<ClientDevice>> m_Clients;

		/// The record file type.
		RECORD_TYPE m_RecordType;

		/// The error message of the system.
		std::string m_ErrorMessage;

		/// The system status flag.
		SYSTEM_STATUS m_Status;

		/// The status to go after recording.
		SYSTEM_STATUS m_PostRecordStatus;

		/// The network buffer size.
		int m_NetworkBufferSize;

		/// The flag for loading GS Boards.
		bool m_LoadGSBoard;

		/// The flag for loading Black Burn Boards.
		bool m_LoadBlackBurnBoard;

		/// The flag for loading Black Burn Boards.
		bool m_LoadICS645BBoard;

		/// The record number to keep track of the trigger numbers.
		int m_CurrentRecord;

		/// The progress of monitoring.
		int m_progress;
	};

}

#endif