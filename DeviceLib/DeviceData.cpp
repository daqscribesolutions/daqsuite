#include "DeviceData.h"

namespace DeviceLib
{

	DeviceData::DeviceData(long long startNanoSecond, long long durationNanoSecond, long sampleratepersecond, int numberofchannels, int inputmilivoltage, DATA_FORMAT format)
		: m_StartNanoSecond(startNanoSecond)
		, m_DurationNanoSecond(durationNanoSecond)
		, m_SampleRatePerSecond(sampleratepersecond)
		, m_InputMilliVolt(inputmilivoltage)
		, m_ADCCountData(0)
		, m_NumberOfChannels(numberofchannels)
		, m_Format(format)
		, m_DataSize(0)
	{
		InitData();
	}

	DeviceData::~DeviceData()
	{
		ClearData();
	}

	bool DeviceData::InitData()
	{
		ClearData();
        m_DataSize = (long long)(m_DurationNanoSecond * m_SampleRatePerSecond / 1000000000.0) * m_NumberOfChannels;
		m_ADCCountData = new char[static_cast<unsigned int>(m_DataSize * m_Format)];

		if (!m_ADCCountData) m_DataSize = 0;
		return m_ADCCountData != 0;
	}

	void DeviceData::ClearData()
	{
		if (m_ADCCountData) delete[] m_ADCCountData;
		m_ADCCountData = 0;
	}

	long long DeviceData::GetStartNanoSecond()
	{
		return m_StartNanoSecond;
	}

	void DeviceData::SetStartNanoSecond(long long value)
	{
		m_StartNanoSecond = value;
	}

	long DeviceData::GetSampleRatePerSecond()
	{
		return m_SampleRatePerSecond;
	}

	void DeviceData::SetSampleRatePerSecond(long value)
	{
		m_SampleRatePerSecond = value;
	}

	long long DeviceData::GetDurationNanoSecond()
	{
		return m_DurationNanoSecond;
	}

	void DeviceData::SetDurationNanoSecond(long long value)
	{
		m_DurationNanoSecond = value;
	}

	DATA_FORMAT DeviceData::GetFormat()
	{
		return m_Format;
	}

	void DeviceData::SetFormat(DATA_FORMAT value)
	{
		m_Format = value;
	}

	int DeviceData::GetInputMilliVolt()
	{
		return m_InputMilliVolt;
	}

	void DeviceData::SetInputMilliVolt(int value)
	{
		m_InputMilliVolt = value;
	}

	long long DeviceData::GetDataSize()
	{
		return m_DataSize;
	}

	bool DeviceData::IsCompariable(DeviceData* pData)
	{
		if (pData)
		{
			if (pData->m_NumberOfChannels == m_NumberOfChannels
				&& pData->m_Format == m_Format
				&& pData->m_DurationNanoSecond == pData->m_DurationNanoSecond)
			{
				return true;
			}
		}
		return false;
	}

	DeviceData* DeviceData::ConvertDataToDifferentSampleRate(int sampleRate)
	{
		double rate = (double)sampleRate / m_SampleRatePerSecond;
		if (rate < 1 && rate > 0)
		{
			DeviceData* output = new DeviceData(m_StartNanoSecond, m_DurationNanoSecond, sampleRate, m_NumberOfChannels, m_InputMilliVolt, m_Format);
			if (output)
			{
				int OneSampleDataSize = m_NumberOfChannels * m_Format;
				long long nSamples = (long long)floor((double)m_DataSize / m_NumberOfChannels);
				double desti = 0;
				const int outputsize = (int)output->GetDataByteSize();
				int icopy = 0;
				for (long long i = 0; i < nSamples; ++i, desti += rate)
				{
					if (icopy > desti) continue;
					int index = (icopy * OneSampleDataSize);
					if (index + OneSampleDataSize <= outputsize)
						memcpy(output->m_ADCCountData + index, m_ADCCountData + static_cast<int>(i * OneSampleDataSize), OneSampleDataSize);
					++icopy;
				}
				return output;
			}
			else
			{
				return 0;
			}
		}
		return 0;
	}

	DeviceData* DeviceData::GetActiveChannelData(const std::vector<ChannelInfo*>& channels)
	{
		std::vector<size_t> activeChannels;
		for (size_t i = 0; i < channels.size(); ++i)
		{
			if (channels[i]->GetActive())
			{
				activeChannels.push_back(i * m_Format);
			}
		}
		if (activeChannels.size() > 0)
		{
			DeviceData* output = new DeviceData(m_StartNanoSecond, m_DurationNanoSecond, m_SampleRatePerSecond, (int)activeChannels.size(), m_InputMilliVolt, m_Format);
			if (output)
			{
				int OneSampleDataSize = m_NumberOfChannels * m_Format;
				long long nSamples = m_DataSize / m_NumberOfChannels;
				int OutputOneSampleDataSize = (int)activeChannels.size() * m_Format;

				for (long long i = 0; i < nSamples; ++i)
				{
					long long begin = i * OneSampleDataSize;
					long long outbegin = i *OutputOneSampleDataSize;
					for (size_t j = 0; j < activeChannels.size(); ++j)
					{
						memcpy(output->m_ADCCountData + static_cast<int>(outbegin + (long long)j * m_Format), m_ADCCountData + static_cast<int>(begin + activeChannels[j]), m_Format);
					}
				}
				return output;
			}
		}
		return 0;
	}

	long long DeviceData::GetADCCountDataSizeOfNanoSecond(long long nanoSecond)
	{
		return static_cast<long long>(nanoSecond / 1000000000.0 * m_SampleRatePerSecond) * m_NumberOfChannels * m_Format;
	}

	long long DeviceData::GetCloseADCCountDataSizeAndNanoSecond(long long& datasize, long long sampleRatePerSecond, int numChannels, int format)
	{
		long long ns = datasize / (format * numChannels);
		if (ns < 0) ns = 1;
		ns = ns * 1000000000 / sampleRatePerSecond;
		return ns;
	}
}
