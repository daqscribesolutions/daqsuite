#include "VirtualDevice.h"
#include "DataConverter.h"
#include "ChannelInfo.h"
#include "DeviceDataReader.h"
#include <algorithm>
#include "DeviceRecordSetting.h"
#include "TriggerManager.h"
#include "SystemManager.h"

namespace DeviceLib
{
	VirtualDevice::VirtualDevice(int numberOfChannels, DATA_FORMAT format) 
		: m_MaximumSampleRatePerSecond(2000000)
		, m_MinimumSampleRatePerSecond(2000)
		, m_SampleRatePerSecond(1000000)
		, m_NumberOfChannels(numberOfChannels)
		, m_Format(format)
		, m_InputVoltageIndex(0)
		, m_Thread(0)
		, m_Runner(0)
		, m_RunNanoSecond(0)
		, m_DataManager(0)
		, m_KeepingDuration(1000000000)
		, PACKET_SIZE(1000)
	{
		m_Voltages.push_back(2500);
		m_Voltages.push_back(5000);
		m_Voltages.push_back(10000);
		m_AvailableChannels.push_back(m_NumberOfChannels);
		m_AvailableChannels.push_back(8);
		std::sort(m_AvailableChannels.begin(), m_AvailableChannels.end());
		m_DeviceInfo = "Virtual Device 1.0";
		m_Runner = new DeviceRunner(this);
		m_RenderData = new DeviceData(0, 100000000, (long)m_SampleRatePerSecond,
			m_NumberOfChannels, m_Voltages[m_InputVoltageIndex], m_Format);
		Initialize();
	}

	VirtualDevice::~VirtualDevice()
	{
		Stop();
		ClearDeviceDatas();
		if (m_Runner) delete m_Runner;
		m_Runner = 0;
		delete m_RenderData;
	}

	const char* VirtualDevice::GetDeviceInfo()
	{
		return m_DeviceInfo.c_str();
	}

	std::vector<unsigned int> VirtualDevice::GetAvailableChannelNumbers()
	{
		return m_AvailableChannels;
	}

	void VirtualDevice::SetChannelNumber(unsigned int nchannels)
	{
		unsigned int channels = m_NumberOfChannels;
		for (unsigned int i = 0; i < (unsigned int)m_AvailableChannels.size(); ++i)
		{
			if (m_AvailableChannels[i] >= nchannels)
			{
				channels = m_AvailableChannels[i];
				break;
			}
		}
		if (channels != m_NumberOfChannels)
		{
			m_NumberOfChannels = channels;
			Initialize();
		}
	}

	long VirtualDevice::GetDeviceMaximumSampleRatePerSecond()
	{
		return m_MaximumSampleRatePerSecond;
	}

	long VirtualDevice::GetDeviceMinimumSampleRatePerSecond()
	{
		return m_MinimumSampleRatePerSecond;
	}

	std::vector<int> VirtualDevice::GetDeviceAvailableInputMilliVoltages()
	{
		return m_Voltages;
	}

	int VirtualDevice::GetDeviceInputMilliVoltage()
	{
		return m_Voltages[m_InputVoltageIndex];
	}

	void VirtualDevice::SetDeviceInputMilliVoltage(int voltage)
	{
		for (unsigned int i = 0; i < m_Voltages.size(); ++i)
		{
			if (m_Voltages[i] == voltage)
			{
				m_InputVoltageIndex = i;
			}
		}
	}

	bool VirtualDevice::Initialize()
	{
		m_HardwardSampleRate = m_SampleRatePerSecond;
		if (m_SampleRatePerSecond > m_MaximumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MaximumSampleRatePerSecond;
		}
		else if (m_SampleRatePerSecond < m_MinimumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MinimumSampleRatePerSecond;
		}
		m_KeepingDuration = long long(1000.0 / m_SampleRatePerSecond * 1000000000);
		if (m_Channels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_Channels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_Channels.back();
				m_Channels.pop_back();
			}
		}
		else if (m_Channels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_Channels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();

			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " 
					+ std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(pChannel);
			}
		}
		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->Init(m_KeepingDuration, m_SampleRatePerSecond);
		}
		
		InitSnapshot();

		if (m_DataManager)
		{
			m_DataManager->Initialize();
			m_DataManager->SetManagedSampleRatePerSecond(m_SampleRatePerSecond);
		}
		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();
		DeviceData* data = new DeviceData(0, m_KeepingDuration / 2, m_SampleRatePerSecond,
			m_NumberOfChannels, GetDeviceInputMilliVoltage(), m_Format);
		DataConverter converter(data, this, false);
		converter.SetSineSignal();
		converter.ConvertADCtoMilivoltAndEUValue();
		DeviceData* sameRate = data->ConvertDataToDifferentSampleRate(m_SampleRatePerSecond);
		if (sameRate == 0)
		{
			sameRate = data;
		}
		else
		{
			delete data;
		}
		m_DeviceDatas.push_back(sameRate);
		m_RenderData->SetInputMilliVolt(m_Voltages[m_InputVoltageIndex]);
		m_RenderData->SetDurationNanoSecond(m_KeepingDuration);
		m_RenderData->SetSampleRatePerSecond(m_SampleRatePerSecond);
		m_RenderData->InitData();
		return true;
	}

	void VirtualDevice::InitSnapshot()
	{
		if (m_SnapshotChannels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_SnapshotChannels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_SnapshotChannels.back();
				m_SnapshotChannels.pop_back();
			}
		}
		else if (m_SnapshotChannels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_SnapshotChannels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();
			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " 
					+ std::to_string((long long)m_SnapshotChannels.size() + ncounts + 1), this);
				m_SnapshotChannels.push_back(pChannel);
			}
		}
		long long duration = std::min((long long)1000000000, 
			(long long)(TriggerManager::GetInstance()->GetPreTrigger() * 1000000000 
			+ DeviceRecordSetting::GetInstance()->GetRecordDuration()));
		for (size_t i = 0; i < m_SnapshotChannels.size(); ++i)
		{
			ChannelInfo* pChannel = m_SnapshotChannels[i];
			pChannel->Init(duration, m_SampleRatePerSecond);
			pChannel->pOrigin = m_Channels[i];
		}
	}

	bool VirtualDevice::Start()
	{
		Stop();
		m_Runner->Reset();
		m_RunNanoSecond = 0;
		m_Thread = new boost::thread(boost::ref(*m_Runner));
		return true;
	}

	bool VirtualDevice::Stop()
	{
		if (m_Thread)
		{
			m_Runner->Stop();
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			delete m_Thread;
			m_Thread = 0;
		}
		return true;
	}

	long VirtualDevice::GetSampleRatePerSecond()
	{
		return m_SampleRatePerSecond;
	}

	void VirtualDevice::SetSampleRatePerSecond(long rate)
	{
		if (m_Thread) return;
		m_SampleRatePerSecond = std::max(rate, (long)1);
	}

	std::vector<ChannelInfo*>& VirtualDevice::GetChannels()
	{
		return m_Channels;
	}

	std::vector<ChannelInfo*>& VirtualDevice::GetSnapshotChannels()
	{
		return m_SnapshotChannels;
	}

	void VirtualDevice::SetDeviceDataManager(const std::shared_ptr<IDeviceDataManager>& manager)
	{
		m_DataManager = manager;
	}

	DATA_FORMAT VirtualDevice::GetDeviceDataFormat()
	{
		return m_Format;
	}

	void VirtualDevice::Update()
	{
		if (m_DataManager && !m_DeviceDatas.empty())
		{
			int duration = (int)(GetDeviceData().front()->GetDurationNanoSecond() / 1000);
			clock_t t = clock();
			long long elipse = clock() - t;
			m_DeviceDatas.front()->SetStartNanoSecond(m_RunNanoSecond);
			m_RunNanoSecond += m_DeviceDatas.front()->GetDurationNanoSecond();
			if (m_SampleRatePerSecond < 100000)
			{
				double dval = (m_RunNanoSecond / m_DeviceDatas.front()->GetDurationNanoSecond() % 2 == 0 ? 0.5 : 2);
				DataConverter converter(m_DeviceDatas.front(), this, false);
				converter.Multiply(dval);
			}
			m_DataManager->AddData(m_DeviceDatas.front());
			m_DataManager->EndSample();
			m_DataManager->BeginSample();
			int remaining = duration - (int)elipse * 1000;
			remaining = std::max(remaining, 0);
			boost::this_thread::sleep(boost::posix_time::microseconds(remaining));
		}
	}

	void VirtualDevice::ClearDeviceDatas()
	{
		Stop();
		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();
		struct ClearChannelsFunctor
		{
			void operator()(ChannelInfo*& data)
			{
				delete data;
			}
		};
		std::for_each(m_Channels.begin(), m_Channels.end(), ClearChannelsFunctor());
		m_Channels.clear();
		std::for_each(m_SnapshotChannels.begin(), m_SnapshotChannels.end(), ClearChannelsFunctor());
		m_SnapshotChannels.clear();
	}

	void VirtualDevice::ReadData(const char* filename)
	{
		DeviceData* data = 0;// = DeviceDataReader::ReadDatas(filename);
		if (data == 0)
		{
			ClearDeviceDatas();
			return;
		}
		m_NumberOfChannels = data->GetNumberOfChannels();
		m_KeepingDuration = data->GetDurationNanoSecond();
		m_SampleRatePerSecond = data->GetSampleRatePerSecond();
		m_Format = data->GetFormat();

		if (m_Channels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_Channels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_Channels.back();
				m_Channels.pop_back();
			}
		}
		else if (m_Channels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_Channels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();

			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " 
					+ std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(pChannel);
			}
		}
		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->Init(m_KeepingDuration, m_SampleRatePerSecond);
		}

		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();

		DataConverter converter(data, this, false);
		converter.ConvertADCtoMilivoltAndEUValue();
		m_DeviceDatas.push_back(data);
	}

	void VirtualDevice::AddChannelUserData(int idx)
	{
		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->AddUserData(idx);
		}
	}

	void VirtualDevice::RemoveChannelUserData(int idx)
	{
		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->RemoveUserData(idx);
		}
	}

	DeviceData* VirtualDevice::GetMonitorData()
	{
		return m_DataManager->GetCurrentData();
	}
}