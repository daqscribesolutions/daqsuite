#ifndef CLIENTDEVICE_H
#define CLIENTDEVICE_H

#include "VirtualDevice.h"
#include <boost/asio.hpp>
#include <boost/array.hpp>
using boost::asio::ip::tcp;

namespace DeviceLib
{

	/**
	 * ClientDevice connects to the server device and sends the data through TCP.
	 */
	class ClientDevice : public IDeviceDataUpdateListener
	{

	public:

		/// The full constructor.
		ClientDevice(const std::shared_ptr<IDevice>& pDevice, const std::string& host, int port);

		virtual ~ClientDevice();

		/**
		 * GetHost is the getter for the server host.
		 * @return The host information.
		 */
		const std::string& GetHost() { return m_Host; }

		/**
		 * SetHost is the setter for the server host.
		 * @param host The host information to be connected.
		 */
		void SetHost(const std::string& host);

		/**
		 * GetNICHost is the getter for the network card to bind.
		 * @return The network card to bind information.
		 */
		const std::string& GetNICHost() { return m_NIC; }

		/**
		 * SetNICHost is the setter for the network card to bind.
		 * @param host The network card to bind information to be connected.
		 */
		void SetNICHost(const std::string& nic);

		/**
		 * GetDevice is the getter for the device.
		 * @return The device of the client.
		 */
		std::shared_ptr<IDevice>& GetDevice() { return m_pDevice; }

		/**
		 * IsConnected checks the status of the network stream and return the status of connection.
		 * @return True if the connection to the server is established, otherwise false.
		 */
		bool IsConnected();

		/**
		 * Connect connects the server using TCP.
		 * @return True if the connection to the server is established, otherwise false.
		 */
		bool Connect();

		/**
		 * Disconnect connects the server using TCP.
		 * @return True if the connection to the server is terminated, otherwise false.
		 */
		bool Disconnect();

		/// IDeviceDataUpdateListener implementation
		virtual void UpdateEntryData(DeviceData* data);

		virtual void UpdateExitData(DeviceData* data);

		virtual bool StartListening();

		virtual void StopListening();

		/// The getter for the port.
		int GetPort() { return m_Port; }

		/// The setter for the port.
		void SetPort(int port);

	protected:

		/// The server information.
		std::string m_Host;

		/// The nic information.
		std::string m_NIC;

		/// The server port.
		int m_Port;

		/// The flag for the TCP connection state.
		bool m_Connected;

		/// TCP end point to support TCP connection.
		tcp::endpoint m_ReceiverEndPoint;

		/// The data buffer to be used for TCP connection.
		std::vector<char> m_UDPData;

		/// TCP connection object.
		tcp::socket m_Sender;

		/// The flag for the state that the header sent to the server.
		bool m_bHeaderSent;

		/// The device to send to the host.
		std::shared_ptr<IDevice> m_pDevice;

		struct ClientDeviceRunner;

		/// The network connection handling functor.
		std::unique_ptr<ClientDeviceRunner> m_NetworkRunner;
		
		/// The thread to run the device.
		std::unique_ptr<boost::thread> m_NetworkThread;

		int PACKET_SIZE;

	};

}
#endif