#include "DeviceDataManager.h"
#include "IADCDevice.h"
#include "DataConverter.h"
#include <algorithm>
#include <iostream>
#include <ctime>

namespace DeviceLib
{

	DeviceDataManager::DeviceDataManager(IADCDevice* pDevice)
		: m_ManagedDataDuration(2000000000)
		, m_pDevice(pDevice)
		, m_ManagedSampleRatePerSecond(0)
		, m_DataProcessedNanoSecond(0)
	{
	}

	DeviceDataManager::~DeviceDataManager()
	{
		for (size_t i = 0; i < m_ManagedDatas.size(); ++i)
		{
			delete m_ManagedDatas[i];
		}
		for (size_t i = 0; i < m_SnapshotDatas.size(); ++i)
		{
			delete m_SnapshotDatas[i];
		}
	}

	bool DeviceDataManager::Initialize()
	{
		SetActiveChannels();
		m_DataProcessedNanoSecond = 0;
		m_SpeedMonitor.Reset();
		for (size_t i = 0; i < m_ManagedDatas.size(); ++i)
		{
			delete m_ManagedDatas[i];
		}
		m_ManagedDatas.clear();
		return true;
	}

	long DeviceDataManager::GetManagedDataDuration()
	{
		return m_ManagedDataDuration;
	}

	void DeviceDataManager::SetManagedDataDuration(long duration)
	{
		m_ManagedDataDuration = duration;
	}

	long DeviceDataManager::GetManagedSampleRatePerSecond()
	{
		return m_ManagedSampleRatePerSecond;
	}

	void DeviceDataManager::SetManagedSampleRatePerSecond(long samplerate)
	{
		m_ManagedSampleRatePerSecond = samplerate;
	}

	void DeviceDataManager::AddData(DeviceData* data)
	{
		if (data)
		{
			DeviceData* input = data->GetActiveChannelData(m_pDevice->GetChannels());
			if (input == 0)
			{
				return;
			}
			DeviceData* sameRate = input->ConvertDataToDifferentSampleRate(m_ManagedSampleRatePerSecond);
			if (sameRate == 0)
			{
				sameRate = input;
			}
			else
			{
				delete input;
			}
			m_SpeedMonitor.UpdateData(sameRate->GetDataByteSize());
			m_ManagedDatas.push_back(sameRate);
			DataConverter converter(sameRate, m_pDevice, false);
			converter.ConvertADCtoMilivoltAndEUValue();
			long long dataDuration = 0;
			int borderIndex = static_cast<int>(m_ManagedDatas.size());
			for (int i = (int)m_ManagedDatas.size() - 1; i > 0; --i)
			{
				dataDuration += m_ManagedDatas[i]->GetDurationNanoSecond();
				if (dataDuration >= m_ManagedDataDuration)
				{
					break;
				}
				--borderIndex;
			}
			while (borderIndex > 1)
			{
				DeviceData* old = m_ManagedDatas.front();
				if (old != 0)
				{
					m_DataProcessedNanoSecond += old->GetDurationNanoSecond();
					for (size_t i = 0; i < m_UpdateDataListeners.size(); ++i)
					{
						if (m_UpdateDataListeners[i]) m_UpdateDataListeners[i]->UpdateExitData(old);
					}
					delete old;
				}
				m_ManagedDatas.pop_front();
				--borderIndex;
			}

			if (!m_ManagedDatas.empty())
			{
				for (size_t i = 0; i < m_UpdateDataListeners.size(); ++i)
				{
					m_UpdateDataListeners[i]->UpdateEntryData(m_ManagedDatas.back());
				}
			}
		}
	}

	void DeviceDataManager::AddUpdateDataListener(IDeviceDataUpdateListener* listener)
	{
		bool bExist = false;
		for (size_t i = 0; i < m_UpdateDataListeners.size(); ++i)
		{
			if (listener == m_UpdateDataListeners[i])
			{
				bExist = true;
			}
		}

		if (!bExist)
			m_UpdateDataListeners.push_back(listener);
	}

	void DeviceDataManager::SetActiveChannels()
	{
		m_ActiveChannels.clear();
		std::vector<ChannelInfo*>& channels = m_pDevice->GetChannels();
		for (size_t i = 0; i < channels.size(); ++i)
		{
			if (channels[i]->GetActive())
			{
				m_ActiveChannels.push_back(channels[i]);
			}
		}
	}

	void DeviceDataManager::RemoveUpdateDataListener(IDeviceDataUpdateListener* listener)
	{
		std::vector<IDeviceDataUpdateListener*> temp;
		for (size_t i = 0; i < m_UpdateDataListeners.size(); ++i)
		{
			if (m_UpdateDataListeners[i] != listener)
			{
				temp.push_back(m_UpdateDataListeners[i]);
			}
		}
		m_UpdateDataListeners.clear();
		for (size_t i = 0; i < temp.size(); ++i)
		{
			m_UpdateDataListeners.push_back(temp[i]);
		}
	}

	double DeviceDataManager::GetDataProcessedSecond()
	{
		return m_DataProcessedNanoSecond / 1000000000.0;
	}

	void DeviceDataManager::AddSnapshotData(DeviceData* data)
	{
		for (size_t i = 0; i < m_SnapshotDatas.size(); ++i)
		{
			delete m_SnapshotDatas[i];
		}
		m_SnapshotDatas.clear();
		DataConverter converter(data, m_pDevice, true);
		converter.ConvertADCtoMilivoltAndEUValue();
		m_SnapshotDatas.push_back(data);
	}

	DeviceData* DeviceDataManager::GetCurrentData()
	{
		DeviceData* res = 0;
		if (!m_ManagedDatas.empty()) res = m_ManagedDatas.back();
		return res;
	}
}