#ifndef DEVICEDATARECORDER_H
#define DEVICEDATARECORDER_H

#include "IDeviceDataManager.h"
#include "DataSpeedMonitor.h"
#include "ITrigger.h"
#include <fstream>
#include <string>
#include <memory>
#include <boost/thread.hpp>

#ifdef WIN32
#pragma warning(disable : 4100)
#endif
namespace LDSF
{
	class FileWriter;
}

namespace DeviceLib
{
	class DeviceDataManager;

	/**
	 * DeviceDataRecorder provides the logic to save to the raw data file (rawfs extension).
	 */
	class DeviceDataRecorder : public IDeviceDataUpdateListener, public ITriggerListener
	{
		enum UpdateStatus { STOP, RUN };
	public:

		/**
		 * DeviceDataRecorder is the full constructor.
		 * @param filename The name of file to be written.
		 * @param mgr The name of DeviceDataManager where data will come from.
		 * @param bSnapshot The flag for the snapshot. If the snapshot is on, the record will not be done.
		 * @return the recorder is ready to be used.
		 */
		DeviceDataRecorder(const char* filename, DeviceDataManager* mgr, bool bSnapshot);

		virtual ~DeviceDataRecorder();

		/// IsReady returns the state of the file.
		bool IsReady();
		
		/// Open the file stream.
		void Open();

		/// Close the file stream.
		void Close();

		/// The getter for the finish record flag.
		bool IsFinished() { return m_bFinished; }
		
		/// The setter to the record duration time in nanosecond.
		void SetRecordNanosecond(long long duration);

		/// IDeviceDataUpdateListener implementation
		virtual void UpdateEntryData(DeviceData* data);

		virtual void UpdateExitData(DeviceData* data);

		virtual bool StartListening();

		virtual void StopListening();

		/// ITriggerListener implementation.
		virtual void PerformTriggerEvent(long long triggertime);

		virtual void StopTriggerEvent(long long triggertime);

		/// The accessor for the current data speed.
		double GetCurrentSpeed() { return m_SpeedMonitor.GetCurrentSpeed(); }

		/// The accessor for the current data sample (peak) speed.
		double GetCurrentSampleSpeed() { return m_SpeedMonitor.GetDataSampleSpeed(); }

		/// The accessor for the current data record size.
		long long GetTotalDataBytes() { return m_RecordDataSize; }

		/// The accessor for the snapshot data.
		char * GetSnapshot() { return m_Snapshot; }

		/// Convert RAWFS to different file formats.
		void Convert();

		static void ConvertToLDSF(const std::string filename, const std::vector<ChannelInfo*>& channels);

	protected:

		bool m_bSnapshot;

		/// Snapshot storage.
		char * m_Snapshot;

		/// The flag for the running status for device data update listener.
		UpdateStatus m_Status;

		/// GetHeader sets the m_UDPData information.
		void GetHeader();

		/// File name to be saved.
		std::string m_FileName;

		/// File stream to manage saving operation.
		std::ofstream m_OutStream;

		/// Header information.
		char m_Header[512];

		/// The record starting time.
		time_t m_RecordTime;

		/// The record format.
		DATA_FORMAT m_Format;

		/// The number of channels to be saved.
		int m_NumberOfChannels;

		/// The input voltage range in milivolt.
		int m_InputMilliVolt;

		/// The recorded data size.
		long long m_RecordDataSize;

		/// The record duration in nanosecond.
		long long m_RecordDuration;

		/// The sample rate.
		double m_SampleRate;

		/// The time of trigger event in nanosecond.
		long long m_TriggerTimeNanoSecond;

		/// The speed monitor.
		DataSpeedMonitor m_SpeedMonitor;

		/// The flag for writing.
		bool m_Recording;

		/// The flag for the finishing record.
		bool m_bFinished;

		/// DeviceDataManager.
		DeviceDataManager* m_Manager;

		std::shared_ptr<LDSF::FileWriter> m_LDSFWriter;

		/// To access control for the file.
		boost::mutex m_mutex;

	};

}

#endif