#include "DataSpeedMonitor.h"
#include <ctime>
#include <algorithm>

namespace DeviceLib
{

	DataSpeedMonitor::DataSpeedMonitor()
		: m_CurrentSpeed(0)
		, m_CurrentTime(0)
		, m_TotalDataBytes(0)
		, m_StartTime(0)
		, m_CurrentSampleSpeed(0)
		, m_SampleTime(0)
		, m_SampleSize(0)
	{
	}

	DataSpeedMonitor::~DataSpeedMonitor()
	{
	}

	void DataSpeedMonitor::BeginSample()
	{
		m_SampleSize = 0;
	}

	void DataSpeedMonitor::EndSample()
	{
		m_CurrentSampleSpeed = m_SampleSize / std::max(0.0001, m_SampleTime - m_CurrentTime);
		m_TotalDataBytes += m_SampleSize;
		m_CurrentSpeed = m_TotalDataBytes / std::max(0.0001, (m_SampleTime - m_StartTime));
		m_CurrentTime = m_SampleTime;
		m_SampleTime = clock() / 1000.0;
	}

	void DataSpeedMonitor::UpdateData(long long datasize)
	{
		if (datasize == 0) return;
		m_SampleSize += datasize;
	}

	double DataSpeedMonitor::GetCurrentSpeed()
	{
		return m_CurrentSpeed;
	}

	double DataSpeedMonitor::GetDataSampleSpeed()
	{
		return m_CurrentSampleSpeed;
	}

	void DataSpeedMonitor::Reset()
	{
		m_CurrentSpeed = 0;
		m_CurrentSampleSpeed = 0;
		m_CurrentTime = clock() / 1000.0;
		m_StartTime = m_CurrentTime;
		m_TotalDataBytes = 0;
	}

	long long DataSpeedMonitor::GetTotalDataBytes()
	{
		return m_TotalDataBytes;
	}

	double DataSpeedMonitor::GetDuration()
	{
		return clock() / 1000.0 -m_StartTime;
	}
}