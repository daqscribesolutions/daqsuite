#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

namespace DeviceLib
{
	/// ILog is log interface to suppor custom log functions.
	struct ILog
	{
		virtual void Log(const std::string& mss) = 0;
	};
	
	/// ConsoleLog is the implementation of standard io log.
	struct ConsoleLog : ILog
	{
		virtual void Log(const std::string& mss)
		{
			std::cout << mss;
		}
	};

	/// FileLog is the implementation of standard file log.
	struct FileLog : ILog
	{
		std::ofstream fout;
		void SetFile(const std::string& filename)
		{
			fout.open(filename.c_str());
		}

		virtual void Log(const std::string& mss)
		{
			fout << mss;
			fout.flush();
		}
	};

	/**
	 * Logger provides the global logging functionality.
	 */
	class Logger
	{

	public:

		/// GetInstance is to support singleton.
		static Logger* GetInstance();

		/// Log outputs the message.
		void Log(const std::string& mss);

		/// Log outputs the message and value.
		void Log(const std::string& mss, int val);

		/// Log outputs the message and value.
		void Log(const std::string& mss, double val);

		/// Add custom log function.
		void AddLog(ILog* log);

		std::string GetLog();

		/// TimeStamp outputs the log time.
		static std::string TimeStamp();

	private:

		Logger();

		~Logger();

		static Logger* instance;

		std::vector<ILog*> loggers;

		std::string logs;

	};
}
#endif // LOGGER_H
