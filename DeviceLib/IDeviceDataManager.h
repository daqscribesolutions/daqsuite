#ifndef IDEVICEDATAMANAGER_H
#define IDEVICEDATAMANAGER_H

#include "DeviceData.h"

namespace DeviceLib
{

	/**
	 * IDeviceDataUpdateListener is the interface to get the event when the data is updated.
	 */
	class IDeviceDataUpdateListener
	{

	public:
		/// Virtual destructor.
		virtual ~IDeviceDataUpdateListener() {}

		/**
		 * UpdateEntryData performs the necessary action when the data is coming to the device data manager from the device.
		 * It can be able to trigger the action for the later update the update exit data.
		 * @param data The device data to be updated.
		 */ 
		virtual void UpdateEntryData(DeviceData* data) = 0;

		/**
		 * UpdateExitData performs the necessary action when the data is exiting from the device data manager.
		 * @param data The custom procedure is performed to the device data before it is discarded.
		 */ 
		virtual void UpdateExitData(DeviceData* data) = 0;

		/**
		 * StartListening enables to perform operation for the device data.
		 * @return True if the listener is ready to perform, otherwise return false.
		 */
		virtual bool StartListening() = 0;

		/// StopListening finishes the operation for the device data.
		virtual void StopListening() = 0;

	};

	/**
	 * IDeviceDataManager defines the interface for DeviceDataManager. DeviceDataManager is responsible to store device data of the given duration
	 * and to supply the information of current data in order to display and to save to the file.
	 */
	class IDeviceDataManager
	{

	public:
		/// Virtual destructor.
		virtual ~IDeviceDataManager() {}

		/**
  		 * Initialize initializes the device manager to able to receive data from the device.
		 * @return True if the device manager is ready, otherwise false.
	 	 */
		virtual bool Initialize() = 0;

		/// GetManagedDataDuration returns the duration in nanosecond of the managed data.
		virtual long GetManagedDataDuration() = 0;

		/**
  		 * SetManagedDataDuration sets the duration in nanosecond of the managed data.
		 * @param duration The duration in nanosecond to be set.
		 * @return The duration of the managed data is set.
	 	 */
		virtual void SetManagedDataDuration(long duration) = 0;

		/// GetManagedSampleRatePerSecond returns the sample rate per second of the managed data.
		virtual long GetManagedSampleRatePerSecond() = 0;

		/**
  		 * SetManagedSampleRatePerSecond adds the data to the queue to processed.
		 * @param samplerate The sample rate per second to be set.
		 * @return The The sample rate per second of the managed data is set.
	 	 */
		virtual void SetManagedSampleRatePerSecond(long samplerate) = 0;

		/**
  		 * AddData adds the data to the queue to processed.
		 * @param data The DeviceData pointer to be added.
		 * @return The DeviceData data is added.
	 	 */
		virtual void AddData(DeviceData* data) = 0;


		virtual DeviceData* GetCurrentData() = 0;
		/**
  		 * AddUpdateDataListener adds the event listener the for data update event.
		 * @param listener The IDeviceDataUpdateListener pointer to be added.
		 * @return The IDeviceDataUpdateListener is added.
	 	 */	
		virtual void AddUpdateDataListener(IDeviceDataUpdateListener* listener) = 0;

		/**
		 * RemoveUpdateDataListener removes an IDeviceDataUpdateListener from the system.
		 * @param listener The IDeviceDataUpdateListener pointer to be removed.
		 */		
		virtual void RemoveUpdateDataListener(IDeviceDataUpdateListener* listener) = 0;

		/**
		 * GetDataProcessedNanoSecond gets the time in nanosecond of the amount processed data.
		 */		
		virtual double GetDataProcessedSecond() = 0;

		/**
		 * GetCurrentSpeed gets the data speed (size per second).
		 */		
		virtual double GetCurrentSpeed() = 0;

		/**
		 * GetCurrentSpeed gets the data peak speed (size per second).
		 */		
		virtual double GetCurrentSampleSpeed() = 0;

		/**
		 * GetActiveChannels gets currently active channels.
		 */		
		virtual std::vector<ChannelInfo*>& GetActiveChannels() = 0;;

		/**
		 * BeginSample sets the mark for beginning of sample.
		 */		
		virtual void BeginSample() = 0;

		/**
		 * EndSample update the sample speed based on the mark for beginning of sample.
		 */		
		virtual void EndSample() = 0;

	};

}
#endif