#ifndef FILENAMEMANAGER_H
#define FILENAMEMANAGER_H

#include <string>
#include <vector>

namespace DeviceLib
{
	enum ITEM_TYPE { NONE_ITEM, PLAN_NAME, DEVICE_NAME, RECORD_NUMBER, RECORD_TIME, RECORD_DATE, USER_DEFINED };

	/**
	* NameItem stores the information of file name element.
	*/
	struct NameItem
	{
		NameItem() : itemType(NONE_ITEM) {}

		ITEM_TYPE itemType;

		std::string item;

		std::string GetItem();

	};

	/// FileNameManager constructs the file name by the items defined by user.
	class FileNameManager
	{

	public:

		static FileNameManager* GetInstance();

		/**
		* GetFileName gets the file name for the device.
		* @param index The device index.
		*/
		std::string GetFileName(int index);

		/**
		* GetNameItems gets the name items.
		* @return The name items.
		*/
		std::vector<NameItem>& GetNameItems() { return m_items; }

	private:

		std::vector<NameItem> m_items;

		FileNameManager();

		~FileNameManager();

		static FileNameManager* m_Instance;

	};

}

#endif