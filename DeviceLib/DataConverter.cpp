#include "DataConverter.h"
#include "IADCDevice.h"
#include <stdint.h> 
#include <iostream>
#include <algorithm>
//#include <omp.h>

#define _USE_MATH_DEFINES
#include <cmath>

namespace DeviceLib
{

	DataConverter::DataConverter(DeviceData* data, IADCDevice* pDevice, bool bSnapshot) 
		: m_DeviceData(data)
		, m_pDevice(pDevice)
		, m_bSnapshot(bSnapshot)
	{
	}

	void DataConverter::ConvertADCtoMilivoltAndEUValue()
	{
		if (m_DeviceData) 
		{
			std::vector<ChannelInfo*> channels = m_bSnapshot ? m_pDevice->GetSnapshotChannels() : m_pDevice->GetChannels();
			DATA_FORMAT format = m_DeviceData->GetFormat();
			int nchannels = (int)channels.size();
			if (!m_bSnapshot)
			{
				auto renderData = m_pDevice->GetRenderData();
				auto ncopy = std::min(renderData->GetDataByteSize(), m_DeviceData->GetDataByteSize());
				memcpy(renderData->GetADCCountData(), m_DeviceData->GetADCCountData(), (size_t)ncopy);
			}
			for (long ci = 0; ci < (long)nchannels; ++ci)
			{
				ChannelInfo* ch = channels[ci];
				ch->SetSnapShotData(m_DeviceData);

				int32_t dmin = INT_MAX;
				int32_t dmax = INT_MIN;

				double sensitivity = ch->GetSensitivity();
				double offset = ch->GetOffset();
				int32_t noffset = m_DeviceData->GetDataValue(offset);
				double EUsensitivity = ch->GetEUSensitivity();
				double EUoffset = ch->GetEUOffset();
				double ave = 0;
				long double rms = 0;
				long nSamples = (long)m_DeviceData->GetNumberSamples();
				int nstep = std::max(1, (int)nSamples / 10);
				if (format == TWO)
				{
					int16_t* ADCCountData = (int16_t*)m_DeviceData->GetADCCountData();
					int tci = (ci % 2 ? ci - 1 : ci + 1) % nchannels;
					for (long long i = 0; i < nSamples; i += nstep)
					{
						int16_t* CountData = &ADCCountData[i * nchannels + tci];
						int32_t v = static_cast<int32_t>((int16_t)(*CountData * sensitivity) + (int16_t)noffset);
						ave += v;
						rms += v * v;
						dmin = std::min(dmin, v);
						dmax = std::max(dmax, v);
					}
					if (nSamples != 0)
					{
						ave /= nSamples;
						rms /= nSamples;
					}
				}
				if (format == FOUR)
				{
					int32_t* ADCCountData = (int32_t*)m_DeviceData->GetADCCountData();

					for (long long i = 0; i < nSamples; i += nstep)
					{
						int32_t* CountData = &ADCCountData[i * nchannels + ci];
						int32_t v = *CountData >> 8;
						v = (int32_t)(v * sensitivity) + noffset;
						ave += v;
						rms += (double)v * v;
						dmin = std::min(dmin, v);
						dmax = std::max(dmax, v);
					}

					if (nSamples != 0)
					{
						ave /= nSamples;
						rms /= nSamples;
					}
				}

				ch->MilivoltDataMin = m_DeviceData->GetConvertedDataValue(dmin);
				ch->MilivoltDataMax = m_DeviceData->GetConvertedDataValue(dmax);
				ch->EUDataMin = ch->MilivoltDataMin * EUsensitivity + EUoffset;
				ch->EUDataMax = ch->MilivoltDataMax * EUsensitivity + EUoffset;
				ch->Average = m_DeviceData->GetConvertedDataValue((int32_t)ave);
				rms = sqrt(rms);
				ch->RMS = m_DeviceData->GetConvertedDataValue(int32_t(rms));
				ch->EUAverage = ch->Average * EUsensitivity + EUoffset;
				ch->EURMS = ch->RMS * EUsensitivity + EUoffset;
			}
		}
	}

	void DataConverter::ConvertADCtoMilivoltAndEUValue(std::vector<std::vector<double> >& EUValues, const std::vector<ChannelInfo*>& channels)
	{
		if (m_DeviceData)
		{
			EUValues.clear();
			EUValues.resize((int)channels.size());
			if (channels.size() == 0) return;
			DATA_FORMAT format = m_DeviceData->GetFormat();
			double MaxADC = MAX_ADCOUNT_23BIT;
			if (format == TWO)
			{
				MaxADC = MAX_ADCOUNT_15BIT;
			}
			double rate = m_DeviceData->GetInputMilliVolt() / MaxADC;
			long long datasize = (long long)(m_DeviceData->GetDataSize() / channels.size());
			for (long long i = 0; i < (long long)EUValues.size(); ++i)
			{
				EUValues[(size_t)i].resize((size_t)datasize);
			}
			if (format == TWO)
			{
				int nchannels = (int)channels.size();
				int16_t* ADCCountData = (int16_t*)m_DeviceData->GetADCCountData();
				for (long long i = 0; i < datasize; ++i)
				{
					int16_t* CountData = &ADCCountData[i * EUValues.size()];
					for (long long ci = 0; ci < (long long)EUValues.size(); ++ci)
					{
						int tci = (ci % 2 ? ci - 1 : ci + 1) % nchannels;
						int16_t* CountData = &ADCCountData[i * nchannels + tci];
						EUValues[(size_t)ci][(size_t)i] = CountData[(size_t)tci] * rate;
					}
				}
			}
			if (format == FOUR)
			{
				int32_t* ADCCountData = (int32_t*)m_DeviceData->GetADCCountData();
				for (long long i = 0; i < datasize; ++i)
				{
					int32_t* CountData = &ADCCountData[i * EUValues.size()];
					for (long long ci = 0; ci < (long long)EUValues.size(); ++ci)
					{
						EUValues[(size_t)ci][(size_t)i] = (CountData[(size_t)ci] >> 8) * rate;
					}
				}
			}
			for (long long ci = 0; ci < (long long)channels.size(); ++ci)
			{
				ChannelInfo* ch = channels[(size_t)ci];
				double sensitivity = ch->GetSensitivity();
				double offset = ch->GetOffset();
				double EUsensitivity = ch->GetEUSensitivity();
				double EUoffset = ch->GetEUOffset();
				auto& val = EUValues[(size_t)ci];
				for (long long i = 0; i < datasize; ++i)
				{
					double v = val[(size_t)i];
					v = v * sensitivity + offset;
					val[(size_t)i] = v * EUsensitivity + EUoffset;
				}
			}
		}
	}

	void DataConverter::SetSineSignal()
	{
		if (m_DeviceData)
		{
			char* ADCCountData = m_DeviceData->GetADCCountData();
			std::vector<ChannelInfo*>& channels = m_pDevice->GetChannels();
			DATA_FORMAT format = m_DeviceData->GetFormat();
			double MaxADC = MAX_ADCOUNT_23BIT;
			if (format == TWO)
			{
				MaxADC = MAX_ADCOUNT_15BIT;
			}
			double rate = m_DeviceData->GetInputMilliVolt() * 2.0 / MaxADC;
			long long datasize = m_DeviceData->GetDataSize() / channels.size();
			double deltaRatian = 6.2831853 / (m_DeviceData->GetSampleRatePerSecond() * m_DeviceData->GetDurationNanoSecond() / 1000000000.0);
			if (format == TWO)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						int idx = (int)((i * channels.size() + ci) * format);
						double val = sin(deltaRatian*i) * 250 * (ci % 4 + 1);
						*(int16_t*)&ADCCountData[idx] = static_cast<int16_t>(val / rate);
					}
				}
			}
			if (format == FOUR)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						unsigned int idx = (unsigned int)((i * channels.size() + ci) * format);
						double val = sin(deltaRatian*i) * 250 * (ci % 4 + 1);
						*(int32_t*)&ADCCountData[idx] = static_cast<int32_t>(val / rate) << 8;
					}
				}
			}
		}
	}

	void DataConverter::Multiply(double val)
	{
		if (m_DeviceData)
		{
			char* ADCCountData = m_DeviceData->GetADCCountData();
			std::vector<ChannelInfo*>& channels = m_pDevice->GetChannels();
			DATA_FORMAT format = m_DeviceData->GetFormat();
			long long datasize = m_DeviceData->GetDataSize() / channels.size();
			if (format == TWO)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						unsigned int idx = (unsigned int)((i * channels.size() + ci) * format);
						double v = (double) *(int16_t*)&ADCCountData[idx];
						*(int16_t*)&ADCCountData[idx] = (int16_t)(v * val);
					}
				}
			}

			if (format == FOUR)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						unsigned int idx = (unsigned int)((i * channels.size() + ci) * format);
						double v = (double)*(int32_t*)&ADCCountData[idx];
						*(int32_t*)&ADCCountData[idx] = (int32_t)(v * val);
					}
				}
			}
		}
	}

	void DataConverter::PrintMilivolt()
	{
		if (m_DeviceData)
		{
			std::cout.setf(std::ios::fixed);
			std::vector<ChannelInfo*>& channels = m_pDevice->GetChannels();
			long long datasize = m_DeviceData->GetDataSize() / channels.size();
			for (long long i = 0; i < datasize; ++i)
			{
				for (int ci = 0; ci < (int)channels.size(); ++ci)
				{
					std::cout << m_DeviceData->GetConvertedDataValue(m_DeviceData->GetValue((int)i, ci)) << " ";
				}
				std::cout << "\n";
			}
		}
	}

	void DataConverter::PrintADCCount()
	{
		if (m_DeviceData)
		{
			std::cout.setf(std::ios::fixed);
			std::vector<ChannelInfo*>& channels = m_bSnapshot ? m_pDevice->GetSnapshotChannels() : m_pDevice->GetChannels();
			DATA_FORMAT format = m_DeviceData->GetFormat();
			char* ADCCountData = m_DeviceData->GetADCCountData();
			long long datasize = m_DeviceData->GetDataSize() / channels.size();
			if (format == TWO)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						unsigned int idx = (unsigned int)((i * channels.size() + ci) * format);
						std::cout << (*(int16_t*)&ADCCountData[idx]) << " ";
					}
					std::cout << "\n";
				}
			}
			if (format == FOUR)
			{
				for (long long i = 0; i < datasize; ++i)
				{
					for (size_t ci = 0; ci < channels.size(); ++ci)
					{
						unsigned int idx = (unsigned int)((i * channels.size() + ci) * format);
						std::cout << (*(int32_t*)&ADCCountData[idx]) << " ";
					}
					std::cout << "\n";
				}
			}
		}
	}
}
