#ifndef DEVICEDATAREADER_H
#define DEVICEDATAREADER_H

#include "DeviceData.h"
#include <vector>
#include <fstream>

namespace DeviceLib
{
	/**
	 * DeviceDataReader provides the access to the raw data file (rawfs extension).
	 */
	class DeviceDataReader
	{

	public:

		DeviceDataReader(const std::string& filename);

		~DeviceDataReader();

		DeviceData* GetDeviceData() { return m_data; }

		void ReadNext();

		void Reset();

		bool IsOpen() { return m_fin.good();  }

		bool HasMoreData() { return m_currentByte < m_nbytes; }

		long long GetDataByteSize() { return m_nbytes; }

		/**
		 * ReadDatas opens the raw file and return the device datas.
		 * @param filename The raw file name.
		 * @return The device data.
		 */
		static std::vector<DeviceData*> ReadDatas(const char* filename);

		/**
		 * ReadDatas opens the raw file and return the device datas.
		 * @param filename The raw file name.
		 * @return The data duration of the file from the header.
		 */
		static double GetDataDuration(const char* filename);

	private:
		
		long long m_currentByte;

		DeviceData *m_data;

		std::ifstream m_fin;

		long long m_nbytes;

		long long m_nframes;

		long long m_dataduration;

		char header[512];

	};

}
#endif