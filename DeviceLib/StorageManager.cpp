#include "StorageManager.h"
#include <iostream>
#include <boost/filesystem.hpp>
#ifdef WIN32
#include <windows.h>
#endif
namespace DeviceLib
{

	StorageManager::StorageManager()
	{
	}

	StorageManager::~StorageManager()
	{
	}

	void StorageManager::GetSpaceInformation(const std::string& path, double& capacity, double& available, double& free)
	{
		try
		{
			boost::filesystem::path p(path);
			boost::filesystem::space_info space = boost::filesystem::space(p);
			capacity = space.capacity / 1000000.0;
			available = space.available / 1000000.0;
			free = space.available / 1000000.0;
		}
		catch (...)
		{
		}
	}

	void StorageManager::CreatePath(const std::string& path)
	{
		boost::filesystem::path dir(path);
		if (!boost::filesystem::exists(dir))
		{
			boost::filesystem::path pa = dir.parent_path();
			if (!boost::filesystem::exists(pa))
			{
				CreatePath(pa.string());
			}
			boost::filesystem::create_directory(dir);
		}
	}

	std::vector<DiskInfo> StorageManager::HardDiskInformation()
	{
#ifdef WIN32
		DWORD mydrives = 100;
		wchar_t lpBuffer[100];

		DWORD test = GetLogicalDriveStrings(mydrives, lpBuffer) / 4;
		std::vector<DiskInfo> infos;
		for (int i = 0; i < (int)test; i++)
		{
			DiskInfo info;
			info.info = (char)lpBuffer[i * 4];
			info.info += (char)lpBuffer[i * 4 + 1];
			info.info += (char)lpBuffer[i * 4 + 2];
			std::wstring wsTmp(info.info.begin(), info.info.end());
			UINT type = GetDriveType(wsTmp.c_str());
			if (type == DRIVE_FIXED)
			{
				GetSpaceInformation(info.info, info.capacity, info.available, info.free);
				if (info.capacity > 0)
				{
					infos.push_back(info);
				}
			}
		}
		return infos;
#else
		static struct hd_driveid hd;
		int fd;

		if ((fd = open("/dev/hda", O_RDONLY | O_NONBLOCK)) < 0) 
		{
			printf("ERROR opening /dev/hda\n");
			exit(1);
		}

		if (!ioctl(fd, HDIO_GET_IDENTITY, &hd))
		{
			printf("%.20s\n", hd.serial_no);
		}
		else if (errno == -ENOMSG)
		{
			printf("No serial number available\n");
		}
		else 
		{
			perror("ERROR: HDIO_GET_IDENTITY");
			exit(1);
		}
#endif
	}
}
