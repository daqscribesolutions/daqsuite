#ifndef DEVICEDATA_H
#define DEVICEDATA_H

#include "ChannelInfo.h"
#include <vector>
#include <stdint.h> 

namespace DeviceLib
{
	const double MAX_ADCOUNT_23BIT = 8388607;		// Power(2, 23) - 1
	const double MAX_ADCOUNT_15BIT = 32767;			// Power(2, 15) - 1

	/**
	 * DATA_FORMAT is the device data format according to the size of the device output per channel.
	 */
	enum DATA_FORMAT { TWO = 2, FOUR = 4 };

	/**
	 * DeviceData is the storage for the device data. 
	 */
	class DeviceData
	{

	public:

		/**
		 * DeviceData is the full constructor.
		 * @param startNanoSecond The start time in nanosecond.
		 * @param durationNanoSecond The duration time in nanosecond.
		 * @param sampleratepersecond The sample rate per second.
		 * @param numberofchannels The number of channels.
		 * @param inputmilivoltage The initial input voltage of the channel.
		 * @param format The format of the channel.
		 * @return The device data is set and ready to use.
		 */
		DeviceData(long long startNanoSecond = 0, long long durationNanoSecond = 10000, long sampleratepersecond = 1000, int numberofchannels = 32, int inputmilivoltage = 1000, DATA_FORMAT format = FOUR);

		~DeviceData();

		/// InitData sets the memory to be used to store data.
		bool InitData();

		/// ClearData clears the memory used to store data.
		void ClearData();

		/// The getter for the start time.
		long long GetStartNanoSecond();

		/// The setter for the start time.
		void SetStartNanoSecond(long long value);

		/// The getter for the sample rate of device.
		long GetSampleRatePerSecond();

		/// The setter for the sample rate of device.
		void SetSampleRatePerSecond(long value);

		/// The getter for the duration of the data.
		long long GetDurationNanoSecond();

		/// The setter for the duration of the data.
		void SetDurationNanoSecond(long long value);

		/// The getter for the format of the data.
		DATA_FORMAT GetFormat();

		/// The setter for the format of the data.
		void SetFormat(DATA_FORMAT value);

		/// The getter for the milivoltage of the device.
		int GetInputMilliVolt();

		/// The setter for the milivoltage  of the device.
		void SetInputMilliVolt(int value);

		/// The getter for the data size. The byte size of the data is GetFormat()*GetDataSize().
		long long GetDataSize();
		
		/// The getter for the ADC data of the device.
		char* GetADCCountData() { return m_ADCCountData; }

		/// The getter for the data size in byte.
		long long GetDataByteSize() { return m_DataSize * m_Format; }

		long long GetDataFrames() { return m_DataSize / m_NumberOfChannels; }

		/**
		 * GetADCCountDataSizeOfNanoSecond gives the data size for the given time.
		 * @param nanoSecond The duration to get the data size.
		 * @return The data size of the given nanosecond.
		 */ 
		long long GetADCCountDataSizeOfNanoSecond(long long nanoSecond);

		/// The getter for the number of channels  of the device.
		int GetNumberOfChannels() { return m_NumberOfChannels; }

		/**
		 * DeviceData is the full constructor.
		 * @param pData The device data pointer to be checked for comparability.
		 * @return True if the device data is compariable with current deivce data, otherwise return false.
		 */
		bool IsCompariable(DeviceData* pData);

		/**
		 * ConvertSampleRate converts the device data into different sample rate.
		 * @param sampleRate The different sample rate. The sample rate must be less than the currnet sample rate and greater than 0.
		 * @return DeviceData pointer with the different sample rate, otherwise return false.
		 */
		DeviceData* ConvertDataToDifferentSampleRate(int sampleRate);

		/**
		 * GetActiveChannelData converts the device data into different sample rate.
		 * @param channels The channel informations for checking active status.
		 * @return DeviceData pointer with the different sample rate, otherwise return false.
		 */
		DeviceData* GetActiveChannelData(const std::vector<ChannelInfo*>& channels);

		/**
		 * GetCloseADCCountDataSizeAndNanoSecond gives the data size for the given time.
		 * @param datasize The approximated duration to get the data size. It will be set to the proper datasize for the result duration.
		 * @param sampleRatePerSecond The sample rate per second for the computation data size.
		 * @param numChannels The number of channels for the computation data size.
		 * @param format The device data format for the computation data size.
		 * @return The approximated duration in nanosecond.
		 */ 
		static long long GetCloseADCCountDataSizeAndNanoSecond(long long& datasize, long long sampleRatePerSecond, int numChannels, int format);

		/// Data value accessor.
		int32_t GetValue(int idx, int chidx) const
		{
			if (m_Format == TWO)
			{
				int tidx = chidx;
				if (chidx % 2 == 0) tidx += 1;
				else tidx -= 1;
				return (int32_t)*(int16_t*)&m_ADCCountData[(tidx + idx * m_NumberOfChannels) * m_Format];
			}
			else return (*(int32_t*)&m_ADCCountData[(chidx + idx * m_NumberOfChannels) * m_Format]) >> 8;
		}

		void SetValue(int idx, int chidx, int32_t val)
		{
			if (m_Format == TWO) *(int16_t*)&m_ADCCountData[(chidx + idx * m_NumberOfChannels) * m_Format] = static_cast<int16_t>(val);
			else *(int32_t*)&m_ADCCountData[(chidx + idx * m_NumberOfChannels) * m_Format] = static_cast<int32_t>(val << 8);
		}

		/// The sample number accessor.
		long long GetNumberSamples() { return m_DataSize / m_NumberOfChannels; }

		int GetDataValue(double d) 
		{ 
			return static_cast<int>((m_Format == TWO ? MAX_ADCOUNT_15BIT : MAX_ADCOUNT_23BIT) * d / m_InputMilliVolt); 
		}

		double GetConvertedDataValue(int32_t d)
		{
			return (m_Format == TWO ? 
				(double)d * m_InputMilliVolt / MAX_ADCOUNT_15BIT
				: (double)d * m_InputMilliVolt / MAX_ADCOUNT_23BIT); 
		}

	private:

		/// The time stamp for the data.
		long long m_StartNanoSecond;

		/// The duration of the data to be stored.
		long long m_DurationNanoSecond;

		/// The data size in format size for the duration.
		long long m_DataSize;

		/// The data memory.
		char * m_ADCCountData;

		/// The sample rate per second.
		long m_SampleRatePerSecond;

		/// The input voltage for ADC.
		int m_InputMilliVolt;

		/// The device number of channels. 
		int m_NumberOfChannels;

		/// The format of the data to be stored.
		DATA_FORMAT m_Format;

	};

}

#endif