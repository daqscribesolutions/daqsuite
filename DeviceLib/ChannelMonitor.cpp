#include "ChannelMonitor.h"
#include "ChannelInfo.h"
#include <iostream>
#include "SystemManager.h"
#include "TriggerManager.h"

namespace DeviceLib
{
	RecordSizeMonitor::RecordSizeMonitor()
		: m_TriggerNanoSecond(0)
		, m_CurrentDataSize(0)
		, m_bStop(false)
	{}

	RecordSizeMonitor::~RecordSizeMonitor()
	{}

	bool RecordSizeMonitor::CheckTriggerCondition()
	{
		return  TriggerManager::GetInstance()->RecordFinished();
	}

	long long RecordSizeMonitor::GetTriggerNanoSecond()
	{
		return m_TriggerNanoSecond;
	}

	ChannelMonitor::ChannelMonitor(ChannelInfo* channel, int chidx, long long triggerTime) 
		: m_pChannel(channel)
		, m_TriggerNanoSecond(triggerTime)
		, m_CurrentDataSize(0)
		, m_ChIdx(chidx)
	{}

	ChannelMonitor::~ChannelMonitor()
	{}

	void ChannelMonitor::SetChannel(ChannelInfo* channel, int idx)
	{
		m_pChannel = channel;
	}

	bool ChannelMonitor::CheckTriggerCondition()
	{
		return TriggerManager::GetInstance()->GetScheduleTrigger();
	}

	long long ChannelMonitor::GetTriggerNanoSecond()
	{
		long long res = m_pChannel ? m_pChannel->GetStartNanoSecond() : m_TriggerNanoSecond;
		return res;
	}

	LimitChannelMonitor::LimitChannelMonitor(ChannelInfo* channel, int chidx, double dlimit, 
		MonitorLimitType limitType, MonitorDataType dataType)
		: ChannelMonitor(channel, chidx)
		, m_Limit(dlimit)
		, m_LimitType(limitType)
		, m_DataType(dataType)
	{}

	LimitChannelMonitor::~LimitChannelMonitor()
	{}

	bool LimitChannelMonitor::CheckTriggerCondition()
	{
		if (m_pChannel)
		{
			DeviceData* data = m_pChannel->GetData();
			int32_t limit = data->GetDataValue(m_Limit);
			if (m_LimitType == UPPER)
			{
				for (long long i = m_CurrentDataSize; i < m_pChannel->GetDataSize(); ++i)
				{
					if (data->GetValue((int)i, m_ChIdx) > limit)
					{
						m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond() 
							+ i * m_pChannel->GetDurationNanoSecond() / m_pChannel->GetDataSize();
						m_CurrentDataSize = i;
						return true;
					}
				}
			}
			else
			{
				for (long long i = m_CurrentDataSize; i < m_pChannel->GetDataSize(); ++i)
				{
					if (data->GetValue((int)i, m_ChIdx) < limit)
					{
						m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond() 
							+ i * m_pChannel->GetDurationNanoSecond() / m_pChannel->GetDataSize();
						m_CurrentDataSize = i;
						return true;
					}
				}
			}
		}
		return false;
	}

	long long LimitChannelMonitor::GetTriggerNanoSecond()
	{
		return m_TriggerNanoSecond;
	}

	EdgeChannelMonitor::EdgeChannelMonitor(ChannelInfo* channel, int chidx, double dlimit, 
		MonitorLimitType limitType, MonitorDataType dataType)
		: LimitChannelMonitor(channel, chidx, dlimit, limitType, dataType)
		, m_first(true)
		, m_lastValue(0)
	{}

	EdgeChannelMonitor::~EdgeChannelMonitor()
	{}

	bool EdgeChannelMonitor::CheckTriggerCondition()
	{
		if (m_pChannel)
		{
			DeviceData* data = m_pChannel->GetData();
			int32_t limit = data->GetDataValue(m_Limit);
			if (m_LimitType == UPPER)
			{
				if (!m_first)
				{
					if (data->GetValue(0, m_ChIdx) >= limit && m_lastValue < limit)
					{
						m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond();
						m_CurrentDataSize = 0;
						return true;
					}
				}
				for (long long i = m_CurrentDataSize + 1; i < m_pChannel->GetDataSize(); ++i)
				{
					if (data->GetValue((int)i, m_ChIdx) >= limit && data->GetValue((int)i - 1, m_ChIdx) < limit)
					{
						m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond() 
							+ i * m_pChannel->GetDurationNanoSecond() / m_pChannel->GetDataSize();
						m_CurrentDataSize = i;
						return true;
					}
				}
			}
			else
			{
				if (data->GetValue(0, m_ChIdx) <= limit && m_lastValue > limit)
				{
					m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond();
					m_CurrentDataSize = 0;
					return true;
				}
				for (long long i = m_CurrentDataSize + 1; i < m_pChannel->GetDataSize(); ++i)
				{
					if (data->GetValue((int)i, m_ChIdx) <= limit && data->GetValue((int)i - 1, m_ChIdx) > limit)
					{
						m_TriggerNanoSecond = m_pChannel->GetStartNanoSecond() 
							+ i * m_pChannel->GetDurationNanoSecond() / m_pChannel->GetDataSize();
						m_CurrentDataSize = i;
						return true;
					}
				}
			}
			m_lastValue = data->GetValue((int)m_pChannel->GetDataSize() - 1, m_ChIdx);
			m_first = false;
		}
		return false;
	}

	long long EdgeChannelMonitor::GetTriggerNanoSecond()
	{
		return m_TriggerNanoSecond;
	}
}