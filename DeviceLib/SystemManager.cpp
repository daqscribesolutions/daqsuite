#include "DeviceLib.h"
#include "SystemManager.h"
#include "VirtualDevice.h"
#include "DeviceDataRecorder.h"
#include "DeviceRecordSetting.h"
#include "DataConverter.h"
#include "ServerDevice.h"
#include "TriggerManager.h"
#include "FileConvertManager.h"
#include <ctime>
#include <iostream>
#include <algorithm>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include "ClientDevice.h"
#include "CalibrationIO.h"
#ifdef WIN32
#pragma warning(disable : 4996)
#endif
namespace DeviceLib
{
	SystemManager* SystemManager::m_Instance = 0;

	SystemManager* SystemManager::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new SystemManager();
		}
		return m_Instance;
	}

	SystemManager::SystemManager()
		: m_Status(STOPPED)
		, m_NetworkBufferSize(512)
		, m_LoadGSBoard(true)
		, m_LoadBlackBurnBoard(true)
		, m_LoadICS645BBoard(true)
		, m_PostRecordStatus(STOPPED)
		, m_RecordType(RAWFS)
		, m_CurrentRecord(0)
	{
	}

	SystemManager::~SystemManager()
	{
	}

	long long SystemManager::GetSystemRunningTime() 
	{ 
		long long t = 0;
		if (m_Devices.size() > 0)
		{
			auto mgr = m_Devices.front()->GetDeviceDataManager();
			if (mgr)
			{
				auto data = mgr->GetCurrentData();
				if (data)
				{
					t = data->GetStartNanoSecond();
				}
			}
		}
		return t;
	}

	bool SystemManager::StartMonitor()
	{
		mutex.lock();

		Logger::GetInstance()->Log("Begin:StartMonitor");
		bool bStatus = true;
		m_CurrentRecord = 0;
		TriggerManager::GetInstance()->nSnapshot = 0;
		SetProgress(1);
		if (m_Status == STOPPED)
		{
			for (int i = 0; i < (int)m_Devices.size(); ++i)
			{
				if (!m_Devices[i]->Initialize())
				{
					bStatus = false;
				}
			}
			if (bStatus)
			{
				for (int i = 0; i < (int)m_Clients.size(); ++i)
				{
					m_Clients[i]->StartListening();
				}

				for (int i = 0; i < (int)m_Devices.size(); ++i)
				{
					if (!m_Devices[i]->Start())
					{
						bStatus = false;
					}
				}
			}
		}
		SetProgress(90);

		TriggerManager::GetInstance()->TriggerBegin();
		m_Status = (bStatus ? MONITORING : STOPPED);
		Logger::GetInstance()->Log("End:StartMonitor");
		mutex.unlock();
		SetProgress(100);

		return bStatus;
	}

	void SystemManager::ArmTrigger()
	{
		if (m_Status == MONITORING)
		{
			TriggerManager::GetInstance()->ArmTrigger();
		}
	}

	bool SystemManager::StartRecord()
	{
		mutex.lock();
		bool bStatus = true;
		if (m_Status == MONITORING)
		{
			TriggerManager::GetInstance()->SetCommandTrigger();
		}
		else
		{
			bStatus = false;
		}
		mutex.unlock();
		return bStatus;
	}

	void SystemManager::SetRecording(bool bRecording)
	{
		mutex.lock();
		if (m_Status == MONITORING && bRecording)
		{
			m_Status = RECORDING;
		}
		else if (m_Status == RECORDING && !bRecording)
		{
			m_Status = MONITORING;
			Sleep(1000);
			TriggerManager::GetInstance()->ResetTrigger();
			if (!TriggerManager::GetInstance()->GetSnapshot())
			{
				++m_CurrentRecord;
				DeviceRecordSetting::GetInstance()->IncrementRecordNumber();
				auto triggerType = TriggerManager::GetInstance()->GetTriggerControlType();
				if ((triggerType == EDGE || triggerType == LIMIT) 
					&& (m_CurrentRecord < DeviceRecordSetting::GetInstance()->GetRecordTimes() 
					|| DeviceRecordSetting::GetInstance()->GetRecordTimes() == 0))
				{
					while (!TriggerManager::GetInstance()->RecordFinished())
					{
					}
					TriggerManager::GetInstance()->TriggerBegin();
					ArmTrigger();
				}
				else if (m_PostRecordStatus == STOPPED)
				{
					while (!TriggerManager::GetInstance()->RecordFinished())
					{
					}
					m_Status = READYTOSTOP;
				}
				else if (triggerType == EDGE || triggerType == LIMIT)
				{
					m_CurrentRecord = 0;
				}
			}
			else
			{
				TriggerManager::GetInstance()->TriggerBegin();
				if (DeviceRecordSetting::GetInstance()->GetRecordTimes() == 0 ||
					DeviceRecordSetting::GetInstance()->GetRecordTimes() > TriggerManager::GetInstance()->nSnapshot + 1)
				{
					ArmTrigger();
				}
				TriggerManager::GetInstance()->nSnapshot++;
			}
		}
		mutex.unlock();
	}

	void SystemManager::StopMonitor()
	{
		mutex.lock();
		Logger::GetInstance()->Log("Begin:StopMonitor");
		for (int i = 0; i < (int)m_DeviceDataListeners.size(); ++i)
		{
			m_DeviceDataListeners[i]->StopListening();
		}

		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			m_Devices[i]->Stop();
		}

		while (!FileConvertManager::GetInstance()->IsEmpty())
		{
			Sleep(100);
		}
		Logger::GetInstance()->Log("End:StopMonitor");
		m_Status = STOPPED;
		mutex.unlock();
	}

	void SystemManager::StopRecord()
	{
		SetRecording(false);
	}

	const char* SystemManager::GetErrorMessage()
	{
		return m_ErrorMessage.c_str();
	}

	void SystemManager::SetErrorMessage(const char* message)
	{
		m_ErrorMessage = message;
	}

	void SystemManager::AddClientDevice(const std::string& host, const std::shared_ptr<IDevice>& pDevice, int port)
	{
		if (pDevice && m_Status == STOPPED)
		{
			if (pDevice->GetDeviceDataManager())
			{
				ClientDevice* client = new ClientDevice(pDevice, host, port);
				pDevice->GetDeviceDataManager()->AddUpdateDataListener(client);
				m_DeviceDataListeners.push_back(client);
				m_Clients.push_back(std::shared_ptr<ClientDevice>(client));
			}
		}
	}

	void SystemManager::RemoveClientDevice(std::shared_ptr<ClientDevice>& pdevice)
	{
		if (m_Status == STOPPED)
		{
			RemoveDataUpdateListener(pdevice.get());
			std::vector<std::shared_ptr<ClientDevice>> devices;
			for (int i = 0; i < (int)m_Clients.size(); ++i)
			{
				if (pdevice != m_Clients[i])
				{
					devices.push_back(m_Clients[i]);
				}
			}
			if (m_Clients.size() != devices.size())
			{
				m_Clients = devices;			
				pdevice.reset();
			}
		}
	}

	std::vector<std::shared_ptr<ClientDevice>> SystemManager::GetClientDevices()
	{
		return m_Clients;
	}

	void SystemManager::AddServerDevice(int port)
	{
		if (m_Status == STOPPED)
		{
			ServerDevice* server = new ServerDevice(port);
			std::shared_ptr<IDeviceDataManager> dataManager(new DeviceDataManager((IADCDevice*)server));
			server->SetDeviceDataManager(dataManager);
			server->SetID((int)m_Devices.size());
			m_DeviceDataManagers.push_back(dataManager);
			m_Devices.push_back(std::shared_ptr<IDevice>(server));
		}
	}

	void SystemManager::AddVirtualDevice()
	{
		//mutex.lock();

		if (m_Status == STOPPED)
		{
			VirtualDevice* pDevice = new VirtualDevice(16, TWO);
			std::shared_ptr<IDeviceDataManager> dataManager(new DeviceDataManager((IADCDevice*)pDevice));
			pDevice->SetDeviceDataManager(dataManager);
			pDevice->SetID((int)m_Devices.size());
			m_DeviceDataManagers.push_back(std::shared_ptr<IDeviceDataManager>(dataManager));
			if (m_Devices.size() > 0)
			{
				auto pre = m_Devices.front();
				IADCDevice* p = dynamic_cast<IADCDevice*>(pre.get());
				if (p)
				{
					auto channels = p->GetChannels();
					if (channels.size() > 0)
					{
						int s = (int)channels.front()->UserData.size();
						auto& vchannels = pDevice->GetChannels();
						for (int i = 0; i < (int)vchannels.size(); ++i)
						{
							vchannels[i]->UserData.resize(s);
						}
					}
				}
			}
			m_Devices.push_back(std::shared_ptr<IDevice>(pDevice));
		}
		//mutex.unlock();
	}
	
	void SystemManager::AddDevices(IDeviceFactory& factory)
	{
		//mutex.lock();
		std::vector<IDevice*> devices;
		factory.GetDevices(devices);
		for (int i = 0; i < (int)devices.size(); ++i)
		{
			IDevice* pDevice = devices[i];
			if (pDevice)
			{
				std::shared_ptr<IDeviceDataManager> dataManager(new DeviceDataManager((IADCDevice*)pDevice));
				pDevice->SetDeviceDataManager(dataManager);
				pDevice->SetID((int)m_Devices.size());
				m_DeviceDataManagers.push_back(std::shared_ptr<IDeviceDataManager>(dataManager));
				m_Devices.push_back(std::shared_ptr<IDevice>(pDevice));
			}
		}
		//mutex.unlock();
	}

	void SystemManager::RemoveDevice(std::shared_ptr<IDevice>& pDevice)
	{
		//mutex.lock();

		if (m_Status == STOPPED)
		{
			std::vector<std::shared_ptr<IDevice>> temp;
			for (size_t i = 0; i < m_Devices.size(); ++i)
			{
				if (m_Devices[i] != pDevice)
				{
					temp.push_back(m_Devices[i]);
				}
			}
			if (m_Devices.size() != temp.size())
			{
				RemoveDeviceDataManager(pDevice->GetDeviceDataManager());
				pDevice.reset();
				m_Devices = temp;
			}
		}
		//mutex.unlock();
	}

	void SystemManager::RemoveDeviceDataManager(std::shared_ptr<IDeviceDataManager>& pDeviceDataManager)
	{
		//mutex.lock();

		if (m_Status == STOPPED)
		{
			std::vector<std::shared_ptr<IDeviceDataManager>> temp;
			for (size_t i = 0; i < m_DeviceDataManagers.size(); ++i)
			{
				if (m_DeviceDataManagers[i] != pDeviceDataManager)
				{
					temp.push_back(m_DeviceDataManagers[i]);
				}
			}
			m_DeviceDataManagers = temp;
		}
		//mutex.unlock();
	}

	void SystemManager::RemoveDataUpdateListener(IDeviceDataUpdateListener* pDeviceDataListener)
	{
		//mutex.lock();
		if (m_Status == STOPPED)
		{
			std::vector<IDeviceDataUpdateListener*> temp;
			for (size_t i = 0; i < m_DeviceDataListeners.size(); ++i)
			{
				if (m_DeviceDataListeners[i] != pDeviceDataListener)
				{
					temp.push_back(m_DeviceDataListeners[i]);
				}
			}
			if (temp.size() != m_DeviceDataListeners.size())
			{
				for (size_t i = 0; i < m_DeviceDataManagers.size(); ++i)
				{
					m_DeviceDataManagers[i]->RemoveUpdateDataListener(pDeviceDataListener);
				}
			}
			m_DeviceDataListeners = temp;
		}
		//mutex.unlock();
	}

	void SystemManager::AddDataUpdateListener(const std::shared_ptr<IDevice>& pDevice, IDeviceDataUpdateListener * pDeviceDataListener)
	{
		//mutex.lock();
		if (m_Status == STOPPED)
		{
			pDevice->GetDeviceDataManager()->AddUpdateDataListener(pDeviceDataListener);
			m_DeviceDataListeners.push_back(pDeviceDataListener);
		}
		//mutex.unlock();
	}

	void SystemManager::Wait(int millisecond)
	{
#if defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WINDOWS__) || defined(__TOS_WIN__)

		Sleep(millisecond);
#else
#include <unistd.h>
		usleep(millisecond * 1000);
#endif 
	}

	double SystemManager::GetMonitorSpeed()
	{
		double speed = 0;
		for (size_t i = 0; i < m_DeviceDataManagers.size(); ++i)
		{
			speed += m_DeviceDataManagers[i]->GetCurrentSpeed();
		}
		return speed;
	}

	double SystemManager::GetMonitorSampleSpeed()
	{
		double speed = 0;
		for (size_t i = 0; i < m_DeviceDataManagers.size(); ++i)
		{
			speed += m_DeviceDataManagers[i]->GetCurrentSampleSpeed();
		}
		return speed;
	}

	double SystemManager::GetRecordDataSize()
	{
		double duration = (TriggerManager::GetInstance()->GetPreTrigger()) + (DeviceRecordSetting::GetInstance()->GetRecordDuration() / 1000000000.0);
		int samplerate = GetSampleRate();
		int samplesize = GetSampleSize();
		int nchannels = 0;
		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					nchannels += padcdev->GetChannels().size();
				}
			}
		}
		return duration * samplerate * samplesize * nchannels / 1000000;
	}

	int SystemManager::GetRecordProgres()
	{
		double speed = TriggerManager::GetInstance()->GetTotalRecordedSize() / GetRecordDataSize();
		return (int)(speed * 100);
	}

	void SystemManager::ReadSystemInfo(const boost::property_tree::ptree& pt)
	{
		try
		{
			using boost::property_tree::ptree;
			if (pt.empty()) return;
            //int version = pt.get<int>("project.version");
			m_LoadGSBoard = pt.get<int>("System.LoadGSBoard") != 0;
			std::vector<std::shared_ptr<IDevice>> gs;
			for (int i = 0; i < (int)m_Devices.size(); ++i)
			{
				std::string info = m_Devices[i]->GetDeviceInfo();
				if (info.find("Virtual") != std::string::npos || info.find("Server") != std::string::npos)
				{
					gs.push_back(m_Devices[i]);
				}
			}
			for (int i = 0; i < (int)gs.size(); ++i)
			{
				RemoveDevice(gs[i]);
			}
			if (!m_LoadGSBoard)
			{
				std::vector<std::shared_ptr<IDevice>> gs;
				for (int i = 0; i < (int)m_Devices.size(); ++i)
				{
					std::string info = m_Devices[i]->GetDeviceInfo();
					if (info.find("GS") != std::string::npos)
					{
						gs.push_back(m_Devices[i]);
					}
				}
				for (int i = 0; i < (int)gs.size(); ++i)
				{
					RemoveDevice(gs[i]);
				}
			}

			m_LoadBlackBurnBoard = pt.get<int>("System.LoadBlackBurnBoard") != 0;

			if (!m_LoadBlackBurnBoard || m_LoadGSBoard)
			{
				std::vector<std::shared_ptr<IDevice>> gs;
				for (int i = 0; i < (int)m_Devices.size(); ++i)
				{
					std::string info = m_Devices[i]->GetDeviceInfo();
					if (info.find("Black") != std::string::npos)
					{
						gs.push_back(m_Devices[i]);
					}
				}
				for (int i = 0; i < (int)gs.size(); ++i)
				{
					RemoveDevice(gs[i]);
				}
			}

			m_LoadICS645BBoard = pt.get_optional<int>("System.LoadICS645BBoard") != 0;

			if (!m_LoadICS645BBoard || m_LoadGSBoard || m_LoadBlackBurnBoard)
			{
				std::vector<std::shared_ptr<IDevice>> gs;
				for (int i = 0; i < (int)m_Devices.size(); ++i)
				{
					std::string info = m_Devices[i]->GetDeviceInfo();
					if (info.find("ICS645B") != std::string::npos)
					{
						gs.push_back(m_Devices[i]);
					}
				}
				for (int i = 0; i < (int)gs.size(); ++i)
				{
					RemoveDevice(gs[i]);
				}
			}

			for (int i = 0; i < (int)m_Devices.size(); ++i)
			{
				m_Devices[i]->SetID(i);
			}
			DeviceRecordSetting* pRecordSetting = DeviceRecordSetting::GetInstance();
			TriggerManager* pTrigger = TriggerManager::GetInstance();
			pRecordSetting->SetRecordName(pt.get<std::string>("Record.RecordName"));
			pRecordSetting->SetRecordDuration(pt.get<long long>("Record.RecordDuration"));
			pRecordSetting->SetRecordNumber(pt.get<int>("Record.RecordNumber"));
			pRecordSetting->SetRecordPath(pt.get<std::string>("Record.RecordPath"));
			pRecordSetting->SetRecordTimes(pt.get<int>("Record.RecordTimes"));
			m_PostRecordStatus = (SYSTEM_STATUS)pt.get<int>("Record.PostStatus");
			m_RecordType = (RECORD_TYPE)pt.get<int>("Record.FileType");
			pTrigger->SetTriggerControlType((TriggerControlType)(pt.get<int>("Record.TriggerType")));
			pTrigger->SetLimit((pt.get<double>("Record.TriggerLimit")));
			pTrigger->SetTriggerLimitType((MonitorLimitType)(pt.get<int>("Record.TriggerLimitType")));
			pTrigger->SetTriggerLimitValueType((MonitorDataType)(pt.get<int>("Record.TriggerDataType")));
			auto pretr = pt.get_optional<double>("Record.PreTriggerDuration");
			if (pretr) pTrigger->SetPreTrigger(pretr.get());
			tm tt, nt;
			std::string ti = pt.get<std::string>("Record.TriggerScheduleTime");
			std::istringstream ss(ti);
			ss >> std::get_time(&tt, "%EY:%m:%d-%H:%M:%S");
			time_t now;
			time(&now);
			nt = *localtime(&now);
			tt.tm_mon = nt.tm_mon;
			tt.tm_mday = nt.tm_mday;
			tt.tm_year = nt.tm_year;
			time_t t = mktime(&tt);
			auto tm = localtime(&t);
			if (tm)
			{
				pTrigger->SetTriggerScheduleTime(*tm);
			}
			pTrigger->SetTriggerChannelIndex(pt.get<int>("Record.TriggerChannelIndex"));
			m_NetworkBufferSize = pt.get<int>("Network.BufferSize");

			auto su = pt.get_child_optional("Devices");
			std::vector<std::shared_ptr<IDevice>> temp;
			int deviceid = 0;
			if (su)
			{
				for (auto itr = su->begin(); itr != su->end(); ++itr)
				{
					if (itr->first == "Device")
					{
						std::string info = itr->second.get<std::string>("<xmlattr>.TYPE");
						int id = itr->second.get<int>("<xmlattr>.ID");
						int inputvtg = itr->second.get<int>("InputVoltage");
						int samplerate = itr->second.get<int>("SampleRatePerSecond");
						IADCDevice * pDevice = 0;
						if (deviceid < (int)m_Devices.size())
							pDevice = (IADCDevice *)m_Devices[deviceid].get();
						deviceid += 1;
						if (info == "Server")
						{
							int port = itr->second.get<int>("Port");
							AddServerDevice(port);
							VirtualDevice* vp = (VirtualDevice*)m_Devices.back().get();
							vp->SetDeviceInputMilliVoltage(inputvtg);
							vp->SetSampleRatePerSecond(samplerate);
							vp->SetID(id);
							pDevice = vp;
						}
						else if (info == "Virtual")
						{
							AddVirtualDevice();
							VirtualDevice* vp = (VirtualDevice*)m_Devices.back().get();
							vp->SetDeviceInputMilliVoltage(inputvtg);
							vp->SetSampleRatePerSecond(samplerate);
							vp->SetID(id);
							pDevice = vp;
						}
						ptree chsu = itr->second.get_child("Channels");
						if (pDevice)
						{
							pDevice->SetDeviceInputMilliVoltage(inputvtg);
							pDevice->SetSampleRatePerSecond(samplerate);
							pDevice->SetID(id);
							auto& channels = pDevice->GetChannels();
							int j = 0;
							for (ptree::iterator citr = chsu.begin(); citr != chsu.end(); ++citr)
							{
								if (citr->first == "Channel")
								{
									auto& chnode = citr->second;
									if ((int)channels.size() > j)
									{
										auto& channel = channels[j];
										++j;
										channel->Name = chnode.get<std::string>("Name");
										channel->SetActive(chnode.get<bool>("Active"));
										channel->SetOffset(chnode.get<double>("Offset"));
										channel->SetSensitivity(chnode.get<double>("Sensitivity"));
										channel->SetEUOffset(chnode.get<double>("EUOffset"));
										channel->SetEUSensitivity(chnode.get<double>("EUSensitivity"));
										channel->EUName = (chnode.get<std::string>("EUUnit"));
										channel->Init(0, samplerate);
										auto userdatas = chnode.get_child_optional("UserDatas");
										if (userdatas)
										{
											for (auto ditr = userdatas->begin(); ditr != userdatas->end(); ++ditr)
											{
												if (ditr->first == "UserData")
												{
													auto& dnode = ditr->second;
													ChannelUserData data;
													data.name = dnode.get<std::string>("Name");
													data.value = dnode.get<std::string>("Value");
													channel->UserData.push_back(data);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			su = pt.get_child_optional("Clients");
			if (su)
			{
				for (auto itr = su->begin(); itr != su->end(); ++itr)
				{
					if (itr->first == "Device")
					{
						int id = itr->second.get<int>("<xmlattr>.ID");
						std::string address = itr->second.get<std::string>("Address");
						int port = itr->second.get<int>("Port");
						for (int i = 0; i < (int)m_Devices.size(); ++i)
						{
							if (m_Devices[i]->GetID() == id)
							{
								AddClientDevice(address, m_Devices[i], port);
								break;
							}
						}
					}
				}
			}
			su = pt.get_child_optional("FileNameItems");
			auto& items = FileNameManager::GetInstance()->GetNameItems();
			if (su)
			{
				items.clear();
				for (auto itr = su->begin(); itr != su->end(); ++itr)
				{
					if (itr->first == "Item")
					{
						NameItem item;
						item.itemType = (ITEM_TYPE)itr->second.get<int>("Type");
						if (item.itemType == USER_DEFINED)
						{
							item.item = itr->second.get<std::string>("Value");
						}
						items.push_back(item);
					}
				}
			}
			TriggerManager::GetInstance()->SetSnapshot(pt.get<int>("System.Snapshot") != 0);
			IADCDevice * pDevice = 0;
			if (!m_Devices.empty())
				pDevice = (IADCDevice *)m_Devices[0].get();

			if (pDevice)
				SetSampleRate(pDevice->GetSampleRatePerSecond());
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << "\n";
		}
	}

	void SystemManager::WriteSystemInfo(boost::property_tree::ptree& pt)
	{
		try
		{
			using boost::property_tree::ptree;
			pt.add("project.version", 1);
			for (int i = 0; i < (int)m_Devices.size(); ++i)
			{
				IADCDevice* pDevice = dynamic_cast<IADCDevice*>(m_Devices[i].get());
				std::string deviceinfo = pDevice->GetDeviceInfo();
				ptree & node = pt.add("Devices.Device", "");
				node.put("<xmlattr>.ID", pDevice->GetID());
				node.put("InputVoltage", pDevice->GetDeviceInputMilliVoltage());
				node.put("SampleRatePerSecond", pDevice->GetSampleRatePerSecond());

				if (deviceinfo.find("Server") != std::string::npos)
				{
					ServerDevice * sdev = dynamic_cast<ServerDevice*>(m_Devices[i].get());
					node.put("<xmlattr>.TYPE", "Server");
					node.put("Port", sdev->GetPort());
				}
				else if (deviceinfo.find("Virtual") != std::string::npos)
				{
					node.put("<xmlattr>.TYPE", "Virtual");
					node.put("Port", 0);
				}
				else if (deviceinfo.find("GS") != std::string::npos)
				{
					node.put("<xmlattr>.TYPE", "GS");
					node.put("Port", 0);
				}
				else if (deviceinfo.find("Black") != std::string::npos)
				{
					node.put("<xmlattr>.TYPE", "Black Burn");
					node.put("Port", 0);
				}
				else
				{
					node.put("<xmlattr>.TYPE", "Unknown");
					node.put("Port", 0);
				}

				auto& channels = pDevice->GetChannels();
				for (int j = 0; j < (int)channels.size(); ++j)
				{
					auto& channel = channels[j];
					ptree & chnode = node.add("Channels.Channel", "");
					chnode.put("Name", channel->Name);
					chnode.put("Active", channel->GetActive());
					chnode.put("Offset", channel->GetOffset());
					chnode.put("Sensitivity", channel->GetSensitivity());
					chnode.put("EUOffset", channel->GetEUOffset());
					chnode.put("EUSensitivity", channel->GetEUSensitivity());
					chnode.put("EUUnit", channel->EUName);
					auto& userdata = channel->UserData;
					for (int k = 0; k < (int)userdata.size(); ++k)
					{
						auto& data = userdata[k];
						ptree & dnode = chnode.add("UserDatas.UserData", "");
						dnode.put("Name", data.name);
						dnode.put("Value", data.value);
					}
				}
			}

			for (int i = 0; i < (int)m_Clients.size(); ++i)
			{
				std::shared_ptr<ClientDevice> pDevice = m_Clients[i];
				ptree & node = pt.add("Clients.Device", "");
				node.put("<xmlattr>.ID", pDevice->GetDevice()->GetID());
				node.put("Address", pDevice->GetHost());
				node.put("Port", pDevice->GetPort());
			}

			DeviceRecordSetting* pRecordSetting = DeviceRecordSetting::GetInstance();
			TriggerManager* pTrigger = TriggerManager::GetInstance();
			pt.put("System.LoadGSBoard", (int)m_LoadGSBoard);
			pt.put("System.LoadBlackBurnBoard", (int)m_LoadBlackBurnBoard);
			pt.put("System.LoadICS645BBoard", (int)m_LoadICS645BBoard);
			pt.put("System.Snapshot", pTrigger->GetSnapshot() ? 1 : 0);

			pt.put("Record.RecordName", pRecordSetting->GetRecordName());
			pt.put("Record.RecordDuration", pRecordSetting->GetRecordDuration());
			pt.put("Record.RecordNumber", pRecordSetting->GetRecordNumber());
			pt.put("Record.RecordPath", pRecordSetting->GetRecordPath());
			pt.put("Record.RecordTimes", pRecordSetting->GetRecordTimes());
			pt.put("Record.PostStatus", (int)m_PostRecordStatus);
			pt.put("Record.TriggerType", (int)pTrigger->GetTriggerControlType());
			pt.put("Record.FileType", (int)m_RecordType);
			pt.put("Record.TriggerLimit", pTrigger->GetLimit());
			pt.put("Record.TriggerLimitType", (int)pTrigger->GetTriggerLimitType());
			pt.put("Record.TriggerDataType", (int)pTrigger->GetTriggerLimitValueType());
			pt.put("Record.TriggerChannelIndex", (int)pTrigger->GetTriggerChannelIndex());
			pt.put("Record.PreTriggerDuration", pTrigger->GetPreTrigger());
			auto t = pTrigger->GetTriggerScheduleTime();
			std::stringstream str;
			str << t.tm_year + 1900 << ":" << t.tm_mon + 1 << ":" << t.tm_mday << "-" << t.tm_hour << ":" << t.tm_min << ":" << t.tm_sec;
			pt.put("Record.TriggerScheduleTime", str.str());
			pt.put("Record.TriggerDataType", (int)pTrigger->GetTriggerLimitValueType());
			pt.put("Network.BufferSize", m_NetworkBufferSize);

			auto& items = FileNameManager::GetInstance()->GetNameItems();

			for (int i = 0; i < (int)items.size(); ++i)
			{
				ptree & node = pt.add("FileNameItems.Item", "");

				auto item = items[i];
				node.put("Type", (int)item.itemType);
				node.put("Value", item.item);
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << "\n";
		}
	}

	void SystemManager::ConnectServerClient()
	{
		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			ServerDevice * pDevice = dynamic_cast<ServerDevice*>(m_Devices[i].get());
			if (pDevice)
			{
				pDevice->Start();
			}
		}

		for (int i = 0; i < (int)m_Clients.size(); ++i)
		{
			m_Clients[i]->StartListening();
		}
	}

	void SystemManager::LoadCalibration(const std::string& calFile, const std::shared_ptr<IDevice>& pDevice)
	{
		if (pDevice)
		{
			IADCDevice * pADCDevice = dynamic_cast<IADCDevice*>(pDevice.get());
			if (pADCDevice)
				CalibrationIO::LoadCalibration(calFile, pADCDevice->GetChannels());
		}
	}

	void SystemManager::DisconnectServerClient()
	{
		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			ServerDevice * pDevice = dynamic_cast<ServerDevice*>(m_Devices[i].get());
			if (pDevice)
			{
				pDevice->Stop();
			}
		}

		for (int i = 0; i < (int)m_Clients.size(); ++i)
		{
			m_Clients[i]->StopListening();
		}
	}

	void SystemManager::Clear()
	{
		m_Devices.clear();
		m_DeviceDataManagers.clear();
		for (int i = 0; i < (int)m_DeviceDataListeners.size(); ++i)
		{
			delete m_DeviceDataListeners[i];
		}
		m_DeviceDataListeners.clear();
	}

	void SystemManager::ClearDevices()
	{
		std::vector<std::shared_ptr<IDevice>> temp = m_Devices;
		for (int i = 0; i < (int)temp.size(); ++i)
		{
			RemoveDevice(temp[i]);
		}
		std::vector<std::shared_ptr<ClientDevice>> temp1 = m_Clients;
		for (int i = 0; i < (int)temp1.size(); ++i)
		{
			RemoveClientDevice(temp1[i]);
		}
	}

	double SystemManager::GetChannelDuration()
	{
		double duration = 10;
		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					auto& channels = padcdev->GetChannels();
					for (int j = 0; j < (int)channels.size(); ++j)
					{
						auto& channel = channels[j];
						if (channel)
						{
							duration = std::min(duration, channel->GetDurationNanoSecond() / 1000000000.0);
						}
					}
				}
			}
		}
		return duration;
	}

	int SystemManager::GetInputMilivoltage()
	{
		int voltage = 0;
		for (int i = 0; i < (int)m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					voltage = std::max(voltage, padcdev->GetDeviceInputMilliVoltage());
				}
			}
		}
		return voltage;
	}

	void SystemManager::SetInputMilivoltage(int voltage)
	{
		for (unsigned int i = 0; i < m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					padcdev->SetDeviceInputMilliVoltage(voltage);
				}
			}
		}
	}

	void SystemManager::SetSampleRate(long samplerate)
	{
		for (unsigned int i = 0; i < m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					padcdev->SetSampleRatePerSecond(samplerate);
				}
			}
		}
	}

	long SystemManager::GetSampleRate()
	{
		long samplerate = 0;
		if (m_Devices.size() == 0) return samplerate;
		auto& pDevice = m_Devices[0];
		if (pDevice)
		{
			IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
			if (padcdev)
			{
				samplerate = padcdev->GetSampleRatePerSecond();
			}
		}
		return samplerate;
	}

	int SystemManager::GetSampleSize()
	{
		int samplesize = 0;
		if (m_Devices.size() == 0) return samplesize;
		auto& pDevice = m_Devices[0];
		if (pDevice)
		{
			IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
			if (padcdev)
			{
				samplesize = (int)padcdev->GetDeviceDataFormat();
			}
		}
		return samplesize;
	}

	bool SystemManager::RecordExist()
	{
		DeviceRecordSetting * recordSetting = DeviceRecordSetting::GetInstance();
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		bool bExist = false;
		for (size_t i = 0; i < devices.size(); ++i)
		{
			if (boost::filesystem::exists(recordSetting->GetRecordFileName(i) + ".rawfs"))
			{
				bExist = true;
				break;
			}
		}
		return bExist;
	}

	void SystemManager::AddChannelUserData(int idx)
	{
		for (unsigned int i = 0; i < m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					padcdev->AddChannelUserData(idx);
				}
			}
		}
	}

	void SystemManager::RemoveChannelUserData(int idx)
	{
		for (unsigned int i = 0; i < m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					padcdev->RemoveChannelUserData(idx);
				}
			}
		}
	}

	int SystemManager::GetCurrentChannelCount()
	{
		int nchannels = 0;
		for (unsigned int i = 0; i < m_Devices.size(); ++i)
		{
			auto& pDevice = m_Devices[i];
			if (pDevice)
			{
				IADCDevice* padcdev = dynamic_cast<IADCDevice*>(pDevice.get());
				if (padcdev)
				{
					nchannels += (int)padcdev->GetChannels().size();
				}
			}
		}
		return nchannels;
	}

	void SystemManager::CheckStatusForStop()
	{
		if (m_Status == READYTOSTOP)
		{
			StopMonitor();
		}
	}

}
