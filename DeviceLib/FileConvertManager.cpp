#include "FileConvertManager.h"
#include "DeviceDataRecorder.h"

namespace DeviceLib
{
	FileConvertManager * FileConvertManager::instance = 0;

	FileConvertManager * FileConvertManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new FileConvertManager();
			instance->Start();
		}
		return instance;
	}

	void FileConvertManager::Start()
	{
		if (m_Runner == 0)
		{
			m_Runner = new FileConvertRunner(FileConvertManager::instance);
		}

		m_Runner->Reset();
		m_Thread = new boost::thread(boost::ref(*m_Runner));
	}

	void FileConvertManager::AddRecorder(DeviceDataRecorder* recorder)
	{
		if (recorder) m_Recorders.push(recorder);
	}

	void FileConvertManager::Stop()
	{
		if (m_Thread)
		{
			if (m_Runner) m_Runner->Stop();
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			delete m_Thread;
			m_Thread = 0;
		}
	}

	void FileConvertManager::Convert()
	{
		if (!m_Recorders.empty())
		{
			auto recorder = m_Recorders.front();
			recorder->Convert();
			delete recorder;
			m_Recorders.pop();
		}
		else
		{
			boost::this_thread::sleep(boost::posix_time::microseconds(1000));
		}
	}

	FileConvertManager::FileConvertManager()
		: m_Runner(0)
	{
	}

	void FileConvertManager::Clear()
	{
		Stop();
		if (m_Runner) delete m_Runner;
	}

}