//
// ExcelImportLibrary.cpp
//

#include "StdAfx.h"
#include "ExcelImportLibrary.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define LIBRARY_GET_NAME                         "GetLibraryName"
#define LIBRARY_GET_DESCRIPTION                  "GetLibraryDescription"
#define LIBRARY_GET_BUILDER                      "GetLibraryBuilder"
#define LIBRARY_GET_VERSION                      "GetLibraryVersion"
#define LIBRARY_SHOW_ABOUT_UI                    "ShowAboutUI"
#define LIBRARY_HAVE_ABOUT_UI                    "HaveAboutUI"
#define LIBRARY_SHOW_SETUP_UI                    "ShowSetupUI"
#define LIBRARY_HAVE_SETUP_UI                    "HaveSetupUI"
#define LIBRARY_IMPORT_EXCEL_FILE                "ImportExcelFile"

//
// CExcelImportLibrary
CExcelImportLibrary::CExcelImportLibrary(void)
	: m_FilePath(_T(""))
	, m_hDLL(NULL)
{
	Init();
}

//
// ~CExcelImportLibrary
CExcelImportLibrary::~CExcelImportLibrary(void)
{
	UnloadLibrary();
}

//
// Init
void CExcelImportLibrary::Init(void)
{
	*(FARPROC*)&GetName         = NULL;
	*(FARPROC*)&GetDescription  = NULL;
	*(FARPROC*)&GetBuilder      = NULL;
	*(FARPROC*)&GetVersion      = NULL;
	*(FARPROC*)&ShowAboutUI     = NULL;
	*(FARPROC*)&HaveAboutUI     = NULL;
	*(FARPROC*)&ShowSetupUI     = NULL;
	*(FARPROC*)&HaveSetupUI     = NULL;
	*(FARPROC*)&ImportExcelFile = NULL;
}

//
// LoadLibrary
BOOL CExcelImportLibrary::LoadLibrary(LPCTSTR pFilePath)
{
	CString text;
	FARPROC pFunction;

	// Check Input Argument
	if(pFilePath == NULL)
		return FALSE;

	// Set File Path
	m_FilePath = pFilePath;
	if(m_FilePath.IsEmpty())
		return FALSE;

	// if some is already loaded, unload that
	if(IsLoaded())
		UnloadLibrary();

	///////////////////////////////////////////////
	// Load Library
	m_hDLL = AfxLoadLibrary(m_FilePath);
	if(!IsLoaded())
	{
		text.Format(_T("Error : Can't load DLL File ( %s )\n\nPlease check out if this file exists."), m_FilePath);
		AfxMessageBox(text);
		return FALSE;
	}

	///////////////////////////////////////////////
	// Import Library Functions
	pFunction = GetProcAddress(m_hDLL, LIBRARY_GET_NAME);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&GetName = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_GET_DESCRIPTION);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&GetDescription = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_GET_BUILDER);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&GetBuilder = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_GET_VERSION);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&GetVersion = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_SHOW_ABOUT_UI);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&ShowAboutUI = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_HAVE_ABOUT_UI);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&HaveAboutUI = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_SHOW_SETUP_UI);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&ShowSetupUI = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_HAVE_SETUP_UI);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&HaveSetupUI = pFunction;

	pFunction = GetProcAddress(m_hDLL, LIBRARY_IMPORT_EXCEL_FILE);
	if(pFunction == NULL)
	{
		Init();
		UnloadLibrary();
		return FALSE;
	}
	*(FARPROC*)&ImportExcelFile = pFunction;

	return TRUE;
}

//
// UnloadLibrary
BOOL CExcelImportLibrary::UnloadLibrary(void)
{
	if(IsLoaded())
	{
		AfxFreeLibrary(m_hDLL);
		m_hDLL = NULL;
	}

	return TRUE;
}
