
// ExcelImport.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "ExcelImport.h"
#include "ExcelImportLibrary.h"
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExcelImportApp

BEGIN_MESSAGE_MAP(CExcelImportApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CExcelImportApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
END_MESSAGE_MAP()


// CExcelImportApp construction

CExcelImportApp::CExcelImportApp()
{
	m_bHiColorIcons = TRUE;

	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// If the application is built using Common Language Runtime support (/clr):
	//     1) This additional setting is needed for Restart Manager support to work properly.
	//     2) In your project, you must add a reference to System.Windows.Forms in order to build.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO: replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("ExcelImport.AppID.NoVersion"));

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CExcelImportApp object

CExcelImportApp theApp;


// CExcelImportApp initialization

BOOL CExcelImportApp::InitInstance()
{
	CWinAppEx::InitInstance();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	CPtrArray            m_DataArray;
	TCHAR currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	CExcelImportLibrary m_pLibrary;
	CString filename;
	filename.Format(_T("%s\\ImportDDRExcel.dll"), currentDir);
	m_pLibrary.LoadLibrary(filename);

	// Show Info Tip
	if (m_pLibrary.IsLoaded())
	{
		if (cmdInfo.m_strFileName != "")
		{
			if (TRUE == m_pLibrary.ImportExcelFile(cmdInfo.m_strFileName, m_DataArray))
			{
				std::ofstream fout("C:\\ProgramData\\DaqScribe\\channel.csv");
				fout << "\n";
				fout.close();
				CStdioFile file;
				if (file.Open(_T("C:\\ProgramData\\DaqScribe\\channel.csv"), CFile::modeReadWrite))
				{
					for (INT_PTR i = 0; i < m_DataArray.GetCount(); ++i)
					{
						CStringArray* strlist = (CStringArray*)m_DataArray[i];
						if (strlist != 0)
						{
							for (INT_PTR j = 0; j < strlist->GetSize(); ++j)
							{
								CString str = strlist->GetAt(j);
								str.Trim();
								file.WriteString(str);
								if (j < strlist->GetSize() - 1) file.WriteString(_T(","));
							}
							file.WriteString(_T("\n"));
						}
					}
					file.Flush();
					file.Close();
				}
			}
		}
	}
	return TRUE;
}

int CExcelImportApp::ExitInstance()
{
	return CWinAppEx::ExitInstance();
}

// CExcelImportApp message handlers


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// App command to run the dialog
void CExcelImportApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CExcelImportApp customization load/save methods

void CExcelImportApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
	bNameValid = strName.LoadString(IDS_EXPLORER);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EXPLORER);
}

void CExcelImportApp::LoadCustomState()
{
}

void CExcelImportApp::SaveCustomState()
{
}

// CExcelImportApp message handlers



