//
// ExcelImportLibrary.h
// DLL Library Handler class
//

#pragma once

//
// CExcelImportLibrary
class CExcelImportLibrary
{
public:

	// Constructor / Destructor
	CExcelImportLibrary(void);
	virtual ~CExcelImportLibrary(void);

	BOOL IsLoaded()                      {  return (m_hDLL != NULL);  }
	BOOL LoadLibrary(LPCTSTR pFilePath);
	BOOL UnloadLibrary(void);

	// Library Functions
	CString (*GetName)(void);
	CString (*GetDescription)(void);
	CString (*GetBuilder)(void);
	CString (*GetVersion)(void);

	int     (*ShowAboutUI)(void);
	BOOL    (*HaveAboutUI)(void);
	int     (*ShowSetupUI)(void);
	BOOL    (*HaveSetupUI)(void);

	BOOL    (*ImportExcelFile)(CString FilePath, CPtrArray &DataArray);

protected:

	void Init(void);

	CString   m_FilePath;
	HINSTANCE m_hDLL;

};
