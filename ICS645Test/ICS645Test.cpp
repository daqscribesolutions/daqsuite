#include <iostream>
#include "OptionLoader.h"
#include "ICSBoard.h"

void RunTest(const Option& option);

int main(const int argc, const char* argv[])
{
	if (argc != 2)
	{
		std::cout << "Wrong option : ex) " << argv[0] << " [Option file]\n";
		return 0;
	}

	Option option;

	if (OptionLoader::LoadOption(option, argv[1]))
	{ 
		if (option.size() == 35)
		{
			std::cout << "Options loaded.\n";
			RunTest(option);
		}
		else
		{
			std::cout << "Options are not for this test.\n";
		}
	}
	else
	{
		std::cout << "Failed to load option file.\n";
	}
	return 1;
}

void RunTest(const Option& option)
{
	int n645Boards = 0;
	int n500Boards = 0;

	std::vector<HANDLE> h645s;
	std::vector<HANDLE> h500s;

	// Get device handles.
	while (true)
	{
		CString deviceName;
		deviceName.Format(_T("\\\\.\\ICS645-%d"), (int)h645s.size() + 1);
		HANDLE hDevice = CreateFile(deviceName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, (HANDLE)NULL);
		if (hDevice != INVALID_HANDLE_VALUE)
		{
			h645s.push_back(hDevice);
		}
		else
		{
			break;
		}
	}

	while (true)
	{
		CString deviceName;
		deviceName.Format(_T("\\\\.\\ICS500-%d"), (int)h500s.size() + 1);
		HANDLE hDevice = CreateFile(deviceName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, (HANDLE)NULL);
		if (hDevice != INVALID_HANDLE_VALUE)
		{
			h500s.push_back(hDevice);
		}
		else
		{
			break;
		}
	}

	std::cout << "ICS645 : " << h645s.size() << "\n";
	std::cout << "ICS500 : " << h500s.size() << "\n";

	OptionKey Option_Key;
	// Configure
	int sampleRate = atoi(option.at(Option_Key[0]).c_str());
	bool bUseFPDP = atoi(option.at(Option_Key[1]).c_str()) == 1;
	double dFPDPClock = atof(option.at(Option_Key[2]).c_str());
	bool bTriggerInternal = atoi(option.at(Option_Key[3]).c_str()) == 1;
	bool bClockInternal = atoi(option.at(Option_Key[4]).c_str()) == 1;
	bool bPACKED_DATA = atoi(option.at(Option_Key[5]).c_str()) == 1;
	bool bContinousMode = atoi(option.at(Option_Key[6]).c_str()) == 1;
	bool bFPDPInterface1 = atoi(option.at(Option_Key[7]).c_str()) == 1;
	bool bUseClockOutput = atoi(option.at(Option_Key[8]).c_str()) == 1;
	bool bUseTriggerOutput = atoi(option.at(Option_Key[9]).c_str()) == 1;
	unsigned long decimation = atoi(option.at(Option_Key[10]).c_str());
	unsigned long nChannelCount = atoi(option.at(Option_Key[11]).c_str());
	unsigned long nBufferLength = atoi(option.at(Option_Key[12]).c_str());
	unsigned long nAcquisitionCount = atoi(option.at(Option_Key[13]).c_str());
	unsigned long FPDPFrameCount = atoi(option.at(Option_Key[14]).c_str());
	int FPDPIndex = atoi(option.at(Option_Key[15]).c_str());
	int FPDPChannelCount = atoi(option.at(Option_Key[16]).c_str());

	bool bCornerTurning = atoi(option.at(Option_Key[17]).c_str()) == 1;
	bool b500PACKED_DATA = atoi(option.at(Option_Key[18]).c_str()) == 1;
	bool bContinuousMode = atoi(option.at(Option_Key[19]).c_str()) == 1;
	bool bFPDPReceiveMode = atoi(option.at(Option_Key[20]).c_str()) == 1;
	bool b500TriggerInternal = atoi(option.at(Option_Key[21]).c_str()) == 1;
	bool bSequenceNumber = atoi(option.at(Option_Key[22]).c_str()) == 1;
	bool bFPDPSuspendControl = atoi(option.at(Option_Key[23]).c_str()) == 1;
	bool bFPDPStrobePECL = atoi(option.at(Option_Key[24]).c_str()) == 1;
	bool bEDGE_SENSITIVE = atoi(option.at(Option_Key[25]).c_str()) == 1;
	int nDataFrameFormat = atoi(option.at(Option_Key[26]).c_str());
	unsigned long long nStartChannelNo = atoi(option.at(Option_Key[27]).c_str());
	unsigned long long nSelectChannelNo = atoi(option.at(Option_Key[28]).c_str());
	unsigned long long n500BufferLength = atoi(option.at(Option_Key[29]).c_str());
	std::string outputDir = option.at(Option_Key[30]);
	int second = atoi(option.at(Option_Key[31]).c_str());
	millisecond = atoi(option.at(Option_Key[32]).c_str());
	bPrintRegister = atoi(option.at(Option_Key[33]).c_str()) == 1;
	bool bPrintOption = atoi(option.at(Option_Key[34]).c_str()) == 1;

	/// Options print
	if (bPrintOption)
	{
		std::cout << "\n\nOptions \n";
		std::cout << "Sample Rate : " << sampleRate << "\n";
		std::cout << "bUseFPDP : " << bUseFPDP << "\n";
		std::cout << "dFPDPClock : " << dFPDPClock << "\n";
		std::cout << "bTriggerInternal : " << bTriggerInternal << "\n";
		std::cout << "bClockInternal : " << bClockInternal << "\n";
		std::cout << "bPACKED_DATA : " << bPACKED_DATA << "\n";
		std::cout << "bContinousMode : " << bContinousMode << "\n";
		std::cout << "bFPDPInterface1 : " << bFPDPInterface1 << "\n";
		std::cout << "bUseClockOutput : " << bUseClockOutput << "\n";
		std::cout << "bUseTriggerOutput : " << bUseTriggerOutput << "\n";
		std::cout << "decimation : " << decimation << "\n";
		std::cout << "nChannelCount : " << nChannelCount << "\n";
		std::cout << "nBufferLength : " << nBufferLength << "\n";
		std::cout << "nAcquisitionCount : " << nAcquisitionCount << "\n";
		std::cout << "FPDPFrameCount : " << FPDPFrameCount << "\n";
		std::cout << "FPDPIndex : " << FPDPIndex << "\n";
		std::cout << "FPDPChannelCount : " << FPDPChannelCount << "\n\n";
		std::cout << "bCornerTurning : " << bCornerTurning << "\n";
		std::cout << "b500PACKED_DATA : " << b500PACKED_DATA << "\n";
		std::cout << "bContinuousMode : " << bContinuousMode << "\n";
		std::cout << "bFPDPReceiveMode : " << bFPDPReceiveMode << "\n";
		std::cout << "b500TriggerInternal : " << b500TriggerInternal << "\n";
		std::cout << "bSequenceNumber : " << bSequenceNumber << "\n";
		std::cout << "bFPDPSuspendControl : " << bFPDPSuspendControl << "\n";
		std::cout << "bFPDPStrobePECL : " << bFPDPStrobePECL << "\n";
		std::cout << "bEDGE_SENSITIVE : " << bEDGE_SENSITIVE << "\n";
		std::cout << "nDataFrameFormat : " << nDataFrameFormat << "\n";
		std::cout << "nStartChannelNo : " << nStartChannelNo << "\n";
		std::cout << "nSelectChannelNo : " << nSelectChannelNo << "\n";
		std::cout << "n500BufferLength : " << n500BufferLength << "\n";
		std::cout << "Output Dir : " << outputDir << "\n";
		std::cout << "Running second : " << second << "\n";
		std::cout << "DelayAPI : " << millisecond << "\n";
		std::cout << "Display Register : " << bPrintRegister << "\n\n";
		std::cout << "Display Option : " << bPrintOption << "\n\n";
	}

	/// Configure devices.
	for (size_t i = 0; i < h645s.size(); ++i)
	{
		if (ConfigureICS645((int)i, h645s[i], h645s, sampleRate, bUseFPDP, dFPDPClock, bTriggerInternal
			, bClockInternal, bPACKED_DATA, bContinousMode, bFPDPInterface1
			, bUseClockOutput, bUseTriggerOutput, decimation, nChannelCount, nBufferLength
			, nAcquisitionCount, FPDPFrameCount, FPDPIndex, FPDPChannelCount) == FALSE)
		{
			std::cout << "ConfigureICS645 failed\n";
		}
	}

	for (size_t i = 0; i < h500s.size(); ++i)
	{
		if (ConfigureICS500(h500s[i], bCornerTurning, b500PACKED_DATA, bContinuousMode, bFPDPReceiveMode
			, b500TriggerInternal, bSequenceNumber, bFPDPSuspendControl, bFPDPStrobePECL, bEDGE_SENSITIVE
			, nDataFrameFormat, nStartChannelNo, nSelectChannelNo, n500BufferLength) ==  FALSE)
		{
			std::cout << "ConfigureICS500 failed\n";
		}
	}

	/// Start devices.
	for (size_t i = 0; i < h500s.size(); ++i)
	{
		StartDeviceICS500(h500s[i]);
	}

	double d = clock();
	while (clock() - d < 2000){}

	for (int i = (int)h645s.size() - 1; i >= 0; --i)
	{
		StartDeviceICS645((int)i, h645s[i]);
	}

	/// Run threads.
	std::vector<std::unique_ptr<boost::thread>> threads;
	std::vector<std::unique_ptr<DeviceRunner>> runners;
	std::vector<std::unique_ptr<std::ofstream>> fouts;
	char header[512];
	char num[5];
	for (size_t i = 0; i < h645s.size(); ++i)
	{
		itoa(i, num, 4);
		std::string filename = outputDir + "ICS645" + num + ".rawfs";
		std::ofstream* fout = bUseFPDP ? 0 : new std::ofstream(filename.c_str(), std::ios::binary);
		if (fout)
		{
			GetHeader(header, 0, sampleRate, nChannelCount);
			if (fout->good()) std::cout << filename << " opened\n";
			else std::cout << filename << " failed open to save.\n";
			fout->write(header, 512);
			fout->flush();
			fouts.push_back(std::unique_ptr<std::ofstream>(fout));
		}
		DeviceRunner* runner = new ICS645DeviceRunner(h645s[i], nBufferLength * 8, fout);
		runner->Reset();
		boost::thread* thread = new boost::thread(boost::ref(*runner));
		threads.push_back(std::unique_ptr<boost::thread>(thread));
		runners.push_back(std::unique_ptr<DeviceRunner>(runner));
	}

	for (size_t i = 0; i < h500s.size(); ++i)
	{
		itoa(i, num, 4);
		std::string filename = outputDir + "ICS500" + num + ".rawfs";
		std::ofstream* fout = bUseFPDP ? new std::ofstream(filename.c_str(), std::ios::binary) : 0;
		if (fout)
		{
			if (fout->good()) std::cout << filename << " opened\n";
			else std::cout << filename << " failed open to save.\n";
			GetHeader(header, 0, sampleRate, b500PACKED_DATA ? nSelectChannelNo * 2 : nSelectChannelNo);
			fout->write(header, 512);
			fout->flush();
			fouts.push_back(std::unique_ptr<std::ofstream>(fout));
		}
		DeviceRunner* runner = new ICS500DeviceRunner(h500s[i], (unsigned long)n500BufferLength * 8, fout, bSequenceNumber);
		runner->Reset();
		boost::thread* thread = new boost::thread(boost::ref(*runner));
		threads.push_back(std::unique_ptr<boost::thread>(thread));
		runners.push_back(std::unique_ptr<DeviceRunner>(runner));
	}

	d = clock();
	double runTime = second * 1000 + (bSequenceNumber ? 500 : 0);
	while (clock() - d < runTime)
	{
	}

	/// End devices.
	for (size_t i = 0; i < h645s.size(); ++i)
	{
		EndDeviceICS645((int)i, h645s[i]);
	}

	for (size_t i = 0; i < h500s.size(); ++i)
	{
		EndDeviceICS500(h500s[i]);
	}

	/// Close devices.
	for (size_t i = 0; i < h645s.size(); ++i)
	{
		CloseICS645(h645s[i]);
	}

	for (size_t i = 0; i < h500s.size(); ++i)
	{
		CloseICS500(h500s[i]);
	}

	for (size_t i = 0; i < runners.size(); ++i)
	{
		runners[i]->Stop();
	}

	for (size_t i = 0; i < threads.size(); ++i)
	{
		threads[i]->interrupt();
	}

	d = clock();
	while (clock() - d < 1000){}

	for (size_t i = 0; i < fouts.size(); ++i)
	{
		std::unique_ptr<std::ofstream>& fout = fouts[i];
		fout->seekp(0);
		int idx = bUseFPDP ? (int)(h645s.size() + i) : (int)i;
		long long datasize = runners[idx]->GetSavedByte();
		if (bUseFPDP)
		{
			GetHeader(header, datasize, sampleRate, b500PACKED_DATA ? nSelectChannelNo * 2 : nSelectChannelNo);
		}
		else
		{
			GetHeader(header, datasize, sampleRate, nChannelCount);
		}
		fout->write(header, 512);
		fout->close();
	}
}
