#ifndef ICSBOARD_H
#define ICSBOARD_H

#include <Windows.h>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include "../../ThirdParty/ICSBoard_32/Include/ICS500Api.h"
#include "../../ThirdParty/ICSBoard_32/Include/ICS645Api.h"
#include <atlstr.h>

struct OptionKey : public std::vector<std::string>
{
	OptionKey()
	{
		push_back("ICS645_SampleRate");
		push_back("ICS645_UseFPDP");
		push_back("ICS645_FPDPClock");
		push_back("ICS645_TriggerInternal");
		push_back("ICS645_ClockInternal");
		push_back("ICS645_PACKED_DATA");
		push_back("ICS645_ContinousMode");
		push_back("ICS645_FPDPInterface1");
		push_back("ICS645_UseClockOutput");
		push_back("ICS645_UseTriggerOutput");
		push_back("ICS645_Decimation");
		push_back("ICS645_ChannelCount");
		push_back("ICS645_BufferLength");
		push_back("ICS645_AcquisitionCount");
		push_back("ICS645_FPDPFrameCount");
		push_back("ICS645_FPDPIndex");
		push_back("ICS645_FPDPChannelCount");
		push_back("ICS500_CornerTurning");
		push_back("ICS500_PACKED_DATA");
		push_back("ICS500_ContinuousMode");
		push_back("ICS500_FPDPReceiveMode");
		push_back("ICS500_TriggerInternal");
		push_back("ICS500_SequenceNumber");
		push_back("ICS500_FPDPSuspendControl");
		push_back("ICS500_FPDPStrobePECL");
		push_back("ICS500_EDGE_SENSITIVE");
		push_back("ICS500_DataFrameFormat");
		push_back("ICS500_StartChannelNo");
		push_back("ICS500_SelectChannelNo");
		push_back("ICS500_BufferLength");
		push_back("OutputDir");
		push_back("RecordSecond");
		push_back("APIDelayMilisecond");
		push_back("PrintRegister");
		push_back("PrintOption");
	}
};

#define ICS645_ONE_MILLIVOLTAGE_RANGE            2000                // 2Vpp
#define ICS645_ONE_B_MILLIVOLTAGE_RANGE          20000               // 20Vpp
#define DEFAULT_ICS645_MILLIVOLTAGE_RANGE        ICS645_ONE_B_MILLIVOLTAGE_RANGE

// Sample Rate
#define ICS645_SAMPLE_RATE_MIN_IN_CLOCK          25000               // 25 kHz
#define ICS645_SAMPLE_RATE_MIN_EX_CLOCK          1000                // 1 kHz
#define ICS645_SAMPLE_RATE_MAX_8X                2500000             // 2.5 MHz
#define ICS645_SAMPLE_RATE_MAX_4X                5000000             // 5 MHz
#define ICS645_SAMPLE_RATE_MAX_2X                10000000            // 10 MHz
#define ICS645_SAMPLE_RATE_MAX_1X                20000000            // 20 MHz
#define DEFAULT_ICS645_SAMPLE_RATE               ICS645_SAMPLE_RATE_MAX_8X

// Sample Size
#define ICS645_SAMPLE_BYTE_SIZE                  2                   // 2 Bytes = 16 bits
#define ICS645_SAMPLE_BIT_SIZE                   16

// Channel Count
#define ICS645_CHANNEL_COUNT_MAX_8X              32
#define ICS645_CHANNEL_COUNT_MAX_4X              16
#define ICS645_CHANNEL_COUNT_MAX_2X              8
#define ICS645_CHANNEL_COUNT_MAX_1X              4
#define DEFAULT_ICS645_CHANNEL_COUNT             ICS645_CHANNEL_COUNT_MAX_8X


// Manual : 0 ~ 524287
#define ICS645_BUFFER_LENGTH_MIN                 1
#define ICS645_BUFFER_LENGTH_MAX                 524288
#define ICS645_BUFFER_WORD_BYTE_SIZE             4                   // 4 Bytes = 32 bits
#define DEFAULT_ICS645_BUFFER_LENGTH             ICS645_BUFFER_LENGTH_MAX

// Acquisition Count
// Manual : 0 ~ 524287
#define ICS645_ACQUISITION_COUNT_MIN             1
#define ICS645_ACQUISITION_COUNT_MAX             524288
#define DEFAULT_ICS645_ACQUISITION_COUNT         ICS645_ACQUISITION_COUNT_MAX

// Decimation
#define ICS645_DECIMATION_MIN                    1
#define ICS645_DECIMATION_MAX                    255
#define DEFAULT_ICS645_DECIMATION                ICS645_DECIMATION_MIN

// Frame Count
// Manual : 0 ~ 1023
#define ICS645_FPDP_FRAME_COUNT_MIN              1
#define ICS645_FPDP_FRAME_COUNT_MAX              1024
#define DEFAULT_ICS645_FPDP_FRAME_COUNT          ICS645_FPDP_FRAME_COUNT_MAX
#define ICS645_ONBOARD_MEMORY_BYTE_SIZE          4194304            // 4 MBytes
#define DEFAULT_ICS645_MEMORY_BYTE_SIZE          ICS645_ONBOARD_MEMORY_BYTE_SIZE

// FPDP Configuration
#define ICS645_FPDP_DATA_RATE_MIN                1   // 1MHz
#define ICS645_FPDP_DATA_RATE_MAX_TTL_STROBE     20  // 20MHz ==> 80 MB/s
#define ICS645_FPDP_DATA_RATE_MAX_PECL           50  // 40MHz ==> 160 MB/s
#define ICS645_FPDP_DATA_RATE_MAX_FPDP_I         50  // 50MHz ==> 200 MB/s

enum OverSamplingRatio
{
	OVER_SAMPLING_8X,
	OVER_SAMPLING_4X,
	OVER_SAMPLING_2X,
	OVER_SAMPLING_1X,
	OVER_SAMPLING_RATIO_COUNT,
};

#define GIGAHERTZ_IN_HERTZ             1000000000
#define MEGAHERTZ_IN_HERTZ             1000000
#define KILOHERTZ_IN_HERTZ             1000
#define MAX_ADC_RESET_COUNT 3

#define ICS645D_SAMPLE_RATE_MIN_IN_CLOCK         57000               // 57 kHz
#define ICS645D_SAMPLE_RATE_MIN_EX_CLOCK         1000                // 1 kHz
#define ICS645D_SAMPLE_RATE_MAX_8X               2500000             // 2.5 MHz
#define ICS645D_SAMPLE_RATE_MAX_4X               5000000             // 5 MHz

#define DEFAULT_ICS645D_SAMPLE_RATE              ICS645D_SAMPLE_RATE_MAX_8X


enum FPDPDataFrameFormat
{
	FIXED_SIZE_REPEATING_FRAME_DATA,
	DYNAMIC_SIZE_REPEATING_FRAME_DATA,
	SINGLE_FRAME_DATA,
	UNFRAMED_DATA,
	FPDP_DATA_FRAME_FORMAT_COUNT,
};

int millisecond = 1;
bool bPrintRegister = false;

BOOL CloseICS645(HANDLE hDevice)
{
	int result;
	// 
	// ics645BoardReset
	result = ics645BoardReset(hDevice);
	if (result != ICS645_OK)
	{
		return FALSE;
	}
	return CloseHandle(hDevice);
}
void print645Control(HANDLE hDevice)
{
	if (!bPrintRegister) return;
	ICS645_CONTROL deviceControl;
	int result = ics645ControlGet(hDevice, &deviceControl);
	if (result != ICS500R_OK)
	{
		return;
	}
	std::cout << "\n645 Control Setting\n";
	std::cout << "acqmode " << deviceControl.acqmode << "\n";
	std::cout << "adcmaster " << deviceControl.adcmaster << "\n";
	std::cout << "adcmode " << deviceControl.adcmode << "\n";
	std::cout << "adcterm " << deviceControl.adcterm << "\n";
	std::cout << "clksel " << deviceControl.clksel << "\n";
	std::cout << "diagenable " << deviceControl.diagenable << "\n";
	std::cout << "enable " << deviceControl.enable << "\n";
	std::cout << "extclk " << deviceControl.extclk << "\n";
	std::cout << "exttrig " << deviceControl.exttrig << "\n";
	std::cout << "filler1 " << deviceControl.filler1 << "\n";
	std::cout << "fpdpenable " << deviceControl.fpdpenable << "\n";
	std::cout << "fpdpmaster " << deviceControl.fpdpmaster << "\n";
	std::cout << "fpdpterm " << deviceControl.fpdpterm << "\n";
	std::cout << "fpdpwidth " << deviceControl.fpdpwidth << "\n";
	std::cout << "fpdp_ii " << deviceControl.fpdp_ii << "\n";
	std::cout << "inttrig " << deviceControl.inttrig << "\n";
	std::cout << "trigsel " << deviceControl.trigsel << "\n\n";
}

BOOL ConfigureICS645(int index, HANDLE hDevice, const std::vector<HANDLE>& h645s, int sampleRate, bool bUseFPDP, double dFPDPClock
	, bool bTriggerInternal, bool bClockInternal, bool bPACKED_DATA, bool bContinousMode, bool bFPDPInterface1
	, bool bUseClockOutput, bool bUseTriggerOutput, unsigned long decimation, unsigned long nChannelCount, unsigned long nBufferLength
	, unsigned long nAcquisitionCount, unsigned long FPDPFrameCount, int FPDPIndex, int FPDPChannelCount)
{
	ICS645_STAT Status;
	int         result;

	result = ics645Disable(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	result = ics645BoardReset(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	int overSamplingRatio = 0;
	if (sampleRate <= ICS645D_SAMPLE_RATE_MAX_8X)
	{
		overSamplingRatio = OVER_SAMPLING_8X;
	}
	else // ICS645D_SAMPLE_RATE_MAX_4X
	{
		overSamplingRatio = OVER_SAMPLING_4X;
	}

	if (index == 0)
	{
		double deviceADCActualClock = 0;
		if (sampleRate <= ICS645_SAMPLE_RATE_MAX_8X)
		{
			overSamplingRatio = OVER_SAMPLING_8X;
		}
		else if (sampleRate <= ICS645_SAMPLE_RATE_MAX_4X)
		{
			overSamplingRatio = OVER_SAMPLING_4X;
		}
		else if (sampleRate <= ICS645_SAMPLE_RATE_MAX_2X)
		{
			overSamplingRatio = OVER_SAMPLING_2X;
		}
		else   // ICS645_SAMPLE_RATE_MAX_1X
		{
			overSamplingRatio = OVER_SAMPLING_1X;
		}

		double deviceADCClock = 0;
		switch (overSamplingRatio)                        // 0:8x, 1:4x, 2:2x, 3:1x Oversampling
		{
		case OVER_SAMPLING_4X:
			deviceADCClock = (sampleRate * 4) / ((double)MEGAHERTZ_IN_HERTZ);
			break;
		case OVER_SAMPLING_2X:
			deviceADCClock = (sampleRate * 2) / ((double)MEGAHERTZ_IN_HERTZ);
			break;
		case OVER_SAMPLING_1X:
			deviceADCClock = sampleRate / ((double)MEGAHERTZ_IN_HERTZ);
			break;
		case OVER_SAMPLING_8X:
		default:
			deviceADCClock = (sampleRate * 8) / ((double)MEGAHERTZ_IN_HERTZ);
		}

		result = ics645ADCClkSet2(hDevice, &deviceADCClock, &deviceADCActualClock);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}

		if (bUseFPDP)
		{
			result = ics645FPDPClkSet(hDevice, &dFPDPClock);
			Sleep(millisecond);
			if (result != ICS645_OK)
			{
				return FALSE;
			}
		}

		Sleep(100);
	}

	ICS645_CONTROL deviceControl;
	ZeroMemory(&deviceControl, sizeof(ICS645_CONTROL));

	deviceControl.trigsel = (bTriggerInternal) ? ICS645_INTERNAL : ICS645_EXTERNAL;
	deviceControl.clksel = (bClockInternal) ? ICS645_INTERNAL : ICS645_EXTERNAL;
	deviceControl.diagenable = ICS645_DISABLE;
	deviceControl.enable = ICS645_DISABLE;
	deviceControl.fpdpenable = (bUseFPDP) ? ICS645_ENABLE : ICS645_DISABLE;

	// 0 : Packed Data (Two Samples in Word), 1 : Unpacked Data (One Sample in Word)
	deviceControl.fpdpwidth = (bPACKED_DATA) ? 0 : 1;

	if (h645s.size() == 1)
	{
		deviceControl.adcmaster = ICS645_ENABLE;
		deviceControl.adcterm = ICS645_ENABLE;
		deviceControl.fpdpmaster = ICS645_ENABLE;
		deviceControl.fpdpterm = ICS645_ENABLE;
	}
	else if (h645s.size() > 1)                            // Multi Board Configuration
	{
		if (index == 0)                                   // 1st Board : Master
		{
			deviceControl.adcmaster = ICS645_ENABLE;
			deviceControl.adcterm = ICS645_DISABLE;
			deviceControl.fpdpmaster = ICS645_ENABLE;
			deviceControl.fpdpterm = ICS645_ENABLE;
		}
		else if (index == h645s.size() - 1)                            // Last Board : End-Slave
		{
			deviceControl.adcmaster = ICS645_DISABLE;
			deviceControl.adcterm = ICS645_ENABLE;
			deviceControl.fpdpmaster = ICS645_DISABLE;
			deviceControl.fpdpterm = ICS645_DISABLE;
		}
		else                                                      // Middle Boards : Mid-Slaves
		{
			deviceControl.adcmaster = ICS645_DISABLE;
			deviceControl.adcterm = ICS645_DISABLE;
			deviceControl.fpdpmaster = ICS645_DISABLE;
			deviceControl.fpdpterm = ICS645_DISABLE;
		}
	}
	else // No Devices
	{
		return FALSE;
	}

	switch (overSamplingRatio)                        // 0:8x, 1:4x, 2:2x, 3:1x Oversampling
	{
	case OVER_SAMPLING_4X:
		deviceControl.adcmode = ICS645_4X;
		break;
	case OVER_SAMPLING_2X:
		deviceControl.adcmode = ICS645_2X;
		break;
	case OVER_SAMPLING_1X:
		deviceControl.adcmode = ICS645_1X;
		break;
	case OVER_SAMPLING_8X:
	default:
		deviceControl.adcmode = ICS645_8X;
	}

	deviceControl.acqmode = bContinousMode ? ICS645_CONTINUOUS : ICS645_CAPTURE;

	deviceControl.inttrig = ICS645_DISABLE;
	deviceControl.fpdp_ii = bFPDPInterface1 ? 0 : 1;  // 0:FPDP-I, 1 :FPDP-II
	deviceControl.extclk = (bUseClockOutput) ? ICS645_ENABLE : ICS645_DISABLE;
	deviceControl.exttrig = (bUseTriggerOutput) ? ICS645_ENABLE : ICS645_DISABLE;
	deviceControl.filler1 = 0;

	result = ics645ControlSet(hDevice, &deviceControl);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	//
	// ics645ImrSet
	ICS645_IMR imr;

	ZeroMemory(&imr, sizeof(imr));

	result = ics645ImrSet(hDevice, &imr);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	result = ics645DecimationSet(hDevice, &decimation);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	//
	// ics645ChannelCountSet
	result = ics645ChannelCountSet(hDevice, &nChannelCount);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	if (!bContinousMode)
	{
		//
		// ics645AcquireCountSet
		result = ics645AcquireCountSet(hDevice, &nAcquisitionCount);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}
	}

	//
	// ics645BufferLengthSet
	result = ics645BufferLengthSet(hDevice, &nBufferLength);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	if (bUseFPDP)
	{
		//
		// ics645FrameCountSet
		result = ics645FrameCountSet(hDevice, &FPDPFrameCount);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}
		ICS645_MASTER_CONTROL deviceMasterControl;
		//
		// ics645MasterControlSet
		ZeroMemory(&deviceMasterControl, sizeof(ICS645_MASTER_CONTROL));

		deviceMasterControl.board_addr = index;

		if (bPACKED_DATA)  // Packed Mode
			deviceMasterControl.channel_cnt = FPDPChannelCount / 2;
		else                                      // Unpacked Mode
			deviceMasterControl.channel_cnt = FPDPChannelCount;

		result = ics645MasterControlSet(hDevice, &deviceMasterControl);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}
	}

	//
	// ics645BufferReset
	result = ics645BufferReset(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}
	//
	// Reset ADC and Check Sync Error

	int iADCResetCount = 0;

	do{

		//
		// ics645ADCReset
		result = ics645ADCReset(hDevice);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}

		// Release Control flow to OS
		Sleep(0);

		//
		// Get Device Status
		result = ics645StatGet(hDevice, &Status);
		Sleep(millisecond);
		if (result != ICS645_OK)
		{
			return FALSE;
		}

		// Check Max Count
		if (iADCResetCount >= MAX_ADC_RESET_COUNT)
		{
		}

		iADCResetCount++;

	} while ((Status.syncerror == 1) && (iADCResetCount < MAX_ADC_RESET_COUNT));
	// ics645Enable
	//result = ics645Enable(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}
	print645Control(hDevice);
	return TRUE;
}

BOOL StartDeviceICS645(int index, HANDLE hDevice)
{
	int result;

	result = ics645Enable(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	if (index == 0)
	{
		result = ics645Trigger(hDevice);
		Sleep(millisecond);
		std::cout << "645 Triggered\n";
		if (result != ICS645_OK)
		{
			return FALSE;
		}
	}

	return TRUE;
}

BOOL EndDeviceICS645(int index, HANDLE hDevice)
{
	int result;

	//
	// ics645Disable
	result = ics645Disable(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	// 
	// ics645BoardReset
	result = ics645BoardReset(hDevice);
	Sleep(millisecond);
	if (result != ICS645_OK)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CloseICS500(HANDLE hDevice)
{
	int result;
	// ics500RBoardReset
	result = ics500RBoardReset(hDevice);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	return CloseHandle(hDevice);
}

void printControl(HANDLE hDevice)
{
	if (!bPrintRegister) return;

	ICS500R_CONTROL deviceControl;
	int result = ics500RControlGet(hDevice, &deviceControl);
	if (result != ICS500R_OK)
	{
		return;
	}
	std::cout << "\n500R Control Setting\n";
	std::cout << "corner_turning " << deviceControl.corner_turning << "\n";
	std::cout << "data_format " << deviceControl.data_format << "\n";
	std::cout << "filler1 " << deviceControl.filler1 << "\n";
	std::cout << "acquisition_mode " << deviceControl.acquisition_mode << "\n";
	std::cout << "board_enable " << deviceControl.board_enable << "\n";
	std::cout << "receiver_mode " << deviceControl.receiver_mode << "\n";
	std::cout << "trigger_source " << deviceControl.trigger_source << "\n";
	std::cout << "sequence_num_en " << deviceControl.sequence_num_en << "\n";
	std::cout << "fpdp_suspend " << deviceControl.fpdp_suspend << "\n";
	std::cout << "filler2 " << deviceControl.filler2 << "\n";
	std::cout << "trigger_edge " << deviceControl.trigger_edge << "\n";
	std::cout << "internal_trigger " << deviceControl.internal_trigger << "\n";
	std::cout << "frame_format " << deviceControl.frame_format << "\n\n";
}

BOOL ConfigureICS500(HANDLE hDevice, bool bCornerTurning, bool bPACKED_DATA, bool bContinuousMode, bool bFPDPReceiveMode
	, bool bTriggerInternal, bool bSequenceNumber, bool bFPDPSuspendControl, bool bFPDPStrobePECL, bool bEDGE_SENSITIVE
	, int nDataFrameFormat, unsigned long long nStartChannelNo, unsigned long long nSelectChannelNo, unsigned long long nBufferLength)
{
	int result;

	result = ics500RDisable(hDevice);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	result = ics500RBoardReset(hDevice);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}
	
	ICS500R_CONTROL       deviceControl;

	// ics500RControlSet
	deviceControl.corner_turning = (bCornerTurning) ? ICS500R_ENABLE : ICS500R_DISABLE;
	deviceControl.data_format = (bPACKED_DATA) ? ICS500R_FPDP_PACKED : ICS500R_FPDP_UNPACKED;
	deviceControl.filler1 = 0;
	deviceControl.acquisition_mode = (bContinuousMode) ? ICS500R_CONTINUOUS : ICS500R_ONESHOT;
	deviceControl.board_enable = ICS500R_DISABLE;
	deviceControl.receiver_mode = bFPDPReceiveMode ? ICS500R_FPDP_RECEIVER : ICS500R_FPDP_MASTER;  // FPDP-R


	deviceControl.trigger_source = (bTriggerInternal) ? ICS500R_INTERNAL : ICS500R_EXTERNAL;
	deviceControl.sequence_num_en = (bSequenceNumber) ? ICS500R_ENABLE : ICS500R_DISABLE;
	deviceControl.fpdp_suspend = (bFPDPSuspendControl) ? ICS500R_SUSPEND_ON : ICS500R_SUSPEND_OFF;
	deviceControl.strobe_selecting = (bFPDPStrobePECL) ? ICS500R_PECLCLK : ICS500R_TTLCLK;
	deviceControl.filler2 = 0;
	deviceControl.trigger_edge = (bEDGE_SENSITIVE) ? ICS500R_EDGE : ICS500R_LEVEL;
	deviceControl.internal_trigger = ICS500R_DISABLE;        // ICS500R_DISABLE       or ICS500R_ENABLE

	switch (nDataFrameFormat)
	{
	case FIXED_SIZE_REPEATING_FRAME_DATA:
		deviceControl.frame_format = ICS500R_FRAME_FIXED;
		break;
	case DYNAMIC_SIZE_REPEATING_FRAME_DATA:
		deviceControl.frame_format = ICS500R_FRAME_DYNAMIC;
		break;
	case SINGLE_FRAME_DATA:
		deviceControl.frame_format = ICS500R_FRAME_SINGLE;
		break;
	case UNFRAMED_DATA:
	default:
		deviceControl.frame_format = ICS500R_FRAME_UNFRAMED;
	}

	deviceControl.filler = 0;

	result = ics500RControlSet(hDevice, &deviceControl);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	//
	// ics500RStartChannelSet
	result = ics500RStartChannelSet(hDevice, &nStartChannelNo);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	//
	// ics500RSelectChannelSet
	result = ics500RSelectChannelSet(hDevice, &nSelectChannelNo);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	//
	// ics500RBufferLengthSet
	unsigned long long bufferlength = nBufferLength - (bSequenceNumber ? 0 : 1);
	result = ics500RBufferLengthSet(hDevice, &bufferlength);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	result = ics500RBufferReset(hDevice);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	result = ics500REnable(hDevice);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}
	printControl(hDevice);
	return TRUE;
}

BOOL StartDeviceICS500(HANDLE hDevice)
{
	if (ics500RTrigger(hDevice) != ICS500R_OK)
	{
		return FALSE;
	}
	Sleep(millisecond);
	std::cout << "500R Triggered\n";
	return TRUE;
}

BOOL EndDeviceICS500(HANDLE hDevice)
{
	int     result;
	// ics500Risable
	result = ics500RDisable(hDevice);
	Sleep(millisecond);
	if (result != ICS500R_OK)
	{
		return FALSE;
	}

	return TRUE;
}

struct DeviceRunner
{
	DeviceRunner(HANDLE pDevice, size_t nDataSize, std::ofstream* fout) : m_hDevice(pDevice), m_bRunning(true), data(nDataSize), nSaved(0), data2(nDataSize), pfout(fout) {}

	void operator()()
	{
		if (m_hDevice != INVALID_HANDLE_VALUE)
		{
			while (m_bRunning)
			{
				Run();
				boost::this_thread::sleep(boost::posix_time::milliseconds(1));
			}
		}
	}

	virtual void Run() {}

	void Reset() { m_bRunning = true; }

	void Stop() { m_bRunning = false; }

	bool IsRunning() { return m_bRunning; }

	size_t GetSavedByte() { return nSaved; }

protected:

	HANDLE  m_hDevice;

	std::vector<char> data;
	std::vector<char> data2;
	size_t nSaved;
	std::ofstream* pfout;
	bool m_bRunning;

};

void GetHeader(char header[512], long long datasize, double samplerate, int nchannels)
{
	time_t recordtime;
	time(&recordtime);
	tm* t = localtime(&recordtime);
	sprintf(header, "%d Bytes Recorded:Run Number: 0:Buffer Size: 512:Record Rate:%.1f:Header File##Channel Count:%d:Stream Type:%d:Record Start Date YYYYMMDD:%d%.2d%.2d:Record Start Time HHMMSS:%.2d%.2d%.2d ",
		(int)datasize, samplerate, nchannels, 4, t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
}

struct ICS645DeviceRunner : public DeviceRunner
{
	ICS645DeviceRunner(HANDLE pDevice, size_t nDataSize, std::ofstream* fout) : DeviceRunner(pDevice, nDataSize, fout) {}
	virtual void Run()
	{
		int WaitSeconds = 5;
		// ics645WaitADCInt 
		if (ICS645_OK == ics645WaitADCInt(m_hDevice, &WaitSeconds))
		{
			unsigned long nActualSize = 0;
			ReadFile(m_hDevice, &data[0], data.size(), &nActualSize, NULL);
			if (pfout)
			{
				std::cout << data.size() << " 645 Write\n";
				pfout->write(&data[0], data.size());
				pfout->flush();
				nSaved += nActualSize;
			}
		}
	}
};

struct ICS500DeviceRunner : public DeviceRunner
{
	ICS500DeviceRunner(HANDLE pDevice, size_t nDataSize, std::ofstream* fout, bool bSequence) : DeviceRunner(pDevice, nDataSize + (bSequence ? 8 : 0), fout), m_bSequence(bSequence), m_nFrames(-1) {}
	virtual void Run()
	{
		int WaitSeconds = 5;
		// ics500RWaitBufferReadInt 
		if (ICS500R_OK == ics500RWaitBufferReadInt(m_hDevice, &WaitSeconds))
		{
			if (m_bSequence) WriteSequence();
			else Write();
		}
		else
		{
			std::cout << "500 failed interrupt\n";
		}
		ICS500R_STATUS status;
		ICS500R_STATUS *pDeviceStatus = &status;
		int             result;
		result = ics500RStatusGet(m_hDevice, pDeviceStatus);
		if (result != ICS500R_OK)
		{
			std::cout << "ics500RStatusGet failed\n";
		}
		if (status.overflow_irq == 1)
		{
			std::cout << "overflow occured\n";
		}
	}

	void Write()
	{
		unsigned long nActualSize = 0;
		ReadFile(m_hDevice, &data[0], data.size(), &nActualSize, NULL);
		if (pfout)
		{
			std::cout << nActualSize << " 500 Write\n";
			pfout->write(&data[0], nActualSize);
			pfout->flush();
			nSaved += nActualSize;
		}
	}

	void WriteSequence()
	{
		unsigned long nActualSize = 0;
		ReadFile(m_hDevice, &data[0], data.size(), &nActualSize, NULL);
		unsigned long long nFrame = *(unsigned long long *)&data[0];
		bool bWrite = false;
		if (m_nFrames == -1)
		{
			m_nFrames = 0;
			bWrite = false;
		}
		else if (m_nFrames == 0)
		{
			m_nFrames = nFrame;
			bWrite = false;
		}
		else if (m_nFrames = nFrame - 1)
		{
			bWrite = true;
			m_nFrames = nFrame;
		}
		else
		{
			std::cout << "Missed frame " << nFrame - m_nFrames << "\n";
			m_nFrames = nFrame;
			bWrite = true;
		}
		nActualSize -= 8;
		if (pfout && bWrite)
		{
			std::cout << nActualSize << " 500 Write\n";
			pfout->write(&data[8], nActualSize);
			pfout->flush();
			nSaved += nActualSize;
		}
	}
	bool m_bSequence;
	long long m_nFrames;
};

#endif