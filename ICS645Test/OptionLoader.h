#ifndef OPTIONLOADER_H
#define OPTIONLOADER_H

#include <string>
#include <map>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

struct RAWFile
{
	RAWFile() : nDataSize(0) { }
	char header[512];
	std::vector<char> data;
	int nDataSize;
};

typedef std::pair<std::string, std::string> Attribute;

typedef std::map<std::string, std::string> Option;

class OptionLoader
{
public:

	static bool LoadOption(Option& option, const std::string& filename)
	{
		using boost::property_tree::ptree;
		ptree pt;
		option.clear();
		try
		{
			read_xml(filename, pt);
			BOOST_FOREACH(ptree::value_type &v, pt)
			{
				option[v.first.data()] = v.second.data();
			}
			return true;
		}
		catch (std::exception& e)
		{
			e.what();
			return false;
		}

	}

	static bool SaveOption(const Option& option, const std::string& filename)
	{
		using boost::property_tree::ptree;
		ptree pt;
		try
		{
			using boost::property_tree::ptree;
			ptree pt;
			for (Option::const_iterator itr = option.begin(), end = option.end(); itr != end;  ++itr)
			{
				pt.put(itr->first, itr->second);
			}
			write_xml(filename, pt);

			return true;
		}
		catch (...)
		{
			return false;
		}
	}

};

#endif