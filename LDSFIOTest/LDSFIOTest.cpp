﻿// LDSFIOTest.cpp : Defines the entry point for the console application.
//
#include "ldsf.h"

#include <iostream>
#include <windows.h>
#include "ldsf.h"
#include <memory>
#include <iostream>
#include <windows.h>
//
//// Logger class and instance (is set in main())
//class LocalLogging : public LDSF::Logging {
//public:
//	virtual void AddMessage(Severity sev, const std::wstring & msg) {
//		switch (sev) {
//		case Logging::TroubleMessage:
//			printf("\n[Trouble] %S\n", msg.c_str());
//			break;
//		case Logging::WarningMessage:
//			printf("\n[Warning] %S\n", msg.c_str());
//			break;
//		case Logging::InfoMessage:
//			printf("\n[Info] %S\n", msg.c_str());
//			break;
//		default:
//			break;
//		};
//	}
//};
//LocalLogging LoggerInstance;
//static LocalLogging *Logger = 0;
//
//void AddEventToFileAnnotation(LDSF::AnnotationDomainWriter fadw)
//{
//	LDSF::ChannelWriter fatime = fadw.getXAxisReference();
//	LDSF::ChannelWriter fannot = fadw.getEventChannel();
//
//	LDSF::NumericSliceWriter<LDSF::INT8> faTimeSlice = fatime.getSlice().getLossFreeSlice<LDSF::INT8>();
//	LDSF::TypedSliceWriter<LDSF::BYTE2> fannotSlice = fannot.getSlice().getTypedSlice<LDSF::BYTE2>();
//
//	LDSF::SliceIterator faTimeIter = faTimeSlice.begin();
//	LDSF::SliceIterator fannotIter = fannotSlice.begin();
//
//	static int tval = 13;
//	faTimeSlice.setValue(tval++, faTimeIter++);
//	fannotSlice.setValue(LDSF::EventUser2, fannotIter++);
//
//}
//
//void ReadFile(const char * fn, bool doNotPrintData);
//
//void EditFile(const char * fn)
//{
//	// Open the file for editing.
//	LDSF::FileReader reader(LDSF::toWString(fn),
//		Logger,
//		-1,     // default I/O buffer size
//		-1,     // default number of async I/O calls
//		true,   // allow threads
//		0.0);  // Do not use system file cache
//	LDSF::FileWriter editor = reader.editFile();
//
//	// In this example get the first data domain of the file.
//	LDSF::DomainWriterVec domainVec;
//	editor.getDataDomains(domainVec);
//	if (!domainVec.empty()) {
//		LDSF::DomainWriter domain = domainVec[0];
//		domain.setAbsoluteTime(L"2009-11-12 14:31:05 ms 987.654321");
//
//		// In this example get the first data channel of the domain.
//		LDSF::ChannelWriterVec channelVec;
//		domainVec[0].getDataChannels(channelVec);
//		if (!channelVec.empty()) {
//			LDSF::ChannelWriter channel = channelVec[0];
//
//			// Set a new channel name. Setting attributes is done the same way as it
//			// is done while writing a file. The same applies to domain and file
//			// attributes, and to application attributes of channels, domains and
//			// files.
//			channel.setChannelName(L"Renamed " + channel.getChannelName());
//
//			// Append some data to the x axis reference channel and the data
//			// channel. In this example the two channels are numeric, so there is no
//			// consideration about channel types. Appending data is done the same way
//			// as it is done while writing a file. The same applies to channel,
//			// domain and file annotations.
//			LDSF::NumericSliceWriter<double> timeSlice = domain.getXAxisReference().getSlice().getLossFreeSlice<double>();
//			LDSF::NumericSliceWriter<double> channelSlice = channel.getSlice().getLossFreeSlice<double>();
//			LDSF::SliceIterator timeIter = timeSlice.begin();
//			LDSF::SliceIterator channelIter = channelSlice.begin();
//			// setValueFromArray() works as well.
//			timeSlice.setValue(130177, timeIter++);
//			channelSlice.setValue(4212, channelIter++);
//		}
//	}
//
//	// Appending some annotations also is done the same way as it is done while
//	// writing a file.
//	AddEventToFileAnnotation(editor.getAnnotationDomain());
//
//	// The ReadFile() call has nothing to do with editing a file, it is only
//	// contained in this test program to see that the modifications are not
//	// visible until the editor is closed.
//	ReadFile(fn, false);
//
//	// Close the editor. This is also done by the destructor. But editing a file
//	// means writing to it, and this is the moment problems may occur. Thus it is
//	// better to call close() explicitely, since close() can throw exceptions,
//	// while the destructor never throws exceptions.
//	editor.close();
//}
//
//
//template <class TYPE>
//void WriteApplAttr(TYPE & obj, const std::wstring & name)
//{
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 1 (INT8)",
//		LDSF::INT8(12)));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 2 (FLOAT8)",
//		34.56));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 3 (RAT8)",
//		LDSF::RAT8(78, 90)));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 4 (WString)",
//		name + L" appl attr text ������ߵ����������������"));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 4a (empty WString)",
//		L""));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 4b (toWString(std::string))",
//		name + LDSF::toWString(" ������ߵ����������������")));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 5 (UTC)",
//		LDSF::UTC(1234567890)));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 6 (Unit)",
//		LDSF::Unit(LDSF::Quantity(name + L" Appl Quantity",
//		true,
//		2,
//		3,
//		4,
//		5,
//		6,
//		7,
//		8,
//		9),
//		10.0,
//		11.0,
//		12.0,  // ???
//		name + L" Appl Unit")));
//	LDSF::INT8Vec vVal;
//	vVal.push_back(1234);
//	vVal.push_back(5678);
//	vVal.push_back(9012);
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 7 (INT8Vec)",
//		vVal));
//	obj.setApplAttr(LDSF::ApplAttribute(name + L" Appl Attr 8 (HiResUTC)",
//		LDSF::HiResUTC(1234567890123456789)));
//}
//
//void WriteHeader(LDSF::FileWriter & file, int domainIdx,
//	LDSF::INT8 nrChannels,
//	int nrFrameValues)
//{
//	LDSF::INT8 count = 0;
//	std::wstring timeName = L"time_" + LDSF::toWString(domainIdx);
//	LDSF::Unit timeUnit(LDSF::Quantity(L"time", false, 0, 0, 1, 0, 0, 0, 0, 0), 1.0, 0.0, 10.0, L"s");
//	LDSF::EquidistantDataType timeType(timeUnit.toSI(0.0),	timeUnit.toSI(1.0));
//
//	LDSF::DomainWriter domain = file.createDomain(timeName,
//		timeType,
//		timeUnit,
//		nrFrameValues);
//	domain.setSession(L"Session_" + LDSF::toWString(domainIdx));
//
//	for (size_t chanIdx = 0;
//		chanIdx < static_cast<size_t>(nrChannels);
//		++chanIdx)
//	{
//		std::wstring channelName = L"channel_" + LDSF::toWString(count);
//
//		LDSF::Unit channelUnit(LDSF::Quantity(L"MV", false, 1, 1, -2, 0, 0, 0, 0, 0), 1.0, 0.0, 1.0, L"MV");
//		LDSF::NumericDataType<LDSF::FLOAT8> channelType;
//		LDSF::ChannelWriter channel = domain.createChannel(channelName,
//			channelType,
//			channelUnit);
//		channel.setPhysicalId(L"Physical" + LDSF::toWString(count));
//		channel.setPrimaryId(L"Primary" + LDSF::toWString(count));
//		channel.setSecondaryId(L"Secondary" + LDSF::toWString(count));
//		channel.setChannelGroup(static_cast<LDSF::ChannelGroupValues>(count % LDSF::FIRST_UNKNOWN_CHANNELGROUPVALUE));
//		channel.setChannelId(chanIdx);
//		//WriteApplAttr(channel, channelName);
//		++count;
//	}
//}
//
//void WriteFile(const char * fn)
//{
//	try {
//		std::shared_ptr<LDSF::FileWriter> ofile(new LDSF::FileWriter(LDSF::toWString(fn), Logger));
//		ofile->setApplication(L"WriteTest");
//		ofile->setApplicationComponent(L"My Component");
//
//		WriteHeader(*ofile.get(), 1, 3, 1000);
//
//		ofile->close();
//
//		//EditFile(fn);
//	}
//	catch (LDSF::Trouble & t) {
//		std::cout << "Trouble: " << t.what() << std::endl;
//		exit(1);
//	}
//}
//
//template<class TYPE>
//void printAttrList(TYPE & obj)
//{
//	LDSF::AttributeVec attributes;
//	obj.getAttrList(attributes);
//	for (LDSF::AttributeVec::const_iterator it = attributes.begin();
//		it != attributes.end(); ++it)
//		std::cout << *it << std::endl;
//
//	LDSF::ApplAttributeVec applAttributes;
//	obj.getApplAttrList(applAttributes);
//	for (LDSF::ApplAttributeVec::const_iterator it = applAttributes.begin();
//		it != applAttributes.end(); ++it)
//		std::cout << *it << std::endl;
//}
//
//template<class TYPE>
//void printBinData(LDSF::SliceReader & inputSlice)
//{
//	LDSF::TypedSliceReader<TYPE> typedInputSlice =
//		inputSlice.getTypedSlice<TYPE>();
//
//	for (LDSF::SliceIterator sliceIter = typedInputSlice.begin();
//		sliceIter != typedInputSlice.end();
//		++sliceIter) {
//		TYPE v;
//		typedInputSlice.getValue(sliceIter, v);
//		std::cout << int(v) << " ";
//	}
//	typedInputSlice.release();
//}
//
//void printChannel(LDSF::FileReader & ifile,
//	const LDSF::ChannelReader & cr,
//	bool doNotPrintData);
//
//void printAnnotationDomain(LDSF::FileReader & ifile,
//	const LDSF::AnnotationDomainReader & adr,
//	bool doNotPrintData)
//{
//
//	if (adr.isInitialized()) {
//		std::cout << std::endl << "Annotation Domain:" << std::endl;
//		printAttrList(adr);
//		std::cout << std::endl << "Annotation Channels:" << std::endl;
//		printChannel(ifile, adr.getXAxisReference(), doNotPrintData);
//		printChannel(ifile, adr.getEventChannel(), doNotPrintData);
//	}
//}
//
//void printChannel(LDSF::FileReader & ifile,
//	const LDSF::ChannelReader & cr,
//	bool doNotPrintData)
//{
//	std::cout << std::endl << "Channel Attributes: " << std::endl;
//	printAttrList(cr);
//
//	const LDSF::DataType * dt = cr.getDataType();
//	std::cout << "Channel Type: ";
//	switch (static_cast<LDSF::DataTypeId>(dt->getDataTypeId())) {
//	case LDSF::DT_Equidistant: std::cout << "Equidistant"; break;
//	case LDSF::DT_BYTE1:       std::cout << "BYTE1";       break;
//	case LDSF::DT_BYTE2:       std::cout << "BYTE2";       break;
//	case LDSF::DT_INT1:        std::cout << "INT1";        break;
//	case LDSF::DT_INT2:        std::cout << "INT2";        break;
//	case LDSF::DT_INT3:        std::cout << "INT3";        break;
//	case LDSF::DT_INT4:        std::cout << "INT4";        break;
//	case LDSF::DT_INT8:        std::cout << "INT8";        break;
//	case LDSF::DT_FLOAT4:      std::cout << "FLOAT4";      break;
//	case LDSF::DT_FLOAT8:      std::cout << "FLOAT8";      break;
//	case LDSF::DT_RAT8:        std::cout << "RAT8";        break;
//	case LDSF::DT_UTC:         std::cout << "UTC";         break;
//	default:
//		std::cout << "unknown data type id '" << dt->getDataTypeId() << "'";
//		break;
//	}
//	std::cout << std::endl;
//
//	if (dt->isEquidistant()) {
//		const LDSF::EquidistantDataType * edt =
//			dynamic_cast<const LDSF::EquidistantDataType*>(dt);
//		std::cout << "Equidistant Channel: start="
//			<< cr.getUnit().toUnit(edt->getStart())
//			<< ", increment="
//			<< cr.getUnit().toUnit(edt->getIncrement())
//			<< std::endl;
//	}
//	if (dt->isIntegral()) {
//		const LDSF::DataTypeIntegral * idt =
//			dynamic_cast<const LDSF::DataTypeIntegral*>(dt);
//		std::cout << "Integral Channel: scale="
//			<< idt->getScale()
//			<< ", offset="
//			<< idt->getOffset()
//			<< " (relating to SI value range)"
//			<< std::endl;
//	}
//
//	LDSF::ChannelReaderVec ichan;
//	ichan.push_back(cr);
//	LDSF::ReadClosure readClosure =
//		ifile.createReadClosure(ichan, LDSF::forward);
//
//	LDSF::ReadResult readResult;
//	std::cout << cr.getNumberOfValues() << " values: ";
//
//	if (!doNotPrintData) {
//		for (;;) {
//			LDSF::SliceReader inputSlice;
//			readResult = readClosure.readSlice(inputSlice);
//
//			if (readResult == LDSF::noDataAvailable) {
//				break;
//			}
//
//			if (dt->isNumeric()) {
//				LDSF::NumericSliceReader<double> numericInputSlice =
//					inputSlice.getLossySlice<double>();
//
//				for (LDSF::SliceIterator sliceIter = numericInputSlice.begin();
//					sliceIter != numericInputSlice.end();
//					++sliceIter) {
//					double v;
//					numericInputSlice.getValue(sliceIter, v);
//					std::cout << v << " ";
//				}
//				numericInputSlice.release();
//
//			}
//			else {
//				switch (dt->getDataTypeId()) {
//				case LDSF::DT_BYTE1: printBinData<LDSF::BYTE1>(inputSlice); break;
//				case LDSF::DT_BYTE2: printBinData<LDSF::BYTE2>(inputSlice); break;
//				case LDSF::DT_Equidistant:
//				case LDSF::DT_INT1:
//				case LDSF::DT_INT2:
//				case LDSF::DT_INT3:
//				case LDSF::DT_INT4:
//				case LDSF::DT_INT8:
//				case LDSF::DT_FLOAT4:
//				case LDSF::DT_FLOAT8:
//				case LDSF::DT_RAT8:
//				case LDSF::DT_UTC:
//					break;
//				}
//			}
//		}
//	}
//	std::cout << std::endl;
//
//
//	try {
//		printAnnotationDomain(ifile, cr.getAnnotationDomain(), doNotPrintData);
//	}
//	catch (LDSF::TroubleWithUndefinedValue) {
//		// This means that there are no channel annotations: ignore.
//	}
//}
//
//void ReadFile(const char * fn, bool doNotPrintData)
//{
//	try {
//		LDSF::FileReader ifile(LDSF::toWString(fn),
//			Logger,
//			-1,     // default I/O buffer size
//			-1,     // default number of async I/O calls
//			true,   // allow threads
//			0.0);  // Do not use system file cache
//		std::cout << "General Attributes: " << std::endl;
//		printAttrList(ifile);
//
//		LDSF::DomainReaderVec domainList;
//		ifile.getDataDomains(domainList);
//		for (LDSF::DomainReaderVec::iterator domainIter = domainList.begin();
//			domainIter != domainList.end();
//			++domainIter) {
//
//			std::cout << std::endl << "Domain Attributes: " << std::endl;
//			printAttrList(*domainIter);
//
//			LDSF::ChannelReaderVec channelList;
//			domainIter->getDataChannels(channelList);
//			channelList.insert(channelList.begin(),
//				domainIter->getXAxisReference());
//			for (LDSF::ChannelReaderVec::iterator channelIter = channelList.begin();
//				channelIter != channelList.end();
//				++channelIter)
//				printChannel(ifile, *channelIter, doNotPrintData);
//
//		}
//		try {
//			printAnnotationDomain(ifile, ifile.getAnnotationDomain(), doNotPrintData);
//		}
//		catch (LDSF::TroubleWithUndefinedValue) {
//			// This means that there are no file annotations: ignore.
//		}
//
//		//ifile.close();
//	}
//	catch (LDSF::Trouble & t) {
//		std::cout << "Trouble: " << t.what() << std::endl;
//		exit(1);
//	}
//}
//
//#define eq(a,b) (strcmp(a,b)==0)
//
#include "../DeviceLib/VirtualDevice.h"

int main(int argc, const char ** argv)
{
	std::string fn = "s:\\WriteTest.ldsf";
	//DeviceLib::VirtualDevice device;
	//device.ReadData("C:\\Simple Record\\Record\\Record003_0.rawfs");
	//device.SaveLDSF();

	//DeleteFileA(fn);
	//WriteFile(fn.c_str());
	//ReadFile(fn, true);
	return 0;
}