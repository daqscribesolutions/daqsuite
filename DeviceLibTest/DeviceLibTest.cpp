// DeviceLibTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../DeviceLib/DeviceLib.h"
#include <string>
#include <ctime>
#include <iostream>
#include "../DeviceLib/DeviceDataReader.h"
#include "../DeviceLib/VirtualDevice.h"
using namespace DeviceLib;
struct ConsoleOut : public IDeviceDataUpdateListener
{
	ConsoleOut(DeviceLib::ChannelInfo* in) : info(in) {}
	void UpdateEntryData(DeviceLib::DeviceData* data)
	{

	}

	void UpdateExitData(DeviceData*)
	{
		char* data = info->GetData()->GetADCCountData();
		for (size_t i = 0; i < info->GetDataSize(); ++i)
		{
			std::cout << data[i] << " ";
		}
		std::cout << "\n";
	}

	bool StartListening(){ return true; }

	void StopListening(){}

	DeviceLib::ChannelInfo* info;

};
int main(int argc, const char * argv[])
{
	//SystemManager::GetInstance()->AddVirtualDevice();
	//DeviceRecordSetting::GetInstance()->SetDeviceName("DeviceTest");
	//const std::vector<IDevice*>& devices = SystemManager::GetInstance()->GetDevices();
	//IADCDevice* pdevice = static_cast<IADCDevice*>(devices[0]);
	//ConsoleOut * console = new ConsoleOut(pdevice->GetChannels()[0]);
	//devices[0]->GetDeviceDataManager()->AddUpdateDataListener(console);
	//if (!devices.empty())
	//{
	//	SystemManager::GetInstance()->StartMonitor();
	//	SystemManager::GetInstance()->StartRecord();
	//	double d = clock();
	//	while (clock() - d < 4000)
	//	{
	//	}
	//	SystemManager::GetInstance()->StopRecord();
	//	//d = clock();
	//	//while (clock() - d < 1000)
	//	//{
	//	//}
	//	SystemManager::GetInstance()->StopMonitor();
	//}
	VirtualDevice device(32, FOUR);
	DeviceDataReader reader("C:\\data\\Record_1_0.rawfs");
	if (reader.IsOpen())
	{
		DeviceData* data = reader.GetDeviceData();
		device.SetSampleRatePerSecond(data->GetSampleRatePerSecond());
		device.Initialize();
		std::vector<std::vector<double> > EUValues;
		DataConverter dataconverter(data, 0, false);
		long long samples = 0;
		while (true)
		{
			dataconverter.ConvertADCtoMilivoltAndEUValue(EUValues, device.GetChannels());
			samples += EUValues[0].size();
			for (int i = 0; i < (int)EUValues[0].size(); i += 100)
			{
				std::cout << EUValues[0][i] << "\n";
			}
			std::cout << samples << "\n\n";
			if (!reader.HasMoreData())
			{
				break;
			}
			reader.ReadNext();
		}
	}

	return 0;
}