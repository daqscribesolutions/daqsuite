#ifndef NETWORKDEVICE_H
#define NETWORKDEVICE_H

#include "pcap_header.h"
#if defined(WIN32) || defined (WIN64)
#define snprintf    _snprintf
#endif

#include "INetworkDevice.h"

namespace NetworkLib
{
	//kjk
	const int NUM_BUFFER = 2;
	/**
	 * NetworkDevice implementation of INetworkDevice for generic network devices with pcap file support.
	 */
	class NetworkDevice : public INetworkDevice
	{

	public:

		/**
		 * Full constructor.
		 * @param d The handle for the pcap device information.
		 */
		NetworkDevice(
#ifdef PCAPINCLUDE
			const pcap_if_t *d
#endif
		);

		/// Default destructor.
		~NetworkDevice();

		/// Implementation of INetworkDevice.
		virtual void Monitor();

		virtual void Stop();
		
		virtual void StopThread();

		virtual void HandlePacket();

		virtual void RecordBegin();

		virtual void RecordEnd();

		/// Write supports pcap file saving.
		void Write();

	private:

		/// The name of device.
		std::string DeviceName;

		/// The thread to write process separate from receiving thread.
		std::unique_ptr<boost::thread> m_WriteThread;

		/// Write operation functor.
		struct WriteRunner;
		std::unique_ptr<WriteRunner> m_WriteRunner;

#ifdef PCAPINCLUDE
		/// pcap device handle.
		pcap_t * adhandle;

		/// pcap file hande.
		pcap_t * filehandle;

		/// pcap file handle.
		pcap_dumper_t * dumpfile;

		/// The packet header to be saved.
		pcap_pkthdr *header[NUM_BUFFER];

		/// The packet data to be saved.
		const u_char *pkt_data[NUM_BUFFER];

		/// Current duation of record.
		double duration;
#endif
	};
}

#endif