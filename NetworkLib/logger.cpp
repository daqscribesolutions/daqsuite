#include "logger.h"
#include <ctime>

#ifdef WIN32
#pragma warning(disable : 4996)
#endif

namespace NetworkLib
{

	Logger* Logger::instance = 0;

	Logger* Logger::GetInstance()
	{
		if (instance == 0)
		{
			instance = new Logger();
		}
		return instance;
	}

   void Logger::Clear()
   {
      delete instance;
      instance = 0;
   }

	Logger::Logger()
	{
	}

	Logger::~Logger()
	{
	}

	void Logger::Log(const std::string& mss)
	{
		std::string log = TimeStamp() + mss + "\n";
		logs += log;
		for (size_t i = 0; i < loggers.size(); ++i)
		{
			loggers[i]->Log(log);
		}
	}

	void Logger::AddLog(ILog* log)
	{
		if (log)
		{
			loggers.push_back(log);
		}
	}

	void Logger::Log(const std::string& mss, double val)
	{
		std::string log = mss + " : " + std::to_string((long double)val);
		Log(log);
	}

	void Logger::Log(const std::string& mss, int val)
	{
		std::string log = mss + " : " + std::to_string((long long)val);
		Log(log);
	}

	std::string Logger::TimeStamp()
	{
		time_t timer;
		char buffer[50] = { '\0' };
		struct tm* tm_info;
		time(&timer);
		tm_info = localtime(&timer);
		strftime(buffer, 25, "%Y-%m-%d %H-%M-%S", tm_info);
		return std::string(buffer);
	}

	std::string Logger::GetLog()
	{
		return logs;
	}
}
