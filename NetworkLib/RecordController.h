#ifndef RECORDCONTROLLER_H
#define RECORDCONTROLLER_H

#include <string>
#include <fstream>
#include <Windows.h>
#include <vector>
#include <chrono>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include <memory>

namespace NetworkLib
{

	/**
	* RecordController provides the functionality of current record state for each port
	* and asynchrronious file record and save the record tag file.
	*/
	class RecordController
	{

	public:

		/// Default constructor.
		RecordController();

		/// Destructor.
		~RecordController();

		/**
		* Open initializes the record file if it is using asynchronious file write and open record tag file.
		* @param record The flag to record/relay.
		*/
		void Open(bool bRead);

		/// Close closes the record.
		void Close();

		/// Accessor for the file name.
		std::string GetFileName();

		/**
		* AddTagToRecord saves a tag to the record tag file.
		* @param tagidx The tag index to be written.
		*/
		void AddTagToRecord(int tagidx, bool bset);

		unsigned long long AddMetaTagToRecord(unsigned long long tagidx, bool bset, const std::string& time);

		/**
		* Write saves the data asynchroniously.
		* @param data The data to be stored.
		* @param size The data size to be stored.
		*/
		void Write(char* data, unsigned int size);

		bool ReadNext(std::string& buffer);

		void Flush();

		void FlushRemain();

		/// The getter for the file status.
		bool IsOpen() { return hFile != INVALID_HANDLE_VALUE; }

		/// NextFile increases the file index and get the file.
		void NextFile();

		void NextFileStdio();

		/// GetDurationFromTags reads the tag file and get the total duration of the record. 
		void GetDurationFromTags(bool tag);

		/// The number of record for each port.
		int RecordNumber;

		/// The recorded data size.
		long long TotalData;

		unsigned long long LastMinuteData;

		/// The record port id. Usually port + 1.
		int ID;

		/// The relay port id. Usually port + 1.
		int RelayID;

		/// The flag of recording.
		bool Running;

		/// Header storage to support the divided files.
		std::vector<char> FileHeader;

		/// The relay offset time in nanosecond.
		unsigned long long RelayOffset;

		/// The relay duration time in nanosecond.
		unsigned long long RelayDuration;

		/// The recorded time in nanosecond.
		unsigned long long TotalDuration;

		unsigned long long TotalDurationTag;

		/// The name of file to be saved or to be read.
		std::string filename;

		std::string error;

		bool DoesExist();

		/// The relay duration time in nanosecond.
		unsigned long long RecordDuration;

		unsigned long long RemainRecordDuration();

		void TagFileOperation(bool begin);

		void SetReplaySessionByTime(unsigned long long b, unsigned long long d);

		void SetReplaySessionBySize(unsigned long long b, unsigned long long d);

		void SetReplaySessionByTag(unsigned long long b, unsigned long long d);

		unsigned long long timestamp;

	private:

		/// The file index to keep track of multiple files.
		int nFiles;

		size_t  n_size;

		/// The tag file stream.
		std::ofstream tag_out;

		/// The tag file stream.
		std::ofstream seg_out;

		std::ifstream seg_info;

		unsigned int headerlen;

		/// The record begin time.
		unsigned long long begin_time;

		/// The record begin time.
		unsigned long long record_begin_time;

		/// Handle for the asychronious file.
		HANDLE hFile;

		FILE *fp;
		FILE *fc;

		long long readdatasize;

		/// The data size to keep track of overlap writing.
		unsigned long long datasize;

		/// The file location to keep.
		OVERLAPPED overlapped_structure;

		/// DMA thread.
		std::shared_ptr<boost::thread> m_IOThread;

		/// DMA thread functor.
		struct Runner;
		std::shared_ptr<Runner> m_IORunner;

		/// The buffer for DMA operation.
		std::vector<char*> buffer;

		std::vector<char*> leftbuffer;

		std::vector<unsigned long long> remainBufferSize;

		unsigned long long leftsize;

		unsigned long long n_current;

		unsigned long long n_written;

		unsigned long long last_written;

		long long second_keep;

		unsigned long long max_file_size;

		DWORD flag;

		unsigned long long MAX_NUM_BUFFER_FILE;

		bool m_bRead;

		bool m_bHeader;

		long long replybegin;
		long long replyduration;
		long long replycurrent;
		long long replydatasize;

		unsigned long NUM_BUFFERS;
		unsigned long BUFFER_SIZE;
		unsigned long SINGLE_BUFFER;

	};

}

#endif
