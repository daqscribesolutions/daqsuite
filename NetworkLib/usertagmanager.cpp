#include "usertagmanager.h"
#include "DeviceRecordSetting.h"
#include "logger.h"
#include <ctime>

namespace NetworkLib
{

	UserTagManager* UserTagManager::instance = 0;

	UserTagManager * UserTagManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new UserTagManager();
		}
		return instance;
	}

	void UserTagManager::Insert(int idx)
	{
		std::vector<UserTag> tmp;
		size_t s = Tags.size();
		for (int i = 0; i < (int)Tags.size(); ++i)
		{
			if (idx == i)
			{
				tmp.push_back(UserTag());
			}
			tmp.push_back(Tags[i]);
		}
		if (idx >= (int)Tags.size())
		{
			tmp.push_back(UserTag());
		}
		Tags = tmp;
		if (s == Tags.size())
		{
			Tags.push_back(UserTag());
		}
	}

	void UserTagManager::Remove(int idx)
	{
		std::vector<UserTag> tmp;
		for (int i = 0; i < (int)Tags.size(); ++i)
		{
			if (idx != i)
			{
				tmp.push_back(Tags[i]);
			}
		}
		Tags = tmp;
	}

}
