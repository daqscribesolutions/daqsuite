#include "NTNetworkDevice.h"
#include <Windows.h>
#include "logger.h"
#include "RecordController.h"

namespace NetworkLib
{

	NTNetworkDevice::NTNetworkDevice()
		: INetworkDevice()
	{
		Name = "NT-20E2-CH";
		FileName = "";
	}

	NTNetworkDevice::~NTNetworkDevice()
	{
	}

	void NTNetworkDevice::Monitor()
	{
		//kjk
		log = LogManager::GetInstance();
		

		Status = "";
		m_nSavedPackets = 0;
		ErrorInfo.clear();
		m_SpeedMonitor.Reset();
		firstPacket = 1;
		status = NT_SUCCESS;
		if (!m_bActive) return;
#ifdef NPTINCLUDE
		bReplay = false;
		if (m_runOption == READ_FROM_DEVICE)
		{
			// Open a config stream to assign a filter to a stream ID.
			if ((status = NT_ConfigOpen(&hCfgStream, Name.c_str())) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_ConfigOpen() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			std::string ntpl = "Assign[streamid=" + std::to_string(long long(GetID()))
				+ ";color=7] = port == " + std::to_string(long long(GetID() - 1));
			// Assign traffic to stream ID 1 and mask all traffic matching the assign statement color=7.
			if ((status = NT_NTPL(hCfgStream, ntpl.c_str(), &ntplInfo, NT_NTPL_PARSER_VALIDATE_NORMAL)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_NTPL() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			// Close the config stream
			if ((status = NT_ConfigClose(hCfgStream)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_ConfigClose() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			// Used if PCAP header used
			pTs = (struct ntpcap_ts_s *)(void *)&(ntplInfo.ts);
			// Get a stream handle with stream ID 1. NT_NET_INTERFACE_SEGMENT specify that we will receive data in a segment based matter.
			if ((status = NT_NetRxOpen(&hNetRx, Name.c_str(), NT_NET_INTERFACE_SEGMENT, GetID(), -1)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_NetRxOpen() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
		}
#endif
		if (m_Runner) m_Runner.release();
		m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
		m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));
	}

	void NTNetworkDevice::Stop()
	{

		//kjk
		log = LogManager::GetInstance();

		if (!m_bActive) return;
		if (m_Runner) m_Runner->Stop();
#ifdef NPTINCLUDE
		if (m_runOption == READ_FROM_DEVICE)
		{
			// Open a config stream to delete a filter.
			if ((status = NT_ConfigOpen(&hCfgStream, "TestStream")) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_ConfigOpen() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			char tmpBuffer[20];
			// Delete the filter
			snprintf(tmpBuffer, 20, "delete=%d", ntplInfo.ntplId);
			if ((status = NT_NTPL(hCfgStream, tmpBuffer, &ntplInfo, NT_NTPL_PARSER_VALIDATE_NORMAL)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_NTPL() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			// Close the config stream
			if ((status = NT_ConfigClose(hCfgStream)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_ConfigClose() failed : " + std::string(errorBuffer);
				log->WriteLog(ErrorInfo);
				return;
			}
			// Close the stream and release the hostbuffer. This will also remove the NTPL assignments performed.
			NT_NetRxClose(hNetRx);
		}
#endif
	}

	void NTNetworkDevice::HandlePacket()
	{
		//kjk
		if (recorder->error != "")
		{
			if (check)
			{
				ErrorInfo = recorder->error;
				check = false;
			}
		}

		if (!m_bActive) return;
		//if (!ErrorInfo.empty() || status != NT_SUCCESS) return;
		if (status != NT_SUCCESS) return;
#ifdef NPTINCLUDE
		
		int numSegments = 0;              // The number of segments received
		int numBytes = 0;                 // The number of bytes received
		if (m_runOption == READ_FROM_DEVICE)
		{
			if (firstPacket == 1)
			{
				// Read the file header.
				readCmd.cmd = NT_NETRX_READ_CMD_GET_FILE_HEADER;
				if ((status = NT_NetRxRead(hNetRx, &readCmd)) != NT_SUCCESS)
				{
					NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
					ErrorInfo = "NTNetworkDevice::HandlePacket() NT_NetRxRead() failed : " + std::string(errorBuffer);
					log->WriteLog(ErrorInfo);
					return;
				}
				recorder->FileHeader.resize(readCmd.u.fileheader.size);
				memcpy(&recorder->FileHeader[0], readCmd.u.fileheader.data, readCmd.u.fileheader.size);
				firstPacket = 0;
			}
			//else
			{
				if ((status = NT_NetRxGet(hNetRx, &hNetBuf, 1000)) != NT_SUCCESS)
				{
					if ((status == NT_STATUS_TIMEOUT) || (status == NT_STATUS_TRYAGAIN))
					{
						// Timeouts are ok, we just need to wait a little longer for a segment
						return;
					}
					// Get the status code as text
					NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
					return;
				}
				// We got a segment. Check if the timestamp is newer than when the NTPL assign command was applied
				// If PCAP header configured, we need to convert the timestamps received
				if (NT_NET_GET_SEGMENT_TIMESTAMP_TYPE(hNetBuf) == NT_TIMESTAMP_TYPE_PCAP)
				{
					if ((((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->sec * 1000000
						+ ((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->usec) >
						(pTs->sec * 1000000 + pTs->usec))
					{
						return; // Break out, we have received a segment that is received after the NTPL assign command was applied
					}
				}
				else
				{
					if (NT_NET_GET_SEGMENT_TIMESTAMP_TYPE(hNetBuf) == NT_TIMESTAMP_TYPE_PCAP_NANOTIME)
					{
						if ((((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->sec * 1000000000
							+ ((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->usec) >
							(pTs->sec * 1000000000 + pTs->usec))
						{
							return; // Break out, we have received a segment that is received after the NTPL assign command was applied
						}
					}
					recorder->Write((char*)NT_NET_GET_SEGMENT_PTR(hNetBuf), NT_NET_GET_SEGMENT_LENGTH(hNetBuf));
					// Increment the number of segments processed.
					numSegments++;
					// Increment the bytes received
					numBytes += NT_NET_GET_SEGMENT_LENGTH(hNetBuf);
				}

				recorder->timestamp = _NT_NET_GET_PKT_TIMESTAMP(hNetBuf);

				// Release the current segment
				if ((status = NT_NetRxRelease(hNetRx, hNetBuf)) != NT_SUCCESS)
				{
					// Get the status code as text
					NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
					return;
				}
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			Replay();
		}
		if (numBytes > 0)
		{
			m_SpeedMonitor.UpdateData(numBytes);
			m_SpeedMonitor.EndSample();
			m_SpeedMonitor.BeginSample();
		}
#endif
	}

	void NTNetworkDevice::StopThread()
	{
		if (m_Thread)
		{
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			m_Thread.release();
		}
	}

	void NTNetworkDevice::RecordBegin()
	{
		ErrorInfo = "";
		check = true;

		if (!m_bActive) return;
#ifdef NPTINCLUDE
		if (recorder->Running || status != NT_SUCCESS) return;

		if (m_runOption == WRITE_TO_DEVICE)
		{
			if (hNetTx != 0)
			{
				// Close the TX stream
				NT_NetTxClose(hNetTx);
				hNetTx = 0;
			}
			firstPacket = 1;
			if (ReplayFlag == SECOND)
			{
				recorder->SetReplaySessionByTime(recorder->RelayOffset, recorder->RelayDuration);
			}
			else
			{
				recorder->SetReplaySessionByTag(recorder->RelayOffset, recorder->RelayDuration);
			}
			bReplay = true;
		}

		recorder->filename = FileName;
		if (m_runOption != WRITE_TO_DEVICE)
		{
			recorder->Open(false);
		}
		else
		{
			// Open the infostream.
			if ((status = NT_InfoOpen(&hInfo, "replay")) != NT_SUCCESS)
			{
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				return;
			}

			// Check whether or not absolut TX timing is supported
			infoRead.cmd = NT_INFO_CMD_READ_ADAPTER_V6;
			infoRead.u.adapter_v6.adapterNo = 0;  // Adapter is hardcoded to adapter 0 as the TX port is hardcoded to port 0
			if ((status = NT_InfoRead(hInfo, &infoRead)) != NT_SUCCESS)
			{
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				NT_InfoClose(hInfo);
				return;
			}

			// Get the TX mode
			txTiming = infoRead.u.adapter_v6.data.txTiming;

			NT_InfoClose(hInfo);
		}
#endif
	}

	void NTNetworkDevice::RecordEnd()
	{
		//kjk
		if (!recorder->Running) return;
		recorder->Close();
		Sleep(100);
#ifdef NPTINCLUDE
		if (m_runOption == WRITE_TO_DEVICE)
		{
			Sleep(100);
			if (hNetTx != 0)
			{
				// Close the TX stream
				NT_NetTxClose(hNetTx);
				hNetTx = 0;
			}
			bReplay = false;
		}
		if (recorder->error != "")
		{
			ErrorInfo = recorder->error;
			log->WriteLog(ErrorInfo);
		}
#endif
	}

	void NTNetworkDevice::Replay()
	{
#ifdef NPTINCLUDE
		if (!bReplay) return;
		bReplay = false;
		recorder->Open(true);
		if (hNetTx != 0)
		{
			// Close the TX stream
			NT_NetTxClose(hNetTx);
			hNetTx = 0;
		}
		
		std::string buffer;
		// Open a TX hostbuffer on NUMA node 0 that can transmit to port 0
		int port = GetID() - 1;
		if ((status = NT_NetTxOpen(&hNetTx, "TxStreamPort", 1 << port, NTNetworkDeviceManager::GetInstance()->numaIdx, 0)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}

		while (recorder->ReadNext(buffer))
		{
			if (buffer.size() == 0) continue;
			//if (false)
			{
				// Get a TX buffer for this segment
			if ((status = NT_NetTxGet(hNetTx, &hNetBufTx, port, buffer.size(), NT_NETTX_SEGMENT_OPTION_RAW, -1 /* wait forever */)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				break;
			}

			// Copy the segment into the TX buffer
			memcpy(NT_NET_GET_SEGMENT_PTR(hNetBufTx), &buffer[0], buffer.size());

				// Build a packet netbuf structure
				if (firstPacket == 1)
				{
					_nt_net_build_pkt_netbuf(hNetBufTx, &pktNetBuf);
					if (txTiming == NT_TX_TIMING_ABSOLUTE)
					{
						// Absolute TX mode is supported.
						// If transmit tx relative is disabled i.e. we are using absolut transmit and
						// the txclock must be synched to the timestamp in the first packet.
						NT_NET_SET_PKT_TXNOW((&pktNetBuf), 0);                    // Wait for tx delay before the packet is sent
						NT_NET_SET_PKT_TXSETCLOCK((&pktNetBuf), 1);               // Synchronize tx clock to timestamp in first packet.
					}
					else
					{
						// Legacy mode/Relative tx timing.
						// First packet must be sent with txnow=1
						NT_NET_SET_PKT_TXNOW((&pktNetBuf), 1);                    // Send the first packet now
					}
					firstPacket = 0;
				}

				// Release the TX buffer and the packets within the segment will be transmitted
				if ((status = NT_NetTxRelease(hNetTx, hNetBufTx)) != NT_SUCCESS)
				{
					// Get the status code as text
					NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
					break;
				}
			}
			m_SpeedMonitor.UpdateData(buffer.size());
			m_SpeedMonitor.EndSample();
			m_SpeedMonitor.BeginSample();
			recorder->TotalData += buffer.size();
		}
		recorder->Running = true;
		recorder->Close();
		Sleep(100);
		if (hNetTx != 0)
		{
			// Close the TX stream
			NT_NetTxClose(hNetTx);
			hNetTx = 0;
		}
		if (recorder->error != "")
		{
			ErrorInfo = recorder->error;
			log->WriteLog(ErrorInfo);
		}
#endif
	}

	NTNetworkDeviceManager * NTNetworkDeviceManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new NTNetworkDeviceManager();
		}
		return instance;
	}

	void NTNetworkDeviceManager::Init()
	{
		m_bInitialized = false;
		numaIdx = 0;
#ifdef NPTINCLUDE
		NtInfoStream_t hInfoStream;     // Info stream handle
		NtInfo_t hInfo;                 // Info handle
		char errorBuffer[NT_ERRBUF_SIZE];         // Error buffer
		int status;                   // Status variable
		numAdapters = 0;
		// Initialize the NTAPI library and thereby check if NTAPI_VERSION can be used together with this library
		if ((status = NT_Init(NTAPI_VERSION)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		// Open the info stream
		if ((status = NT_InfoOpen(&hInfoStream, "Info")) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		// Read the system info
		hInfo.cmd = NT_INFO_CMD_READ_SYSTEM;
		if ((status = NT_InfoRead(hInfoStream, &hInfo)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		nPorts = hInfo.u.system.data.numPorts;
		numAdapters = hInfo.u.system.data.numAdapters;
		// Close the info stream
		if ((status = NT_InfoClose(hInfoStream)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
#endif
		m_bInitialized = true;
	}

	bool NTNetworkDeviceManager::IsOpen()
	{
		return m_bInitialized;
	}

	void NTNetworkDeviceManager::Close()
	{
#ifdef NPTINCLUDE
		NT_Done();
#endif
	}

	void NTNetworkDeviceManager::Clear()
	{
		delete instance;
		instance = 0;
	}

	NTNetworkDeviceManager * NTNetworkDeviceManager::instance = 0;
}
