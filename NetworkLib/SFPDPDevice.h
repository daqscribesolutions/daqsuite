#ifndef SFPDPDEVICE_H
#define SFPDPDEVICE_H

#include "INetworkDevice.h"
#ifdef WIN32
#include "EK-SFM2 Drv.h"
#endif

/// constant variable for memory handling.
const int PACKET_BNUM = 1024 * 256;

namespace NetworkLib
{
	struct SFPDPManager;
	/**
	* SFPDPDevice implementation of INetworkDevice for SFPDP network device port with binary file.
	*/
	class SFPDPDevice : public INetworkDevice
	{

	public:

		/// Default constructor.
		SFPDPDevice(int port);

		/// Default destructor.
		~SFPDPDevice();

		/// Implementation of INetworkDevice.
		virtual void Monitor();

		virtual void Stop();

		virtual void StopThread();

		virtual void HandlePacket();

		virtual void RecordBegin();

		virtual void RecordEnd();

	private:

		/// WriteDuration writes the duration of record on the header.
		void WriteDuration();

		/// Global SFPDPManager cached. 
		SFPDPManager * pDeviceManager;

		/// DMA thread.
		std::unique_ptr<boost::thread> m_DMAThread;

		/// DMA thread functor.
		struct DMARunner;
		std::unique_ptr<DMARunner> m_DMARunner;

		/// The buffer for DMA operation.
		unsigned int readbuffer[PACKET_BNUM];

		/// The buffer size of DMA.
		unsigned int m_BufferSize;

		/// The current record data size.
		unsigned int m_DataSize;

		/// The output stream to file.
		std::ofstream m_WriteFileStream;

		/// The input stream from file.
		std::ifstream m_ReadFileStream;

	};

	/// SFPDPManager controls one device board with multiple ports.
	struct SFPDPManager
	{
		/// The getter for the singleton instance.
		static SFPDPManager * GetInstance();

		/// Clear releases memory.
		static void Clear();

		/// Init initializes the device board.
		void Init();

		/// IsOpen is the getter for the device state whether it is open and operable.
		bool IsOpen();

		/// Reset clears the registry.
		void Reset();

#ifdef SFPDPINCLUDE
		EK_DEVICE_OBJ hDev;
#endif

	private:

		/// Destructor.
		~SFPDPManager();

		/// The flag for the DMA memory.
		bool m_MemoryOK;

		static SFPDPManager * instance;

	};
}

#endif
