#ifndef GESFPDPDEVICE_H
#define GESFPDPDEVICE_H

#include "INetworkDevice.h"

namespace NetworkLib
{
	struct GESFPDPManager;

	/**
	* GESFPDPDevice implementation of INetworkDevice for SFPDP network device port with binary file.
	*/
	class  GESFPDPDevice : public INetworkDevice
	{

	public:

		/// Default constructor.
		GESFPDPDevice(int port);

		/// Default destructor.
		~GESFPDPDevice();

		/// Implementation of INetworkDevice.
		virtual void Monitor();

		virtual void Stop();

		virtual void StopThread();

		virtual void HandlePacket();

		virtual RunOption GetRunOption();

		virtual void SetRunOption(RunOption flag);

		virtual void RecordBegin();

		virtual void RecordEnd();

		void SetDataIndex(unsigned int idx) { m_DataIdx = idx; }

	private:

		unsigned int m_nFinished;

		void WriteDuration();

		GESFPDPManager * pDeviceManager;

		unsigned int m_DataSize;

		unsigned int m_DataIdx;

		std::ofstream m_WriteFileStream;

		std::ifstream m_ReadFileStream;

	};

	struct GESFPDPManager
	{

		static GESFPDPManager * GetInstance();

		static void Clear();

		void Init();

		bool IsOpen();

		bool Start(int dataid, std::string& error);

		void Close();

		void Update(int dataid);

		void Stop(int dataid);

		bool bRunning;

		void SetDevice(unsigned int idx, unsigned int i, GESFPDPDevice* pdev);

		struct Data;

		std::vector<Data*> datas;

		bool HasError;

		int   BUFFER_SIZE;

	private:

		GESFPDPManager();

		static GESFPDPManager * instance;

	};

}

#endif
