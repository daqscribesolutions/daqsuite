#ifndef NETWORK_STORAGEMANAGER_H
#define NETWORK_STORAGEMANAGER_H

#include <string>
#include <vector>

namespace NetworkLib
{
	/**
	 * DiskInfo stores the information of the disk in the system.
	 */
	struct DiskInfo
	{
		/// The path of the disk.
		std::string info;

		/// The capacity of the disk in MB.
		double capacity;

		/// The available of the disk in MB.
		double available;

		/// The free of the disk in MB.
		double free;
	};

	/// StorageManager is the utility holder for the storage information.
	class StorageManager
	{

	public:

		/**
		 * GetSpaceInformation gets the storage information for the input path.
		 * @param path The path of the storage.
		 * @param capacity The output capacity space information of the disk.
		 * @param available The output available space information of the disk.
		 * @param free The output free space information of the disk.
		 */
		static void GetSpaceInformation(const std::string& path, double& capacity, double& available, double& free);

		/**
		 * CreatePath creates the directory for the input path.
		 * @param path The path to be created.
		 */
		static void CreatePath(const std::string& path);

		/**
		 * HardDiskInformation gets the logical hard drive information and return.
		 * @return The all disk information.
		 */
		static std::vector<DiskInfo> HardDiskInformation();

	private:

		StorageManager();

		~StorageManager();

	};

}

#endif