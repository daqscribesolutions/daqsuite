#include "pcap_header.h"
#include "NetworkDeviceFactory.h"
#include "SFPDPDevice.h"
#include "GESFPDPDevice.h"
#include "NetworkDevice.h"
#include "NetworkDeviceManager.h"
#include "NTNetworkDevice.h"
#include "logger.h"

namespace NetworkLib
{
	void NetworkDeviceFactory::GetNetworkDevices(std::vector<INetworkDevice*>& devices)
	{
		devices.clear();
		if (m_NetworkType == PCAP)
		{
#ifdef PCAPINCLUDE
			pcap_if_t *alldevs;
			pcap_if_t *d;
			char errbuf[PCAP_ERRBUF_SIZE];

			if (pcap_findalldevs(&alldevs, errbuf) == -1)
			{
				return;
			}

			for (d = alldevs; d; d = d->next)
			{
				INetworkDevice* pDevice = new NetworkDevice(d);

				pDevice->SetRunOption(READ_FROM_DEVICE);
				devices.push_back(pDevice);
				//pDevice = new NetworkDevice(d);
				//pDevice->SetRunOption(WRITE_TO_DEVICE);
				//devices.push_back(pDevice);
			}

			pcap_freealldevs(alldevs);
#endif
		}
		if (m_NetworkType == NAPA_TECH)
		{
#ifdef NPTINCLUDE
			auto mgr = NTNetworkDeviceManager::GetInstance();
			mgr->Init();
			if (mgr->IsOpen())
			{
				for (int i = 0; i < mgr->nPorts; ++i)
				{
					INetworkDevice* pDevice = new NTNetworkDevice();
					if (pDevice->ErrorInfo.empty())
					{
						pDevice->SetRunOption(READ_FROM_DEVICE);
						pDevice->Name += " " + std::to_string(i + 1);
						devices.push_back(pDevice);
					}
					pDevice = new NTNetworkDevice();
					if (pDevice->ErrorInfo.empty())
					{
						pDevice->SetRunOption(WRITE_TO_DEVICE);
						pDevice->Name += " " + std::to_string(i + 1);
						devices.push_back(pDevice);
					}
				}
			}
#endif
		}
		else
		{
#ifdef SFPDPINCLUDE
			SFPDPManager::GetInstance()->Init();
			if (SFPDPManager::GetInstance()->IsOpen())
			{
				for (int i = 0; i < 4; ++i)
				{
					int nPort = i + 1;
					SFPDPDevice* pdev = new SFPDPDevice(nPort);
					devices.push_back(pdev);
				}
			}
#endif
#ifdef GESFPDPINCLUDE
			auto pmanager = GESFPDPManager::GetInstance();
			pmanager->Init();
			if (pmanager->IsOpen())
			{
				for (unsigned int idx = 0; idx < (unsigned int)pmanager->datas.size(); ++idx)
				{
					for (int i = 0; i < 4; ++i)
					{
						int nPort = i + 1;
						GESFPDPDevice* pdev = new GESFPDPDevice(nPort);
						pmanager->SetDevice(idx, i, pdev);
						pdev->SetDataIndex(idx);
						devices.push_back(pdev);
					}
				}
			}
#endif
		}
	}
}
