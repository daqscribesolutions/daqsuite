#include "GESFPDPDevice.h"
#include <stdio.h>
#include <stdlib.h>
#include "RecordController.h"

#ifdef GESFPDPINCLUDE
#include "spr507BCoreApi.h"
#include "spr507BFunctionalApi.h"
#endif

#if defined(WIN32) || defined(_WIN64)
#include <conio.h>
#elif defined(LINUX)
#include <fcntl.h>
#include <sys/types.h>
#include <sys/poll.h>
#elif defined(VXWORKS)
#include <vxWorks.h>
#include <signal.h>
#endif

const int NUM_STREAM_CHANNELS = 4;
const int NUM_STREAM_BLOCKS = 1;

namespace NetworkLib
{
	struct GESFPDPManager::Data
	{
#ifdef GESFPDPINCLUDE
		SPR507B_HANDLE_T hDevice;
		SPR507B_UINT32_T gBoardIdx;
		SPR507B_UINT32_T type[NUM_STREAM_CHANNELS];
		SPR507B_STREAM_BLOCK  stream_block[NUM_STREAM_CHANNELS][NUM_STREAM_BLOCKS];
		SPR507B_UINT32_T size[NUM_STREAM_CHANNELS][NUM_STREAM_BLOCKS];
		char *buffer[NUM_STREAM_CHANNELS][NUM_STREAM_BLOCKS];
		GESFPDPDevice* devices[NUM_STREAM_CHANNELS];
		unsigned int nReady;
#endif
	};

	GESFPDPDevice::GESFPDPDevice(int port)
		: INetworkDevice()
		, m_DataSize(0)
		, m_nFinished(0)
		, m_DataIdx(0)
	{
		networkType = SFPDP;

		pDeviceManager = GESFPDPManager::GetInstance();
		Name = "GE SFPDP Device";
		Address = std::to_string((long long)port);
		FileName = "SFPDP" + Address + ".sfpdp";
		SetRunOption(READ_FROM_DEVICE);
	}

	GESFPDPDevice::~GESFPDPDevice()
	{
		Stop();
	}

	RunOption GESFPDPDevice::GetRunOption()
	{
		return m_runOption;
	}

	void GESFPDPDevice::SetRunOption(RunOption flag)
	{
#ifdef GESFPDPINCLUDE
		int port = std::stoi(Address) - 1;
		pDeviceManager->datas[m_DataIdx]->type[port] = flag == READ_FROM_DEVICE ? F_SPR507B_RECEIVER : F_SPR507B_TRANSMITTER;
#endif
		m_runOption = flag;
	}

	void GESFPDPDevice::Monitor()
	{
		Status = "";
		m_nSavedPackets = 0;
		ErrorInfo.clear();
		m_SpeedMonitor.Reset();
		m_nFinished = 0;
		int port = port = std::stoi(Address) - 1;
		if (port == 0)
		{
			if (!pDeviceManager->Start(m_DataIdx, ErrorInfo))
			{
				return;
			}
		}
		if (!pDeviceManager->IsOpen()) return;
		if (!pDeviceManager->bRunning) return;

		if (m_runOption == READ_FROM_DEVICE)
		{
			if (m_bActive)
			{
				try
				{
					m_WriteFileStream.open(FileName.c_str(), std::ios::binary);
					if (!m_WriteFileStream.good())
					{
						ErrorInfo = FileName + " is not opened";
					}
				}
				catch (const std::exception& e)
				{
					ErrorInfo = e.what();
				}
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			if (recorder->Running)
			{
				if (m_bActive)
				{
					try
					{
						m_ReadFileStream.open(FileName.c_str(), std::ios::binary);
						if (!m_ReadFileStream.good())
						{
							ErrorInfo = FileName + " is not opened";
						}
					}
					catch (const std::exception& e)
					{
						ErrorInfo = e.what();
					}
				}
			}
		}
		if (m_Runner) m_Runner.release();
		m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
		m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));
	}

	void GESFPDPDevice::Stop()
	{
		if (m_Runner) m_Runner->Stop();

		if (m_runOption == READ_FROM_DEVICE)
		{
			if (m_bActive)
			{
				if (recorder->Running)
				{
					WriteDuration();
					m_WriteFileStream.close();
				}
				Status = recorder->Running ? "\n  Recorded" : "\n  Received";
				Status += " data (MBytes) : " + std::to_string((long double)m_SpeedMonitor.GetTotalDataBytes() / 1000000.0);
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			if (m_bActive)
			{

				if (recorder->Running)
				{
					m_ReadFileStream.close();
				}
				Status = "\n  Sent data (MBytes) : " + std::to_string((long double)m_SpeedMonitor.GetTotalDataBytes() / 1000000.0);
			}
		}
	}

	void GESFPDPDevice::StopThread()
	{
		int port = std::stoi(Address);
		if (port == NUM_STREAM_CHANNELS)
		{
			pDeviceManager->Stop(m_DataIdx);
		}
		pDeviceManager->bRunning = false;

		if (m_Thread)
		{
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			m_Thread.release();
		}
	}

	void GESFPDPDevice::WriteDuration()
	{
		if (m_bActive)
		{
			double t = m_SpeedMonitor.GetDataDuration();
			if (t > 0)
			{
				m_WriteFileStream.seekp(sizeof(double));
				m_WriteFileStream.write((char*)&t, sizeof(double));
			}
		}
	}

	void GESFPDPDevice::RecordBegin()
	{
		recorder->Open(true);
	}

	void GESFPDPDevice::RecordEnd()
	{
		recorder->Close();
	}

	void GESFPDPDevice::HandlePacket()
	{
		if (!pDeviceManager->bRunning) return;
		int port = std::stoi(Address) - 1;
		if (port == 0)
		{
			pDeviceManager->Update(m_DataIdx);
		}
#ifdef GESFPDPINCLUDE
		auto data = pDeviceManager->datas[m_DataIdx];
		if (m_nFinished < data->nReady)
		{
			int nPackets = 0;
			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
			{
				m_DataSize = 0;

				char * readbuffer = data->buffer[port][streamBlockIdx];
				m_DataSize = data->size[port][streamBlockIdx];

				if (m_DataSize > 8)
				{
					if (m_runOption == READ_FROM_DEVICE)
					{
						if (!recorder->Running)
						{
							if (m_bActive)
							{

								unsigned int first = *(int*)&readbuffer[0];
								unsigned int last = *(int*)&readbuffer[m_DataSize / 4 - 1];
								ErrorInfo = "first : " + std::to_string((long long)first) + " last : "
									+ std::to_string((long long)last) + " : counter "
									+ std::to_string((long long)m_nSavedPackets);
							}
							nPackets += m_DataSize;
							if (recorder->Running)
							{

								double ts[2] = { m_SpeedMonitor.GetDuration(), 0 };
								m_WriteFileStream.write((char*)&ts[0], sizeof(double) * 2);
								m_WriteFileStream.write((char*)&readbuffer[0], m_DataSize);
								m_nSavedPackets += m_DataSize;
							}
						}
					}
					else if (m_runOption == WRITE_TO_DEVICE)
					{
						if (m_bActive)
						{

							double ts[2] = { 0, 0 };
							if (!m_ReadFileStream.eof() && !m_ReadFileStream.fail() && !m_ReadFileStream.bad() && m_DataSize > 0)
							{
								m_ReadFileStream.read((char*)&ts[0], sizeof(double) * 2);
								m_ReadFileStream.read((char*)&readbuffer[0], m_DataSize);
							}
							if (!recorder->Running)
							{
								*(int*)&readbuffer[0] = (int)m_SpeedMonitor.GetTotalDataBytes();
								*(int*)&readbuffer[m_DataSize / 4 - 1] = (int)m_SpeedMonitor.GetTotalDataBytes();
							}
							nPackets += m_DataSize;
						}
					}
				}
			}

			m_SpeedMonitor.UpdateData(nPackets);
			m_SpeedMonitor.EndSample();
			m_SpeedMonitor.BeginSample();

			for (size_t i = 0; i < m_Monitors.size(); ++i)
			{
				m_Monitors[i]->Update((int)nPackets);
			}
			m_nFinished++;
		}
#endif
	}

	GESFPDPManager * GESFPDPManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new GESFPDPManager();
		}
		return instance;
	}

	GESFPDPManager::GESFPDPManager()
		: bRunning(false)
		, HasError(false)
		, BUFFER_SIZE(0x80000)
	{
	}

	bool GESFPDPManager::Start(int dataid, std::string& error)
	{
		bRunning = false;
      HasError = false;
      error = "";
      if (dataid < 0) dataid = 0;
#ifdef GESFPDPINCLUDE
      Data* data = datas[dataid];
      data->nReady = 0;
      F_SPR507B_FPDP structFpdp;
		F_SPR507B_SFPDP structSfpdp;
		F_SPR507B_TRIGGER structTrigger;
		SPR507B_STREAM_INIT structInit;
		F_SPR507B_SFPDP_STATUS structSfpdpStatus;
		SPR507B_UINT32_T timeout = 3000;

		SPR507B_UINT32_T trigOutput = F_SPR507B_DISABLE;
		SPR507B_UINT32_T trigSource = F_SPR507B_INTERNAL;

		memset(&structInit, 0, sizeof(SPR507B_STREAM_INIT));
		memset(data->stream_block, 0, sizeof(SPR507B_STREAM_BLOCK));
		memset(&structSfpdpStatus, 0, sizeof(F_SPR507B_SFPDP_STATUS));

		F_SPR507B_BAUD_RATE baudRate = BAUD_RATE_2_5;
		if (data->devices[0]->GetSFPDPSpeedOption() == SPEED_1G)
		{
			baudRate = BAUD_RATE_1_0625;
		}
		else if (data->devices[0]->GetSFPDPSpeedOption() == SPEED_2_1G)
		{
			baudRate = BAUD_RATE_2_125;
		}
		else if (data->devices[0]->GetSFPDPSpeedOption() == SPEED_3_1G)
		{
			baudRate = BAUD_RATE_3_125;
		}
		else if (data->devices[0]->GetSFPDPSpeedOption() == SPEED_4_25G)
		{
			baudRate = BAUD_RATE_4_25;
		}
		if (SPR507B_OK != spr507BBaudRateSet(data->hDevice, baudRate))
		{
			error = ("ERROR: Functional spr507BBaudRateSet failed.\n");
			HasError = true;
			return false;
		}
		for (int idx = 0; idx < NUM_STREAM_CHANNELS; idx++)
		{
			data->type[idx] = (data->devices[idx]->GetRunOption() == WRITE_TO_DEVICE) ? F_SPR507B_TRANSMITTER : F_SPR507B_RECEIVER;
		}
		if (SPR507B_OK != spr507BTransferChannelSet(data->hDevice, NUM_STREAM_CHANNELS, data->type))
		{
			HasError = true;
			error = ("ERROR: Functional spr507BTransferChannelSet failed.\n");
			return false;
		}

		memset(&structSfpdp, 0, sizeof(F_SPR507B_SFPDP));
		structSfpdp.loopBack = F_SPR507B_DISABLE;
		structSfpdp.enable = F_SPR507B_ENABLE;
		structSfpdp.startOnSync = F_SPR507B_DISABLE;
		structSfpdp.flowControlEnable = F_SPR507B_ENABLE;
		structSfpdp.crcEnable = F_SPR507B_ENABLE;
		structSfpdp.rcvMode = F_SPR507B_SFPDP_RCV_MODE_NORMAL;

		structSfpdp.frame.type = F_SPR507B_SFPDP_FRAME_TYPE_FIXED_SIZE;
		structSfpdp.frame.size = F_SPR507B_SFPDP_FRAME_MAXIMUM_SIZE;
		structSfpdp.frame.idles = F_SPR507B_SFPDP_FRAME_IDLES_RECOMMENDED_SETTING;
		structSfpdp.frame.deadIdles = F_SPR507B_ENABLE;

		for (int idx = 0; idx < NUM_STREAM_CHANNELS; idx++)
		{
			if (!data->devices[idx]->GetActiveFlag()) continue;

			structSfpdp.type = data->type[idx];
			if (SPR507B_OK != spr507BSfpdpSet(data->hDevice, idx, &structSfpdp))
			{
				HasError = true;
				error = ("ERROR: Functional spr507BSfpdpSet failed.\n");
				return false;
			}
		}

		memset(&structFpdp, 0, sizeof(F_SPR507B_FPDP));
		structFpdp.frameType = F_SPR507B_FPDP_UNFRAMED;
		structFpdp.frameSize = 1;

		for (int idx = 0; idx < NUM_STREAM_CHANNELS; idx++)
		{
			if (SPR507B_OK != spr507BFpdpSet(data->hDevice, idx, &structFpdp))
			{
				HasError = true;
				error = ("ERROR: Functional spr507BFpdpSet failed.\n");
				return false;
			}
		}

		memset(&structTrigger, 0, sizeof(F_SPR507B_TRIGGER));
		structTrigger.output = trigOutput;
		structTrigger.source = trigSource;
		structTrigger.externalTermination = F_SPR507B_ENABLE;
		structTrigger.externalMode = F_SPR507B_RISING_EDGE;

		if (SPR507B_OK != spr507BTriggerSet(data->hDevice, &structTrigger))
		{
			HasError = true;
			error = ("ERROR: Functional spr507BTriggerSet failed.\n");
		}

		if (SPR507B_OK != spr507BTransferReset(data->hDevice))
		{
			HasError = true;
			error = ("ERROR : Functional spr507BTransferReset failed.\n");
			return false;
		}

		for (int streamChannelIdx = 0; streamChannelIdx < NUM_STREAM_CHANNELS; streamChannelIdx++)
		{
			if (!data->devices[streamChannelIdx]->GetActiveFlag()) continue;

			structInit.flags = data->type[streamChannelIdx] != F_SPR507B_RECEIVER ? SPR507B_STREAM_DIRECTION_WRITE : SPR507B_STREAM_DIRECTION_READ;
			structInit.flags |= SPR507B_STREAM_MODE_COMMAND_QUEUE;
			structInit.watermark = SPR507B_STREAM_MAX_COMMAND_QUEUE_SIZE / 2;

			if (SPR507B_OK != spr507BInitStream(data->hDevice, streamChannelIdx, &structInit))
			{
				HasError = true;
				error = ("spr507BInitStream failed.\n");
				return false;
			}

			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
			{
				data->size[streamChannelIdx][streamBlockIdx] = BUFFER_SIZE;
				if (SPR507B_OK != spr507BAllocateStreamBlock(data->hDevice, &data->stream_block[streamChannelIdx][streamBlockIdx],
					streamChannelIdx, (void**)&data->buffer[streamChannelIdx][streamBlockIdx], data->size[streamChannelIdx][streamBlockIdx]))
				{
					HasError = true;
					error = ("spr507BAllocateStreamBlock failed\n");
					return false;
				}
			}
			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
				memset(data->buffer[streamChannelIdx][streamBlockIdx], 0, BUFFER_SIZE);
			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
			{
				if (SPR507B_OK != spr507BPushStreamBlock(data->hDevice, &data->stream_block[streamChannelIdx][streamBlockIdx]))
				{
					HasError = true;
					error = ("spr507BPushStreamBlock DMA:%d failed\n", streamChannelIdx);
					return false;
				}
			}
		}

		if (SPR507B_OK != spr507BTransferEnable(data->hDevice))
		{
			HasError = true;
			error = ("ERROR : Functional spr507BTransferEnable failed.\n");
			return false;
		}

		if (SPR507B_OK != spr507BSfpdpSynchronizationTrigger(data->hDevice))
		{
			HasError = true;
			error = ("ERROR: Functional spr507BSfpdpSynchronizationTrigger failed.\n");
			return false;
		}

		for (int streamChannelIdx = 0; streamChannelIdx < NUM_STREAM_CHANNELS; streamChannelIdx++)
		{
			if (!data->devices[streamChannelIdx]->GetActiveFlag()) continue;

			if (F_SPR507B_RECEIVER == data->type[streamChannelIdx])
			{
				SPR507B_TICK_COUNT_T goal = SPR507B_MSEC_TO_TICK(5000) + SPR507B_TICK_COUNT_GET();
				do
				{
					if (SPR507B_OK != spr507BSfpdpStatusGet(data->hDevice, streamChannelIdx, &structSfpdpStatus))
					{
						HasError = true;
						error = ("ERROR : Functional spr507BSfpdpStatusGet failed.\n");
						return false;
					}
				} while ((F_SPR507B_HIGH != structSfpdpStatus.gtxRecvSync) && (SPR507B_TICK_COUNT_GET() < goal));

				if (F_SPR507B_LOW == structSfpdpStatus.gtxRecvSync)
				{
					HasError = true;
					error = ("ERROR : sFPDP TX is not ready.\n");
					return false;
				}
			}
		}

		if (structTrigger.source == F_SPR507B_INTERNAL)
		{
			if (SPR507B_OK != spr507BTriggerTrigger(data->hDevice))
			{
				HasError = true;
				error = ("ERROR: Functional spr507BTriggerTrigger failed.\n");
				return false;
			}
		}

		for (int streamChannelIdx = 0; streamChannelIdx < NUM_STREAM_CHANNELS; ++streamChannelIdx)
		{
			if (!data->devices[streamChannelIdx]->GetActiveFlag()) continue;
			if (F_SPR507B_RECEIVER == data->type[streamChannelIdx])
			{
				if (SPR507B_OK != spr507BStartStream(data->hDevice, streamChannelIdx))
				{
					HasError = true;
					error = ("spr507BStartStream DMA:%d failed.\n", streamChannelIdx);
					return false;
				}
			}
		}

		for (int streamChannelIdx = 0; streamChannelIdx < NUM_STREAM_CHANNELS; ++streamChannelIdx)
		{
			if (!data->devices[streamChannelIdx]->GetActiveFlag()) continue;
			if (F_SPR507B_RECEIVER != data->type[streamChannelIdx])
			{
				if (SPR507B_OK != spr507BStartStream(data->hDevice, streamChannelIdx))
				{
					HasError = true;
					error = ("spr507BStartStream DMA:%d failed.\n", streamChannelIdx);
					return false;
				}
			}
      }
#endif
		bRunning = true;
		return true;
	}

	void GESFPDPManager::Init()
	{
#ifdef GESFPDPINCLUDE
		int idx = 0;
		do
		{
			Data* data = new Data;
			data->hDevice = SPR507B_INVALID_HANDLE_VALUE;
			data->gBoardIdx = idx;
			if (SPR507B_OK != spr507BCreateFileEx(&data->hDevice, data->gBoardIdx, SPR507B_CREATE_ASYNC))
			{
				printf("spr507BCreateFile failed!\n");
				delete data;
				break;
			}
			if (SPR507B_OK != spr507BBoardInitialize(data->hDevice))
			{
				delete data;
				HasError = true;
				printf("ERROR: Functional spr507BBoardInitialize failed.\n");
				break;
			}
			datas.push_back(data);
			++idx;
		} while (true);
#endif
	}

	bool GESFPDPManager::IsOpen()
	{
		if (HasError) return false;
		bool res = true;
#ifdef GESFPDPINCLUDE
		for (unsigned int i = 0; i < (unsigned int)datas.size(); ++i)
		{
			Data* data = datas[i];

			res = SPR507B_INVALID_HANDLE_VALUE != data->hDevice;
			if (!res) break;
		}
#endif
		return res;
	}

   void GESFPDPManager::Clear()
   {
      delete instance;
      instance = 0;
   }

	void GESFPDPManager::Close()
	{
#ifdef GESFPDPINCLUDE
		if (IsOpen())
		{
			for (unsigned int i = 0; i < (unsigned int)datas.size(); ++i)
			{
				Data* data = datas[i];

				if (SPR507B_OK != spr507BBoardShutdown(data->hDevice))
				{
					HasError = true;
					printf("ERROR : Functional spr507BBoardShutdown failed.\n");
				}

				/* Close device. */
				if (SPR507B_OK != spr507BCloseFileEx(data->hDevice))
				{
					HasError = true;
					printf("spr507BCloseFile failed.\n");
				}

				data->hDevice = SPR507B_INVALID_HANDLE_VALUE;
				delete data;
			}
		}
#endif
	}

	void GESFPDPManager::Update(int dataid)
	{
      if (dataid < 0) dataid = 0;
#ifdef GESFPDPINCLUDE
		if (IsOpen())
		{
			SPR507B_UINT32_T    timeout = 3000;
			Data* data = datas[dataid];
			for (int idx = 0; idx < NUM_STREAM_CHANNELS; ++idx)
			{
				for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
				{
					if (SPR507B_OK != spr507BWaitCompleteStreamBlock(data->hDevice,
						&data->stream_block[idx][streamBlockIdx], timeout))
					{
						HasError = true;
						printf("HandlePacket : spr507BWaitCompleteStreamBlock");
						return;
					}
				}
			}
			data->nReady++;
		}
#endif

#ifdef GESFPDPINCLUDE
		if (IsOpen())
		{
			Data* data = datas[dataid];
			for (int idx = 0; idx < NUM_STREAM_CHANNELS; ++idx)
			{
				for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
				{
					if (SPR507B_OK != spr507BPushStreamBlock(data->hDevice, &data->stream_block[idx][streamBlockIdx]))
					{
						HasError = true;
						printf("spr507BPushStreamBlock DMA failed\n");
						return;
					}
				}
			}
		}
#endif
	}

	void GESFPDPManager::SetDevice(unsigned int idx, unsigned int i, GESFPDPDevice* pdev)
	{
      if (pdev == NULL || idx > 10 || i > 10) return;
#ifdef GESFPDPINCLUDE
      datas[idx]->devices[i] = pdev;
#endif
   }

	void GESFPDPManager::Stop(int dataid)
	{
      if (dataid < 0) dataid = 0;
#ifdef GESFPDPINCLUDE
		SPR507B_UINT32_T timeout = 3000;
		Data* data = datas[dataid];

		if (SPR507B_INVALID_HANDLE_VALUE != data->hDevice)
		{
			if (SPR507B_OK != spr507BTransferDisable(data->hDevice))
			{
				HasError = true;
				printf("ERROR : Functional spr507BTransferDisable failed.\n");
			}
		}

		for (int idx = 0; idx < NUM_STREAM_CHANNELS; ++idx)
		{
			if (SPR507B_OK != spr507BStopStream(data->hDevice, idx))
			{
				HasError = true;
				printf("spr507BStopStream failed\n");
			}
			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; ++streamBlockIdx)
			{
				if (SPR507B_OK != spr507BWaitCompleteStreamBlock(data->hDevice,
					&data->stream_block[idx][streamBlockIdx], timeout))
				{
					HasError = true;
					printf("Stop spr507BWaitCompleteStreamBlock.");
				}
			}

			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
			{
				if (SPR507B_OK != spr507BPopStreamBlock(data->hDevice, &data->stream_block[idx][streamBlockIdx]))
				{
					HasError = true;
					printf("spr507BPopStreamBlock failed");
				}
			}

			for (int streamBlockIdx = 0; streamBlockIdx < NUM_STREAM_BLOCKS; streamBlockIdx++)
			{
				if (data->stream_block[idx][streamBlockIdx].hdma != 0)
				{
					if (SPR507B_OK != spr507BFreeStreamBlock(data->hDevice, &data->stream_block[idx][streamBlockIdx]))
					{
						HasError = true;
						printf("spr507BFreeStreamBlock failed.\n");
					}
					data->stream_block[idx][streamBlockIdx].buffer = 0;
					data->stream_block[idx][streamBlockIdx].hdma = 0;
				}
			}

			if (SPR507B_OK != spr507BReleaseStream(data->hDevice, idx))
			{
				HasError = true;
				printf("spr507BReleaseStream failed.\n");
			}
		}
#endif

	}
	GESFPDPManager * GESFPDPManager::instance = 0;

}
