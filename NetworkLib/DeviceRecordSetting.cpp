#include "DeviceRecordSetting.h"
#include <boost/filesystem.hpp>
#include <sstream>
#include "StorageManager.h"

namespace NetworkLib
{
	std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
	{
		size_t current;
		size_t next = -1;
		std::vector<std::string> columns;
		int nColumns = 0;
		do
		{
			current = next + 1;
			next = line.find_first_of(delimiters, current);
			std::string field = line.substr(current, next - current);
			columns.push_back(field);
			++nColumns;
		} while (next != std::string::npos);
		return columns;
	}

	DeviceRecordSetting* DeviceRecordSetting::m_Instance = 0;

	DeviceRecordSetting* DeviceRecordSetting::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new DeviceRecordSetting();
		}
		return m_Instance;
	}

	void DeviceRecordSetting::Clear()
	{
		delete m_Instance;
		m_Instance = 0;
	}

	std::string DeviceRecordSetting::GetNetworkRecordFileName(size_t id, int record, bool bsession)
	{
		std::stringstream namebuilder;
		namebuilder << m_RecordPath << "/" << m_RecordName;
		StorageManager::CreatePath(namebuilder.str());
		namebuilder << "/" << m_RecordName;
		if (!bsession) namebuilder << "_" << id << "_";
		namebuilder.width(3); namebuilder.fill('0');
		namebuilder << record;
		return namebuilder.str();
	}

	DeviceRecordSetting::DeviceRecordSetting()
		: m_RecordPath("C:\\Simple Record")
		, m_RecordName("Record")
		, m_RecordMaxSize(1000000000)
		, WRITE_NUM_SINGLE_BUFFERS(48)
		, WRITE_NUM_BUFFERS(32)
		, WRITE_BUFFER_SIZE(1048576)
		, READ_NUM_SINGLE_BUFFERS(48)
		, READ_NUM_BUFFERS(32)
		, READ_BUFFER_SIZE(1048576)
	{
	}

	DeviceRecordSetting::~DeviceRecordSetting()
	{
	}

	unsigned long long DeviceRecordSetting::GetRecordMaximumSize()
	{
		return m_RecordMaxSize;
	}

	void DeviceRecordSetting::SetRecordMaximumSize(unsigned long long recordsize)
	{
		m_RecordMaxSize = recordsize;
	}

}
