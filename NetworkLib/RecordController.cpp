#include "RecordController.h"
#include <boost/filesystem.hpp>
#include "usertagmanager.h"
#include "logger.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
#include "StorageManager.h"

namespace NetworkLib
{
	struct RecordController::Runner
	{
		RecordController* recorder;
		Runner(RecordController* r)
			: m_bRunning(true)
			, recorder(r)
		{

		}

		/**
		* operator() is the implementation of the device running logic.
		*/
		void operator()()
		{
			while (m_bRunning)
			{
				recorder->Flush();
			}
		}

		/**
		* Reset makes to start the thread.
		* @return The device is set to run continually.
		*/
		void Reset() { m_bRunning = true; }

		/**
		* Stop stops the thread.
		* @return The device is stopped.
		*/
		void Stop() { m_bRunning = false; }

		/**
		* IsRunning is the getter for the running state.
		* @return True if the device is running otherwise return false.
		*/
		bool IsRunning() { return m_bRunning; }

	private:

		/// The flag for running state of the thread.
		bool m_bRunning;

	};

	std::ifstream::pos_type filesize(const std::string& filename)
	{
		std::ifstream in(filename.c_str(), std::ifstream::ate | std::ifstream::binary);
		return in.tellg();
	}

	RecordController::RecordController()
		: begin_time(0)
		, datasize(0)
		, ID(0)
		, RelayID(0)
		, nFiles(0)
		, hFile(INVALID_HANDLE_VALUE)
		, RelayOffset(0)
		, RelayDuration(1)
		, TotalDuration(0)
		, n_current(0)
		, n_written(0)
		, leftsize(0)
		, m_IOThread(0)
		, m_IORunner(0)
		, Running(false)
		, RecordDuration(0)
		, m_bRead(false)
		, m_bHeader(true)
		, replybegin(0)
		, replyduration(0)
		, replydatasize(0)
		, readdatasize(0)
		, LastMinuteData(0)
	{
		begin_time = clock();
		flag = FILE_FLAG_NO_BUFFERING | FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH;
	}

	RecordController::~RecordController()
	{
		Close();
		for (unsigned long i = 0; i < NUM_BUFFERS; ++i)
		{
			delete [] buffer[i];
			delete [] leftbuffer[i];
		}
	}

	unsigned long long RecordController::RemainRecordDuration()
	{
		if (Running)
		{
			return RecordDuration - (clock() - record_begin_time);
		}
		else
		{
			return 0;
		}
	}

	std::string RecordController::GetFileName()
	{
		return filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
	}

	void RecordController::Open(bool bRead)
	{
		if (!Running)
		{
			auto setting = DeviceRecordSetting::GetInstance();
			max_file_size = setting->GetRecordMaximumSize();
			error = "";
			m_bRead = bRead;
			TotalData = 0;
			NUM_BUFFERS = bRead ? setting->READ_NUM_BUFFERS : setting->WRITE_NUM_BUFFERS;

			BUFFER_SIZE = bRead ? setting->READ_BUFFER_SIZE : setting->WRITE_BUFFER_SIZE;
			SINGLE_BUFFER = BUFFER_SIZE * (bRead ? setting->READ_NUM_SINGLE_BUFFERS : setting->WRITE_NUM_SINGLE_BUFFERS);
			for (unsigned long i = 0; i < buffer.size(); ++i)
			{
				delete [] buffer[i];
				delete [] leftbuffer[i];
			}
			buffer.resize(NUM_BUFFERS, 0);
			leftbuffer.resize(NUM_BUFFERS, 0);
			remainBufferSize.resize(NUM_BUFFERS, 0);
			memset(&remainBufferSize[0], 0, sizeof(unsigned long) * NUM_BUFFERS);
			for (unsigned long i = 0; i < NUM_BUFFERS; ++i)
			{
				buffer[i] = new char[SINGLE_BUFFER];
				leftbuffer[i] = new char[BUFFER_SIZE];
			}
			max_file_size = setting->GetRecordMaximumSize();
			datasize = 0;
			nFiles = 1;
			n_current = n_written = 0;
			leftsize = 0;
			MAX_NUM_BUFFER_FILE = max_file_size / SINGLE_BUFFER;
			if (!m_bRead)
			{
				StorageManager::CreatePath(filename);
			}
			std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
			if (!m_bRead)
			{
				record_begin_time = clock();
				TagFileOperation(true);
				seg_out.open((filename + "/record.seg").c_str(), std::ios::binary);
				std::wstring name;
				for (int i = 0; i < (int)fname.size(); ++i)
				{
					name.push_back(fname[i]);
				}
				hFile = CreateFile(name.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, flag, NULL);
				memset(&overlapped_structure, 0, sizeof(overlapped_structure));
				char* p = (char*)&datasize;
				memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
				memcpy(&overlapped_structure.Offset, p, 4);
				overlapped_structure.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
				Running = true;
				Write(&FileHeader[0], (unsigned int)FileHeader.size());
				m_IORunner = std::shared_ptr<Runner>(new Runner(this));
				m_IOThread = std::shared_ptr<boost::thread>(new boost::thread(boost::ref(*m_IORunner)));
				second_keep = clock();
			}
			else
			{
				m_bHeader = true;
				seg_info.close();
				seg_info.open((filename + "/record.seg").c_str(), std::ios::binary);
				if (!seg_info.good())
				{
					error = "RecordController::Open() File Opening Failure : " + filename + "/record.seg";
					return;
				}
				seg_info.read((char*)&headerlen, sizeof(headerlen));
				replycurrent = 0;
				unsigned int len = 0;
				readdatasize = filesize(fname);
				while (replybegin > replycurrent + readdatasize)
				{
					long long total = 0;
					readdatasize -= headerlen;
					while (total < readdatasize)
					{
						seg_info.read((char*)&len, sizeof(len));
						if (seg_info.bad()) break;
						replycurrent += len;
						total += len;
					}
					if (seg_info.bad()) break;
					++nFiles;
					fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
					readdatasize = filesize(fname);
				}
				std::wstring name;
				for (int i = 0; i < (int)fname.size(); ++i)
				{
					name.push_back(fname[i]);
				}
				CloseHandle(hFile);
				readdatasize = filesize(fname);
				hFile = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
				std::string buffer;
				Running = true;
				m_IORunner = std::shared_ptr<Runner>(new Runner(this));
				m_IOThread = std::shared_ptr<boost::thread>(new boost::thread(boost::ref(*m_IORunner)));
				Sleep(10);
				while (replybegin > replycurrent)
				{
					ReadNext(buffer);
				}
			}
		}
	}

	void RecordController::Close()
	{
		if (Running)
		{
			Running = false;
			if (m_IORunner && m_IORunner->IsRunning())
			{
				m_IORunner->Stop();
				Sleep(200);
				seg_out.close();
				if (seg_info) seg_info.close();
				FlushRemain();
				++RecordNumber;
			}
			hFile = INVALID_HANDLE_VALUE;
		}
	}

	void RecordController::NextFile()
	{
		if (Running)
		{
			FlushRemain();
			++nFiles;
			datasize = 0;
			std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
			std::wstring name;
			for (int i = 0; i < (int)fname.size(); ++i)
			{
				name.push_back(fname[i]);
			}
			if (m_bRead && !DoesExist())
			{
				hFile = INVALID_HANDLE_VALUE;
				Running = false;
				readdatasize = 0;
				return;
			}
			if (m_bRead)
			{
				readdatasize = filesize(fname);
				CloseHandle(hFile);
				hFile = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
				//FILE_FLAG_NO_BUFFERING | FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH; NULL   FILE_FLAG_OVERLAPPED FILE_FLAG_RANDOM_ACCESS
				m_bHeader = true;
				if (hFile == INVALID_HANDLE_VALUE)
				{
					Running = false;
				}
			}
			else
			{
				CloseHandle(hFile);
				hFile = CreateFile(name.c_str(), GENERIC_WRITE,	0, NULL, CREATE_ALWAYS, flag, NULL);
				memset(&overlapped_structure, 0, sizeof(overlapped_structure));
				char* p = (char*)&datasize;
				memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
				memcpy(&overlapped_structure.Offset, p, 4);
				overlapped_structure.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
				TagFileOperation(true);
			}
		}
	}

	void RecordController::NextFileStdio()
	{
		if (Running)
		{
			FlushRemain();
			++nFiles;
			datasize = 0;
			std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
			std::wstring name;
			for (int i = 0; i < (int)fname.size(); ++i)
			{
				name.push_back(fname[i]);
			}
			if (m_bRead && !DoesExist())
			{
				hFile = INVALID_HANDLE_VALUE;
				Running = false;
				readdatasize = 0;
				return;
			}
			if (m_bRead)
			{
				
				readdatasize = filesize(fname);

				//CloseHandle(hFile);
				fclose(fp);

				//fp = fopen(fname.c_str(), "rb");
				fopen_s(&fp, fname.c_str(), "rb");
				//hFile = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, NULL, NULL);

				m_bHeader = true;
				if (hFile == INVALID_HANDLE_VALUE)
				{
					Running = false;
				}
			}
			else
			{
				CloseHandle(hFile);
				hFile = CreateFile(name.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, flag, NULL);
				memset(&overlapped_structure, 0, sizeof(overlapped_structure));
				char* p = (char*)&datasize;
				memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
				memcpy(&overlapped_structure.Offset, p, 4);
				overlapped_structure.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
				TagFileOperation(true);
			}
		}
	}

	void RecordController::Write(char* data, unsigned int size)
	{
		if (Running && size > 0)
		{
			seg_out.write((char*)&size, sizeof(size));
			auto idx = n_current % NUM_BUFFERS;
			if (leftsize + size >= SINGLE_BUFFER)
			{
				unsigned long long tail = SINGLE_BUFFER - leftsize;
				memcpy(&buffer[idx][leftsize], data, tail);
				//idx = n_current % NUM_BUFFERS;
				memcpy(leftbuffer[idx], &data[tail], size - tail);
				remainBufferSize[idx] = size - tail;
				if (n_current % MAX_NUM_BUFFER_FILE == 0 && n_current != 0)
				{
					++n_current;
					idx = n_current % NUM_BUFFERS;
					memcpy(buffer[idx], &FileHeader[0], FileHeader.size());
					leftsize = FileHeader.size();
				}
				else
				{
					remainBufferSize[idx] = 0;
					++n_current;
					idx = n_current % NUM_BUFFERS;
					memcpy(buffer[idx], &data[tail], size - tail);
					leftsize = size - tail;
				}
			}
			else
			{
				memcpy(&buffer[idx][leftsize], data, size);
				leftsize += size;
			}
			leftsize %= SINGLE_BUFFER;
			if (n_current > n_written + NUM_BUFFERS)
			{
				error = "RecordController::Write() Write Error : overflowing";
			}
			if (RecordDuration < (unsigned long long)(clock() - record_begin_time))
			{
				Close();
			}
		}
	}

	bool RecordController::ReadNext(std::string& data)
	{
		data = "";
		if (replycurrent > replyduration) return false;
		if (n_current >= n_written)
		{
			Sleep(5);
			return true;
		}
		unsigned int size = 0;
		while (size == 0) 
		{
			if (seg_info.fail() || seg_info.eof()) return false;
			seg_info.read((char*)&size, sizeof(size));
		}
		data.resize(size);
		char * pdata = &data[0];
		auto idx = n_current % NUM_BUFFERS;
		unsigned long long test = remainBufferSize[idx];
		if (remainBufferSize[idx] == 0) return false;
		unsigned long long cpsize = 0;
		while (cpsize < size)
		{
			unsigned long long currentsize = size - cpsize;
			while (n_current >= n_written)
			{
				if (!Running) return false;
				Sleep(5);
			}
			idx = n_current % NUM_BUFFERS;
			test = remainBufferSize[idx];
			if (leftsize + currentsize >= test)
			{
				unsigned long long tail = test - leftsize;
				memcpy(pdata + cpsize, &buffer[idx][leftsize], tail);
				cpsize += tail;
				remainBufferSize[idx] = 0;
				++n_current;
				leftsize = 0;
			}
			else
			{
				memcpy(pdata + cpsize, &buffer[idx][leftsize], currentsize);
				leftsize += currentsize;
				cpsize += currentsize;
			}

			leftsize %= test;
		}
		replycurrent += data.size();
		return data.size() != 0;
	}

	void RecordController::Flush()
	{
		if (m_bRead)
		{
			if (Running)
			{
				if (NUM_BUFFERS > n_written - n_current)
				{
					auto idx = n_written % NUM_BUFFERS;
					unsigned long readsize = (unsigned long)SINGLE_BUFFER;
					if (readsize > readdatasize) readsize = (unsigned long)readdatasize;
					DWORD actualsize = 0;

					//while (!(0 < (n_size = fread(buffer[idx], readsize, 1, fp))))
					//{
					//	//error = "Error code : " + std::to_string(GetLastError());
					//}

					if (!ReadFile(hFile, buffer[idx], readsize, &actualsize, NULL))
					{
						//error = "Error code : " + std::to_string(GetLastError());
					}
					readsize = actualsize;
					readdatasize -= readsize;
					if (readsize > 0)
					{
						if (m_bHeader)
						{
							m_bHeader = false;
							readsize -= headerlen;
							std::string temp;
							temp.resize(readsize);
							memcpy(&temp[0], buffer[idx] + headerlen, readsize);
							memcpy(buffer[idx], &temp[0], readsize);
						}
						datasize += readsize;
						remainBufferSize[idx] = readsize;
						++n_written;
					}
					else
					{
						NextFile();
					}
				}
				else
				{
					Sleep(1);
				}
			}
		}
		else
		{
			if (n_current > n_written)
			{
				int idx = n_written % NUM_BUFFERS;
				char* p = (char*)&datasize;
				memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
				memcpy(&overlapped_structure.Offset, p, 4);

				WriteFile(hFile, buffer[idx], SINGLE_BUFFER, NULL, &overlapped_structure);
				datasize += SINGLE_BUFFER;
				TotalData += SINGLE_BUFFER;
				if (n_written % MAX_NUM_BUFFER_FILE == 0 && n_written != 0)
				{
					NextFile();
				}
				++n_written;
				unsigned long long dur = clock() - second_keep;
				if (dur > 1000)
				{
					LastMinuteData = (unsigned long long)((n_written - last_written) * SINGLE_BUFFER / (dur / 1000.0));
					last_written = n_written;
					second_keep += dur;
				}
			}
		}
	}

	void RecordController::FlushRemain()
	{
		if (!m_bRead)
		{
			CloseHandle(hFile);
			hFile = INVALID_HANDLE_VALUE;
			std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
			std::fstream fout(fname.c_str(), std::ios::app | std::ios::binary);
			auto idx = n_written % NUM_BUFFERS;
			fout.write(leftbuffer[idx], remainBufferSize[idx]);
			fout.close();
			TagFileOperation(false);
		}
		else
		{
			CloseHandle(hFile);
			hFile = INVALID_HANDLE_VALUE;
		}
	}

	void RecordController::AddTagToRecord(int tagidx, bool bset)
	{
		if (!m_bRead)
		{
			auto tags = UserTagManager::GetInstance()->Tags;
			if (tagidx < (int)tags.size())
			{
				auto tag = tags[tagidx];
				tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
				std::string mss = Logger::TimeStamp();
				tag_out << tagidx << ",LOCAL_EVENT" << (bset ? ",SET," : ",RESET,") << timestamp << "," << mss << "." << timestamp % 1000
					<< "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension() << "\n";
				tag_out.close();
			}
		}
	}

	unsigned long long RecordController::AddMetaTagToRecord(unsigned long long tagidx, bool bset, const std::string& time)
	{
		unsigned long long res = timestamp;
		if (!m_bRead)
		{
			tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
			tag_out << tagidx << ",REMOTE_EVENT" << (bset ? ",SET," : ",RESET,") << timestamp << "," << time
				<< "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension() << "\n";
			tag_out.close();
		}
		return res;
	}

	void RecordController::GetDurationFromTags(bool tag)
	{
		RelayOffset = 0;
		RelayDuration = 0;
		std::ifstream fin((filename + "/session.tag").c_str());
		std::string line;
		std::vector<unsigned long long> times;
		while (getline(fin, line))
		{
			auto fields = GetFields(line, ",");
			if (fields.size() > 3)
			{
				times.push_back(stoull(fields[3]));
			}
		}
		if (times.size() > 1)
		{
			if (tag)
			{
				RelayDuration = times.back();
				RelayOffset = times.front();
				TotalDurationTag = RelayDuration - RelayOffset;
			}
			else
			{
				RelayDuration = TotalDuration = (times.back() - times.front()) / 100;
			}
		}
	}

	bool RecordController::DoesExist()
	{
		std::string file = GetFileName();
		return boost::filesystem::exists(file);
	}

	void RecordController::TagFileOperation(bool begin)
	{
		if (!m_bRead)
		{
			tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
			std::string mss = Logger::TimeStamp();
			tag_out << (begin ? 1 : 2) << ",LOCAL_EVENT,SET," << timestamp << "," << mss << "." << timestamp % 1000
				<< "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension() << "\n";
			tag_out.close();
		}
	}

	void RecordController::SetReplaySessionByTime(unsigned long long b, unsigned long long d)
	{
		nFiles = 1;
		std::string file = GetFileName();
		replydatasize = 0;
		while (boost::filesystem::exists(file))
		{
			replydatasize += filesize(file);
			++nFiles;
			file = GetFileName();
		}
		nFiles = 1;
		long double br = 0;
		long double dr = 0;
		if (TotalDuration != 0)
		{
			br = (long double)b / TotalDuration;
			dr = (long double)(d+b) / TotalDuration;
		}
		replybegin = (long long)(br * replydatasize);
		replyduration = (long long)(dr * replydatasize);
	}

	void RecordController::SetReplaySessionBySize(unsigned long long b, unsigned long long d)
	{
		replybegin = b;
		replyduration = d;
	}

	void RecordController::SetReplaySessionByTag(unsigned long long b, unsigned long long d)
	{
		std::ifstream fin((filename + "/session.tag").c_str());
		std::string line;
		std::vector<unsigned long long> times;
		unsigned long long starttag = 0;
		while (getline(fin, line))
		{
			auto fields = GetFields(line, ",");
			if (fields.size() > 3)
			{
				starttag = stoull(fields[3]);
				break;
			}
		}
		nFiles = 1;
		std::string file = GetFileName();
		replydatasize = 0;
		while (boost::filesystem::exists(file))
		{
			replydatasize += filesize(file);
			++nFiles;
			file = GetFileName();
		}
		nFiles = 1;
		long double br = 0;
		long double dr = 0;
		if (TotalDurationTag != 0)
		{
			br = (long double)(b - starttag) / (TotalDurationTag);
			dr = (long double)(d - b) / (TotalDurationTag);
		}
		replybegin = (long long)(br * replydatasize);
		replyduration = (long long)(dr * replydatasize);
	}
}
