#include "INetworkDevice.h"
#include "RecordController.h"
#include <algorithm>

namespace NetworkLib
{

	void NetworkDeviceRunner::operator()()
	{
		if (m_pDevice)
		{
			while (m_bRunning)
			{
				m_pDevice->HandlePacket();
			}
		}
	}

	INetworkDevice::INetworkDevice()
		: m_bActive(false)
		, m_nSavedPackets(0)
		, m_runOption(READ_FROM_DEVICE)
		, m_bRelay(false)
		, m_SFPDPSpeed(SPEED_2_5G)
		, recorder(new RecordController())
		, ReplayFlag(SECOND)
	{
	}

	INetworkDevice::~INetworkDevice()
	{
	}

	void INetworkDevice::SetRunOption(RunOption flag)
	{
		m_runOption = flag;
		if (m_runOption == WRITE_TO_DEVICE)
		{
			recorder->GetDurationFromTags(ReplayFlag == TAG);
		}
	}

	void INetworkDevice::SetReplayFile(const std::string& filename)
	{
		FileName = filename;
		if (m_runOption == WRITE_TO_DEVICE)
		{
			recorder->filename = filename;
			recorder->GetDurationFromTags(ReplayFlag == TAG);
		}
	}

	std::string INetworkDevice::GetRecordFile()
	{
		return recorder->GetFileName();
	}

	int INetworkDevice::GetRunNumber()
	{
		return recorder->RecordNumber;
	}

	void INetworkDevice::SetRunNumber(int num)
	{
		recorder->RecordNumber = num;
	}

	void INetworkDevice::AddPacketMonitor(const std::shared_ptr<IPacketMonitor>& pMonitor)
	{
		m_Monitors.push_back(pMonitor);
	}

	void INetworkDevice::RecordBegin()
	{
		recorder->Open(true);
	}

	void INetworkDevice::RecordEnd()
	{
		recorder->Close();
	}

	bool INetworkDevice::IsRecording()
	{
		return recorder->Running;
	}

	void INetworkDevice::AddTag(int tagidx, bool bset)
	{
		recorder->AddTagToRecord(tagidx, bset);
	}

	unsigned long long INetworkDevice::AddTag(unsigned long long tagidx, bool bset, const std::string& time)
	{
		return recorder->AddMetaTagToRecord(tagidx, bset, time);
	}

	int INetworkDevice::GetID()
	{
		return recorder->ID;
	}

	void INetworkDevice::SetID(int id)
	{
		recorder->ID = id;
	}

	long long INetworkDevice::GetTotalDataSize()
	{
		return recorder->TotalData;
	}
	
	int INetworkDevice::GetRelayID()
	{
		return recorder->RelayID;
	}

	void INetworkDevice::SetRelayID(int id)
	{
		recorder->RelayID = id;
		if (m_runOption == WRITE_TO_DEVICE)
		{
			recorder->GetDurationFromTags(ReplayFlag == TAG);
		}
	}

	void INetworkDevice::SetRelayDuration(unsigned long long ns)
	{
		if (ReplayFlag == SECOND)
		{
			recorder->RelayDuration = min(ns, recorder->TotalDuration - recorder->RelayOffset);
		}
		else if (ReplayFlag == TAG)
		{
			recorder->RelayDuration = min(ns, recorder->TotalDurationTag - recorder->RelayOffset);
		}
	}

	unsigned long long INetworkDevice::GetRelayDuration()
	{
		return recorder->RelayDuration;
	}

	void INetworkDevice::SetRelayOffset(unsigned long long ns)
	{
		recorder->RelayOffset = ns;
	}

	unsigned long long INetworkDevice::GetRelayOffset()
	{
		return recorder->RelayOffset;
	}

	unsigned long long INetworkDevice::GetRelayRecordDuration()
	{
		if (ReplayFlag == SECOND)
		{
			return recorder->TotalDuration;
		}
		else if (ReplayFlag == TAG)
		{
			return recorder->TotalDurationTag;
		}
		return 0;
	}

	void INetworkDevice::SetRelayRecordDuration(unsigned long long ns)
	{
		recorder->TotalDuration = recorder->TotalDurationTag = ns;
	}

	unsigned long long INetworkDevice::GetRecordDuration()
	{
		return recorder->RecordDuration;
	}

	void INetworkDevice::SetRecordDuration(unsigned long long d)
	{
		recorder->RecordDuration = d;
	}

	long long INetworkDevice::RemainRecordTime()
	{
		return recorder->RemainRecordDuration();
	}

	unsigned long long INetworkDevice::RecordSpeed()
	{
		return recorder->LastMinuteData;
	}

}
