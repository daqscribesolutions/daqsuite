#ifndef NETWORK_DEVICERECORDSETTING_H
#define NETWORK_DEVICERECORDSETTING_H

#include <string>
#ifdef max
#undef max
#endif

#include <algorithm>
#include <vector>

namespace NetworkLib
{

	std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters);

	/**
	 * DeviceRecordSetting stores the setting for the recording such as the trigger information and the recording path.
	 */
	class DeviceRecordSetting
	{

	public:

		/// GetInstance supports singleton architecture.
		static DeviceRecordSetting* GetInstance();

		static void Clear();

		/**
		 * GetNetworkRecordFileName for the device index. This handles session name.
		 * @param id The index of the device.
		 * @param record The run number of device.
		 * @param bsession The flag for session file instead of device file.
		 * @return The file name to record the device with the index id.
		 */
		std::string GetNetworkRecordFileName(size_t id, int record, bool bsession = false);

		/// The getter for record durataion.
		unsigned long long GetRecordMaximumSize();

		/// The setter for record duration.
		void SetRecordMaximumSize(unsigned long long recordsize);

		/// The getter for record path.
		const std::string& GetRecordPath() { return m_RecordPath; }

		/// The setter for record path.
		void SetRecordPath(const std::string& path) { m_RecordPath = path; }

		/// The getter for replay path.
		const std::string& GetReplayPath() { return m_ReplayPath; }

		/// The setter for replay path.
		void SetReplayPath(const std::string& path) { m_ReplayPath = path; }

		/// The getter for record section name.
		const std::string& GetRecordName() { return m_RecordName; }

		/// The setter for record section name.
		void SetRecordName(const std::string& recordName) { m_RecordName = recordName; }

		/// The getter for replay section name.
		const std::string& GetReplayName() { return m_ReplayName; }

		/// The setter for replay section name.
		void SetReplayName(const std::string& replayName) { m_ReplayName = replayName; }

		/// Trigger Time.
		tm RecordTriggerTime;

		unsigned long WRITE_NUM_SINGLE_BUFFERS;

		unsigned long WRITE_NUM_BUFFERS;

		unsigned long WRITE_BUFFER_SIZE;

		unsigned long READ_NUM_SINGLE_BUFFERS;

		unsigned long READ_NUM_BUFFERS;

		unsigned long READ_BUFFER_SIZE;

	private:

		/// The directory to store the device data.
		std::string m_RecordPath;

		/// The directory to store the device data.
		std::string m_ReplayPath;

		/// The record name for the file name.
		std::string m_RecordName;

		/// The replay name for the file name.
		std::string m_ReplayName;

		/// The record beginning time.
		long long m_RecordMaxSize;

		/// The single instance to support singleton.
		static DeviceRecordSetting* m_Instance;

		DeviceRecordSetting();

		~DeviceRecordSetting();

	};

}

#endif
