#include "string"
#include "iostream"
#include "fstream"
#include <ctime>
#include <windows.h>
#include "LogManager.h"


namespace NetworkLib
{

	LogManager* LogManager::instance = 0;

	LogManager* LogManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new LogManager();
		}
		return instance;
	}

	LogManager::LogManager()
	{

	}

	LogManager::~LogManager()
	{

	}

	void LogManager::GetDate()
	{
		GetLocalTime(&today);
		char currentTime[80] = "";

		sprintf_s(currentTime, "%d/%d/%d %d:%d:%d %d ", today.wDay, today.wMonth, today.wYear,
			today.wHour, today.wMinute, today.wSecond, today.wMilliseconds);

		date = currentTime;
	}

	void LogManager::WriteLog(string message)
	{
		//kjk
		GetDate();
		os.open("DaqScribeLog.txt", ios::app);
		os << date << splite << message << endl;
		os.close();
	}

	void LogManager::WriteTimeLog(string method, clock_t message)
	{
		//kjk
		GetDate();
		os.open("DaqScribeLog.txt", ios::app);
		os << date << splite << method << "_" <<message << endl;
		os.close();
	}

}