TEMPLATE = lib
TARGET = NetworkLib
DESTDIR = ../build
CONFIG += staticlib
CONFIG += c++11

win32: DEFINES += _XKEYCHECK_H
DEFINES += _NT_TOOLS \
        _NTAPI_EXTDESCR_7_ \
        HAVE_STRING_H \
        _NT_HOST_64BIT

INCLUDEPATH += . \
    ./../../../boost_1_59_0 \
    ./../../ThirdParty/Napatech/include

win32: INCLUDEPATH += ./../../ThirdParty/winpcap/Include \
    ./../../ThirdParty/SFPDP/include

LIBS += -L"./../../../boost_1_59_0/lib32-msvc-12.0" \

win32: LIBS += -L"./../../ThirdParty/winpcap/lib" \   -lwpcap \
    -lWs2_32 \
    -l./../../ThirdParty/Napatech/lib -llibntapi64 \
    -l./../../ThirdParty/Napatech/lib -llibntutil64

include(NetworkLib.pri)
