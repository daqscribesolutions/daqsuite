#include "NetworkDevice.h"
#include "SFPDPDevice.h"
#include "GESFPDPDevice.h"
#include "NTNetworkDevice.h"
#include "NetworkDeviceManager.h"
#include "NetworkDeviceFactory.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "DeviceRecordSetting.h"
#include <boost/filesystem.hpp>
#include "logger.h"
#include "usertagmanager.h"

namespace NetworkLib
{
	NetworkDeviceManager* NetworkDeviceManager::instance = 0;

	NetworkDeviceManager* NetworkDeviceManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new NetworkDeviceManager();
		}
		return instance;
	}

	void NetworkDeviceManager::Clear()
	{
		Logger::Clear();
		DeviceRecordSetting::Clear();
		SFPDPManager::Clear();
		GESFPDPManager::Clear();
		delete instance;
		instance = 0;
	}

	NetworkDeviceManager::NetworkDeviceManager()
		: m_bRunning(false)
		, m_MonitoringMode(STOP)
		, m_PacketSize(2048)
		, m_Option(SFPDP)
		, ServerPort(5999)
		, TriggerPort(1)
	{
	}

	NetworkDeviceManager::~NetworkDeviceManager()
	{
	}

	void NetworkDeviceManager::AddPacketMonitor(const std::shared_ptr<IPacketMonitor>& pMonitor)
	{
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = recordDevices[i];
			pDevice->AddPacketMonitor(pMonitor);
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = replayDevices[i];
			pDevice->AddPacketMonitor(pMonitor);
		}
	}

	void NetworkDeviceManager::Monitor()
	{
		if (m_bRunning) return;
		m_bRunning = true;
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = recordDevices[i];
			if (pDevice)
			{
				pDevice->Monitor();
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = replayDevices[i];
			if (pDevice)
			{
				pDevice->Monitor();
			}
		}
		m_Monitor.Reset();
	}

	void NetworkDeviceManager::Stop()
	{
		if (!m_bRunning) return;
		m_bRunning = false;

		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
			if (pDevice)
			{
				pDevice->RecordEnd();
				pDevice->Stop();
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice)
			{
				pDevice->RecordEnd();
				pDevice->Stop();
			}
		}

		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
			if (pDevice)
			{
				pDevice->StopThread();
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice)
			{
				pDevice->StopThread();
			}
		}
		m_MonitoringMode = STOP;
	}

	std::string NetworkDeviceManager::GetErrorInfo()
	{
		std::string error;
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
			if (pDevice && pDevice->ErrorInfo != "")
			{
				error += std::to_string((long long)i + 1) + ":" + pDevice->ErrorInfo + ";";
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice && pDevice->ErrorInfo != "")
			{
				error += std::to_string((long long)i + 1) + ":" + pDevice->ErrorInfo + ";";
			}
		}
		return error;
	}

	void NetworkDeviceManager::Init(const std::string& filename)
	{
		ReadSystemInfo(filename);
		recordDevices.clear();
		replayDevices.clear();
		NetworkDeviceFactory factory(m_Option);
		AddDevices(factory);
		ReadSystemInfo(filename);
		for (int i = 0; i < (int)recordDevices.size(); ++i)
		{
			recordDevices[i]->SetID(i + 1);
		}
		for (int i = 0; i < (int)replayDevices.size(); ++i)
		{
			replayDevices[i]->SetID(i + 1);
		}
	}

	void NetworkDeviceManager::AddDevices(INetworkDeviceFactory& factory)
	{
		std::vector<INetworkDevice*> devices;
		factory.GetNetworkDevices(devices);
		for (int i = 0; i < (int)devices.size(); ++i)
		{
			INetworkDevice* pDevice = devices[i];
			if (pDevice)
			{
				if (pDevice->GetRunOption() != WRITE_TO_DEVICE)
				{
					pDevice->SetID((int)recordDevices.size() + 1);
					recordDevices.push_back(std::shared_ptr<INetworkDevice>(pDevice));
				}
				else
				{
					pDevice->SetID((int)replayDevices.size() + 1);
					replayDevices.push_back(std::shared_ptr<INetworkDevice>(pDevice));
				}
			}
		}
	}

	void NetworkDeviceManager::Record()
	{
		if (!IsRunning())
		{
			Monitor();
		}
		for (int i = 0; i < (int)recordDevices.size(); ++i)
		{
			recordDevices[i]->SetReplayFile(DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber()));
			recordDevices[i]->RecordBegin();
		}
	}

	void NetworkDeviceManager::ReadSystemInfo(const std::string& xmlFile)
	{
		try
		{
			using boost::property_tree::ptree;
			ptree pt;
			read_xml(xmlFile, pt);
			m_Option = (NetworkType)pt.get<int>("Network.Setting");
			m_PacketSize = pt.get<int>("Network.BufferSize");
			ServerPort = pt.get<int>("Network.ServerPort");
			TriggerPort = pt.get<int>("Network.TriggerPort");
			auto pRecordSetting = DeviceRecordSetting::GetInstance();
			pRecordSetting->SetRecordPath(pt.get<std::string>("Record.Path"));
			pRecordSetting->SetRecordName(pt.get<std::string>("Record.RecordName"));
			pRecordSetting->SetRecordMaximumSize(pt.get<long long>("Record.RecordMaxSize"));
			pRecordSetting->READ_NUM_SINGLE_BUFFERS = pt.get<unsigned long>("Record.READ_NUM_SINGLE_BUFFERS");
			pRecordSetting->READ_NUM_BUFFERS = pt.get<unsigned long>("Record.READ_NUM_BUFFERS");
			pRecordSetting->READ_BUFFER_SIZE = pt.get<unsigned long>("Record.READ_BUFFER_SIZE");
			pRecordSetting->WRITE_NUM_SINGLE_BUFFERS = pt.get<unsigned long>("Record.WRITE_NUM_SINGLE_BUFFERS");
			pRecordSetting->WRITE_NUM_BUFFERS = pt.get<unsigned long>("Record.WRITE_NUM_BUFFERS");
			pRecordSetting->WRITE_BUFFER_SIZE = pt.get<unsigned long>("Record.WRITE_BUFFER_SIZE");
			tm tt;
			std::string ti = pt.get<std::string>("Record.RecordTriggerTime");
			std::istringstream ss(ti);
			ss >> std::get_time(&tt, "%EY:%m:%d-%H:%M:%S");
			time_t t = mktime(&tt);
			auto tm = localtime(&t);
			if (tm)
			{
				pRecordSetting->RecordTriggerTime = (*tm);
			}

			NTNetworkDeviceManager::GetInstance()->numaIdx = pt.get<int>("Record.NUMA");

			ptree su = pt.get_child("NetworkDevices");
			std::vector<bool> setdevs(recordDevices.size(), false);
			for (ptree::iterator itr = su.begin(); itr != su.end(); ++itr)
			{
				if (itr->first == "RecordDevice")
				{
					std::string name = itr->second.get<std::string>("Name");
					std::string address = itr->second.get<std::string>("Address(Port)");
					std::string filename = itr->second.get<std::string>("FileReplayName");
					std::string Filter = itr->second.get<std::string>("Filter");
					int num = itr->second.get<int>("RunNumber");
					ReplayOption relayflag = (ReplayOption)itr->second.get<int>("ReplayFlag");
					unsigned long long relayoffset = itr->second.get<unsigned long long>("RelayOffset");
					unsigned long long relayduration = itr->second.get<unsigned long long>("RelayDuration");
					unsigned long long recordduration = itr->second.get<unsigned long long>("RecordDuration");
					int nLink = itr->second.get<int>("LinkRate");
					bool bSave = itr->second.get<bool>("Active");
					for (int i = 0; i < (int)recordDevices.size(); ++i)
					{
						if (setdevs[i]) continue;
						auto& dev = recordDevices[i];
						if (dev->Address == address)
						{
							setdevs[i] = true;
							dev->Name = name;
							dev->SetReplayFile(filename);
							dev->SetActiveFlag(bSave);
							dev->ReplayFlag = relayflag;
							dev->SetRecordDuration(recordduration);
							dev->SetRelayOffset(relayoffset);
							dev->SetRelayDuration(relayduration);
							dev->PCAPFilter = Filter;
							dev->SetRunNumber(num);
							auto pDev = dynamic_cast<INetworkDevice*>(dev.get());
							auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
							if (pSFPDP) pSFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
							if (pGESFPDP) pGESFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							break;
						}
					}
				}
			}
			std::vector<bool> setreplaydevs(replayDevices.size(), false);
			for (ptree::iterator itr = su.begin(); itr != su.end(); ++itr)
			{
				if (itr->first == "ReplayDevice")
				{
					std::string name = itr->second.get<std::string>("Name");
					std::string address = itr->second.get<std::string>("Address(Port)");
					std::string filename = itr->second.get<std::string>("FileReplayName");
					std::string Filter = itr->second.get<std::string>("Filter");
					int num = itr->second.get<int>("RunNumber");
					ReplayOption relayflag = (ReplayOption)itr->second.get<int>("ReplayFlag");
					unsigned long long relayoffset = itr->second.get<unsigned long long>("RelayOffset");
					unsigned long long relayduration = itr->second.get<unsigned long long>("RelayDuration");
					unsigned long long recordduration = itr->second.get<unsigned long long>("RecordDuration");
					unsigned long long replyrecordduration = itr->second.get<unsigned long long>("RelayRecordDuration");
					int nLink = itr->second.get<int>("LinkRate");
					bool bSave = itr->second.get<bool>("Active");
					for (int i = 0; i < (int)replayDevices.size(); ++i)
					{
						if (setreplaydevs[i]) continue;
						auto& dev = replayDevices[i];
						if (dev->Address == address)
						{
							setreplaydevs[i] = true;
							dev->Name = name;
							dev->SetReplayFile(filename);
							dev->SetActiveFlag(bSave);
							dev->PCAPFilter = Filter;
							dev->SetRunNumber(num);
							dev->ReplayFlag = relayflag;
							dev->SetRecordDuration(recordduration);
							dev->SetRelayOffset(relayoffset);
							dev->SetRelayDuration(relayduration);
							dev->SetRelayRecordDuration(replyrecordduration);
							auto pDev = dynamic_cast<INetworkDevice*>(dev.get());
							auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
							if (pSFPDP) pSFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
							if (pGESFPDP) pGESFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							break;
						}
					}
				}
			}
			auto tu = pt.get_child_optional("UserTags");
			if (tu)
			{
				auto& tags = UserTagManager::GetInstance()->Tags;
				tags.clear();
				for (ptree::iterator itr = tu->begin(); itr != tu->end(); ++itr)
				{
					if (itr->first == "UserTag")
					{
						UserTag tag;
						tag.Name = itr->second.get<std::string>("Name");
						tag.Description = itr->second.get<std::string>("Description");
						tags.push_back(tag);
					}
				}
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << "\n";
		}
	}

	void NetworkDeviceManager::WriteSystemInfo(const std::string& xmlFile)
	{
		try
		{
			using boost::property_tree::ptree;
			ptree pt;
			pt.put("NetworkRecorder.version", 1);
			pt.put("Network.Setting", m_Option);
			pt.put("Network.Setting.Descirption", "0:PCAP, 1:SFPDP, 2:GESFPDP, 3:NAPATECH");
			pt.put("Network.BufferSize", m_PacketSize);
			pt.put("Network.ServerPort", ServerPort);
			pt.put("Network.TriggerPort", TriggerPort);
			auto pRecordSetting = DeviceRecordSetting::GetInstance();
			pt.put("Record.Path", pRecordSetting->GetRecordPath());
			pt.put("Record.NUMA", NTNetworkDeviceManager::GetInstance()->numaIdx);
			pt.put("Record.RecordName", pRecordSetting->GetRecordName());
			pt.put("Record.RecordMaxSize", pRecordSetting->GetRecordMaximumSize());
			pt.put("Record.READ_NUM_SINGLE_BUFFERS", pRecordSetting->READ_NUM_SINGLE_BUFFERS);
			pt.put("Record.READ_NUM_BUFFERS", pRecordSetting->READ_NUM_BUFFERS);
			pt.put("Record.READ_BUFFER_SIZE", pRecordSetting->READ_BUFFER_SIZE);
			pt.put("Record.WRITE_NUM_SINGLE_BUFFERS", pRecordSetting->WRITE_NUM_SINGLE_BUFFERS);
			pt.put("Record.WRITE_NUM_BUFFERS", pRecordSetting->WRITE_NUM_BUFFERS);
			pt.put("Record.WRITE_BUFFER_SIZE", pRecordSetting->WRITE_BUFFER_SIZE);
			auto t = pRecordSetting->RecordTriggerTime;
			std::stringstream str;
			str << t.tm_year + 1900 << ":" << t.tm_mon + 1 << ":" << t.tm_mday << "-" << t.tm_hour << ":" << t.tm_min << ":" << t.tm_sec;
			pt.put("Record.RecordTriggerTime", str.str());
			
			for (int i = 0; i < (int)recordDevices.size(); ++i)
			{
				auto pDevice = recordDevices[i];
				ptree & node = pt.add("NetworkDevices.RecordDevice", "");
				node.put("Name", pDevice->Name);
				node.put("Address(Port)", pDevice->Address);
				node.put("FileReplayName", pDevice->GetReplayFile());
				node.put("Filter", pDevice->PCAPFilter);
				node.put("Active", pDevice->GetActiveFlag());
				node.put("RunNumber", pDevice->GetRunNumber());
				node.put("RecordDuration", (unsigned long long)pDevice->GetRecordDuration());
				node.put("RelayOffset", (unsigned long long)pDevice->GetRelayOffset());
				node.put("RelayDuration", (unsigned long long)pDevice->GetRelayDuration());
				node.put("RelayRecordDuration", (unsigned long long)pDevice->GetRelayRecordDuration());
				node.put("ReplayFlag", pDevice->ReplayFlag);
				auto pDev = dynamic_cast<INetworkDevice*>(pDevice.get());
				auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
				auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
				node.put("LinkRate", pSFPDP ? pSFPDP->GetSFPDPSpeedOption() : (pGESFPDP ? pGESFPDP->GetSFPDPSpeedOption() : 0));
			}
			for (int i = 0; i < (int)replayDevices.size(); ++i)
			{
				auto pDevice = replayDevices[i];
				ptree & node = pt.add("NetworkDevices.ReplayDevice", "");
				node.put("Name", pDevice->Name);
				node.put("Address(Port)", pDevice->Address);
				node.put("FileReplayName", pDevice->GetReplayFile());
				node.put("Filter", pDevice->PCAPFilter);
				node.put("Active", pDevice->GetActiveFlag());
				node.put("RunNumber", pDevice->GetRunNumber());
				node.put("RecordDuration", (unsigned long long)pDevice->GetRecordDuration());
				node.put("RelayOffset", (unsigned long long)pDevice->GetRelayOffset());
				node.put("RelayDuration", (unsigned long long)pDevice->GetRelayDuration());
				node.put("RelayRecordDuration", (unsigned long long)pDevice->GetRelayRecordDuration());
				
				node.put("ReplayFlag", pDevice->ReplayFlag);
				auto pDev = dynamic_cast<INetworkDevice*>(pDevice.get());
				auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
				auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
				node.put("LinkRate", pSFPDP ? pSFPDP->GetSFPDPSpeedOption() : (pGESFPDP ? pGESFPDP->GetSFPDPSpeedOption() : 0));
			}
			auto tags = UserTagManager::GetInstance()->Tags;
			for (int i = 0; i < (int)tags.size(); ++i)
			{
				auto tag = tags[i];
				ptree & node = pt.add("UserTags.UserTag", "");
				node.put("Name", tag.Name);
				node.put("Description", tag.Description);
			}
			boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
			write_xml(xmlFile, pt, std::locale(), settings);
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << "\n";
		}
	}

	std::string NetworkDeviceManager::GetExtension()
	{
		return (m_Option == PCAP ? ".pcap" : (m_Option == NAPA_TECH ? ".ntcap" : ".sfpdp"));
	}

}
