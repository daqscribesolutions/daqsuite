#ifndef NTNETWORKDEVICE_H
#define NTNETWORKDEVICE_H

#include "NetworkDevice.h"
#include "nt.h"
#include "LogManager.h"

namespace NetworkLib
{

	/// ntpcap_ts_s is the time structure for Napatech device.
	struct ntpcap_ts_s
	{
		/// Second
		uint32_t sec;

		/// Used second
		uint32_t usec;
	};

	/**
	* NTNetworkDevice implementation of INetworkDevice for Napatech devices with ntpcap file support.
	*/
	class NTNetworkDevice : public INetworkDevice
	{

	public:

		/// Default constructor.
		NTNetworkDevice();

		/// Default destructor.
		~NTNetworkDevice();

		/// Implementation of INetworkDevice.
		virtual void Monitor();

		virtual void Stop();

		virtual void StopThread();

		virtual void HandlePacket();

		virtual void RecordBegin();

		virtual void RecordEnd();

		void Replay();

	private:
		
#ifdef NPTINCLUDE

		/// Handle to the RX stream
		NtNetStreamRx_t hNetRx;

		/// Handle to a config stream
		NtConfigStream_t hCfgStream;

		/// Return data structure from the NT_NTPL() call.
		NtNtplInfo_t ntplInfo;

		/// Net buffer container. Segment data is returned in this when calling NT_NetRxGet().
		NtNetBuf_t hNetBuf;

		/// NetRx read command structure.
		NtNetRx_t readCmd;

		/// Packet netbuf structure.
		NtNetBuf_s pktNetBuf;

		/// The time stamp.
		ntpcap_ts_s *pTs;

		/// Handle to the TX stream
		NtNetStreamTx_t hNetTx;

		/// Net buffer container. Used when getting a transmit buffer
		NtNetBuf_t hNetBufTx;

		/// Handle to a info stream
		NtInfoStream_t hInfo;

		/// Buffer to hold data from infostream
		NtInfo_t infoRead;

		/// TX mode - Do we use absolut or relative mode
		NtTxTimingMethod_e txTiming;

		/// The error information storage.
		char errorBuffer[NT_ERRBUF_SIZE];

		bool bReplay;

		bool check = true;
#endif

		int firstPacket;

		/// The status flag for the device.
		int status;

		//kjk
		LogManager* log;
	};

	struct NTNetworkDeviceManager
	{

		static NTNetworkDeviceManager * GetInstance();

		static void Clear();

		void Init();

		bool IsOpen();

		void Close();

		uint8_t numAdapters;

		int nPorts;

		int numaIdx;

	private:

		bool m_bInitialized;

		static NTNetworkDeviceManager * instance;



	};

}
#endif
