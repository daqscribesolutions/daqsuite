#ifndef NETWORKDEVICEMANAGER_H
#define NETWORKDEVICEMANAGER_H

#include <string>
#include <vector>
#include <memory>
#include "INetworkDevice.h"
#include "INetworkDeviceFactory.h"
#include "DataSpeedMonitor.h"

namespace NetworkLib
{
	struct RecordRunner;

	/// System State.
	enum RunMode { MONITORING, STOP };

	/**
	 * NetworkDeviceManager supports the singleton architecture to handle the network devices recording and replaying.
	 */
	class NetworkDeviceManager
	{

	public:

		/// The accessor for the single instance.
		static NetworkDeviceManager* GetInstance();

		/// Clear releases resources.
		static void Clear();

		/// The getter for the network devices.
		const std::vector<std::shared_ptr<INetworkDevice>>& GetRecordDevices() { return recordDevices; }

		/// The getter for the network devices.
		const std::vector<std::shared_ptr<INetworkDevice>>& GetReplayDevices() { return replayDevices; }


		/// Moitor begins monitoring network data.
		void Monitor();

		/// Stop ends the recording.
		void Stop();

		/// Close devices.
		void Close();

		/// The accessor for the record state.
		bool IsRunning() { return m_bRunning; }

		/**
		 * AddPacketMonitor adds the packet monitor to be notified when the packet is updated.
		 * @param pMonitor The pointer of IPacketMonitor to be added.
		 */
		void AddPacketMonitor(const std::shared_ptr<IPacketMonitor>& pMonitor);

		/// The accessor for the network buffer size to be used in client/server network.
		int GetNetworkPacketSize() { return m_PacketSize; }

		/// The setter for the network buffer size to be used in client/server network.
		void SetNetworkPacketSize(int bufferSize) { m_PacketSize = bufferSize; }

		/**
		 * ReadSystemInfo loads the saved system information.
		 * @param xmlFile The xml file name to load from.
		 */
		void ReadSystemInfo(const std::string& xmlFile);

		/**
		 * WriteSystemInfo saves the system information.
		 * @param xmlFile The xml file name to be saved.
		 */
		void WriteSystemInfo(const std::string& xmlFile);

		/// The accessor for the network device setting option.
		NetworkType GetNetworkDeviceSetting() { return m_Option; }

		/// The setter for the network device setting option.
		void SetNetworkDeviceSetting(NetworkType option) { m_Option = option; }

		/**
		 * AddDevices add devices from the factory to the system.
		 * @param factory The device factory for the different network devices.
		 */
		void AddDevices(INetworkDeviceFactory& factory);

		/// The setter the mode for the running.
		void SetRunMode(RunMode m) { m_MonitoringMode = m; }

		/// The getter for the running mode.
		RunMode GetRunMode() { return m_MonitoringMode; }

		/**
		 * Init gets the network devices available.
		 * @param filename The option file name to read.
		 * @return The available network devices are added.
		 */
		void Init(const std::string& filename);

		/// The accessor for the system error.
		std::string GetErrorInfo();

		/// The getter for the file extension for the current device type.
		std::string GetExtension();

		/// Record all record devices.
		void Record();

		int ServerPort;

		int TriggerPort;

	private:

		/// Default constructor.
		NetworkDeviceManager();

		/// Default destructor.
		~NetworkDeviceManager();

		/// The network device pointers.
		std::vector<std::shared_ptr<INetworkDevice>> replayDevices;

		std::vector<std::shared_ptr<INetworkDevice>> recordDevices;

		/// The static instance.
		static NetworkDeviceManager* instance;

		/// The flag for monitoring.
		bool m_bRunning;

		/// The flag for monitoring mode.
		RunMode m_MonitoringMode;

		/// The packet for ADC device server/client connection.
		int m_PacketSize;

		/// The flag for the system option whether PCAP or SFPDP.
		NetworkType m_Option;

		/// The time monitor.
		DataSpeedMonitor m_Monitor;

	};

}

#endif
