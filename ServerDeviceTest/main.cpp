#include <ctime>
#include <iostream>
#include <string>
#include "../DeviceLib/DeviceLib.h"

using namespace DeviceLib;

class DataMonitor : public IDeviceDataUpdateListener
{
	DataMonitor() {}

	~DataMonitor() {}

	virtual void UpdateEntryData(DeviceData* data)
	{
		
	}

	virtual void UpdateExitData(DeviceData* data)
	{

	}

	virtual bool StartListening()
	{

	}

	virtual void StopListening()
	{

	}
};

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		std::cout << "Command: [EXE] [Number of Server Devices] [Host port] [Running time in second]\n";
		return 1;
	}

	int nDevice = atoi(argv[1]);
	int nPort = atoi(argv[2]);
	int nMillisecond = atoi(argv[3]) * 1000;

	for (int i = 0; i < nDevice; ++i)
	{
		SystemManager::GetInstance()->AddServerDevice(nPort);
		++nPort;
	}
	
	while (!SystemManager::GetInstance()->StartMonitor())
	{
		Sleep(100);
		std::cout << "Failed \n";
	}

	double d = clock();
	while (clock() - d < nMillisecond)
	{
	}

	SystemManager::GetInstance()->StopMonitor();

	return 0;
}
