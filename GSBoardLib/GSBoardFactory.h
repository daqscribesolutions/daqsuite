#ifndef GSBOARDFACTORY_H
#define GSBOARDFACTORY_H

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

namespace GenralStandard
{
	/**
	 * GSBoardFactory is the implementation of IDeviceFactory to support adding GS Boards to the system.
	 */
	class GSBoardFactory : public IDeviceFactory
	{

	public:

		/// Default constructor.
		GSBoardFactory();

		/// Default destructor.
		~GSBoardFactory();

		/// Implementation of IDeviceFactory interface.
		virtual void GetDevices(std::vector<IDevice*>& devices);

	};


}

#endif