//
// Gs16Ai64DeviceImpl.h
//

#pragma once
#include <Windows.h>
#include <string>
#include "66-AI64SSAintface.h"

#define DISPLAY_DATA_SAMPLE_COUNT		262144  // 256 k Samples

#define GS16AI64_SAMPLE_RATE_MIN		5000    // 5 KHz
#define GS16AI64_SAMPLE_RATE_MAX		200000  // 200 KHz
#define GS16AI64_BUFFER_LENGTH_MIN		1
#define GS16AI64_BUFFER_LENGTH_MAX		DISPLAY_DATA_SAMPLE_COUNT 

// Signal Type
enum GS16AI64SignalType
{
	GS16AI64_SIGNAL_SINGLE_ENDED,
	GS16AI64_SIGNAL_PSEUDO_DIFFERENTIAL,
	GS16AI64_SIGNAL_DIFFERENTIAL,
	GS16AI64_SIGNAL_TYPE_COUNT,
};

// Signal Type Text
extern std::string GS16AI64DeviceSignalTypeText[GS16AI64_SIGNAL_TYPE_COUNT];

//
// CGs16Ai64DeviceImpl
class CGs16Ai64DeviceImpl
{
public:

	// Constructor / Destructor
	CGs16Ai64DeviceImpl(void);
	virtual ~CGs16Ai64DeviceImpl(void);

	DWORD GetDeviceCount(void)            {  return m_DeviceCount;       }
	char *GetDeviceBoardInfo(void)        {  return m_pDeviceBoardInfo;  }
	
	DWORD GetDeviceNumber(void)           {  return m_DeviceNumber;      }
	void SetDeviceNumber(U32 value)       {  m_DeviceNumber = value;     }

	DWORD GetDeviceChannelCount(void);
	void SetDeviceChannelCount(DWORD val);

	DWORD GetDeviceMaxChannelCount(void)  {  return m_DeviceMaxChannelCount;  }

	bool IsOpen(void)                     {  return m_OpenFlag;          }
	bool Open(void);
	bool Close(void);

	bool SetInputRange(double value);
	
	bool InitDMA(int samplerate);
	void StartDMA();
	void UpdataDMA();
	void EndDMA();

	double GetRange() { return m_range; }

	U32    GetSampleSize()				{ return m_SampleSize; }
	void   SetSampleSize(U32 value)		{ m_SampleSize = value; }

	U32    GetSampleFormat()			{ return m_SampleFormat;  } // 0 for 2' complement, 1 forbinary offset
	void   SetSampleFormat(U32 value)	{ m_SampleFormat = value; }
	U32    GetBuffSize()				{ return m_Block2.Size; }
	U32	   GetReadBlockCount()			{ return m_ReadBlockCount; }
	void   ResetBlockCount()			{ m_ReadBlockCount = 0; }
	U32*   GetCurrentData();
	bool   GetOverFlow();

	GS16AI64SignalType GetSignalType()	{ return m_SignalType; }
	void   SetSignalType(GS16AI64SignalType value);
	void Calibrate();
	static std::string GetApiErrorDescription(U32 apiReturnCode);

protected:

	void CheckChannelCount();

	// Input Range
	float  m_range;

	int	   m_SampleFormat;
	// Local System Information
	DWORD  m_DeviceCount;
	char  *m_pDeviceBoardInfo;

	// Device
	DWORD  m_DeviceNumber;
	bool   m_OpenFlag;
	DWORD  m_DeviceChannelCount;
	DWORD  m_DeviceMaxChannelCount;
	GS16AI64SignalType m_SignalType;
	U32    m_SampleSize;
	U32    m_ReadBlockCount;
	U32    m_DataRateSampleCount;
	GS_PHYSICAL_MEM   m_Block1;
	GS_PHYSICAL_MEM   m_Block2;
	U32               m_pBuffer1[2096896];
	U32               m_pBuffer2[2096896];
	U32               m_Block1Allocated;
	U32               m_Block2Allocated;
	U32               m_DmaDataMode;
	GS_DMA_DESCRIPTOR m_DmaSetup;
	HANDLE            m_hEvent;
	GS_NOTIFY_OBJECT  m_NotifyObject;

	HANDLE            m_hThread;

	// Register Values
	U32    m_BoardControlRegister;
	U32    m_InterruptControlRegister;
	U32    m_InputDataBufferRegister;
	U32    m_InputBufferControlRegister;
	U32    m_RateAGeneratorRegister;
	U32    m_RateBGeneratorRegister;
	U32    m_BufferSizeRegister;
	U32    m_BurstSizeRegister;
	U32    m_ScanSyncControlRegister;
	U32    m_ActiveChannelAssignmentRegister;
	U32    m_BoardConfigurationRegister;
	U32    m_AutoCalValuesRegister;
	U32    m_AuxRwRegister;
	U32    m_AuxSyncIoControlRegister;
	U32    m_ScanMarkerFirstRegister;
	U32    m_ScanmarkerSecondRegister;

};