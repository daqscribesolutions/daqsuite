#ifndef BLACKBURNBOARDFACTORY_H
#define BLACKBURNBOARDFACTORY_H

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

namespace Blackburn
{
	/**
	* BlackburnBoardFactory is the implementation of IDeviceFactory to support adding Blackburn Boards to the system.
	*/
	class BlackburnBoardFactory : public IDeviceFactory
	{

	public:

		/// Default constructor.
		BlackburnBoardFactory();

		/// Default destructor.
		~BlackburnBoardFactory();

		/// Implementation of IDeviceFactory interface.
		virtual void GetDevices(std::vector<IDevice*>& devices);

	};


}

#endif