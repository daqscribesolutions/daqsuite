//
// Gs24Dsi32eDeviceArray.h
//

#pragma once

#include <vector>

DWORD WINAPI RunDeviceInit(LPVOID lpParam);

class CGs24Dsi32eDeviceImpl;

//
// CGs24Dsi32eDeviceArray
class CGs24Dsi32eDeviceArray
{
public:

	bool Init(void);
	void Free(void);

	// Local System Information
	DWORD GetDeviceCount(void)            { return m_DeviceCount; }
	char *GetDeviceBoardInfo(void)        { return m_pDeviceBoardInfo; }

	// Device
	INT_PTR GetCount(void)                { return m_Array.size(); }

	CGs24Dsi32eDeviceImpl *GetDevice(INT_PTR index);

	CGs24Dsi32eDeviceImpl *GetMasterDevice();

	CGs24Dsi32eDeviceImpl * GetCurrentDevice() { return m_pCurrentDevice; }
	void SetCurrentDevice(INT_PTR index);

	void SetInputRange(double range);

	bool SetSampleRate(double rate);
	double GetActualSampleRate() { return m_SampleRate; }


	bool RunAutoCal();

	bool RunInitialize();

	bool InitDMA(bool bSetByInterval = false);

	void CloseDMA();

	bool StartDMAAqusition();

	bool UpdateDMAData();

	bool IsMaster(DWORD boardNumber) { return m_MasterBoardNumber == boardNumber; }

	static CGs24Dsi32eDeviceArray* GetInstance();

	bool m_bAutoCalibration;
	void SetMemoryLimit(int nlimit) { m_MemoryLimit = nlimit; }
	double GetEstimatedInterval() { return m_EstimateInterval; }
	void SetEstimatedInterval(double interval) { m_EstimateInterval = interval; }
	bool IsDMA() { return m_bDMA; }

	int GetNUM_BLOCKS() { return m_NUM_BLOCKS; }

	void SetNUM_BLOCKS(int val) { m_NUM_BLOCKS = val; }

protected:

	static CGs24Dsi32eDeviceArray* pInstance;
	bool m_bDMA;
	int         dNumChan;
	double      m_SampleRate;
	int         buffer_data;
	int m_MemoryLimit;
	// Local System Information
	DWORD       m_DeviceCount;
	char  *     m_pDeviceBoardInfo;
	HANDLE *    m_pDeviceEvents;
	CGs24Dsi32eDeviceImpl*    m_pCurrentDevice;

	int m_NUM_BLOCKS;

	// Device Array
	std::vector<CGs24Dsi32eDeviceImpl*> m_Array;

private:
	double m_EstimateInterval;

	DWORD m_MasterBoardNumber;

	bool Initialized;

	bool needCalibration;
	double oldSampleRate;

	CGs24Dsi32eDeviceArray();

	virtual ~CGs24Dsi32eDeviceArray();

};

