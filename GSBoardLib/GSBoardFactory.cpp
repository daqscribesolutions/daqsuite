#include "GSBoardFactory.h"
#include "GS24DSI32EDevice.h"
#include "Gs24Dsi32eDeviceArray.h"

namespace GenralStandard
{
	void GSBoardFactory::GetDevices(std::vector<IDevice*>& devices)
	{
		devices.clear();
#ifndef X64
		CGs24Dsi32eDeviceArray * pDeviceArray = CGs24Dsi32eDeviceArray::GetInstance();
		long long id = 1;
		for (int i = 0; i < pDeviceArray->GetCount(); ++i)
		{
			GS24DSI32EDevice * pDevice = new GS24DSI32EDevice(i);
			auto& channels = pDevice->GetChannels();
			for (int i = 0; i < (int)channels.size(); ++i)
			{
				auto& ch = channels[i];
				ch->Name = "Channel " + std::to_string(id);
				++id;
			}
			devices.push_back(pDevice);
		}
#endif
	}

	GSBoardFactory::GSBoardFactory()
	{
	}

	GSBoardFactory::~GSBoardFactory()
	{
	}
}