//
// CGs24Dsi32eDeviceArray.cpp
//

#include "Gs24Dsi32eDeviceImpl.h"
#include "Gs24Dsi32eDeviceArray.h"
#include "Gs24Dsi32eRegister.h"
#include <omp.h>
#include <algorithm>
#include "66DSIPLLeintface.h"
//
// 4 boards info : 277 bytes
// -> 1 board info : about 70 ch
// this needs to support up to 16 boards : 70 x 16 = at least 1120 bytes
#define DEVICE_BOARD_INFO_SIZE         2048

CGs24Dsi32eDeviceArray* CGs24Dsi32eDeviceArray::pInstance = 0;

CGs24Dsi32eDeviceArray* CGs24Dsi32eDeviceArray::GetInstance()
{
	if (pInstance == 0)
	{
		pInstance = new CGs24Dsi32eDeviceArray();
		if (pInstance->Init())
			pInstance->SetCurrentDevice(0);
	}
	return pInstance;
}

//
// CGs24Dsi32eDeviceArray
CGs24Dsi32eDeviceArray::CGs24Dsi32eDeviceArray()
	: m_pDeviceEvents(NULL), dNumChan(32), Initialized(false), m_MasterBoardNumber(1), m_bAutoCalibration(true)
	, m_EstimateInterval(0)
	, m_bDMA(false)
	, m_NUM_BLOCKS(4)
	, m_MemoryLimit(DATA_SIZE)
	, oldSampleRate(m_SampleRate)
	, needCalibration(true)
{
	m_pDeviceBoardInfo = new char[DEVICE_BOARD_INFO_SIZE];
	ZeroMemory(m_pDeviceBoardInfo, DEVICE_BOARD_INFO_SIZE);
}

//
// ~CGs24Dsi32eDeviceArray
CGs24Dsi32eDeviceArray::~CGs24Dsi32eDeviceArray()
{
	Free();
}

//
// Init
bool CGs24Dsi32eDeviceArray::Init(void)
{
#ifndef X64
	U32 error = 0;

	// Get Device Count & Device Board Info
	m_DeviceCount = ::DSI32_66_FindBoards(m_pDeviceBoardInfo, &error);
	if (m_DeviceCount == 0) return false;
	std::string info(m_pDeviceBoardInfo);
	if (error || info.find("16AI64") != std::string::npos)
	{
		m_DeviceCount = 0;
		_tprintf(_T("Error : Failed to find boards in local system - %s\r\n"),
			error, CGs24Dsi32eDeviceImpl::GetApiErrorDescription(error));
		return false;
	}
	_tprintf(_T("Device Board Info : %d Boards are found (%d bytes)\r\n%S\r\n"),
		m_DeviceCount,
		strlen(m_pDeviceBoardInfo),
		m_pDeviceBoardInfo);

	if (GetCount() > 0)
		Free();

	DWORD    index;
	CGs24Dsi32eDeviceImpl *pDevice;
	m_pDeviceEvents = new HANDLE[m_DeviceCount];
	bool bSucceeded = true;
	struct device
	{
		CGs24Dsi32eDeviceImpl* pdev;
		bool operator<(const device& d) { return pdev->GetUID() < d.pdev->GetUID(); }
	};
	std::vector<device> devices;
	for (index = 1; index <= m_DeviceCount; index++)
	{
		pDevice = new CGs24Dsi32eDeviceImpl();
		pDevice->SetDeviceNumber(index);
		m_Array.push_back(pDevice);
		device dev;
		dev.pdev = pDevice;
		if (!dev.pdev->Open())
		{
			bSucceeded = false;
		}
		m_pDeviceEvents[index - 1] = dev.pdev->GetEventHandle();
		devices.push_back(dev);
	}

	std::sort(devices.begin(), devices.end());

	for (U32 i = 0; i < m_DeviceCount; i++)
	{
		device& dev = devices[i];
		m_pDeviceEvents[i] = dev.pdev->GetEventHandle();
		m_Array[i] = dev.pdev;
	}

	if (m_Array.size() > 0)
		m_MasterBoardNumber = m_Array[0]->GetDeviceNumber();

	if (!bSucceeded)
	{
		Free();
	}
	else
	{
		RunInitialize();
	}

	Initialized = bSucceeded;
	return bSucceeded;
#else
	return false;
#endif
}

//
// Free
void CGs24Dsi32eDeviceArray::Free(void)
{
	// Device Board Info
	if (m_pDeviceBoardInfo != NULL)
	{
		delete[] m_pDeviceBoardInfo;
		m_pDeviceBoardInfo = NULL;
	}

	if (m_pDeviceEvents != NULL)
	{
		delete[] m_pDeviceEvents;
		m_pDeviceEvents = NULL;
	}
	for (size_t i = 0; i < m_Array.size(); ++i)
	{
		delete m_Array[i];
	}
	m_Array.clear();
	m_DeviceCount = 0;
}

//
// GetDevice
CGs24Dsi32eDeviceImpl *CGs24Dsi32eDeviceArray::GetDevice(INT_PTR index)
{
	if (index < 0 || index >= GetCount())
		return 0;

	return m_Array[index];
}

//
// GetDevice
CGs24Dsi32eDeviceImpl * CGs24Dsi32eDeviceArray::GetMasterDevice()
{
	for (U32 i = 0; i < m_DeviceCount; i++)
	{
		if (IsMaster(m_Array[i]->GetDeviceNumber()))
		{
			return m_Array[i];
		}
	}
	return 0;
}

// 
// SetSampleRate
bool CGs24Dsi32eDeviceArray::SetSampleRate(double rate)
{
#ifndef X64
	U32 error = 0;
	if (rate < GS24DSI32E_SAMPLE_RATE_MIN)
	{
		rate = GS24DSI32E_SAMPLE_RATE_MIN;
	}
	if (rate > GS24DSI32E_SAMPLE_RATE_MAX)
	{
		rate = GS24DSI32E_SAMPLE_RATE_MAX;
	}

	// Master will be the Initiator board
	CGs24Dsi32eDeviceImpl * pMaster = GetMasterDevice();

	if (pMaster == 0) return false;

	// Sample Rate, Event
	{
		U32               nVco, nRef, nDiv;
		double            iClockRate = rate;

		// Set sampling rate
		if (m_SampleRate = CGs24Dsi32eDeviceImpl::CalculateParameters(pMaster->GetDeviceNumber(), &nVco, &nRef, &nDiv, iClockRate))
		{
			::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_NREF_PLL_CONTROL, nRef);
			::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_NVCO_PLL_CONTROL, nVco);
			::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_RATE_DIVISOR, nDiv);
		}

		::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_RATE_ASSIGNMENTS, 0x0000);

		// Master Board Event
		{
			// Master Board Event
			::DSI32_66_EnableInterrupt(pMaster->GetDeviceNumber(), 0x02, LOCAL, &error);  // Channels Ready Interrupt
			::DSI32_66_Register_Interrupt_Notify(pMaster->GetDeviceNumber(), pMaster->GetEvent(), 0x02, LOCAL, &error);

			U32 indata = ::DSI32_66_Read_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata |= 0x10000; // Set Syncronize Scan bit
			::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);

			DWORD dwStatus = ::WaitForSingleObject(pMaster->GetEventHandle(), 2 * 1000); // Wait for the interrupt
			switch (dwStatus)
			{
			case WAIT_OBJECT_0:
				break;
			default:
				return false;
				break;
			}
		}

		CGs24Dsi32eDeviceImpl * pDevice = 0;

		// Slave Board Event
		for (INT_PTR index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);

			if (pMaster == pDevice || pDevice == 0) continue;

			U32 indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata &= 0xFFFFDF; // Set Target Board
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);

			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_NREF_PLL_CONTROL, nRef);
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_NVCO_PLL_CONTROL, nVco);
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_RATE_DIVISOR, nDiv);

			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_RATE_ASSIGNMENTS, 0x5555);
			Sleep(100);

			// Store event handle
			::DSI32_66_EnableInterrupt(pDevice->GetDeviceNumber(), 0x02, LOCAL, &error);
			::DSI32_66_Register_Interrupt_Notify(pDevice->GetDeviceNumber(), pDevice->GetEvent(), 0x02, LOCAL, &error);

			indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata |= 0x10000; // Set Syncronize Scan bit
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);

			DWORD dwStatus = WaitForSingleObject(pDevice->GetEventHandle(), 2 * 1000); // Wait for the interrupt
			switch (dwStatus)
			{
			case WAIT_OBJECT_0:
				break;
			default:
				return false;
				break;
			}
		}
	}

	// Synchronize
	{
		// Sync All Boards
		U32 indata = ::DSI32_66_Read_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
		indata |= 0x40; // Sync ALL Boards
		::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);
		DWORD dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 10 * 1000); // Wait for the interrupt

		if (dwStatus < WAIT_OBJECT_0 || dwStatus > WAIT_OBJECT_0 + GetCount())
		{
			return false;
		}

		// Set Clear Buffer on Sync Bit
		for (INT_PTR index = 0; index < GetCount(); index++)
		{
			CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);

			indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata |= 0x20000; // Set Clear Buffer on Sync bit, Initiator
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);
		}

		// Set Software Sync Bit
		indata = ::DSI32_66_Read_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
		indata |= 0x40; // Set Software Sync bit 
		::DSI32_66_Write_Local32(pMaster->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);// Actually clears all the buffers

		dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 10 * 1000); // Wait for the interrupt

		if (dwStatus < WAIT_OBJECT_0 || dwStatus > WAIT_OBJECT_0 + GetCount())
		{
			return false;
		}
	}

	return true;
#else
	return false;
#endif
}

bool CGs24Dsi32eDeviceArray::RunAutoCal()
{
#ifndef X64
	U32 error = 0, indata;
	if (needCalibration || oldSampleRate != m_SampleRate)
	{
		DWORD dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 1); // Wait for the interrupt

		for (INT_PTR index = 0; index < GetCount(); index++)
		{
			CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);
			::DSI32_66_EnableInterrupt(pDevice->GetDeviceNumber(), 0x1, LOCAL, &error);
			::DSI32_66_Register_Interrupt_Notify(pDevice->GetDeviceNumber(), pDevice->GetEvent(), LOCAL, 0, &error);
			indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata |= 0x00000080;
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);
		}

		dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 10 * 1000); // Wait for the interrupt
		if (dwStatus < WAIT_OBJECT_0 || dwStatus > WAIT_OBJECT_0 + GetCount())
		{
			return false;
		}
	}
	oldSampleRate = m_SampleRate;
	needCalibration = false;
	return true;
#else
	return false;
#endif
}

bool CGs24Dsi32eDeviceArray::RunInitialize()
{
#ifndef X64
	U32 error = 0;
	DWORD dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 1); // Wait for the interrupt

	for (INT_PTR index = 0; index < GetCount(); index++)
	{
		CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);
		::DSI32_66_EnableInterrupt(pDevice->GetDeviceNumber(), 0x0, LOCAL, &error);
		::DSI32_66_Register_Interrupt_Notify(pDevice->GetDeviceNumber(), pDevice->GetEvent(), 0, LOCAL, &error);
		::DSI32_66_Initialize(pDevice->GetDeviceNumber(), &error);
	}

	dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 10 * 1000); // Wait for the interrupt
	if (dwStatus < WAIT_OBJECT_0 || dwStatus > WAIT_OBJECT_0 + GetCount())
	{
		return false;
	}

	// Set Scan Count
	{
		// Threshold is not being used for this example 
		// 24Bit 0x300000 - For 16Bit change to 0x000000
		U32 value = 0x20000 | 0x300000;

		for (INT_PTR index = 0; index < GetCount(); index++)
		{
			CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL, value);
		}
	}

	// Data Format : 2's Complement
	{
		BOARD_CONTROL_REGISTER bcr;

		for (INT_PTR index = 0; index < GetCount(); index++)
		{
			CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);
			U32 value = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);

			bcr.Value.Data = value;
			bcr.Value.FieldValue.Format = BCR_FORMAT_2S_COMPLEMENT;
			value = bcr.Value.Data;

			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, value);
		}
	}

	return true;
#else
	return false;
#endif
}

bool CGs24Dsi32eDeviceArray::InitDMA(bool bSetByInterval)
{
#ifndef X64
	CGs24Dsi32eDeviceImpl*	 pDevice = 0;
	U32          value, error;
	INT_PTR index = 0;

	// Reset block count
	{
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pDevice->ResetBlockCount();
		}
	}

	// Cancel Interrupt Notify & Disable Interrupt
	{
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			::DSI32_66_Cancel_Interrupt_Notify(pDevice->GetDeviceNumber(), pDevice->GetEvent(), &error);
		}

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			::DSI32_66_DisableInterrupt(pDevice->GetDeviceNumber(), 0x02, LOCAL, &error);
		}
	}

	// DMA Memory
	{
		if (m_SampleRate < 50000) m_MemoryLimit = 128000;
		else if (m_SampleRate < 160000) m_MemoryLimit = 256000;
		else m_MemoryLimit = 384000;
		//m_MemoryLimit = m_SampleRate * 16;
		ldiv_t ldivResult;
		U32    smallestSize;
		U32 memNeeded = (U32)(m_MemoryLimit);
		U32 nLimitMemory = 1 << 23;

		// This is the memory we will be using for DMA
		// Try for 1 seconds worth, but if > than can transfer in one block,
		// use max size for # of channels
		if ((memNeeded * 4) > (nLimitMemory - 1))
		{
			ldivResult = ldiv(nLimitMemory, dNumChan);
			ldivResult.quot -= 32;
			memNeeded = (U32)((ldivResult.quot * dNumChan) / 4);
		}
		smallestSize = (memNeeded * 4); // Start with the biggest block we can use


		// Scenerio
		// Start out trying to get all blocks as big as we can use, and if successful ok
		// else... reduce the requested size for the next block to be equal to the smallest
		// block obtained at the time. This will reduce wasted memory, and increase the
		// chance of obtaining bigger blocks as we proceed
		// 
		// We want all the blocks we use to be the same size when using multiple boards
		// to simplify the code later

		GS_PHYSICAL_MEM *pBlocks;
		U32             *pBlockAllocated;
		int              j;

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pBlocks = pDevice->GetBlocks();
			pBlockAllocated = pDevice->GetBlockAllocated();

			for (j = 0; j < m_NUM_BLOCKS; j++)
			{
				memset(&pBlocks[j], 0, sizeof(GS_PHYSICAL_MEM));
				pBlocks[j].Size = smallestSize;

				// Board # may not be in order, so assign 0 to first board #
				::DSI32_66_Get_Physical_Memory(pDevice->GetDeviceNumber(), &pBlocks[j], 1, &error);

				pBlockAllocated[j] = pBlocks[j].Size;

				ldivResult = ldiv(pBlocks[j].Size, dNumChan);
				pBlocks[j].Size = (U32)(ldivResult.quot * dNumChan); // Ensure integer multiple of channels
				if (pBlocks[j].Size < smallestSize) // Need them all the same size
					smallestSize = pBlocks[j].Size;

			} // end j
		} // end i

		U32 memOutOfRange = 0;

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pBlocks = pDevice->GetBlocks();

			for (j = 0; j< m_NUM_BLOCKS; j++)
			{
				if (pBlocks[j].PhysicalAddr >(0xFFFFFFFF - (1 << 23)))
					memOutOfRange = 1;
			}
		}

		if (memOutOfRange)
		{
			for (index = 0; index < GetCount(); index++)
			{
				pDevice = GetDevice(index);
				pBlocks = pDevice->GetBlocks();
				pBlockAllocated = pDevice->GetBlockAllocated();

				for (j = 0; j < m_NUM_BLOCKS; j++)
				{
					pBlocks[j].Size = pBlockAllocated[j];
					::DSI32_66_Free_Physical_Memory(pDevice->GetDeviceNumber(), &pBlocks[j], &error);
				}

			}
			return false;
		}

		// Ensure all the same size
		// and while we're at it, save the virtual address'
		PU32 *pBuff;
		double v = smallestSize / 4.0;
		m_EstimateInterval = v / (m_SampleRate * dNumChan);
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pBlocks = pDevice->GetBlocks();
			pBuff = pDevice->GetBuff();

			for (j = 0; j < m_NUM_BLOCKS; j++)
			{
				pBlocks[j].Size = smallestSize;
				pBuff[j] = (U32*)pBlocks[j].UserAddr;
			}

			pDevice->SetBuffSize((U32)v);
		}
	}

	// Setup CmdChaining
	{
		GS_PHYSICAL_MEM   *pBlocks;
		GS_DMA_DESCRIPTOR *pDmaSetup;
		U32               *pBlockAllocated;
		int i, j;

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pBlocks = pDevice->GetBlocks();
			pDmaSetup = pDevice->GetDmaSetup();

			memset(pDmaSetup, 0, sizeof(GS_DMA_DESCRIPTOR));
			pDmaSetup->DmaChannel = (index % 2);
			pDmaSetup->NumDescriptors = m_NUM_BLOCKS;
			pDmaSetup->LocalToPciDesc_1 = 1;
			pDmaSetup->BytesDesc_1 = pBlocks[0].Size;
			pDmaSetup->PhyAddrDesc_1 = pBlocks[0].PhysicalAddr;
			pDmaSetup->InterruptDesc_1 = 1;
			pDmaSetup->LocalToPciDesc_2 = 1;
			pDmaSetup->BytesDesc_2 = pBlocks[1].Size;
			pDmaSetup->PhyAddrDesc_2 = pBlocks[1].PhysicalAddr;
			pDmaSetup->InterruptDesc_2 = 1;
			if (m_NUM_BLOCKS > 2)
			{
				pDmaSetup->LocalToPciDesc_3 = 1;
				pDmaSetup->BytesDesc_3 = pBlocks[2].Size;
				pDmaSetup->PhyAddrDesc_3 = pBlocks[2].PhysicalAddr;
				pDmaSetup->InterruptDesc_3 = 1;
			}
			if (m_NUM_BLOCKS > 3)
			{
				pDmaSetup->LocalToPciDesc_4 = 1;
				pDmaSetup->BytesDesc_4 = pBlocks[3].Size;
				pDmaSetup->PhyAddrDesc_4 = pBlocks[3].PhysicalAddr;
				pDmaSetup->InterruptDesc_4 = 1;
			}

			if (::DSI32_66_Setup_DmaCmdChaining(pDevice->GetDeviceNumber(), pDmaSetup, &error))
			{
				for (i = 0; i < (int)GetCount(); i++)
				{
					pDevice = GetDevice(i);
					pBlocks = pDevice->GetBlocks();
					pBlockAllocated = pDevice->GetBlockAllocated();

					for (j = 0; j < m_NUM_BLOCKS; j++)
					{
						pBlocks[j].Size = pBlockAllocated[j];
						::DSI32_66_Free_Physical_Memory(pDevice->GetDeviceNumber(), &pBlocks[j], &error);
					}
				}

				return false;
			}
		}
	}

	// Enable Interrupt & Register Interrupt Notify
	{
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);

			// Even board (0,2,...) - DMA Channel 0
			// Odd boad   (1,3,...) - DMA Channel 1
			value = (index % 2);
			::DSI32_66_EnableInterrupt(pDevice->GetDeviceNumber(), value, 1, &error);
		}

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			// Even board (0,2,...) - DMA Channel 0
			// Odd boad   (1,3,...) - DMA Channel 1
			value = (index % 2);
			::DSI32_66_Register_Interrupt_Notify(pDevice->GetDeviceNumber(), pDevice->GetEvent(), value, 1, &error);
		}

		U32 *pData;
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pData = pDevice->GetData();

			memset(pData, 0, DATA_SIZE);
		}

	}
	// Set Thread Priority
	{
		HANDLE hThread = ::GetCurrentThread();
		::SetThreadPriority(hThread, THREAD_PRIORITY_ABOVE_NORMAL);
	}

	return true;
#else
	return false;
#endif
}

bool CGs24Dsi32eDeviceArray::StartDMAAqusition()
{
#ifndef X64
	CGs24Dsi32eDeviceImpl     *pDevice = NULL;
	U32			error, indata;
	INT_PTR index = 0;

	// Start Acquisition & DMA Cmd Chaining
	{
		for (index = GetCount() - 1; index >= 0; index--)
		{
			pDevice = GetDevice(index);

			buffer_data = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL);
			buffer_data &= 0xFBFFFF;
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL, buffer_data);
		}

		pDevice = GetMasterDevice();
		indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
		indata |= 0x40; // Set Software Sync bit which will clear all buffers
		::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);

		GS_PHYSICAL_MEM   *pBlocks;
		GS_DMA_DESCRIPTOR *pDmaSetup;
		U32               *pBlockAllocated;
		int i, j;

		for (index = GetCount() - 1; index >= 0; index--)
		{
			pDevice = GetDevice(index);
			pDmaSetup = pDevice->GetDmaSetup();

			if (::DSI32_66_Start_DmaCmdChaining(pDevice->GetDeviceNumber(), pDmaSetup->DmaChannel, &error))
			{

				for (i = 0; i < (int)GetCount(); i++)
				{
					pDevice = GetDevice(i);
					pBlocks = pDevice->GetBlocks();
					pBlockAllocated = pDevice->GetBlockAllocated();

					for (j = 0; j < m_NUM_BLOCKS; j++)
					{
						pBlocks[j].Size = pBlockAllocated[j];
						::DSI32_66_Free_Physical_Memory(pDevice->GetDeviceNumber(), &pBlocks[j], &error);
					}
				}
				return false;
			}
		}
	}
	m_bDMA = true;

	return true;
#else
	return false;
#endif
}

void CGs24Dsi32eDeviceArray::CloseDMA()
{
#ifndef X64
	if (!m_bDMA) return;
	m_bDMA = false;
	CGs24Dsi32eDeviceImpl     *pDevice = NULL;
	U32  value, error, indata;
	INT_PTR index = 0;

	//Stop the acquisition cleanly so we can verify overflow is not set
	for (index = 0; index < GetCount(); index++)
	{
		CGs24Dsi32eDeviceImpl * pDevice = GetDevice(index);

		// Disable Board
		::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL, buffer_data);
	}

	// Close DMA Cmd Chaining
	{
		U32                of;
		GS_DMA_DESCRIPTOR *pDmaSetup;
		bool               overflowFlag = false;

		// If the user exits prior to the count expiring and the buffers disabled,
		// there will be an overflow since the clock is still running
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);

			of = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL);


			if (of & 0x1000000)
				overflowFlag = true;
		}


		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pDmaSetup = pDevice->GetDmaSetup();

			::DSI32_66_Close_DmaCmdChaining(pDevice->GetDeviceNumber(), pDmaSetup->DmaChannel, &error);
		}
	}

	// Set Thread Priority
	{
		HANDLE hThread = ::GetCurrentThread();
		::SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);
	}

	// Cancel Interrupt Notify & Disable Interrupt
	{
		GS_NOTIFY_OBJECT *pEvent;

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pEvent = pDevice->GetEvent();

			::DSI32_66_Cancel_Interrupt_Notify(pDevice->GetDeviceNumber(), pEvent, &error);
		}

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pEvent = pDevice->GetEvent();

			value = (index % 2);
			::DSI32_66_DisableInterrupt(pDevice->GetDeviceNumber(), value, 1, &error);
		}
	}

	// Clear Buffer & release Memory
	{
		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);

			indata = ::DSI32_66_Read_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL);
			indata &= 0xFFFDFFFF; // clear the Clear Buffer on Sync bit, Initiator
			::DSI32_66_Write_Local32(pDevice->GetDeviceNumber(), &error, ADDRESS_BOARD_CONTROL, indata);
		}

		GS_PHYSICAL_MEM *pBlocks;
		U32             *pBlockAllocated;
		int              j;

		for (index = 0; index < GetCount(); index++)
		{
			pDevice = GetDevice(index);
			pBlocks = pDevice->GetBlocks();
			pBlockAllocated = pDevice->GetBlockAllocated();

			for (j = 0; j < m_NUM_BLOCKS; j++)
			{
				pBlocks[j].Size = pBlockAllocated[j];
				::DSI32_66_Free_Physical_Memory(pDevice->GetDeviceNumber(), &pBlocks[j], &error);
			}
		}
	}
#endif
}

bool CGs24Dsi32eDeviceArray::UpdateDMAData()
{
#ifndef X64
	DWORD dwStatus = WaitForMultipleObjects(GetCount(), m_pDeviceEvents, TRUE, 10 * 1000);

	if (dwStatus == WAIT_TIMEOUT)
	{
		return false;
	}
	else
	{
#pragma omp for
		for (INT_PTR index = 0; index < GetCount(); ++index)
		{
			GetDevice(index)->CopyToPhysicalMemory();
		}
		return true;
	}
#endif
	return false;
}

void CGs24Dsi32eDeviceArray::SetCurrentDevice(INT_PTR index)
{
	if (index < 0 || index > GetCount())
	{
		index = 0;
	}
	m_pCurrentDevice = m_Array[index];
}

void CGs24Dsi32eDeviceArray::SetInputRange(double range)
{
	for (INT_PTR index = 0; index < GetCount(); index++)
	{
		CGs24Dsi32eDeviceImpl* pDevice = GetDevice(index);
		pDevice->SetInputRange(range);
	}
	needCalibration = true;
}