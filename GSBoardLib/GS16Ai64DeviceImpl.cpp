//
// Gs16Ai64DeviceImpl.cpp
//

#include "Gs16Ai64SsaRegister.h"
#include "Gs16Ai64DeviceImpl.h"
#include <string>
#include <cmath>

#define DEVICE_BOARD_INFO_SIZE         2048

std::string GS16AI64DeviceSignalTypeText[GS16AI64_SIGNAL_TYPE_COUNT] =
{
	"Single-Ended Signal",
	"Pseudo-Differential Signal",
	"Differential Signal",
};

//
// CGs16Ai64DeviceImpl
CGs16Ai64DeviceImpl::CGs16Ai64DeviceImpl(void)
: m_DeviceNumber(0)
, m_OpenFlag(false)
, m_DeviceMaxChannelCount(64)
, m_DeviceChannelCount(64)
, m_BoardControlRegister(0)
, m_InterruptControlRegister(0)
, m_InputDataBufferRegister(0)
, m_InputBufferControlRegister(0)
, m_RateAGeneratorRegister(0)
, m_RateBGeneratorRegister(0)
, m_BufferSizeRegister(0)
, m_BurstSizeRegister(0)
, m_ScanSyncControlRegister(0)
, m_ActiveChannelAssignmentRegister(0)
, m_BoardConfigurationRegister(0)
, m_AutoCalValuesRegister(0)
, m_AuxRwRegister(0)
, m_AuxSyncIoControlRegister(0)
, m_ScanMarkerFirstRegister(0)
, m_ScanmarkerSecondRegister(0)
, m_Block1Allocated(0)
, m_Block2Allocated(0)
, m_DmaDataMode(0)
, m_SampleFormat(1)
, m_SampleSize(2)
, m_range(2.5)
, m_SignalType(GS16AI64_SIGNAL_SINGLE_ENDED)
{
#ifndef X64
	U32 error = 0;

	m_pDeviceBoardInfo = new char[DEVICE_BOARD_INFO_SIZE];
	ZeroMemory(m_pDeviceBoardInfo, DEVICE_BOARD_INFO_SIZE);

	// Get Device Count
	m_DeviceCount = ::AI64SSA_FindBoards(m_pDeviceBoardInfo, &error);
	std::string info(m_pDeviceBoardInfo);
	if (info.find("16AI64") == std::string::npos)
	{
		m_DeviceCount = 0;
	}
#endif
}

//
// ~CGs16Ai64DeviceImpl
CGs16Ai64DeviceImpl::~CGs16Ai64DeviceImpl(void)
{
	Close();

	if(m_pDeviceBoardInfo != NULL)
	{
		delete [] m_pDeviceBoardInfo;
		m_pDeviceBoardInfo = NULL;
	}
}

//
// Open
bool CGs16Ai64DeviceImpl::Open(void)
{
#ifndef X64
	if(IsOpen())
		Close();

	U32 error = 0;

	// Open Device

	::AI64SSA_Get_Handle(&error, m_DeviceNumber);

	SetSignalType(m_SignalType);
	// Initialize
	::AI64SSA_Initialize(m_DeviceNumber, &error);
	::Sleep(20);
	m_OpenFlag = true;

	return true;
#else
	return false;
#endif
}

//
// Close
bool CGs16Ai64DeviceImpl::Close(void)
{
	U32 error = 0;

	if(IsOpen())
	{
#ifndef X64
		::AI64SSA_Close_Handle(m_DeviceNumber, &error);
#endif
		m_OpenFlag = false;
	}

	return true;
}

//
// SetInputRange
bool CGs16Ai64DeviceImpl::SetInputRange(double value)
{
	if(IsOpen() == false)
		return false;

	m_range = (float)value;
#ifndef X64
	U32 error = 0;
	BOARD_CONTROL_REGISTER bcRegister;
	bcRegister.Value.Data = AI64SSA_Read_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONTROL);
	// Input Range
	if(m_range == 2.5)
		bcRegister.Value.FieldValue.Range = BCR_RANGE_2_5_VOLT;
	else if(m_range == 5)
		bcRegister.Value.FieldValue.Range = BCR_RANGE_5_VOLT;
	else
		bcRegister.Value.FieldValue.Range = BCR_RANGE_10_VOLT;

	::AI64SSA_Write_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONTROL, bcRegister.Value.Data);

	return true;
#else
	return false;
#endif
}

bool CGs16Ai64DeviceImpl::GetOverFlow()
{
	BOARD_CONTROL_REGISTER bcRegister;
	bcRegister.Value.Data = m_BoardControlRegister;
	return bcRegister.Value.FieldValue.BufferOverflow == 1; 
}

bool CGs16Ai64DeviceImpl::InitDMA(int samplerate)
{
	U32 error = 0;
	m_ReadBlockCount = 0;
#ifndef X64
	::AI64SSA_Initialize(m_DeviceNumber, &error);
	::Sleep(20);

	if (m_SignalType == GS16AI64_SIGNAL_PSEUDO_DIFFERENTIAL)
	{
		AI64SSA_Set_Processing_Mode(m_DeviceNumber, &error,0x01);
	}
	else if (m_SignalType == GS16AI64_SIGNAL_DIFFERENTIAL)
	{
		AI64SSA_Set_Processing_Mode(m_DeviceNumber, &error,0x02);
	}
	else
	{
		AI64SSA_Set_Processing_Mode(m_DeviceNumber, &error,0x0);
	}
	int flag = 6;
	if (m_DeviceChannelCount == 63)
	{
		flag = 6;
	}
	else if (m_DeviceChannelCount > 32)
	{
	}
	else if (m_DeviceChannelCount > 16)
	{
		flag = 5;
	}
	else if (m_DeviceChannelCount > 8)
	{
		flag = 4;
	}
	else if (m_DeviceChannelCount > 4)
	{
		flag = 3;
	}
	else if (m_DeviceChannelCount > 2)
	{
		flag = 2;
	}
	else if (m_DeviceChannelCount > 1)
	{
		flag = 1;
	}
	else if (m_DeviceChannelCount == 1)
	{
		flag = 0;
	}
	if (GS16AI64_SIGNAL_DIFFERENTIAL == m_SignalType)
	{
		++flag;
	}
	AI64SSA_Write_Local32(m_DeviceNumber, &error, SCAN_CNTRL, flag);
	// Getting Memory
	ZeroMemory(&m_Block1, sizeof(GS_PHYSICAL_MEM));
	ZeroMemory(&m_Block2, sizeof(GS_PHYSICAL_MEM));
			
	// This is the memory we will be using for DMA
	// Try for 1 seconds worth, but if > than can transfer in one block,
	// decrement till there
	m_DataRateSampleCount = samplerate * m_DeviceChannelCount;

	while((m_DataRateSampleCount * m_SampleSize) > ((1<<23)-1))
		m_DataRateSampleCount /= 2;
	
	m_Block1.Size = m_DataRateSampleCount * m_SampleSize;

	// but settle for less
	::AI64SSA_Get_Physical_Memory(m_DeviceNumber, &m_Block1, 1, &error); 

	m_Block1Allocated = m_Block1.Size;
	ldiv_t result = ldiv(m_Block1.Size, m_DeviceChannelCount);

	// Ensure integer multiple of channels
	m_Block1.Size = (U32)(result.quot * m_DeviceChannelCount); 

	// Try to make them both equal, regardless of size
	m_Block2.Size = m_Block1.Size;                   

	// 1 = will take less
	::AI64SSA_Get_Physical_Memory(m_DeviceNumber, &m_Block2, 1, &error); 

	m_Block2Allocated = m_Block2.Size;
	result            = ldiv(m_Block2.Size, m_DeviceChannelCount);

	// Ensure integer multiple of channels
	m_Block2.Size  = (U32)(result.quot * m_DeviceChannelCount); 

	m_Block1.Size = m_Block2.Size;

	BOARD_CONTROL_REGISTER bcRegister;
	bcRegister.Value.Data = AI64SSA_Read_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONTROL);

	// Input Range
	if(m_range == 2.5)
		bcRegister.Value.FieldValue.Range = BCR_RANGE_2_5_VOLT;
	else if(m_range == 5)
		bcRegister.Value.FieldValue.Range = BCR_RANGE_5_VOLT;
	else
		bcRegister.Value.FieldValue.Range = BCR_RANGE_10_VOLT;

	// Sample Format
	if(m_SampleFormat ==  1)
		bcRegister.Value.FieldValue.Format = BCR_FORMAT_2S_COMPLEMENT;
	else
		bcRegister.Value.FieldValue.Format = BCR_FORMAT_OFFSET_BINARY;

	// Disable Scan Marker
	bcRegister.Value.FieldValue.DisableScanMarker = BCR_SCAN_MARKER_DISABLE;

	// Data Packing
	if(m_SampleSize == 4)
		bcRegister.Value.FieldValue.DataPacking = BCR_DATA_PACKING_DISABLE;
	else
		bcRegister.Value.FieldValue.DataPacking = BCR_DATA_PACKING_ENABLE;

	// Demand Mode
	bcRegister.Value.FieldValue.DemandMode = BCR_DEMAND_MODE_AUTONOMOUS;

	AI64SSA_Write_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONTROL, bcRegister.Value.Data);
	Sleep(1000);
	DWORD nRate = 50000000/samplerate;
	AI64SSA_Write_Local32(m_DeviceNumber, &error, ADDRESS_RATE_A_GENERATOR, 0x10000 | (nRate&0xFFFF));

	m_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (m_hEvent == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	
	m_NotifyObject.hEvent = (U64)m_hEvent;
	m_hThread = ::GetCurrentThread();
	::SetThreadPriority(m_hThread, THREAD_PRIORITY_TIME_CRITICAL);
			
	AI64SSA_Dma_DataMode(m_DeviceNumber, m_DmaDataMode); 

	// Setup CmdChaining here
	ZeroMemory(&m_DmaSetup, sizeof(GS_DMA_DESCRIPTOR));

	m_DmaSetup.DmaChannel       = 1;
	m_DmaSetup.NumDescriptors   = 2;
	m_DmaSetup.LocalToPciDesc_1 = 1;
	m_DmaSetup.BytesDesc_1      = m_Block1.Size;
	m_DmaSetup.PhyAddrDesc_1    = m_Block1.PhysicalAddr;
	m_DmaSetup.InterruptDesc_1  = 1;
	m_DmaSetup.LocalToPciDesc_2 = 1;
	m_DmaSetup.BytesDesc_2      = m_Block2.Size;
	m_DmaSetup.PhyAddrDesc_2    = m_Block2.PhysicalAddr;
	m_DmaSetup.InterruptDesc_2  = 1;

	if (::AI64SSA_Setup_DmaCmdChaining(m_DeviceNumber, &m_DmaSetup,  &error))
	{
		::SetThreadPriority(m_hThread,THREAD_PRIORITY_NORMAL);
		m_Block1.Size = m_Block1Allocated;
		m_Block2.Size = m_Block2Allocated;
			
		::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block1, &error);
		::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block2, &error);
		::AI64SSA_Reset_Device(m_DeviceNumber, &error);
		return false;
	}
#endif 
	return true;
}

void CGs16Ai64DeviceImpl::StartDMA()
{
	U32 error = 0;
	// DMA Channel 1
#ifndef X64
	::AI64SSA_EnableInterrupt(m_DeviceNumber, 1, 1, &error); 

	::AI64SSA_Register_Interrupt_Notify(m_DeviceNumber, &m_NotifyObject, 1, 1, &error);

	if(::AI64SSA_Start_DmaCmdChaining(m_DeviceNumber, m_DmaSetup.DmaChannel, &error))
	{
		::SetThreadPriority(m_hThread, THREAD_PRIORITY_NORMAL);
		m_Block1.Size = m_Block1Allocated;
		m_Block2.Size = m_Block2Allocated;
			
		::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block1, &error);
		::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block2, &error);
		::AI64SSA_Reset_Device(m_DeviceNumber, &error);
		return;
	}

	// Enable Clock
	U32 registerValue = ::AI64SSA_Read_Local32(m_DeviceNumber, &error, ADDRESS_RATE_A_GENERATOR);         
	::AI64SSA_Write_Local32(m_DeviceNumber, &error, ADDRESS_RATE_A_GENERATOR, registerValue & 0xFFFEFFFF); 
#endif
}

void CGs16Ai64DeviceImpl::UpdataDMA()
{
#ifndef X64
	DWORD eventStatus = ::WaitForSingleObject(m_hEvent, 4 * 1000); 
	
	switch(eventStatus)	
	{						
	case WAIT_OBJECT_0:	
		if (m_ReadBlockCount % 2 == 0)
			memcpy(m_pBuffer1, (U32*)m_Block1.UserAddr, m_Block1.Size);
		else
			memcpy(m_pBuffer2, (U32*)m_Block2.UserAddr, m_Block2.Size);
		++m_ReadBlockCount;
		break;
	default:
		break;
	}
	U32 error = 0;
	m_BoardControlRegister  = ::AI64SSA_Read_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONTROL);
#endif
}

U32* CGs16Ai64DeviceImpl::GetCurrentData()
{
	if (m_ReadBlockCount % 2 == 1)
	{
		return m_pBuffer1;
	}
	else
	{
		return m_pBuffer2;
	}
}

void CGs16Ai64DeviceImpl::EndDMA()
{
#ifndef X64
	U32 error = 0;

	::AI64SSA_Cancel_Interrupt_Notify(m_DeviceNumber, &m_NotifyObject, &error);
	::AI64SSA_Close_DmaCmdChaining(m_DeviceNumber, m_DmaSetup.DmaChannel, &error);

	m_Block1.Size = m_Block1Allocated;
	m_Block2.Size = m_Block2Allocated;

	::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block1, &error);
	::AI64SSA_Free_Physical_Memory(m_DeviceNumber, &m_Block2, &error);

	::CloseHandle(m_hEvent);

	::AI64SSA_DisableInterrupt(m_DeviceNumber, 0, 1, &error);
	::AI64SSA_Reset_Device(m_DeviceNumber, &error);
#endif
}

DWORD CGs16Ai64DeviceImpl::GetDeviceChannelCount(void)
{
	CheckChannelCount();
	return m_DeviceChannelCount;  
}

void CGs16Ai64DeviceImpl::SetDeviceChannelCount(DWORD val) 
{  
	m_DeviceChannelCount = val;   
	CheckChannelCount();
}

void CGs16Ai64DeviceImpl::CheckChannelCount()
{
	if (m_DeviceChannelCount < 1) m_DeviceChannelCount = 1;
	if (m_DeviceChannelCount > m_DeviceMaxChannelCount) m_DeviceChannelCount = m_DeviceMaxChannelCount;
}

//
// GetApiErrorDescription
std::string CGs16Ai64DeviceImpl::GetApiErrorDescription(U32 apiReturnCode)
{
	std::string text;

	switch (apiReturnCode)
	{
	case ApiConfigAccessFailed:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiConfigAccessFailed";
		break;
	case ApiDmaChannelInvalid:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaChannelInvalid";
		break;
	case ApiDmaChannelUnavailable:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaChannelUnavailable";
		break;
	case ApiDmaCommandInvalid:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaCommandInvalid";
		break;
	case ApiDmaDone:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaDone";
		break;
	case ApiDmaInProgress:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaInProgress";
		break;
	case ApiDmaInvalidChannelPriority:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaInvalidChannelPriority";
		break;
	case ApiDmaPaused:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaPaused";
		break;
	case ApiFailed:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiFailed";
		break;
	case ApiInsufficientResources:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInsufficientResources";
		break;
	case ApiInvalidAccessType:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidAccessType";
		break;
	case ApiInvalidAddress:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidAddress";
		break;
	case ApiInvalidDeviceInfo:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDeviceInfo";
		break;
	case ApiInvalidHandle:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidHandle";
		break;
	case ApiInvalidPowerState:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidPowerState";
		break;
	case ApiInvalidSize:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidSize";
		break;
	case ApiNoActiveDriver:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiNoActiveDriver";
		break;
	case ApiNullParam:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiNullParam";
		break;
	case ApiPowerDown:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiPowerDown";
		break;
	case ApiInvalidBoardNumber:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidBoardNumber";
		break;
	case ApiInvalidDMANumWords:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDMANumWords";
		break;
	case ApiUnsupportedFunction:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiUnsupportedFunction";
		break;
	case ApiInvalidDriverVersion:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDriverVersion";
		break;
	case ApiInvalidOffset:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidOffset";
		break;
	case ApiInvalidData:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidData";
		break;
	case ApiInvalidIndex:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidIndex";
		break;
	case ApiInvalidIopSpace:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidIopSpace";
		break;
	case ApiInvalidPciSpace:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidPciSpace";
		break;
	case ApiInvalidBusIndex:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidBusIndex";
		break;
	case ApiWaitTimeout:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiWaitTimeout";
		break;
	case ApiWaitCanceled:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiWaitCanceled";
		break;
	case ApiDmaSglPagesGetError:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaSglPagesGetError";
		break;
	case ApiDmaSglPagesLockError:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaSglPagesLockError";
		break;
	case ApiMuFifoEmpty:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiMuFifoEmpty";
		break;
	case ApiMuFifoFull:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiMuFifoFull";
		break;
	case ApiHSNotSupported:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiHSNotSupported";
		break;
	case ApiVPDNotSupported:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiVPDNotSupported";
		break;
	case ApiDeviceInUse:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDeviceInUse";
		break;
	case ApiDmaNotReady:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaNotReady";
		break;
	default:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " Is not Listed as a valid return code.";
		break;
  }

  return text;
}

void  CGs16Ai64DeviceImpl::SetSignalType(GS16AI64SignalType value)
{
	m_SignalType = value; 
	if (m_SignalType == GS16AI64_SIGNAL_DIFFERENTIAL)
	{
		m_DeviceMaxChannelCount = 32;
	}
	else
	{
		m_DeviceMaxChannelCount = 64;
	}
}

void CGs16Ai64DeviceImpl::Calibrate()
{
#ifndef X64
	U32 error = 0;
	U32 registerValue = ::AI64SSA_Autocal(m_DeviceNumber, &error);
#endif
}