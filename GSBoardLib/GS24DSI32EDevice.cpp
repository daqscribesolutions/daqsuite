#include "Gs24Dsi32eDeviceImpl.h"
#include "Gs24Dsi32eDeviceArray.h"
#include "GS24DSI32EDevice.h"
#include "../DeviceLib/logger.h"
#include "../DeviceLib/SystemManager.h"

namespace GenralStandard
{

	GS24DSI32EDevice::GS24DSI32EDevice(int index)
		: VirtualDevice(32, FOUR)
		, m_pDevice(0)
	{
		m_mgr = CGs24Dsi32eDeviceArray::GetInstance();
		m_MinimumSampleRatePerSecond = 20000;
		m_SampleRatePerSecond = 20000;
		m_HardwardSampleRate = m_SampleRatePerSecond;
		m_AvailableChannels.clear();
		m_AvailableChannels.push_back(32);

		VirtualDevice::Initialize();
		m_DeviceInfo = "GS24DSI32";

		m_pDevice = m_mgr->GetDevice(index);
		if (m_pDevice)
		{
			SetDeviceInputMilliVoltage(int(m_pDevice->GetRange() * 1000));
		}
	}

	GS24DSI32EDevice::~GS24DSI32EDevice()
	{
		m_pDevice->Close();
	}

	bool GS24DSI32EDevice::Initialize()
	{
		m_HardwardSampleRate = m_SampleRatePerSecond;
		if (m_SampleRatePerSecond > m_MaximumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MaximumSampleRatePerSecond;
		}
		else if (m_SampleRatePerSecond < m_MinimumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MinimumSampleRatePerSecond;
		}

		if (m_mgr->IsMaster(m_pDevice->GetDeviceNumber()))
		{
			auto sysmgr = SystemManager::GetInstance();
			m_mgr->SetInputRange(GetDeviceInputMilliVoltage() / 1000.0);
			sysmgr->SetProgress(10);
			if (m_mgr->m_bAutoCalibration)
			{
				m_mgr->RunAutoCal();
			}
			sysmgr->SetProgress(40);
			if (m_mgr->SetSampleRate(m_HardwardSampleRate))
			{
				sysmgr->SetProgress(50);
				if (m_mgr->InitDMA())
				{
					sysmgr->SetProgress(90);
					return true;
				}
			}
		}
		else
		{
			return true;
		}
		return false;
	}

	bool GS24DSI32EDevice::Start()
	{
		Init();
		if (m_mgr->IsMaster(m_pDevice->GetDeviceNumber()))
		{
			if (m_mgr->StartDMAAqusition())
			{
				m_Runner->Reset();
				m_Thread = new boost::thread(boost::ref(*m_Runner));
				m_RunNanoSecond = 0;

				return true;
			}
		}
		else
		{
			m_Runner->Reset();
			m_Thread = new boost::thread(boost::ref(*m_Runner));
			m_RunNanoSecond = 0;

			return true;
		}

		return false;
	}

	bool GS24DSI32EDevice::Stop()
	{
		if (m_Thread)
		{
			m_Runner->Stop();
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			delete m_Thread;
			m_Thread = 0;
			if (m_mgr->IsMaster(m_pDevice->GetDeviceNumber()))
			{
				m_mgr->CloseDMA();
			}
		}
		return true;
	}

	void GS24DSI32EDevice::Update()
	{
		DWORD dwStatus = WaitForSingleObject(m_pDevice->GetEventHandle(), 2000);
		if (dwStatus != WAIT_TIMEOUT)
		{
			m_pDevice->CopyToPhysicalMemory();
			if (m_pDevice->GetReadBlockCount() > 3)
			{
				int nDataSize = m_pDevice->GetBuffSize() * 4;
				char* pData = (char*)m_pDevice->GetData();
				if (m_DataManager && !m_DeviceDatas.empty())
				{
					DeviceData* data = m_DeviceDatas.front();
					int nDeviceDataSize = (int)data->GetDataByteSize();
					char * pDeviceData = data->GetADCCountData();
					memcpy(&pDeviceData[0], &pData[0], (int)nDeviceDataSize);
					data->SetStartNanoSecond(m_RunNanoSecond);
					m_RunNanoSecond += data->GetDurationNanoSecond();
					m_DataManager->AddData(data);
					m_DataManager->EndSample();
					m_DataManager->BeginSample();
				}
			}
		}
		else
		{
			Logger::GetInstance()->Log("Wait timeout - id", ID);
		}
	}

	void GS24DSI32EDevice::Init()
	{
		CGs24Dsi32eDeviceArray* parray = CGs24Dsi32eDeviceArray::GetInstance();
		m_KeepingDuration = (long long)(parray->GetEstimatedInterval() * 1000000000);
		if (m_Channels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_Channels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_Channels.back();
				m_Channels.pop_back();
			}
		}
		else if (m_Channels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_Channels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();
			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " + std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(pChannel);
			}
		}

		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->Init(m_KeepingDuration, m_SampleRatePerSecond);
		}

		VirtualDevice::InitSnapshot();
		
		if (m_DataManager)
		{
			m_DataManager->Initialize();
			m_DataManager->SetManagedSampleRatePerSecond(m_SampleRatePerSecond);
		}

		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();

		DeviceData* data = new DeviceData(0, m_KeepingDuration, m_HardwardSampleRate, m_NumberOfChannels, GetDeviceInputMilliVoltage(), m_Format);
		m_DeviceDatas.push_back(data);
		m_RenderData->SetInputMilliVolt(m_Voltages[m_InputVoltageIndex]);
		m_RenderData->SetDurationNanoSecond(m_KeepingDuration);
		m_RenderData->SetSampleRatePerSecond(m_SampleRatePerSecond);
		m_RenderData->InitData();
	}
}
