//
// Gs24Dsi32eDeviceImpl.cpp
//

#include "Gs24Dsi32eRegister.h"
#include "Gs24Dsi32eDeviceImpl.h"
#include "Gs24Dsi32eDeviceArray.h"
#include <cmath>

//
// CGs24Dsi32eDeviceImpl
CGs24Dsi32eDeviceImpl::CGs24Dsi32eDeviceImpl()
	: m_DeviceNumber(0)
	, m_OpenFlag(false)
	, m_DeviceChannelCount(0)
	, m_BoardControlRegister(0)
	, m_NrefPllControlRegister(0)
	, m_NvcoPllControlRegister(0)
	, m_RateAssigmentsRegister(0)
	, m_RateDivisorRegister(0)
	, m_PllRefFrequencyRegister(0)
	, m_BufferControlRegister(0)
	, m_BoardConfigurationRegister(0)
	, m_BufferSizeRegister(0)
	, m_AutoCalValueRegister(0)
	, m_InputDataBufferRegister(0)
	, m_hEvent(INVALID_HANDLE_VALUE)
	, m_CurrentBufferIndex(0)
	, m_bOverFlow(false)
	, m_ReadBlockCount(0)
	, m_range(5)
{
}

//
// ~CGs24Dsi32eDeviceImpl
CGs24Dsi32eDeviceImpl::~CGs24Dsi32eDeviceImpl(void)
{
	Close();
}

//
// Open
bool CGs24Dsi32eDeviceImpl::Open(void)
{
#ifndef X64
	if (IsOpen())
		Close();
	m_ReadBlockCount = 0;
	U32 error = 0;
	::DSI32_66_Get_Handle(&error, m_DeviceNumber);
	// Board Configuration Register
	{
		m_BoardConfigurationRegister = ::DSI32_66_Read_Local32(m_DeviceNumber, &error, ADDRESS_BOARD_CONFIGURATION);

		BOARD_CONFIGURATION_REGISTER data;
		data.Value.Data = m_BoardConfigurationRegister;

		if (data.Value.FieldValue.InputChannel16 == BCONR_INPUT_CHANNEL_16_HIGH)
			m_DeviceChannelCount = BOARD_CHANNEL_16;
		else if (data.Value.FieldValue.InputChannel8 == BCONR_INPUT_CHANNEL_8_HIGH)
			m_DeviceChannelCount = BOARD_CHANNEL_8;
		else
			m_DeviceChannelCount = BOARD_CHANNEL_32;
	}
	m_hEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	if (m_hEvent == NULL)
	{
		return false;
	}
	m_Event.hEvent = (U64)m_hEvent;
	m_UID = ::DSI32_66_Read_UID(m_DeviceNumber, &error);
	m_OpenFlag = true;

	return true;
#else
	return false;
#endif
}

void CGs24Dsi32eDeviceImpl::SetUID(int uid)
{
#ifndef X64
	U32 error = 0;
	::DSI32_66_Write_UID(m_DeviceNumber, &error, uid);
	m_UID = ::DSI32_66_Read_UID(m_DeviceNumber, &error);
#endif
}

//
// Close
bool CGs24Dsi32eDeviceImpl::Close(void)
{
#ifndef X64
	U32 error = 0;

	if (m_hEvent != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hEvent);
		m_hEvent = INVALID_HANDLE_VALUE;
	}

	DSI32_66_Close_Handle(m_DeviceNumber, &error);

	if (IsOpen())
	{
		m_OpenFlag = false;
	}

	return true;
#else
	return false;
#endif
}

//
// GetApiErrorDescription
std::string CGs24Dsi32eDeviceImpl::GetApiErrorDescription(U32 apiReturnCode)
{
	std::string text;

	switch (apiReturnCode)
	{
	case ApiConfigAccessFailed:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiConfigAccessFailed";
		break;
	case ApiDmaChannelInvalid:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaChannelInvalid";
		break;
	case ApiDmaChannelUnavailable:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaChannelUnavailable";
		break;
	case ApiDmaCommandInvalid:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaCommandInvalid";
		break;
	case ApiDmaDone:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaDone";
		break;
	case ApiDmaInProgress:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaInProgress";
		break;
	case ApiDmaInvalidChannelPriority:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaInvalidChannelPriority";
		break;
	case ApiDmaPaused:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaPaused";
		break;
	case ApiFailed:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiFailed";
		break;
	case ApiInsufficientResources:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInsufficientResources";
		break;
	case ApiInvalidAccessType:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidAccessType";
		break;
	case ApiInvalidAddress:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidAddress";
		break;
	case ApiInvalidDeviceInfo:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDeviceInfo";
		break;
	case ApiInvalidHandle:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidHandle";
		break;
	case ApiInvalidPowerState:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidPowerState";
		break;
	case ApiInvalidSize:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidSize";
		break;
	case ApiNoActiveDriver:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiNoActiveDriver";
		break;
	case ApiNullParam:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiNullParam";
		break;
	case ApiPowerDown:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiPowerDown";
		break;
	case ApiInvalidBoardNumber:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidBoardNumber";
		break;
	case ApiInvalidDMANumWords:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDMANumWords";
		break;
	case ApiUnsupportedFunction:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiUnsupportedFunction";
		break;
	case ApiInvalidDriverVersion:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidDriverVersion";
		break;
	case ApiInvalidOffset:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidOffset";
		break;
	case ApiInvalidData:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidData";
		break;
	case ApiInvalidIndex:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidIndex";
		break;
	case ApiInvalidIopSpace:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidIopSpace";
		break;
	case ApiInvalidPciSpace:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidPciSpace";
		break;
	case ApiInvalidBusIndex:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiInvalidBusIndex";
		break;
	case ApiWaitTimeout:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiWaitTimeout";
		break;
	case ApiWaitCanceled:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiWaitCanceled";
		break;
	case ApiDmaSglPagesGetError:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaSglPagesGetError";
		break;
	case ApiDmaSglPagesLockError:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaSglPagesLockError";
		break;
	case ApiMuFifoEmpty:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiMuFifoEmpty";
		break;
	case ApiMuFifoFull:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiMuFifoFull";
		break;
	case ApiHSNotSupported:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiHSNotSupported";
		break;
	case ApiVPDNotSupported:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiVPDNotSupported";
		break;
	case ApiDeviceInUse:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDeviceInUse";
		break;
	case ApiDmaNotReady:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " = ApiDmaNotReady";
		break;
	default:
		text = "Return Code  " + std::to_string((long long)apiReturnCode) + " Is not Listed as a valid return code.";
		break;
	}

	return text;
}


//
// CalculateParameters

#define MAX_SAMPLE_RATE_IN  200000.0
#define MIN_SAMPLE_RATE_IN  2000.0

double CGs24Dsi32eDeviceImpl::m_OscFrequency = 0.0;

double CGs24Dsi32eDeviceImpl::CalculateParameters(U32 board, U32* nVco, U32* nRef, U32* nDiv, double desired_Freq)
{
	#ifndef X64
	double div = 0.5;
	double fGen;
	double ratio;
	double actual_Freq;
	double theValue, theMin = 1000.0;
	U32 theData;
	int i;

	if ((desired_Freq < MIN_SAMPLE_RATE_IN) || (desired_Freq > MAX_SAMPLE_RATE_IN))
		return 0;

	for (i = 0; i<26; i++)
	{
		fGen = 512.0*div*desired_Freq;
		if ((fGen >= 20000000.0) && (fGen <= 55000000.0))
		{
			*nDiv = (U32)i;
			i = 26;
		}
		if (div == 0.5)
			div = 1.0;
		else
			div += 1.0;
	}
	if (i != 27)
	{	// Didn't find a valid fGen value
		*nVco = 30; // Should never happen, but...
		*nRef = 30;
		*nDiv = 25;
		return 0;
	}
	*nVco = 30;

	theData = 32768000;
	m_OscFrequency = (double)theData;

	ratio = fGen / m_OscFrequency;

	for (i = 30; i<1000; i++)
	{
		theValue = fabs(fmod(((double)i*ratio), 1.0));
		if (theValue < theMin)
		{
			theMin = theValue;
			*nRef = i;
			*nVco = (U32)((double)i*ratio);
			if ((double)*nVco >(1000.0 - ratio))
				i = 1000;
		}
		if (theMin == 0.0)
			i = 1000;
	}
	if ((*nVco < 30) || (*nRef < 30)){
		*nVco *= 2;
		*nRef *= 2;
	}
	// Should have a value when we get to here
	actual_Freq = (double)*nVco / (double)*nRef;
	actual_Freq *= m_OscFrequency;
	if (*nDiv)
		actual_Freq /= ((double)*nDiv * 512.0);
	else
		actual_Freq /= (0.5 * 512.0);

	return(actual_Freq);
#endif
	return 0;
}

void CGs24Dsi32eDeviceImpl::SetInputRange(double range)
{
	U32 error = 0;
#ifndef X64
	DSI32_66_Set_Voltage_Range(m_DeviceNumber, &error, (range < 3 ? 1 : (range < 6 ? 2 : 3)));
#endif
	m_range = range;
}

void CGs24Dsi32eDeviceImpl::CopyToPhysicalMemory()
{
	memmove(m_Data, m_Buff[m_CurrentBufferIndex], (m_BuffSize * 4)); // BYTES
	for (U32 i = 0; i < m_BuffSize; ++i)
	{
		m_Data[i] = m_Data[i] << 8;
	}
#ifndef X64
	U32 error;
	U32 value = ::DSI32_66_Read_Local32(GetDeviceNumber(), &error, ADDRESS_BUFFER_CONTROL);
	bool bOverview = (value & 0x1000000) != 0;
	if (bOverview) { SetOverFlow(bOverview); OutputDebugString(_T("GS24DSI32E : Overflow")); }
#endif
	++m_ReadBlockCount;
	m_CurrentBufferIndex = (int)(m_ReadBlockCount % CGs24Dsi32eDeviceArray::GetInstance()->GetNUM_BLOCKS());
}

void CGs24Dsi32eDeviceImpl::WriteToFile(HANDLE hFile, DWORD& writtenSize)
{
	::WriteFile(hFile, m_Data, (m_BuffSize * 4), &writtenSize, NULL);
}

void CGs24Dsi32eDeviceImpl::CopyData(U32* dest, DWORD& writtensize)
{
	memcpy(dest, m_Data, (m_BuffSize * 4)); // BYTES
	writtensize = m_BuffSize * 4;
}
