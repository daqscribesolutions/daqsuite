#ifndef GB104DEVICE_H
#define GB104DEVICE_H

#include "../DeviceLib/VirtualDevice.h"
using namespace DeviceLib;
#ifdef X64
#define GB104_WINDOWS_OS
#include "gb104os.h"
#include "gb104api.h"
#endif
namespace Blackburn
{
	const int NUM_BUFFERS = 2;

	class GB104Device : public VirtualDevice
	{

	public:

		GB104Device(int id);

		~GB104Device();

		virtual bool Initialize();

		virtual bool Start();

		virtual bool Stop();

		virtual void Update();

	protected:

		struct GB104DeviceRunner;

		int m_ID;

		GB104DeviceRunner * m_DeviceRunner;
#ifdef X64
		GB104_DEVICE  hDevice;

		GB104_VOID   *pBuffers[NUM_BUFFERS];

		GB104_CONTROL         gb104_control;

		GB104_ADC_CONTROL     gb104_adc_control;

		GB104_ANALOG_CONTROL  gb104_analog_control;

		GB104_ID              gb104_id;

		GB104_UINT32          gb104_acquisition_count;

		GB104_UINT32          gb104_channels_number;

		GB104_UINT32          gb104_buffer_length;

		GB104_UINT32          gb104_oversampling;

		GB104_UINT32          gb104_gain; 

		GB104_DOUBLE          gb104_sampling_frequency; 

		GB104_DOUBLE          gb104_actual_frequency;
#endif
		int count;

		int NUM_BYTES;

		long long m_RemainData;

	};
}

#endif