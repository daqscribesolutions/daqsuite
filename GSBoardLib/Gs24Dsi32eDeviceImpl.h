//
// Gs24Dsi32eDeviceImply.h
//

#pragma once
#include <Windows.h>
#include <string>
#include "66DSIPLLeintface.h"
#include <atlstr.h>

#define SAMPLE_RATE_MIN                          2000 
#define SAMPLE_RATE_MAX                          200000

#define BOARD_CHANNEL_32                         32
#define BOARD_CHANNEL_16                         16
#define BOARD_CHANNEL_8                          8

#define NUM_BLOCKS                               4
#define DATA_SIZE                                8387584 // 2^23, 1<<23

#define MAX_ADCCOUNT_23BIT                       8388607 // 2^23 - 1

#define GS24DSI32E_SAMPLE_RATE_MAX               200000
#define GS24DSI32E_SAMPLE_RATE_MIN				 17000
#define GS24DSI32E_BUFFER_LENGTH_MIN             1
#define GS24DSI32E_BUFFER_LENGTH_MAX             DATA_SIZE 

//
// CGs24Dsi32eDeviceImpl class
class CGs24Dsi32eDeviceImpl
{
public:

	// Constructor / Destructor
	CGs24Dsi32eDeviceImpl();
	virtual ~CGs24Dsi32eDeviceImpl();

	DWORD GetDeviceNumber(void)           { return m_DeviceNumber; }
	void SetDeviceNumber(U32 value)       { m_DeviceNumber = value; }

	DWORD GetDeviceChannelCount(void)     { return m_DeviceChannelCount; }

	bool IsOpen(void)                     { return m_OpenFlag; }
	bool Open(void);
	bool Close(void);

	GS_NOTIFY_OBJECT *GetEvent(void)           { return &m_Event; }
	void SetEventHandle(HANDLE value)          { m_hEvent = value; m_Event.hEvent = (U64)m_hEvent; }
	HANDLE GetEventHandle() { return m_hEvent;  }
	GS_PHYSICAL_MEM  *GetBlocks(void)          { return m_Blocks; }
	U32              *GetBlockAllocated(void)  { return m_BlockAllocated; }

	U32 GetBuffSize(void)        { return m_BuffSize; }
	void SetBuffSize(U32 value)  { m_BuffSize = value; }
	PU32 *GetBuff(void)          { return m_Buff; }
	void SetInputRange(double range);
	GS_DMA_DESCRIPTOR *GetDmaSetup(void)  { return &m_DmaSetup; }

	U32 *GetData(void) { return m_Data; }

	static std::string GetApiErrorDescription(U32 apiReturnCode);

	static double CalculateParameters(U32 board, U32* nVco, U32* nRef, U32* nDiv, double desired_Freq);
	double GetRange() { return m_range; }

	void CopyToPhysicalMemory();

	void WriteToFile(HANDLE file, DWORD& writtensize);
	void CopyData(U32* dest, DWORD& writtensize);

	bool GetOverFlow() { return m_bOverFlow; }
	void SetOverFlow(bool overflow) { m_bOverFlow = overflow; }
	long long GetReadBlockCount() { return m_ReadBlockCount; }
	void ResetBlockCount() { m_ReadBlockCount = 0; m_CurrentBufferIndex = 0; m_bOverFlow = false; }

	void SetUID(int uid);

	int GetUID() { return m_UID; }

protected:

	static double m_OscFrequency;
	double m_range;
	int m_CurrentBufferIndex;
	// Device
	DWORD  m_DeviceNumber;
	int    m_UID;
	bool   m_OpenFlag;
	DWORD  m_DeviceChannelCount;

	// Register Values
	U32    m_BoardControlRegister;
	U32    m_NrefPllControlRegister;
	U32    m_NvcoPllControlRegister;
	U32    m_RateAssigmentsRegister;
	U32    m_RateDivisorRegister;
	U32    m_PllRefFrequencyRegister;
	U32    m_BufferControlRegister;
	U32    m_BoardConfigurationRegister;
	U32    m_BufferSizeRegister;
	U32    m_AutoCalValueRegister;
	U32    m_InputDataBufferRegister;
	long long    m_ReadBlockCount;
	// Event
	GS_NOTIFY_OBJECT m_Event;
	HANDLE           m_hEvent;

	// Memory
	GS_PHYSICAL_MEM   m_Blocks[NUM_BLOCKS];
	U32               m_BlockAllocated[NUM_BLOCKS];
	U32               m_BuffSize;
	PU32              m_Buff[NUM_BLOCKS];

	GS_DMA_DESCRIPTOR m_DmaSetup;
	bool              m_bOverFlow;
	U32               m_Data[DATA_SIZE / 4];

};

