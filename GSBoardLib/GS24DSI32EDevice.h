#ifndef GS24DSI32EDEVICE_H
#define GS24DSI32EDEVICE_H

#include "../DeviceLib/VirtualDevice.h"
using namespace DeviceLib;

class CGs24Dsi32eDeviceImpl;
class CGs24Dsi32eDeviceArray;
namespace GenralStandard
{

	class GS24DSI32EDevice : public VirtualDevice
	{

	public:

		GS24DSI32EDevice(int index);

		virtual ~GS24DSI32EDevice();

		virtual bool Initialize();

		virtual bool Start();

		virtual bool Stop();

		virtual void Update();

	private:

		void Init();

		CGs24Dsi32eDeviceImpl * m_pDevice;

		CGs24Dsi32eDeviceArray * m_mgr;

	};

}
#endif