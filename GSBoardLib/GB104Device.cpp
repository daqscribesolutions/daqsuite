#include "GB104Device.h"
#include "../DeviceLib/SystemManager.h"

namespace Blackburn
{
	/**
	 * GSDeviceRunner is the functor that implements the virtual device thread logic.
	 */
	struct GB104Device::GB104DeviceRunner
	{
		/**
		 * GSDeviceRunner is the full constructor.
		 * @param pDevice The device pointer to be run. set.
		 * @return The virtual device is set to be used.
		 */
		GB104DeviceRunner(GB104Device* pDevice) : m_pDevice(pDevice), m_bRunning(true) {}
		/**
		 * operator() is the implementation of the device running logic.
		 * @return The device is updated.
		 */
		void operator()()
		{
			if (m_pDevice)
			{
				while (m_bRunning)
				{
					m_pDevice->Update();
					boost::this_thread::sleep(boost::posix_time::microseconds(1));
				}
			}
		}
		/**
		 * Reset makes to start the thread.
		 * @return The device is set to run continually.
		 */
		void Reset() { m_bRunning = true; }
		/**
		 * Stop stops the thread.
		 * @return The device is stopped.
		 */
		void Stop() { m_bRunning = false; }

	private:

		bool m_bRunning;

		GB104Device* m_pDevice;

	};

	GB104Device::GB104Device(int id)
		: VirtualDevice(32, FOUR)
		, m_DeviceRunner(0)
		, m_ID(id)
		, count(0)
		, NUM_BYTES(0)
		, m_RemainData(0)
	{
		m_Voltages.clear();
		m_Voltages.push_back(40000);
		m_Voltages.push_back(5000);
		m_Voltages.push_back(10000);
		m_Voltages.push_back(2000);
		m_Voltages.push_back(27200);
		m_MinimumSampleRatePerSecond = 4000;
		m_MaximumSampleRatePerSecond = 216000;
		m_AvailableChannels.clear();
		m_AvailableChannels.push_back(32);

#ifdef X64
		hDevice = GB104_INVALID_DEVICE_VALUE;
		if (GB104_OK != gb104OpenDevice(&hDevice, m_ID))
		{
			hDevice = GB104_INVALID_DEVICE_VALUE;
			return;
		}
		memset(&gb104_id, 0, sizeof(GB104_ID));
		if (GB104_OK != gb104IdGet(hDevice, &gb104_id)) 
		{
			return;
		}

		for (int idx = 0; idx < NUM_BUFFERS; idx++)
		{
			pBuffers[idx] = NULL;
		}

		m_DeviceInfo = "GB104";
#endif

		m_SampleRatePerSecond = 20000;
		m_HardwardSampleRate = m_SampleRatePerSecond;
		VirtualDevice::Initialize();

		if (m_Runner)
		{
			delete m_Runner;
			m_Runner = 0;
		}
		m_DeviceRunner = new GB104Device::GB104DeviceRunner(this);
		m_DeviceRunner->Stop();
	}

	GB104Device::~GB104Device()
	{
		Stop();
		if (m_DeviceRunner) delete m_DeviceRunner;
		m_DeviceRunner = 0;
#ifdef X64
		if (GB104_INVALID_DEVICE_VALUE != hDevice)
		{
			for (int idx = 0; idx < NUM_BUFFERS; idx++)
			{
				if (NULL != pBuffers[idx])
				{
					gb104FreeMemory(hDevice, pBuffers[idx], NUM_BYTES);
					pBuffers[idx] = NULL;
				}
			}
			gb104CloseDevice(hDevice);
			hDevice = GB104_INVALID_DEVICE_VALUE;
		}
#endif
	}

	bool GB104Device::Initialize()
	{
		m_HardwardSampleRate = m_SampleRatePerSecond;
		if (m_SampleRatePerSecond > m_MaximumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MaximumSampleRatePerSecond;
		}
		else if (m_SampleRatePerSecond < m_MinimumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MinimumSampleRatePerSecond;
		}

#ifdef X64
		gb104_oversampling = GB104_OVERSAMPLING_256X;
		if (m_HardwardSampleRate > 100000)
		{
			gb104_oversampling = GB104_OVERSAMPLING_64X;
		}
		else if (m_HardwardSampleRate > 500000)
		{
			gb104_oversampling = GB104_OVERSAMPLING_128X;
		}
		else
		{
			gb104_oversampling = GB104_OVERSAMPLING_256X;
		}

		gb104_gain = GB104_GAIN_0DB;
		if (m_InputVoltageIndex == 0)
		{
			gb104_gain = GB104_GAIN_0DB;
		}
		else if (m_InputVoltageIndex == 1)
		{
			gb104_gain = GB104_GAIN_18DB;
		}
		else if (m_InputVoltageIndex == 2)
		{
			gb104_gain = GB104_GAIN_13DB;
		}
		else if (m_InputVoltageIndex == 3)
		{
			gb104_gain = GB104_GAIN_22DB;
		}

		gb104_sampling_frequency = 6.4;
		gb104_actual_frequency = 0.0;
		NUM_BYTES = (m_NumberOfChannels * m_HardwardSampleRate * sizeof(GB104_INT32));
		for (int idx = 0; idx < NUM_BUFFERS; idx++)
		{
			if (NULL != pBuffers[idx])
			{
				gb104FreeMemory(hDevice, pBuffers[idx], NUM_BYTES);
				pBuffers[idx] = NULL;
			}

			if (GB104_OK != gb104AllocateMemory(hDevice, &pBuffers[idx], NUM_BYTES)) 
			{
				return false;
			}
			memset(pBuffers[idx], 0x00, NUM_BYTES);
		}
		if (GB104_OK != gb104BoardReset(hDevice)) 
		{
			return false;
		}
		GB104_SLEEP(2);

		memset(&gb104_control, 0, sizeof(GB104_CONTROL));
		gb104_control.trigger_select = GB104_INTERNAL;
		gb104_control.adc_clock_select = GB104_INTERNAL;
		gb104_control.diag_mode_enable = GB104_DISABLE;
		gb104_control.fpdp_enable = GB104_ENABLE;
		gb104_control.packed_data = GB104_UNPACKED;
		gb104_control.adc_master = GB104_ENABLE;
		gb104_control.adc_term = GB104_ENABLE;
		gb104_control.acq_mode = GB104_CAPTURE;
		gb104_control.sw_trigger = GB104_DISABLE;
		gb104_control.acq_enable = GB104_DISABLE;
		gb104_control.arm = GB104_DISABLE;
		gb104_control.ext_trig_polarity = 0x01;

		if (GB104_OK != gb104ControlSet(hDevice, &gb104_control)) 
		{
			return false;
		}

		/* Set ADC Control */
		memset(&gb104_adc_control, 0, sizeof(GB104_ADC_CONTROL));
		gb104_adc_control.adc_reset = GB104_DISABLE;
		gb104_adc_control.adcm0 = (gb104_oversampling >> 0) & 0x01;
		gb104_adc_control.adcm1 = (gb104_oversampling >> 1) & 0x01;
		gb104_adc_control.adc_cal_done = GB104_DISABLE;
		gb104_adc_control.adc_hpfilter_reset = GB104_DISABLE;

		if (GB104_OK != gb104AdcControlSet(hDevice, &gb104_adc_control)) 
		{
			return false;
		}

		/* Set Analog Control */
		memset(&gb104_analog_control, 0, sizeof(GB104_ANALOG_CONTROL));
		gb104_analog_control.input_voltage = gb104_gain;
		gb104_analog_control.bw_select = 0x00;

		if (GB104_OK != gb104AnalogControlSet(hDevice, &gb104_analog_control)) 
		{
			return false;
		}

		/* Set ADC clock */
		if (GB104_OK != gb104AdcClockSet(hDevice, gb104_sampling_frequency, &gb104_actual_frequency)) 
		{
			return false;
		}

		/* Optimize PLL use for external clock */
		if (GB104_OK != gb104PllClockSourceSet(hDevice, gb104_control.adc_clock_select)) 
		{
			return false;
		}

		/* Channels Number */
		gb104_channels_number = m_NumberOfChannels;

		if (GB104_OK != gb104ChannelsNumberSet(hDevice, gb104_channels_number)) 
		{
			return false;
		}

		/* Acquisition Count */
		gb104_acquisition_count = m_HardwardSampleRate;

		if (GB104_OK != gb104AcquisitionCountSet(hDevice, gb104_acquisition_count)) 
		{
			return false;
		}

		/* Buffer Length */
		gb104_buffer_length = gb104_channels_number * gb104_acquisition_count * ((GB104_PACKED == gb104_control.packed_data) ? 1 : 2);
		if (GB104_OK != gb104BufferLengthSet(hDevice, gb104_buffer_length)) 
		{
			return false;
		}

		/* Acquisition Reset */
		if (GB104_OK != gb104AcquisitionReset(hDevice)) 
		{
			return false;
		}
#endif
		count = 0;
		if (m_Channels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_Channels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_Channels.back();
				m_Channels.pop_back();
			}
		}
		else if (m_Channels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_Channels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();
			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " + std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(pChannel);
			}
			}

		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->Init(m_KeepingDuration, m_SampleRatePerSecond);
		}

		VirtualDevice::InitSnapshot();

		if (m_DataManager)
		{
			m_DataManager->Initialize();
			m_DataManager->SetManagedSampleRatePerSecond(m_SampleRatePerSecond);
		}

		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();

		DeviceData* data = new DeviceData(0, m_KeepingDuration, m_HardwardSampleRate, m_NumberOfChannels, GetDeviceInputMilliVoltage(), m_Format);
		m_DeviceDatas.push_back(data);
		return true;
	}

	bool GB104Device::Start()
	{
#ifdef X64
		if (GB104_OK != gb104Enable(hDevice)) 
		{
			return false;
		}
		else
#endif
		{
			m_DeviceRunner->Reset();
			m_Thread = new boost::thread(boost::ref(*m_DeviceRunner));
			m_RunNanoSecond = 0;

			return true;
		}
	}

	bool GB104Device::Stop()
	{
		if (m_Thread)
		{
			m_DeviceRunner->Stop();
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			delete m_Thread;
			m_Thread = 0;
		}
#ifdef X64
		if (GB104_INVALID_DEVICE_VALUE != hDevice) 
		{
			gb104BoardReset(hDevice);
		}
#endif
		return true;
	}

	void GB104Device::Update()
	{
#ifdef X64
		for (int idx = 0; idx < NUM_BUFFERS; idx++) 
		{
			/* Trigger once for continuous or every time for capture mode */
			if ((0 == count) || (GB104_CAPTURE == gb104_control.acq_mode)) 
			{
				if (GB104_OK != gb104Trigger(hDevice)) 
				{
					return;
				}
			}

			/* Read data into buffer in demand mode */
			if (GB104_OK != gb104Read(hDevice, pBuffers[idx], NUM_BYTES, NULL)) 
			{
				return;
			}
			int nDataSize = NUM_BYTES;
			char* pData = (char*)pBuffers[idx];
			if (m_DataManager && !m_DeviceDatas.empty())
			{
				DeviceData* data = m_DeviceDatas.front();
				int nDeviceDataSize = (int)data->GetDataByteSize();
				char * pDeviceData = data->GetADCCountData();
				if (m_RemainData == 0)
					m_RemainData = nDeviceDataSize;
				int nIndex = 0;
				while (nDataSize > nIndex)
				{
					int nCurrent = nDataSize - nIndex;
					if (nCurrent >= m_RemainData)
					{
						int nCopy = nDeviceDataSize - (int)m_RemainData;
						memcpy(&pDeviceData[nCopy], &pData[nIndex], (int)m_RemainData);
						data->SetStartNanoSecond(m_RunNanoSecond);
						m_RunNanoSecond += data->GetDurationNanoSecond();
						m_DataManager->AddData(data);
						nIndex += (int)m_RemainData;
						m_RemainData = nDeviceDataSize;
					}
					else
					{
						int nCopy = nDeviceDataSize - (int)m_RemainData;
						memcpy(&pDeviceData[nCopy], &pData[nIndex], nCurrent);
						nIndex += nCurrent;
						m_RemainData -= nCurrent;
					}
				}
				m_DataManager->EndSample();
				m_DataManager->BeginSample();
			}
		}
#endif
	}
}