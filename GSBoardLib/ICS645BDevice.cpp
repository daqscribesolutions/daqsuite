#include "ICS645BDevice.h"
#include "../DeviceLib/SystemManager.h"

#ifdef max
#undef max
#endif
//
// Input Voltage Range

// * ICS 645B Operating Manual
// Full Scale Input
//  x8 Oversampling  +/-1.00V = -32768 ~ +32767
//  x4 Oversampling  +/-1.03V = -32484 ~ +32483
//  x2 Oversampling  +/-1.03V = -32703 ~ +32702
//  x1 Oversampling  +/-1.03V = -1848  ~ +1847  (12bit resolution)
//

#define ICS645B_ONE_INPUT_RANGE                  2000                // 2Vpp
#define ICS645B_ONE_B_INPUT_RANGE                20000               // 20Vpp

#define DEFAULT_ICS645B_MILLIVOLTAGE_RANGE       ICS645B_ONE_B_INPUT_RANGE

//
// Sample Rate
#define ICS645B_SAMPLE_RATE_MIN_IN_CLOCK         57000               // 57 kHz
#define ICS645B_SAMPLE_RATE_MAX_8X               2500000             // 2.5 MHz
#define ICS645B_SAMPLE_RATE_MAX_4X               5000000             // 5 MHz
#define ICS645B_SAMPLE_RATE_MAX_2X               10000000            // 10 MHz
#define ICS645B_SAMPLE_RATE_MAX_1X               20000000            // 20 MHz

#define DEFAULT_ICS645B_SAMPLE_RATE              ICS645B_SAMPLE_RATE_MAX_8X

// Copy from SR 3.8.0.16e
// ADC Clock
#define ICS645B_ADC_CLOCK_MAX                    40  // 40 MHz

//
// Channel Count
#define ICS645B_CHANNEL_COUNT_MAX_8X             32
#define ICS645B_CHANNEL_COUNT_MAX_4X             32
#define ICS645B_CHANNEL_COUNT_MAX_2X             16
#define ICS645B_CHANNEL_COUNT_MAX_1X             8
#define DEFAULT_ICS645B_CHANNEL_COUNT            ICS645B_CHANNEL_COUNT_MAX_8X

//
// Buffer Length
// Manual : 0 - 1048575
#define ICS645B_BUFFER_LENGTH_MIN                1
#define ICS645B_BUFFER_LENGTH_MAX                1048576
#define ICS645B_BUFFER_WORD_BYTE_SIZE            8        // 8 Bytes = 64 bits
#define DEFAULT_ICS645B_BUFFER_LENGTH            131072   //ICS645B_BUFFER_LENGTH_MAX

//
// Acquisition Count
// Manual : 0 - 1048575
#define ICS645B_ACQUISITION_COUNT_MIN            1
#define ICS645B_ACQUISITION_COUNT_MAX            1048576
#define DEFAULT_ICS645B_ACQUISITION_COUNT        ICS645B_ACQUISITION_COUNT_MAX

//
// Decimation
#define ICS645B_DECIMATION_MIN                   1    // No Decimation
#define ICS645B_DECIMATION_MAX                   255
#define DEFAULT_ICS645B_DECIMATION               ICS645B_DECIMATION_MIN

//
// Frame Count
#define ICS645B_FPDP_FRAME_COUNT_MAX_4           1020
#define ICS645B_FPDP_FRAME_COUNT_MAX_8           510
#define ICS645B_FPDP_FRAME_COUNT_MAX_12          339
#define ICS645B_FPDP_FRAME_COUNT_MAX_16          254
#define ICS645B_FPDP_FRAME_COUNT_MAX_20          202
#define ICS645B_FPDP_FRAME_COUNT_MAX_24          168
#define ICS645B_FPDP_FRAME_COUNT_MAX_28          144
#define ICS645B_FPDP_FRAME_COUNT_MAX_32          126
#define ICS645B_FPDP_FRAME_COUNT_MIN             0
#define DEFAULT_ICS645B_FPDP_FRAME_COUNT         8

//
// OnBoard Memory
#define ICS645B_ONBOARD_MEMORY_BYTE_SIZE         16777216            // 16 MBytes
#define DEFAULT_ICS645B_MEMORY_BYTE_SIZE         ICS645B_ONBOARD_MEMORY_BYTE_SIZE

//
// FPDP Configuration
#define ICS645B_FPDP_DATA_RATE_MIN               1   // 1MHz
#define ICS645B_FPDP_DATA_RATE_MAX_TTL_STROBE    20  // 20MHz ==> 80 MB/s
#define ICS645B_FPDP_DATA_RATE_MAX_PECL          40  // 40MHz ==> 160 MB/s
#define ICS645B_FPDP_DATA_RATE_MAX_FPDP_II       50  // 50MHz ==> 400 MB/s
#define DEFAULT_ICS645B_FPDP_CLOCK               30  // 30MHz

#include "../DeviceLib/logger.h"

namespace ICS
{

	ICS645BDevice::ICS645BDevice(int id, DeviceVersion version)
		: VirtualDevice(32, TWO)
		, m_ID(id)
		, count(0)
		, m_DeviceVersion(version)
		, m_DeviceBufferLength(ICS645B_ONBOARD_MEMORY_BYTE_SIZE - 1)
		, m_OverSamplingRatio(OVER_SAMPLING_8X)
		, m_Data(ICS645B_ONBOARD_MEMORY_BYTE_SIZE)
		, m_FPDPInterface(FPDP_1_INTERFACE)
		, m_bUseFPDP(true)
		, m_TriggerSource(TRIGGER_INTERNAL)
		, m_ClockSource(CLOCK_INTERNAL)
		, m_FPDPDataFormat(PACKED_DATA)
        , m_FPDPFrameCount(DEFAULT_ICS645B_FPDP_FRAME_COUNT)
		, m_bUseClockOutput(false)
		, m_TriggerMode(TRIGGER_HIGH_LEVEL)
		, m_DeviceFPDPClock(40)
		, m_DeviceDecimation(1)
	{
		m_Voltages.clear(); 
		m_AvailableChannels.clear();
		m_AvailableChannels.push_back(32);
		m_AvailableChannels.push_back(16);
		m_AvailableChannels.push_back(8);
		std::sort(m_AvailableChannels.begin(), m_AvailableChannels.end());
		// Check Version
		if (m_DeviceVersion == ICS645B_ONE || m_DeviceVersion == ICS645B_ONE_A)
		{
			m_Voltages.push_back(ICS645B_ONE_INPUT_RANGE);
		}
		else  // ICS645B_ONE_B
		{
			m_Voltages.push_back(ICS645B_ONE_B_INPUT_RANGE/2);
		}
		m_MinimumSampleRatePerSecond = 57000;
		m_MaximumSampleRatePerSecond = 20000000;
		m_DeviceInfo = "ICS645B-" + std::to_string((long long)m_ID);

		std::wstring path = L"\\\\.\\";
		path += std::wstring(m_DeviceInfo.begin(), m_DeviceInfo.end());
		// Device
		m_hDevice = CreateFile(path.c_str(),
			GENERIC_READ|GENERIC_WRITE, 
			0,
			NULL, 
			OPEN_EXISTING, 
			0, 
			(HANDLE)NULL);

		SetSampleRatePerSecond(60000);

		VirtualDevice::Initialize();

		m_Runner->Stop();
	}
	
	ICS645BDevice::~ICS645BDevice()
	{
		Stop();

		if (m_hDevice != INVALID_HANDLE_VALUE)
		{
			ics645bBoardReset(m_hDevice);
			CloseHandle(m_hDevice);
			m_hDevice = INVALID_HANDLE_VALUE;
		}
	}

	bool ICS645BDevice::Initialize()
	{
		m_HardwardSampleRate = m_SampleRatePerSecond;
		if (m_SampleRatePerSecond > m_MaximumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MaximumSampleRatePerSecond;
		}
		else if (m_SampleRatePerSecond < m_MinimumSampleRatePerSecond)
		{
			m_HardwardSampleRate = m_MinimumSampleRatePerSecond;
		}
		m_DeviceBufferLength = 1048000;

		if (m_SampleRatePerSecond <= 100000)
		{
			m_DeviceBufferLength = 160000;
		}
		else if (m_SampleRatePerSecond <= 200000)
		{
			m_DeviceBufferLength = 320000;
		}
		else if (m_SampleRatePerSecond <= 500000)
		{
			m_DeviceBufferLength = 800000;
		}

		ICS645B_STATUS Status;
		int            result;
		auto logger = Logger::GetInstance();
		//
		// ics645bDisable
		result = ics645bDisable(m_hDevice);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bDisable");
			return false;
		}

		//
		// ics645BoardReset
		result = ics645bBoardReset(m_hDevice);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bBoardReset");
			return false;
		}

		if (m_SampleRatePerSecond <= ICS645B_SAMPLE_RATE_MAX_8X)
		{
			m_OverSamplingRatio = OVER_SAMPLING_8X;
		}
		else if (m_SampleRatePerSecond <= ICS645B_SAMPLE_RATE_MAX_4X)
		{
			m_OverSamplingRatio = OVER_SAMPLING_4X;
		}
		else if (m_SampleRatePerSecond <= ICS645B_SAMPLE_RATE_MAX_2X)
		{
			m_OverSamplingRatio = OVER_SAMPLING_2X;
		}
		else   // ICS645B_SAMPLE_RATE_MAX_1X
		{
			m_OverSamplingRatio = OVER_SAMPLING_1X;
		}

		//
		// ics645bControlSet
		m_DeviceControl.trigsel = (m_TriggerSource == TRIGGER_INTERNAL) ? 0 : 1;   // 0:Internal Trigger, 1:External Trigger
		m_DeviceControl.clksel = (m_ClockSource == CLOCK_INTERNAL) ? 0 : 1;   // 0:Internal Clock,   1:External Clock
		m_DeviceControl.diag = ICS645B_DISABLE;                                     // Disable Diagnose
		m_DeviceControl.adcen = ICS645B_DISABLE;                                     // ADC Disable
		m_DeviceControl.fpdpen = (m_bUseFPDP) ? ICS645B_ENABLE : ICS645B_DISABLE;
		m_DeviceControl.fpdpwidth = (m_FPDPDataFormat == PACKED_DATA) ? 1 : 0;   // 0:1Sample In 32bit Data, 1:2Sample in 32bit Data

		if (true)           // Single Board Configuration
		{
			m_DeviceControl.adcmaster = ICS645B_ENABLE;
			m_DeviceControl.adcterm = ICS645B_ENABLE;
			m_DeviceControl.fpdpmaster = ICS645B_ENABLE;
			m_DeviceControl.fpdpterm = ICS645B_ENABLE;
		}
		else 
		{
			//if (m_DeviceConfigureMode == DEVICE_CONFIGURE_MODE_DDR2001)
			//{
			//	// Master ADC enables 'adcmaster'
			//	if (IsMasterADCDevice())
			//		m_DeviceControl.adcmaster = ICS645B_ENABLE;
			//	else
			//		m_DeviceControl.adcmaster = ICS645B_DISABLE;

			//	// End-Slave ADC enables 'adcterm'
			//	if (IsEndSlaveADCDevice())
			//		m_DeviceControl.adcterm = ICS645B_ENABLE;
			//	else
			//		m_DeviceControl.adcterm = ICS645B_DISABLE;

			//	// Master FPDP enables 'fpdpmaster'
			//	if (IsMasterFPDPDevice())
			//		m_DeviceControl.fpdpmaster = ICS645B_ENABLE;
			//	else
			//		m_DeviceControl.fpdpmaster = ICS645B_DISABLE;

			//	// End-Slave FPDP enables 'fpdpterm'
			//	if (IsEndSlaveFPDPDevice())
			//		m_DeviceControl.fpdpterm = ICS645B_ENABLE;
			//	else
			//		m_DeviceControl.fpdpterm = ICS645B_DISABLE;
			//}
			//else // Standard
			//{
			//	if (IsMasterADCDevice())         // 1st Board : Master
			//	{
			//		m_DeviceControl.adcmaster = ICS645B_ENABLE;
			//		m_DeviceControl.adcterm = ICS645B_DISABLE;
			//		m_DeviceControl.fpdpmaster = ICS645B_ENABLE;
			//		m_DeviceControl.fpdpterm = ICS645B_ENABLE;
			//	}
			//	else if (IsEndSlaveADCDevice())  // Last Board : End-Slave
			//	{
			//		m_DeviceControl.adcmaster = ICS645B_DISABLE;
			//		m_DeviceControl.adcterm = ICS645B_ENABLE;
			//		m_DeviceControl.fpdpmaster = ICS645B_DISABLE;
			//		m_DeviceControl.fpdpterm = ICS645B_DISABLE;
			//	}
			//	else                            // Middle Boards : Mid-Slaves
			//	{
			//		m_DeviceControl.adcmaster = ICS645B_DISABLE;
			//		m_DeviceControl.adcterm = ICS645B_DISABLE;
			//		m_DeviceControl.fpdpmaster = ICS645B_DISABLE;
			//		m_DeviceControl.fpdpterm = ICS645B_DISABLE;
			//	}
			//}
		}

		switch (m_OverSamplingRatio)                        // 0:8x, 1:4x, 2:2x, 3:1x Oversampling
		{
		case OVER_SAMPLING_4X:
			m_DeviceControl.adcoversamp = 1;
			break;
		case OVER_SAMPLING_2X:
			m_DeviceControl.adcoversamp = 2;
			break;
		case OVER_SAMPLING_1X:
			m_DeviceControl.adcoversamp = 3;
			break;
		case OVER_SAMPLING_8X:
		default:
			m_DeviceControl.adcoversamp = 0;
		}

		m_DeviceControl.acqmode = 0;

		m_DeviceControl.inttrig = 0;
		m_DeviceControl.fpdp_ii = (m_FPDPInterface == FPDP_1_INTERFACE) ? 0 : 1;               // 0:FPDP-1, 1:FPDP-2
		m_DeviceControl.clkoe = (m_bUseClockOutput) ? ICS645B_ENABLE : ICS645B_DISABLE;    // ADC Clock Output Disabled
		m_DeviceControl.trigoe = (m_bUseTriggerOutput) ? ICS645B_ENABLE : ICS645B_DISABLE;    // Trigger Output Disabled

		switch (m_TriggerMode)  // 0:high, 1:rising, 2:low, 3:falling
		{
		case TRIGGER_HIGH_LEVEL:
			m_DeviceControl.trigmode = 0;
			break;
		case TRIGGER_LOW_LEVEL:
			m_DeviceControl.trigmode = 2;
			break;
		case TRIGGER_FALLING_EDGE:
			m_DeviceControl.trigmode = 3;
			break;
		case TRIGGER_RISING_EDGE:
		default:
			m_DeviceControl.trigmode = 1;
		}
		m_DeviceControl.filler2 = 0;
		m_DeviceControl.extclkterm = ICS645B_DISABLE;
		m_DeviceControl.exttrigterm = ICS645B_DISABLE;
		m_DeviceControl.filler1 = 0;

		result = ics645bControlSet(m_hDevice, &m_DeviceControl);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bControlSet");
			return false;
		}

		//
		// ics645bLocalControlSet
		m_DeviceLocalControl.byte_shift = 1;
		m_DeviceLocalControl.byte_order = 0;
		m_DeviceLocalControl.filler = 0;
		m_DeviceLocalControl.filler1 = 0;

		result = ics645bLocalControlSet(m_hDevice, &m_DeviceLocalControl);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bLocalControlSet");
			return false;
		}

		//
		// ics645bChannelNumberSet
		ULONGLONG m_DeviceChannelNumber = m_NumberOfChannels - 1;

		result = ics645bChannelNumberSet(m_hDevice, &m_DeviceChannelNumber);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bChannelNumberSet");
			return false;
		}

		//
		// ics645bBufferLengthSet
		ULONGLONG m_BufferLength = m_DeviceBufferLength - 1;

		result = ics645bBufferLengthSet(m_hDevice, &m_BufferLength);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bBufferLengthSet");
			return false;
		}

		//
		// ics645bDecimationSet
		ULONGLONG m_Decimation = m_DeviceDecimation - 1;

		result = ics645bDecimationSet(m_hDevice, &m_Decimation);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bDecimationSet");
			return false;
		}

		if (m_bUseFPDP)
		{
			//
			// ics645bMasterControlSet
			m_DeviceMasterControl.board_addr = 31;// m_FPDPIndex;

			if (m_DeviceControl.fpdpwidth == 1)  // Packed Mode
				m_DeviceMasterControl.channel_cnt = 1023;// m_NumberOfChannels / 2 - 1;// m_FPDPChannelCount / 2 - 1;
			else                                // Unpacked Mode
				m_DeviceMasterControl.channel_cnt = m_NumberOfChannels - 1;// m_FPDPChannelCount - 1;

			result = ics645bMasterControlSet(m_hDevice, &m_DeviceMasterControl);
			if (result != ICS645B_OK)
			{
				logger->Log("fail : ics645bMasterControlSet");
				return false;
			}

			//
			// ics645bFrameCountSet
			if (m_NumberOfChannels <= 4 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_4)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_4;
			}
			else if (m_NumberOfChannels <= 8 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_8)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_8;
			}
			else if (m_NumberOfChannels <= 12 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_12)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_12;
			}
			else if (m_NumberOfChannels <= 16 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_16)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_16;
			}
			else if (m_NumberOfChannels <= 20 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_20)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_20;
			}
			else if (m_NumberOfChannels <= 24 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_24)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_24;
			}
			else if (m_NumberOfChannels <= 28 && m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_28)
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_28;
			}
			else if (m_FPDPFrameCount > ICS645B_FPDP_FRAME_COUNT_MAX_32)  // m_ChannelCount <= 32
			{
				m_FPDPFrameCount = ICS645B_FPDP_FRAME_COUNT_MAX_32;
			}

			m_DeviceFrameCount = m_FPDPFrameCount - 1;

			result = ics645bFrameCountSet(m_hDevice, &m_DeviceFrameCount);
			if (result != ICS645B_OK)
			{
				logger->Log("fail : ics645bFrameCountSet");
				return false;
			}
		}

		if (m_ID == 1)  // Master Board
		{
			//if (m_DeviceConfigureMode == DEVICE_CONFIGURE_MODE_STANDARD)
			{
				// Copy code from SR3.8.0.16e : (3.8.0.16 d) 
				// Don't use ics645bADCFPDPDefaultClockSet() for DDR2001-645B LM Sunnyvale

				//
				// ics645bADCFPDPDefualtClockSet
				//result = ics645bADCFPDPDefaultClockSet(m_hDevice, &m_DeviceADCDefaultClock, &m_DeviceFPDPDefaultClock);
				//if (result != ICS645B_OK)
				{
					//logger->Log("fail : ics645bADCFPDPDefaultClockSet");
					//return false;
				}
			}

			// Copy code from SR3.8.0.16e : (3.8.0.16 c) Limit ADC Clock value
			// ics645bADCClockSet
			double maxAdcClock;
			double MEGAHERTZ_IN_HERTZ = 1000000;
			switch (m_OverSamplingRatio)                        // 0:8x, 1:4x, 2:2x, 3:1x Oversampling
			{
			case OVER_SAMPLING_4X:
				m_DeviceADCClock = (m_SampleRatePerSecond * 4) / ((double)MEGAHERTZ_IN_HERTZ);
				break;
			case OVER_SAMPLING_2X:
				m_DeviceADCClock = (m_SampleRatePerSecond * 2) / ((double)MEGAHERTZ_IN_HERTZ);
				break;
			case OVER_SAMPLING_1X:
				m_DeviceADCClock = m_SampleRatePerSecond / ((double)MEGAHERTZ_IN_HERTZ);
				break;
			case OVER_SAMPLING_8X:
			default:
				m_DeviceADCClock = (m_SampleRatePerSecond * 8) / ((double)MEGAHERTZ_IN_HERTZ);
			}
			maxAdcClock = ICS645B_ADC_CLOCK_MAX;
			m_DeviceADCClock *= 2;
			if (m_DeviceADCClock > maxAdcClock)
				m_DeviceADCClock = maxAdcClock;

			result = ics645bADCClockSet(m_hDevice, &m_DeviceADCClock, &m_DeviceADCActualClock);
			if (result != ICS645B_OK)
			{
				logger->Log("fail : ics645bADCClockSet");
				return false;
			}

			if (m_bUseFPDP)
			{
				//
				// ics645bFPDPClockSet
				//m_DeviceFPDPClock = m_FPDPClock;

				result = ics645bFPDPClockSet(m_hDevice, &m_DeviceFPDPClock, &m_DeviceFPDPActualClock);
				if (result != ICS645B_OK)
				{
					logger->Log("fail : ics645bFPDPClockSet");
					return false;
				}
			}

			Sleep(5);
		}

		//
		// ics645bBufferReset
		result = ics645bBufferReset(m_hDevice);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bBufferReset");
			return false;
		}

		//
		// Reset ADC and Check Sync Error

		m_ADCResetCount = 0;

		do{
			//
			// ics645bADCReset
			result = ics645bADCReset(m_hDevice);
			if (result != ICS645B_OK)
			{
				logger->Log("fail : ics645bADCReset");
				return false;
			}

			//
			// Get Device Status
			result = ics645bStatusGet(m_hDevice, &Status);
			if (result != ICS645B_OK)
			{
				logger->Log("fail : ics645bStatusGet");
				return false;
			}

			// Check Max Count
			if (m_ADCResetCount >= 3)
			{
				//return false;
			}

			m_ADCResetCount++;

		} while ((Status.syncerr == 1) && (m_ADCResetCount < 3));

		//
		// ics645bEnable
		result = ics645bEnable(m_hDevice);
		if (result != ICS645B_OK)
		{
			logger->Log("fail : ics645bEnable");
			return false;
		}

		m_KeepingDuration = 500000000;
		count = 0;
		if (m_Channels.size() > m_NumberOfChannels)
		{
			size_t RemoveChannels = m_Channels.size() - m_NumberOfChannels;
			for (size_t i = 0; i < RemoveChannels; ++i)
			{
				delete m_Channels.back();
				m_Channels.pop_back();
			}
		}
		else if (m_Channels.size() < m_NumberOfChannels)
		{
			size_t AddChannels = m_NumberOfChannels - m_Channels.size();
			int ncounts = SystemManager::GetInstance()->GetCurrentChannelCount();
			for (size_t i = 0; i < AddChannels; ++i)
			{
				ChannelInfo* pChannel = new ChannelInfo("Channel " + std::to_string((long long)m_Channels.size() + ncounts + 1), this);
				m_Channels.push_back(pChannel);
			}
		}

		for (size_t i = 0; i < m_Channels.size(); ++i)
		{
			ChannelInfo* pChannel = m_Channels[i];
			pChannel->Init(m_KeepingDuration, m_SampleRatePerSecond);
		}
		VirtualDevice::InitSnapshot();
		if (m_DataManager)
		{
			m_DataManager->Initialize();
			m_DataManager->SetManagedSampleRatePerSecond(m_SampleRatePerSecond);
		}

		struct ClearFunctor
		{
			void operator()(DeviceData*& data)
			{
				delete data;
			}
		};
		std::for_each(m_DeviceDatas.begin(), m_DeviceDatas.end(), ClearFunctor());
		m_DeviceDatas.clear();

		DeviceData* data = new DeviceData(0, m_KeepingDuration, m_HardwardSampleRate, m_NumberOfChannels, GetDeviceInputMilliVoltage(), m_Format);
		m_DeviceDatas.push_back(data);
		m_RenderData->SetInputMilliVolt(m_Voltages[m_InputVoltageIndex]);
		m_RenderData->SetDurationNanoSecond(m_KeepingDuration);
		m_RenderData->SetSampleRatePerSecond(m_SampleRatePerSecond);
		m_RenderData->InitData();
		return true;
	}

	bool ICS645BDevice::IsOpen()
	{
		return m_hDevice != INVALID_HANDLE_VALUE;
	}

	void ICS645BDevice::SetSampleRatePerSecond(long rate)
	{
		m_SampleRatePerSecond = rate;
	}

	void ICS645BDevice::SetChannelNumber(unsigned int nchannels)
	{
		VirtualDevice::SetChannelNumber(nchannels);
	}

	bool ICS645BDevice::Start()
	{
		int result = ics645bEnable(m_hDevice);
		if (result != ICS645B_OK)
		{
			return false;
		}
		if (m_ID == 1) 
		{
			if (ics645bTrigger(m_hDevice) != ICS645B_OK)
			{
				return false;
			}
		}

		m_Runner->Reset();
		m_Thread = new boost::thread(boost::ref(*m_Runner));
		m_RunNanoSecond = 0;

		return true;
	}

	bool ICS645BDevice::Stop()
	{
		if (m_Thread)
		{
			m_Runner->Stop();
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			delete m_Thread;
			m_Thread = 0;
			if (ics645bDisable(m_hDevice) != ICS645B_OK)
			{
				return false;
			}
		}
		return true;
	}

	void ICS645BDevice::Update()
	{
		if (m_Thread == 0) return;

		// Read Data from Device
		DWORD DataSize = (DWORD)m_DeviceBufferLength;
		DWORD ActualSize = 0;
		bool bResult = ReadFile(m_hDevice, &m_Data[0], DataSize, &ActualSize, NULL) == TRUE;
		if (bResult)
		{
			int nDataSize = ActualSize;
			char* pData = &m_Data[0];
			if (m_DataManager && !m_DeviceDatas.empty())
			{
				DeviceData* data = m_DeviceDatas.front();
				int nDeviceDataSize = (int)data->GetDataByteSize();
				if (nDeviceDataSize != nDataSize)
				{
					long long ds = nDataSize;
					long long ns = DeviceData::GetCloseADCCountDataSizeAndNanoSecond(ds, m_SampleRatePerSecond, m_NumberOfChannels, m_Format);
					data->SetDurationNanoSecond(ns);
					data->InitData();
				}
				nDeviceDataSize = (int)data->GetDataByteSize();
				char * pDeviceData = data->GetADCCountData();
				memcpy(&pDeviceData[0], &pData[0], (int)nDeviceDataSize);
				data->SetStartNanoSecond(m_RunNanoSecond);
				m_RunNanoSecond += data->GetDurationNanoSecond();
				m_DataManager->AddData(data);
				m_DataManager->EndSample();
				m_DataManager->BeginSample();
			}
		}
		
		ics645bStatusGet(m_hDevice, &m_DeviceStatus);
	}
} 