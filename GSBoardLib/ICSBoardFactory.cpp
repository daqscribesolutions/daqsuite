#include "ICSBoardFactory.h"
#include "ICS645BDevice.h"

namespace ICS
{
	void ICSBoardFactory::GetDevices(std::vector<IDevice*>& devices)
	{
		devices.clear();
		int i = 0;
		long long id = 1;
		while (true)
		{
			ICS645BDevice *pDevice = new ICS645BDevice(i + 1, ICS645B_ONE_B);
			if (pDevice->IsOpen())
			{
				auto& channels = pDevice->GetChannels();
				for (int k = 0; k < (int)channels.size(); ++k)
				{
					auto& ch = channels[k];
					ch->Name = "Channel " + std::to_string(id);
					++id;
				}
				devices.push_back(pDevice);
				++i;
			}
			else
			{
				break;
			}
		}
	}

	ICSBoardFactory::ICSBoardFactory()
	{
	}

	ICSBoardFactory::~ICSBoardFactory()
	{
	}
}