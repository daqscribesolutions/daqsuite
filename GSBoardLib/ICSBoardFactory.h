#ifndef ICSBOARDFACTORY_H
#define ICSBOARDFACTORY_H

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

namespace ICS
{
	/**
	* ICSBoardFactory is the implementation of IDeviceFactory to support adding ICS Boards to the system.
	*/
	class ICSBoardFactory : public IDeviceFactory
	{

	public:

		/// Default constructor.
		ICSBoardFactory();

		/// Default destructor.
		~ICSBoardFactory();

		/// Implementation of IDeviceFactory interface.
		virtual void GetDevices(std::vector<IDevice*>& devices);

	};


}

#endif