#ifndef ICS645BDEVICE_H
#define ICS645BDEVICE_H

#include "../DeviceLib/VirtualDevice.h"
using namespace DeviceLib;
#include <Windows.h>
#include "ICS645bApi.h"

namespace ICS
{
	const int NUM_BUFFERS = 2;
	// Version
	enum DeviceVersion
	{
		ICS645B_ONE,              // ICS-645B-xx  : Max 2Vpp Input Range
		ICS645B_ONE_A,            // ICS-645B-xxA : Max 2Vpp Input Range, 50 Ohm Impedance
		ICS645B_ONE_B,            // ICS-645B-xxB : Max 20Vpp Input Range
		ICS645B_VERSION_COUNT,
	};

	// ICS 645B OverSampling Ratio
	enum OverSamplingRatio
	{
		OVER_SAMPLING_8X,
		OVER_SAMPLING_4X,
		OVER_SAMPLING_2X,
		OVER_SAMPLING_1X,
		OVER_SAMPLING_RATIO_COUNT,
	};

	// Trigger Mode
	enum TriggerMode
	{
		TRIGGER_HIGH_LEVEL,
		TRIGGER_RISING_EDGE,
		TRIGGER_LOW_LEVEL,
		TRIGGER_FALLING_EDGE,
		TRIGGER_MODE_COUNT,
	};

	enum FPDPInterface
	{
		FPDP_1_INTERFACE,
		FPDP_2_INTERFACE,
		FPDP_INTERFACE_COUNT,
	};

	// Clock Source
	enum ClockSource
	{
		CLOCK_INTERNAL,
		CLOCK_EXTERNAL,
		CLOCK_SOURCE_COUNT,
	};

	// Trigger Source
	enum TriggerSource
	{
		TRIGGER_INTERNAL,
		TRIGGER_EXTERNAL,
		TRIGGER_SOURCE_COUNT,
	};
	// ADC Data Format
	enum DataFormat
	{
		UNPACKED_DATA,
		PACKED_DATA,
		DATA_FORMAT_COUNT,
	};
	class ICS645BDevice : public VirtualDevice
	{

	public:

		ICS645BDevice(int id, DeviceVersion version);

		~ICS645BDevice();

		virtual bool Initialize();

		virtual bool Start();

		virtual bool Stop();

		virtual void Update();

		virtual void SetSampleRatePerSecond(long rate);

		virtual void SetChannelNumber(unsigned int nchannels);

		bool IsOpen();

	protected:

		int m_ID;

		int count;

		HANDLE m_hDevice;

		// ICS 645B Information 
		DeviceVersion          m_DeviceVersion;
		OverSamplingRatio      m_OverSamplingRatio;
		BOOL                   m_bUseClockOutput;         // Clock Output Enable
		BOOL                   m_bUseTriggerOutput;       // Trigger Output Enable
		TriggerMode            m_TriggerMode;

		// ADC Configuration Data
		ICS645B_LOCAL_CONTROL  m_DeviceLocalControl;
		ICS645B_CONTROL        m_DeviceControl;
		ULONGLONG              m_DeviceDecimation;
		ULONGLONG              m_DeviceAcquisitionCount;
		ULONGLONG              m_DeviceBufferLength;
		double                 m_DeviceADCClock;
		double                 m_DeviceADCActualClock;
		double                 m_DeviceADCDefaultClock;

		// FPDP Configuration Data
		ULONGLONG              m_DeviceFrameCount;
		ICS645B_MASTER_CONTROL m_DeviceMasterControl;
		double                 m_DeviceFPDPClock;
		double                 m_DeviceFPDPActualClock;
		double                 m_DeviceFPDPDefaultClock;

		// Device Status
		ICS645B_STATUS m_DeviceStatus;
		DWORD m_ADCResetCount;
		FPDPInterface    m_FPDPInterface;
		bool             m_bUseFPDP;

		TriggerSource m_TriggerSource;
		ClockSource m_ClockSource;

		DataFormat m_FPDPDataFormat;
		ULONGLONG m_FPDPFrameCount;

		std::vector<char> m_Data;

	};
}

#endif