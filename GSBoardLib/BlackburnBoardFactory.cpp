#include "BlackburnBoardFactory.h"
#include "GB104Device.h"

namespace Blackburn
{
	void BlackburnBoardFactory::GetDevices(std::vector<IDevice*>& devices)
	{
		devices.clear();
		int i = 1;
		long long id = 1;
		while (true)
		{
			GB104Device * pDevice = new GB104Device(i);
			if (std::string(pDevice->GetDeviceInfo()).find("GB104") == std::string::npos)
			{
				delete pDevice;
				break;
			}
			auto& channels = pDevice->GetChannels();
			for (int k = 0; k < (int)channels.size(); ++k)
			{
				auto& ch = channels[k];
				ch->Name = "Channel " + std::to_string(id);
				++id;
			}
			devices.push_back(pDevice);
			++i;
		}
	}

	BlackburnBoardFactory::BlackburnBoardFactory()
	{
	}

	BlackburnBoardFactory::~BlackburnBoardFactory()
	{
	}
}