#include "monitoringchannelmodel.h"
#include "channelgraphicview.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

QVector<QColor> DisplayChannel::ChannelColors;

MonitoringChannelModel::MonitoringChannelModel(QObject *parent, bool bSnapshot)
	: QAbstractTableModel(parent)
	, m_nDevices(0)
	, m_bSnapshot(bSnapshot)
	, m_bSnapshotReady(false)
{
	QVector<QColor> colors;
	colors.push_back(Qt::black);
	colors.push_back(Qt::red);
	colors.push_back(Qt::darkRed);
	colors.push_back(Qt::green);
	colors.push_back(Qt::darkGreen);
	colors.push_back(Qt::blue);
	colors.push_back(Qt::darkBlue);
	colors.push_back(Qt::cyan);
	colors.push_back(Qt::darkCyan);
	colors.push_back(Qt::magenta);
	colors.push_back(Qt::darkMagenta);
	colors.push_back(Qt::yellow);
	colors.push_back(Qt::darkYellow);
	DisplayChannel::ChannelColors = colors;
}

MonitoringChannelModel::~MonitoringChannelModel()
{
}

void MonitoringChannelModel::UpdateChannels()
{
	const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
	int nChannels = 0;
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[i].get());
		if (pDevice)
		{
			std::vector<ChannelInfo*> channels = pDevice->GetChannels();
			for (size_t j = 0; j < channels.size(); ++j)
			{
				if (channels[j]->GetActive())
					++nChannels;
			}
		}
	}

	if (nChannels != m_Channels.size())
		m_Channels.resize(nChannels);

	nChannels = 0;
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[i].get());
		if (pDevice)
		{
			std::vector<ChannelInfo*> channels = m_bSnapshot ? pDevice->GetSnapshotChannels() : pDevice->GetChannels();
			int ni = 0;
			for (size_t j = 0; j < channels.size(); ++j)
			{
				int nIdx = ni + nChannels;
				if (!(m_bSnapshot ? channels[j]->pOrigin->GetActive() : channels[j]->GetActive()) || nIdx >= (int)m_Channels.size()) continue;
				++ni;
				DisplayChannel& channel = m_Channels[nIdx];
				channel.m_DeviceID = (int) i;
				channel.m_ChannalIndex = nIdx;
				channel.m_Channel = channels[j];
				channel.m_Color = DisplayChannel::ChannelColors[nIdx % DisplayChannel::ChannelColors.size()];
			}
			nChannels += ni;
		}
	}
	m_nDevices = nChannels;
	dataChanged(QModelIndex(), QModelIndex());
}

Qt::ItemFlags MonitoringChannelModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.column() > 0 && index.column() < 4)
		return flags | Qt::ItemIsEditable;
	return flags;
}

int MonitoringChannelModel::rowCount(const QModelIndex &) const
{
	return m_Channels.size();
}

int MonitoringChannelModel::columnCount(const QModelIndex &) const
{
	return 12;
}

QVariant MonitoringChannelModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if ((int)m_Channels.size() > index.row())
		{
			DisplayChannel channel = m_Channels[index.row()];
			if (channel.m_Channel)
			{
				if (index.column() == 0)
				{
					return m_bSnapshot? channel.m_Channel->pOrigin->Name.c_str() : channel.m_Channel->Name.c_str();
				}
				else if (index.column() == 1)
				{
					return channel.m_DisplayViewIndex + 1;
				}
				else if (index.column() == 2)
				{
					return channel.m_Color;
				}
				else if (index.column() == 3)
				{
					return channel.m_DisplayValueType == VOLT ? "VOLT" : "EU";
				}
				else if (index.column() == 4)
				{
					return QString::number(channel.m_Channel->MilivoltDataMax, 'f', 2);
				}
				else if (index.column() == 5)
				{
					return QString::number(channel.m_Channel->MilivoltDataMin, 'f', 2);
				}
				else if (index.column() == 6)
				{
					return QString::number(channel.m_Channel->EUDataMax, 'f', 2);
				}
				else if (index.column() == 7)
				{
					return QString::number(channel.m_Channel->EUDataMin, 'f', 2);
				}
				else if (index.column() == 8)
				{
					return QString::number(channel.m_Channel->Average, 'f', 2);
				}
				else if (index.column() == 9)
				{
					return QString::number(channel.m_Channel->RMS, 'f', 2);
				}
				else if (index.column() == 10)
				{
					return QString::number(channel.m_Channel->EUAverage, 'f', 2);
				}
				else if (index.column() == 11)
				{
					return QString::number(channel.m_Channel->EURMS, 'f', 2);
				}
			}
		}
	}

	return QVariant();
}

bool MonitoringChannelModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		DisplayChannel& channel = m_Channels[index.row()];

		if (index.column() == 0)
		{
		}
		else if (index.column() == 1)
		{
			channel.m_DisplayViewIndex = std::max(-1, value.toInt() - 1);
		}
		else if (index.column() == 2)
		{
			channel.m_Color = value.toString();
		}
		else if (index.column() == 3)
		{
			channel.m_DisplayValueType = value.toString() ==  "VOLT" ? VOLT : EU;
		}
		emit render();
		return true;
	}
	return false;
}

QVariant MonitoringChannelModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		if (section == 0)
		{
			return QString("Channel");
		}
		else if (section == 1)
		{
			return QString("View ID\n(0:invisible)");
		}
		else if (section == 2)
		{
			return QString("Color");
		}
		else if (section == 3 )
		{
			return QString("Value");
		}
		else if (section == 4)
		{
			return QString("Milivolt\nMax");
		}
		else if (section == 5)
		{
			return QString("Milivolt\nMin");
		}
		else if (section == 6)
		{
			return QString("EU\nMax");
		}
		else if (section == 7)
		{
			return QString("EU\nMin");
		}
		else if (section == 8)
		{
			return QString("Milivolt\nAverage");
		}
		else if (section == 9)
		{
			return QString("Milivolt\nRMS");
		}
		else if (section == 10)
		{
			return QString("EU\nAverage");
		}
		else if (section == 11)
		{
			return QString("EU\nRMS");
		}
	}

	return QVariant();
}

std::vector<DisplayChannel>& MonitoringChannelModel::GetChannels()
{
    return m_Channels;
}

void MonitoringChannelModel::SetView(ChannelGraphicView* view)
{
	m_view = view;
}

void MonitoringChannelModel::clearChannels()
{
	m_Channels.clear();
}

void MonitoringChannelModel::Include(int id)
{
	m_Channels[id].m_DisplayViewIndex = m_view->getSelectedItemID();
}

void MonitoringChannelModel::SetView(int id, int viewid)
{
	m_Channels[id].m_DisplayViewIndex = viewid - 1;
}

void MonitoringChannelModel::Exclude(int id)
{
	m_Channels[id].m_DisplayViewIndex = -1;
}
void MonitoringChannelModel::SaveChannelSetting(boost::property_tree::ptree& pt, int nviews)
{
	try
	{
		using boost::property_tree::ptree;
		ptree& display = pt.add(m_bSnapshot ? "SnapshotDisplay" : "ScopeDisplay", "");
		display.add("ScopeDisplay.Number", nviews);
		for (int i = 0; i < (int)m_Channels.size(); ++i)
		{
			auto& channel = m_Channels[i];
			ptree & node = display.add("Channels.Channel", "");
			node.put("<xmlattr>.ID", (int)channel.m_ChannalIndex);
			node.put("R", channel.m_Color.red());
			node.put("G", channel.m_Color.green());
			node.put("B", channel.m_Color.blue());
			node.put("DisplayValue", (int)channel.m_DisplayValueType);
			node.put("DisplayIdex", channel.m_DisplayViewIndex);
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}

void MonitoringChannelModel::ReadChannelSetting(const boost::property_tree::ptree& pt, int& nviews)
{
	try
	{
		using boost::property_tree::ptree;
		auto display = pt.get_child_optional(m_bSnapshot ? "SnapshotDisplay" : "ScopeDisplay");
		if (display)
		{
			nviews = display->get<int>("ScopeDisplay.Number");
			auto su = display->get_child_optional("Channels");
			if (su)
			{
				for (auto itr = su->begin(); itr != su->end(); ++itr)
				{
					if (itr->first == "Channel")
					{
						int id = itr->second.get<int>("<xmlattr>.ID");
						int r = itr->second.get<int>("R");
						int g = itr->second.get<int>("G");
						int b = itr->second.get<int>("B");
						int displayvalue = itr->second.get<int>("DisplayValue");
						int displayindex = itr->second.get<int>("DisplayIdex");
						for (int i = 0; i < (int)m_Channels.size(); ++i)
						{
							auto& channel = m_Channels[i];
							if (channel.m_ChannalIndex == id)
							{
								channel.m_Color.setRed(r);
								channel.m_Color.setGreen(g);
								channel.m_Color.setBlue(b);
								channel.m_DisplayValueType = (DisplayValueType)(displayvalue);
								channel.m_DisplayViewIndex = displayindex;
								break;
							}
						}
					}
				}
			}
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}