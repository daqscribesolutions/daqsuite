#include "channelmodel.h"
#include <qfile.h>
#include <qdir.h>
#include <qfileinfo.h>
#include <qcoreapplication.h>
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

ChannelModel::ChannelModel(QObject *parent)	: QAbstractTableModel(parent)
{
}

ChannelModel::~ChannelModel()
{
}

void ChannelModel::UpdateChannels()
{
	const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
	int nChannels = 0;
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[i].get());
		if (pDevice)
		{
			std::vector<ChannelInfo*> channels = pDevice->GetChannels();
			for (size_t j = 0; j < channels.size(); ++j)
			{
				++nChannels;
			}
		}
	}

	if (nChannels != m_Channels.size())
		m_Channels.resize(nChannels);

	nChannels = 0;
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[i].get());
		if (pDevice)
		{
			std::vector<ChannelInfo*> channels = pDevice->GetChannels();
			for (size_t j = 0; j < channels.size(); ++j)
			{
				m_Channels[nChannels] = channels[j];
				++nChannels;
			}
		}
	}
	dataChanged(QModelIndex(), QModelIndex());
}

Qt::ItemFlags ChannelModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (((index.column() < 3 || index.column() > 4) && index.row() > 0) || (index.column() > 7 && index.row() == 0))
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int ChannelModel::rowCount(const QModelIndex &) const
{
	return m_Channels.size() + 1;
}

int ChannelModel::columnCount(const QModelIndex &) const
{
	int res = 8;
	if (m_Channels.size() > 0 && m_Channels.front())
		res += m_Channels.front()->UserData.size();
	return res;
}

QVariant ChannelModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.row() == 0)
		{
			int section = index.column();
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Description");
			}
			else if (section == 2)
			{
				return QString("Status");
			}
			else if (section == 3)
			{
				return QString("Sensitivity");
			}
			else if (section == 4)
			{
				return QString("Offset");
			}
			else if (section == 5)
			{
				return QString("EU Sensitivity");
			}
			else if (section == 6)
			{
				return QString("EU Offset");
			}
			else if (section == 7)
			{
				return QString("EU Unit");
			}
			else
			{
				if (m_Channels.size() > 0)
				{
					ChannelInfo* channel = m_Channels.front();
					if (channel && (int)channel->UserData.size() > section - 8)
					{
						int idx = section - 8;
						if (idx < (int)channel->UserData.size())
						{
							return channel->UserData[idx].name.c_str();
						}
					}
				}
			}
		}
		else
		{
			ChannelInfo* channel = m_Channels[index.row() - 1];
			if (channel)
			{
				if (index.column() == 0)
				{
					return channel->Name.c_str();
				}
				else if (index.column() == 1)
				{
					return channel->Description.c_str();
				}
				else if (index.column() == 2)
				{
					return QString(channel->GetActive() ? "Active" : "Inactive");
				}
				else if (index.column() == 3)
				{
					return QString("%1").arg(channel->GetSensitivity());
				}
				else if (index.column() == 4)
				{
					return QString("%1").arg(channel->GetOffset());
				}
				else if (index.column() == 5)
				{
					return QString("%1").arg(channel->GetEUSensitivity());
				}
				else if (index.column() == 6)
				{
					return QString("%1").arg(channel->GetEUOffset());
				}
				else if (index.column() == 7)
				{
					return channel->EUName.c_str();
				}
				else
				{
					int idx = index.column() - 8;
					if (idx < (int)channel->UserData.size())
					{
						return channel->UserData[idx].value.c_str();
					}
				}
			}
		}
	}

	return QVariant();
}

bool ChannelModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.row() == 0)
		{
			int section = index.column();
			if (section > 7)
			{
				if (m_Channels.size() > 0)
				{
					ChannelInfo* channel = m_Channels.front();
					if (channel && (int)channel->UserData.size() > section - 8)
					{
						channel->UserData[section - 8].name = value.toString().toStdString();
					}
				}
			}
		}
		else
		{
			ChannelInfo* channel = m_Channels[index.row() - 1];
			if (channel)
			{
				if (index.column() == 0)
				{
					channel->Name = value.toString().toStdString();
				}
				else if (index.column() == 1)
				{
					channel->Description = value.toString().toStdString();
				}
				else if (index.column() == 2)
				{
					channel->SetActive(value.toString() == "Active");
					dataChanged(QModelIndex(), QModelIndex());
				}
				else if (index.column() == 3)
				{
					channel->SetSensitivity(value.toDouble());
				}
				else if (index.column() == 4)
				{
					channel->SetOffset(value.toDouble());
				}
				else if (index.column() == 5)
				{
					channel->SetEUSensitivity(value.toDouble());
				}
				else if (index.column() == 6)
				{
					channel->SetEUOffset(value.toDouble());
				}
				else if (index.column() == 7)
				{
					channel->EUName = value.toString().toStdString();
				}
				else
				{
					channel->UserData[index.column() - 8].value = value.toString().toStdString();
				}
			}
			return true;
		}
	}
	return false;
}

QVariant ChannelModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Description");
			}
			else if (section == 2)
			{
				return QString("Status");
			}
			else if (section == 3)
			{
				return QString("Sensitivity");
			}
			else if (section == 4)
			{
				return QString("Offset");
			}
			else if (section == 5)
			{
				return QString("EU\nSensitivity");
			}
			else if (section == 6)
			{
				return QString("EU\nOffset");
			}
			else if (section == 7)
			{
				return QString("EU\nUnit");
			}
			else 
			{
				if (m_Channels.size() > 0)
				{
					ChannelInfo* channel = m_Channels.front();
					if (channel && (int)channel->UserData.size() > section - 8)
					{
						return channel->UserData[section - 8].name.c_str();
					}
				}
			}
		}
		else
		{
			if (section > 0)
				return "Ch. " + QString::number(section);
		}
	}

	return QVariant();
}

void ChannelModel::LoadUserData(QString filename)
{
	QList<QStringList> parms;
	QString data;
	QFile f(filename);
	if (f.open(QIODevice::ReadOnly))
	{
		parms.clear();
		data = f.readAll();
		QStringList lines = data.split('\n');
		for (int i = 0; i < lines.size(); ++i)
		{
			QStringList items = lines[i].split(',');
			for (int k = 0; k < items.size(); ++k)
			{
				items[k] = items[k].simplified();
			}
			parms.push_back(items);
		}
		int nlines = parms.size();
		if (nlines >(int)m_Channels.size() + 1)
		{
			nlines = (int)m_Channels.size() + 1;
		}
		for (int i = 0; i < nlines; ++i)
		{
			int nitems = parms[i].size();
			if (i == 0)
			{
				for (int k = 0; k < (int)m_Channels.size(); ++k)
				{
					auto ch = m_Channels[k];
					int n = std::max(nitems - 8, 0);
					ch->UserData.resize(n);
					for (int section = 0; section < n; ++section)
					{
						ch->UserData[section].name = parms[i][section + 8].toStdString();
					}
				}
			}
			else
			{
				auto ch = m_Channels[i - 1];
				int n = std::max(nitems - 8, 0);
				ch->UserData.resize(n);
				for (int section = 0; section < n; ++section)
				{
					ch->UserData[section].value = parms[i][section + 8].toStdString();
				}
				for (int section = 0; section < nitems; ++section)
				{
					if (section == 0)
					{
						ch->Name = parms[i][section].toStdString();
					}
					else if (section == 1)
					{
						ch->Description = parms[i][section].toStdString();
					}
					else if (section == 2)
					{
						ch->SetActive(parms[i][section].toInt() != 0); 
					}
					else if (section == 3)
					{
						//ch->SetSensitivity(parms[i][section].toDouble());
					}
					else if (section == 4)
					{
						//ch->SetOffset(parms[i][section].toDouble());
					}
					else if (section == 5)
					{
						ch->SetEUSensitivity(parms[i][section].toDouble());
					}
					else if (section == 6)
					{
						ch->SetEUOffset(parms[i][section].toDouble());
					}
					else if (section == 7)
					{
						ch->EUName = parms[i][section].toStdString();
					}
				}
			}
		}
	}
}

void ChannelModel::SaveUserData(QString filename, int idx)
{
	std::ofstream fout(filename.toStdString().c_str());
	fout << "Name,Description,Status,Sensitivity,Offset,EU Sensitivity,EU Offset,EU Unit";
	if (m_Channels.size() > 0)
	{
		ChannelInfo* channel = m_Channels.front();
		for (int i = 0; i < (int)channel->UserData.size(); ++i)
		{
			fout << "," << channel->UserData[i].name;
		}
		fout << "\n";
		int rows = (int)m_Channels.size();
	
		for (int i = 0; i < rows; ++i)
		{
			auto ch = m_Channels[i];
			int id = ch->GetDeviceId();
			if (id == idx || idx < 0)
			{
				fout << ch->Name << "," << ch->Description << "," << (ch->GetActive() ? 1 : 0);
				fout << "," << ch->GetSensitivity() << "," << ch->GetOffset();
				fout << "," << ch->GetEUSensitivity() << "," << ch->GetEUOffset();
				fout << "," << ch->EUName;

				for (int j = 0; j < (int)ch->UserData.size(); ++j)
				{
					fout << "," << ch->UserData[j].value;
				}
				fout << "\n";
			}
		}
	}
}

void ChannelModel::SaveDSI()
{
	auto devices = SystemManager::GetInstance()->GetDevices();
	auto setting = DeviceRecordSetting::GetInstance();
	QDir dir = QFileInfo(QCoreApplication::applicationFilePath()).dir();
	auto current = dir.absolutePath();
	for (int i = 0; i < (int)devices.size(); ++i)
	{
		auto dev = devices[i];
		std::string filename = setting->GetRecordFileName(i) + ".CSV";
		SaveUserData(filename.c_str(), dev->GetID());
		std::string command = current.toStdString() + "/DSIWriter.exe " + filename + "," + setting->GetRecordName();
		process.start(command.c_str());
		process.waitForFinished();
		dir.remove(filename.c_str());
	}
}
