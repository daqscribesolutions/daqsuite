#include "zoomchannelviewitem.h"

ZoomChannelViewItem::ZoomChannelViewItem(std::vector<ChannelRenderData *>* d, int id)
	: ChannelViewItem(d, id)
	, m_DisplayBeginSecond(0)
	, m_DisplayDurationSecond(0.1f)
{
}

ZoomChannelViewItem::~ZoomChannelViewItem()
{
}

void ZoomChannelViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
	if (channelRenderDatas->empty()) return;

	ChannelRenderData* channelRenderData = channelRenderDatas->front();

	QPen pen;
	pen.setColor(m_LineColor);
	QBrush brush(m_BackGroundColor);
	painter->setPen(pen);
	painter->setBrush(brush);
	painter->drawRect(QRect(channelRenderData->YAxisWidth + 10, 10, channelRenderData->ChartAreaWidth, channelRenderData->ChartAreaHeight));
	painter->drawLines(channelRenderData->lines);

	std::vector<DisplayChannel>& channels = channelRenderData->model->GetChannels();

	if (channelRenderData->beginSecond > m_DisplayBeginSecond)
	{
		m_DisplayBeginSecond = channelRenderData->beginSecond;
	}
	if (channelRenderData->durationSecond < (m_DisplayBeginSecond + m_DisplayDurationSecond))
	{
		m_DisplayDurationSecond = channelRenderData->durationSecond - m_DisplayBeginSecond;
	}

	int nDisplayNumPts = 0;
	int nDisplayBeginIndex = 0;
	double fwidthStep = 0;
	if (channels.size() > 0)
	{
		std::vector<PointData>& pts = channels.front().points;
		double width = 1;
		if (pts.size() > 0) width = pts.back().x - pts.front().x;
		nDisplayNumPts = static_cast<int>(floor(m_DisplayDurationSecond / channelRenderData->durationSecond * pts.size()));
		if (nDisplayNumPts <= 0)
		{
			return;
		}
		nDisplayBeginIndex = static_cast<int>(floor(m_DisplayBeginSecond / channelRenderData->durationSecond * pts.size()));
		int nNumPts = nDisplayNumPts;
		if (nNumPts > channelRenderData->nXAxis)
		{
			nNumPts = channelRenderData->nXAxis;
		}
		if (nNumPts == 0)
		{
			nNumPts = channelRenderData->nXAxis;
		}

		int nStep = nDisplayNumPts / nNumPts;
		double val = m_DisplayBeginSecond;
		double fStep = m_DisplayDurationSecond / nNumPts;
		fwidthStep = width / nNumPts;
		float b = pts.front().x;
		for (int i = 0; i <= nNumPts; ++i)
		{
			painter->drawText(QPoint(b + fwidthStep * i - 10, channelRenderData->height), QString::number(val, 'f', 2));
			val += fStep;
		}

		nStep = channelRenderData->ChartAreaHeight / channelRenderData->nNumLevels;
		val = channelRenderData->MaxValue;
		fStep = (channelRenderData->MaxValue - channelRenderData->MinValue) / channelRenderData->nNumLevels;
		for (int i = 0; i < channelRenderData->nNumLevels; ++i)
		{
			painter->drawText(QPoint(channelRenderData->YAxisWidth - 35, nStep * i + 16), QString::number(val, 'f', 2));
			val -= fStep;
		}
		painter->drawText(QPoint(channelRenderData->YAxisWidth - 35, nStep * channelRenderData->nNumLevels + 16), QString::number(val, 'f', 2));
		fwidthStep = width / nDisplayNumPts;
	}

	if (nDisplayNumPts <= 0)
	{
		return;
	}

	for (int ci = 0; ci < (int)channels.size(); ++ci)
	{
		if (channels[ci].m_DisplayViewIndex == m_ID)
		{
			pen.setColor(channels[ci].m_Color);
			painter->setPen(pen);
			QVector<QPoint>& points = channels[ci].renderPoints;
			std::vector<PointData>& pts = channels[ci].points;
			points.resize(nDisplayNumPts);
			double b = pts.front().x;
			for (int j = 0; j < nDisplayNumPts; ++j)
			{
				QPoint& p = points[j];
				p.setX(b + fwidthStep * j);
				p.setY(pts[j + nDisplayBeginIndex].y);
			}
			painter->drawPolyline(points);
		}
	}
}

void ZoomChannelViewItem::Update(float begin, float duration)
{
	if (channelRenderDatas->empty()) return;
	m_DisplayBeginSecond = begin;
	m_DisplayDurationSecond = duration;
}