#ifndef CHANNELVIEWITEM_H
#define CHANNELVIEWITEM_H

#include <QGraphicsItem>
#include <qpainter.h>
#include "channelrenderdata.h"

class ChannelViewItem : public QGraphicsItem
{

public:

	ChannelViewItem(std::vector<ChannelRenderData *>* d, int id);

	~ChannelViewItem();

	QRectF boundingRect() const;

	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

	int GetID() const { return m_ID; }

	void SetBackGroundColor(const QColor& color) {	m_BackGroundColor = color; }

protected:

	std::vector<ChannelRenderData *>* channelRenderDatas;

	QColor m_BackGroundColor;

	QColor m_LineColor;

	int m_ID;

};

#endif // CHANNELVIEWITEM_H
