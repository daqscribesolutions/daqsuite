#include "ChannelInfoImportUI.h"
#include <qfiledialog.h>
#include <qprocess.h>
#include <qfile.h>
#include "DeviceLib.h"
using namespace DeviceLib;

ChannelInfoImportUI::ChannelInfoImportUI(QWidget *parent)
	: QWidget(parent)
	, model(this)
{
	ui.setupUi(this);
	ui.toolButton_2->hide();
	ui.tableView->setModel(&model);
	ui.tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	connect(ui.toolButton, SIGNAL(pressed()), this, SLOT(load()));
	connect(ui.selectallButton, SIGNAL(pressed()), this, SLOT(selectall()));
	connect(ui.unselectallButton, SIGNAL(pressed()), this, SLOT(unselectall()));
	connect(ui.importButton, SIGNAL(pressed()), this, SLOT(apply()));
}

ChannelInfoImportUI::~ChannelInfoImportUI()
{
}

void ChannelInfoImportUI::load()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Channel File (xls)"), "", tr("Files (*.xls)"));
	if (fileName != "")
	{
		QDir dir = QFileInfo(QCoreApplication::applicationFilePath()).dir();
		auto current = dir.absolutePath();
		QProcess process;
		QString command = "\"" + current + "/ExcelImport.exe\" \"" + fileName + "\"";
		process.start(command);
		process.waitForFinished();
		QFile file("C:\\ProgramData\\DaqScribe\\channel.csv");
		if (file.open(QIODevice::ReadOnly))
		{
			QStringList wordList;
			int i = 0;
			model.tableData.clear();
			while (!file.atEnd())
			{
				QString line = file.readLine();
				wordList = line.split(',');
				if (i == 0)
				{
					model.tableHeader = wordList;
				}
				else
				{
					model.tableData.push_back(wordList);
				}
				++i;
			}
			ui.tableView->setModel(0);
			ui.tableView->setModel(&model);
		}
	}
}

void ChannelInfoImportUI::save()
{
}

void ChannelInfoImportUI::apply()
{
	const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
	int nChannels = 0;
	for (size_t i = 0; i < devices.size(); ++i)
	{
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[i].get());
		if (pDevice)
		{
			std::vector<ChannelInfo*> channels = pDevice->GetChannels();
			for (size_t j = 0; j < channels.size(); ++j)
			{
				if (nChannels >= model.tableData.size())
				{
					continue;
				}
				auto& data = model.tableData[nChannels];
				auto channel = channels[j];
				if (data.size() > 9 && data[0] == "1")
				{
					channel->Name = data[3].toStdString();
					channel->EUName = data[4].toStdString();
					channel->Description = data[9].toStdString();
				}
				++nChannels;
			}
		}
	}
	dataChanged();
}

void ChannelInfoImportUI::selectall()
{
	for (int i = 0; i < model.tableData.size(); ++i)
	{
		if (model.tableData[i].size() > 0) model.tableData[i][0] = "1";
	}
	model.dataChanged(QModelIndex(), QModelIndex());
}

void ChannelInfoImportUI::unselectall()
{
	for (int i = 0; i < model.tableData.size(); ++i)
	{
		if (model.tableData[i].size() > 0) model.tableData[i][0] = "0";
	}
	model.dataChanged(QModelIndex(), QModelIndex());
}
