#include "monitoringchannelitemdelegate.h"
#include <qcombobox.h>
#include <qwidget.h>
#include <qpainter.h>
#include "monitoringchannelmodel.h"

ColorListEditor::ColorListEditor(QWidget *widget) : QComboBox(widget)
{
	populateList();
}

QColor ColorListEditor::color() const
{
	return *(QColor*)(itemData(currentIndex(), Qt::DecorationRole)).data();
}

void ColorListEditor::setColor(QColor color)
{
	setCurrentIndex(findData(color, int(Qt::DecorationRole)));
}

void ColorListEditor::populateList()
{
	for (int i = 0; i < DisplayChannel::ChannelColors.size(); ++i)
	{
		insertItem(i, DisplayChannel::ChannelColors[i].name());
		setItemData(i, DisplayChannel::ChannelColors[i], Qt::DecorationRole);
	}
}

MonitoringChannelItemDelegate::MonitoringChannelItemDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

MonitoringChannelItemDelegate::~MonitoringChannelItemDelegate()
{
}

void MonitoringChannelItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QBrush brush = option.palette.highlight();
		brush.setColor(value);
		painter->fillRect(option.rect, brush);
	}
	else
	{
		QItemDelegate::paint(painter, option, index);
	}
}

QString MonitoringChannelItemDelegate::displayText(const QVariant & , const QLocale & locale) const
{
	Q_UNUSED(locale);
	return "";
}

QWidget* MonitoringChannelItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		ColorListEditor* comboBox = new ColorListEditor(parent);
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	if (index.column() == 3)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("VOLT");
		comboBox->addItem("EU");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void MonitoringChannelItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		ColorListEditor* comboBox = static_cast<ColorListEditor*>(editor);
		comboBox->setColor(value);
	}
	else if (index.column() == 3)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void MonitoringChannelItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		ColorListEditor* comboBox = static_cast<ColorListEditor*>(editor);
		model->setData(index, comboBox->color().name(), Qt::EditRole);
	}
	else if (index.column() == 3)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void MonitoringChannelItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}