#include "filenamemodel.h"
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

FileNameModel::FileNameModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

FileNameModel::~FileNameModel()
{
}

Qt::ItemFlags FileNameModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		auto items = FileNameManager::GetInstance()->GetNameItems();
		if (index.column() == 0 || items[index.row()].itemType == USER_DEFINED)
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int FileNameModel::rowCount(const QModelIndex &) const
{
	return FileNameManager::GetInstance()->GetNameItems().size();
}

int FileNameModel::columnCount(const QModelIndex &) const
{
	return 2;
}

QVariant FileNameModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		auto& items = FileNameManager::GetInstance()->GetNameItems();
		if (index.row() < (int)items.size())
		{
			if (index.column() == 0)
			{
				if (items[index.row()].itemType == NONE_ITEM)
				{
					return "None";
				}
				else if (items[index.row()].itemType == PLAN_NAME)
				{
					return "Plan Name";
				}
				else if (items[index.row()].itemType == DEVICE_NAME)
				{
					return "Device Name";
				}
				else if (items[index.row()].itemType == RECORD_NUMBER)
				{
					return "Record Number";
				}
				else if (items[index.row()].itemType == RECORD_TIME)
				{
					return "Record Time";
				}
				else if (items[index.row()].itemType == RECORD_DATE)
				{
					return "Record Date";
				}
				else
				{
					return "User Defined";
				}
			}
			else if (index.column() == 1)
			{
				return items[index.row()].GetItem().c_str();
			}
		}
	}

	return QVariant();
}

bool FileNameModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		auto& items = FileNameManager::GetInstance()->GetNameItems();
		if (index.row() < (int)items.size())
		{
			if (index.column() == 0)
			{
				if (value.toString() == "None")
				{
					items[index.row()].itemType = NONE_ITEM;
				}
				else if (value.toString() == "Plan Name")
				{
					items[index.row()].itemType = PLAN_NAME;
				}
				else if (value.toString() == "Device Name")
				{
					items[index.row()].itemType = DEVICE_NAME;
				}
				else if (value.toString() == "Record Number")
				{
					items[index.row()].itemType = RECORD_NUMBER;
				}
				else if (value.toString() == "Record Time")
				{
					items[index.row()].itemType = RECORD_TIME;
				}
				else if (value.toString() == "Record Date")
				{
					items[index.row()].itemType = RECORD_DATE;
				}
				else
				{
					items[index.row()].itemType = USER_DEFINED;
				}
			}
			else if (index.column() == 1)
			{
				if (items[index.row()].itemType = USER_DEFINED)
					items[index.row()].item = value.toString().toStdString();
			}
		}
		update();
		return true;
	}
	return false;
}

QVariant FileNameModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Value");
			}
		}
		if (orientation == Qt::Vertical)
		{
			return "Field " + QString::number(section + 1);
		}
	}

	return QVariant();
}

void FileNameModel::addItem(int idx)
{
	auto temps = FileNameManager::GetInstance()->GetNameItems();
	FileNameManager::GetInstance()->GetNameItems().clear();
	if (idx < 0)
	{
		FileNameManager::GetInstance()->GetNameItems().push_back(NameItem());
	}
	for (int i = 0; i < (int)temps.size(); ++i)
	{
		if (idx == i)
		{
			FileNameManager::GetInstance()->GetNameItems().push_back(NameItem());
		}
		FileNameManager::GetInstance()->GetNameItems().push_back(temps[i]);
	}
	if (idx == (int)temps.size())
	{
		FileNameManager::GetInstance()->GetNameItems().push_back(NameItem());
	}
	update();
}

void FileNameModel::removeItem(int idx)
{
	auto temps = FileNameManager::GetInstance()->GetNameItems();
	FileNameManager::GetInstance()->GetNameItems().clear();

	for (int i = 0; i < (int)temps.size(); ++i)
	{
		if (idx != i)
		{
			FileNameManager::GetInstance()->GetNameItems().push_back(temps[i]);
		}
	}
	update();
}

void FileNameModel::resetData()
{
	auto& items = FileNameManager::GetInstance()->GetNameItems();
	for (int i = 0; i < (int)items.size(); ++i)
	{
		items[i].itemType = NONE_ITEM;
	}
	update();
}
