#include "clientdevicemodel.h"

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

ClientDeviceModel::ClientDeviceModel(QObject *parent)
	: QAbstractTableModel(parent)
{

}

ClientDeviceModel::~ClientDeviceModel()
{

}

Qt::ItemFlags ClientDeviceModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (index.column() >= 0)
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int ClientDeviceModel::rowCount(const QModelIndex &) const
{
	return (int)SystemManager::GetInstance()->GetClientDevices().size();
}

int ClientDeviceModel::columnCount(const QModelIndex &) const
{
	return 4;
}

QVariant ClientDeviceModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		int nRow = index.row();
		const std::vector<std::shared_ptr<ClientDevice>>& devices = SystemManager::GetInstance()->GetClientDevices();
		if (nRow < (int)devices.size())
		{
			if (index.column() == 0)
			{
				return QString(devices[nRow]->GetDevice()->GetDeviceInfo());
			}
			else if (index.column() == 1)
			{
				return QString(devices[nRow]->GetHost().c_str());
			}
			else if (index.column() == 2)
			{
				return QString::number(devices[nRow]->GetPort());
			}
			else if (index.column() == 3)
			{
				return QString(devices[nRow]->IsConnected() ? "Connect" : "Disconnect");
			}
		}
	}

	return QVariant();
}

bool ClientDeviceModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		int nRow = index.row();
		const std::vector<std::shared_ptr<ClientDevice>>& devices = SystemManager::GetInstance()->GetClientDevices();
		if (nRow < (int)devices.size())
		{
			if (index.column() == 0)
			{
			}
			else if (index.column() == 1)
			{
				devices[nRow]->SetHost(value.toString().toStdString());
			}
			else if (index.column() == 2)
			{
				devices[nRow]->SetPort(value.toInt());
			}
			else if (index.column() == 3)
			{
				if (value.toString().toStdString() == "Connect")
				{
					devices[nRow]->Connect();
				}
				else
				{
					devices[nRow]->Disconnect();
				}
			}
			return true;
		}
	}
	return false;
}

QVariant ClientDeviceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Device Name");
			}
			else if (section == 1)
			{
				return QString("Host");
			}
			else if (section == 2)
			{
				return QString("Port");
			}
			else if (section == 3)
			{
				return QString("Connection");
			}
		}
	}

	return QVariant();
}
