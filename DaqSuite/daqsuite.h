#ifndef DAQSUITE_H
#define DAQSUITE_H

#include <qwidget.h>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/qdatawidgetmapper.h>
#include <qstandarditemmodel.h>
#include "ui_daqsuite.h"
#include "devicemodel.h"
#include "channelmodel.h"
#include "channelitemdelegate.h"
#include "monitoringchannelmodel.h"
#include "monitoringchannelitemdelegate.h"
#include "channelgraphicview.h"
#include "filechannelgraphicview.h"
#include "filechannelmodel.h"
#include "recordoptionmodel.h"
#include "recorditemdelegate.h"
#include "clientdevicemodel.h"
#include "devicemodeldelegate.h"
#include "clientdevicemodeldelegate.h"
#include "systemsettingmodel.h"
#include "testplanmodel.h"
#include "filenamemodeldelegate.h"
#include "filenamemodel.h"
#include "ChannelInfoImportUI.h"
#include <boost/property_tree/ptree.hpp>


class SystemRunner;

class DaqSuite : public QMainWindow
{

	Q_OBJECT

public:

	DaqSuite(QWidget *parent = 0);

	~DaqSuite();

	void Init();

public slots :

	void AddClient();

	void AddServer();

	void AddVirtualDevice();

	void RemoveClient();

	void RemoveServer();

	void Monitoring();

	void Recording();

	void Run();

	void GoFront();

	void GoBack();

	void GoFrontStep();

	void GoBackStep();

	void OpenRawFile();

	void UpdateSpeed();

	void CloseFile();

	void Connect();

	void ArmTrigger();

	void Disconnect();

	void NewProject();

	void LoadSetting();

	void SaveSetting();

	void LoadCalibration();

	void InsertNameItemBefore();

	void InsertNameItemAfter();

	void RemoveNameItem();

	void InsertChannelDataBefore();

	void InsertChannelDataAfter();

	void RemoveChannelDataItem();

	void Include();

	void Exclude();

	void IncludeView();

	void ExcludeView();

	void SetView(int val);

	void SetSnapview(int val);

	void SetMinMaxDuration();

	void LoadDisplaySetting();

	void SaveDisplaySetting();

	void UpdateMonitorChannelColumns();

	void UpdateMonitorSnapshotChannelColumns();

	void AddField();
	
	void RemoveField();

	void ReadUISetting(const boost::property_tree::ptree& pt);
	
	void WriteUISetting(boost::property_tree::ptree& pt);

	void ReadTestPlan(const boost::property_tree::ptree& pt);

	void WriteTestPlan(boost::property_tree::ptree& pt);

	void updateTrigger();
	
	void manualCheck();
	
	void scheduleCheck();

	void channelCheck();

	void scheduleTime();

	void channelIndex();

	void typeChanged();

	void levelChanged();

	void recordTimesChanged();

	void updateSampleFileName();

	void setSnapshot(bool bSnapshot);

	void updateChannels();

	void exportChannelInfo();

	void importChannelInfo();

	void finished();

	void closeEvent(QCloseEvent *event);

	void openimportui();

	void ConvertRawFile();

	void onMaximumPointsChanged();

	void onRefreshRateChanged();

private:

	void UpdateModel();

	void UpdateUI();

	QTimer* iTimer;

	QTimer* iviewerTimer;
	
	ChannelInfoImportUI* channelImportUi;

	Ui::DaqSuiteClass ui;

	DeviceModelDelegate deviceModelDelegate;

	DeviceModel deviceModel;

	ChannelModel channelModel;

	ClientDeviceModelDelegate clientModelDelegate;

	ClientDeviceModel clientModel;

	RecordOptionModel recordModel;

	RecordItemDelegate recordDelegate;

	ChannelItemDelegate channelDelegate;

	MonitoringChannelModel monitoringChannelModel;

	MonitoringChannelItemDelegate monitoringChannelDelegate;

	MonitoringChannelModel monitoringSnapshotChannelModel;

	MonitoringChannelItemDelegate monitoringSnapshotChannelDelegate;

	ChannelGraphicView* m_view;

	ChannelGraphicView* m_snapshotView;

	FileChannelGraphicView* m_FileView;

	FileChannelModel m_FileModel;

	MonitoringChannelItemDelegate fileChannelDelegate;

	SystemSettingModel m_SystemModel;

	TestPlanModel m_PlanModel;

	FileNameModel m_FileNameModel;

	FileNameModelDelegate m_FileNameModelDelegate;

	float m_currentViewerSecond;

	QMutex systemMutex;

	int nSnapshot;

	int nCurrentRecordNumber;

	std::shared_ptr<SystemRunner> m_thread;

};

#endif // DAQSUITE_H
