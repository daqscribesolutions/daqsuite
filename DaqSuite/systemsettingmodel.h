#ifndef SYSTEMSETTINGMODEL_H
#define SYSTEMSETTINGMODEL_H

#include <QAbstractTableModel>

class SystemSettingModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	SystemSettingModel(QObject *parent);

	~SystemSettingModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	
};

#endif // SYSTEMSETTINGMODEL_H
