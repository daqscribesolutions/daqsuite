#include "filenamemodeldelegate.h"
#include <qcombobox.h>
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

FileNameModelDelegate::FileNameModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

QWidget* FileNameModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 0)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("None");
		comboBox->addItem("Plan Name");
		comboBox->addItem("Device Name");
		comboBox->addItem("Record Number");
		comboBox->addItem("Record Time");
		comboBox->addItem("Record Date");
		comboBox->addItem("User Defined");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void FileNameModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 0)
	{
		QString value = index.model()->data(index, Qt::DisplayRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void FileNameModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 0)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void FileNameModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}