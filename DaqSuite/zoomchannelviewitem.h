#ifndef ZOOMCHANNELVIEWITEM_H
#define ZOOMCHANNELVIEWITEM_H

#include "channelviewitem.h"

class ZoomChannelViewItem : public ChannelViewItem
{

public:

	ZoomChannelViewItem(std::vector<ChannelRenderData *>* d, int id);

	~ZoomChannelViewItem();

	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

	void Update(float begin, float duration);

private:

	float m_DisplayBeginSecond;

	float m_DisplayDurationSecond;

};

#endif // ZOOMCHANNELVIEWITEM_H
