#include "channelviewitem.h"
#include "../DeviceLib/ChannelInfo.h"

ChannelViewItem::ChannelViewItem(std::vector<ChannelRenderData *>* d, int id)
	: channelRenderDatas(d)
	, m_ID(id)
	, m_BackGroundColor(Qt::darkGray)
	, m_LineColor(Qt::gray)
{
	setPos(0, 0);
}

ChannelViewItem::~ChannelViewItem()
{
}

QRectF ChannelViewItem::boundingRect() const
{
	if (channelRenderDatas->size() > 0)
	{ 
		ChannelRenderData* channelRenderData = channelRenderDatas->front();
		return QRectF(0, 0, channelRenderData->width, channelRenderData->height);
	}
	else
	{
		return QRectF(0, 0, 0, 0);
	}
}

void ChannelViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
	if (channelRenderDatas->empty()) return;

	ChannelRenderData* channelRenderData = channelRenderDatas->front();

	QPen pen;
	pen.setColor(m_LineColor);
	QBrush brush(m_BackGroundColor);
	painter->setPen(pen);
	painter->setBrush(brush);
	painter->drawRect(QRect(channelRenderData->YAxisWidth + 10, 10, channelRenderData->ChartAreaWidth, channelRenderData->ChartAreaHeight));
	painter->drawLines(channelRenderData->lines);

	std::vector<DisplayChannel>& channels = channelRenderData->model->GetChannels();
	double displayBeginSecond = channelRenderData->beginSecond;
	double displayDurationSecond = channelRenderData->displayDuration;

	if (channelRenderData->durationSecond < displayDurationSecond)
	{
		displayDurationSecond = channelRenderData->durationSecond;
	}
	double nUnit = 1.0;
	if (displayDurationSecond < 1000)
	{
		nUnit = 1000.0;
	}
	else if (displayDurationSecond < 1000000)
	{
		nUnit = 10.0;
	}
	int nDisplayNumPts = 0;
	double fwidthStep = 0;
	if (channels.size() > 0)
	{
		std::vector<PointData>& pts = channels.front().points;
		double width = 1;
		if (pts.size() > 0) width = pts.back().x - pts.front().x;
		nDisplayNumPts = static_cast<int>(floor(displayDurationSecond / channelRenderData->durationSecond * pts.size()));
		if (nDisplayNumPts <= 0)
		{
			return;
		}
		int nNumPts = nDisplayNumPts;
		if (nNumPts > channelRenderData->nXAxis)
		{
			nNumPts = channelRenderData->nXAxis;
		}
		if (nNumPts == 0)
		{
			nNumPts = channelRenderData->nXAxis;
		}

		int nStep = nDisplayNumPts / nNumPts;
		double val = displayBeginSecond < 0 ? displayBeginSecond : 0;
		double fStep = displayDurationSecond / nNumPts;
		fwidthStep = width / nNumPts;
		double b = pts.front().x;
		painter->drawText(QPoint(10, channelRenderData->height), QString::number(displayBeginSecond, 'f', 3) + " s");
		QString unit = nUnit == 1 ? " s" : nUnit == 1000 ? " m" : " M";
		for (int i = 0; i < nNumPts; ++i)
		{
			painter->drawText(QPoint(b + fwidthStep * i - 10, channelRenderData->height), QString::number(val * nUnit, 'f', 1) + unit);
			val += fStep;
		}

		nStep = channelRenderData->ChartAreaHeight / channelRenderData->nNumLevels;
		val = channelRenderData->MaxValue;
		fStep = (channelRenderData->MaxValue - channelRenderData->MinValue) / channelRenderData->nNumLevels;
		for (int i = 0; i < channelRenderData->nNumLevels; ++i)
		{
			painter->drawText(QPoint(channelRenderData->YAxisWidth - 35, nStep * i + 16), QString::number(val, 'f', 1));
			val -= fStep;
		}
		painter->drawText(QPoint(channelRenderData->YAxisWidth - 35, nStep * channelRenderData->nNumLevels + 16), QString::number(val, 'f', 1));
		fwidthStep = width / nDisplayNumPts;
	}
	int nCh = 1;
	bool bsnap = channelRenderData->model->IsSnapshot();
	for (int ci = 0; ci < (int)channels.size(); ++ci)
	{
		if (channels[ci].m_DisplayViewIndex == m_ID)
		{
			pen.setColor(channels[ci].m_Color);
			painter->setPen(pen);
			painter->drawText(QPoint(5, nCh * 20), bsnap ? channels[ci].m_Channel->pOrigin->Name.c_str() : channels[ci].m_Channel->Name.c_str());
			++nCh;
			QVector<QPoint>& points = channels[ci].renderPoints;
			std::vector<PointData>& pts = channels[ci].points;
			points.resize(nDisplayNumPts + 1);
			double b = pts.front().x;
			for (int j = 0; j < nDisplayNumPts; ++j)
			{
				QPoint& p = points[j];
				p.setX(b + fwidthStep * j);
				p.setY(pts[j].y);
			}
			QPoint& p = points[nDisplayNumPts];
			p.setX(b + fwidthStep * nDisplayNumPts);
			p.setY(pts[nDisplayNumPts - 1].y);
			painter->drawPolyline(points);
		}
	}
}