#ifndef CHANNELINFOMODEL_H
#define CHANNELINFOMODEL_H

#include <QAbstractTableModel>

class ChannelInfoModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	ChannelInfoModel(QObject *parent);

	~ChannelInfoModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	void UpdateChannels();

	QList<QStringList> tableData;

	QStringList tableHeader;

};

#endif // CHANNELINFOMODEL_H
