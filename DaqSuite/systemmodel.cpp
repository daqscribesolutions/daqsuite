#include "systemmodel.h"
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

SystemModel::SystemModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

SystemModel::~SystemModel()
{
}

Qt::ItemFlags SystemModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (index.column() == 0)
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int SystemModel::rowCount(const QModelIndex &) const
{
	return 13;
}

int SystemModel::columnCount(const QModelIndex &) const
{
	return 1;
}

QVariant SystemModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordName().c_str());
			}
			else if (index.row() == 1)
			{
				return (DeviceRecordSetting::GetInstance()->GetRecordNumber());
			}
			else if (index.row() == 2)
			{
				return (DeviceRecordSetting::GetInstance()->GetRecordStartTime() / 1000000000.0);
			}
			else if (index.row() == 3)
			{
				return (DeviceRecordSetting::GetInstance()->GetRecordDuration() / 1000000000.0);
			}
			else if (index.row() == 4)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordPath().c_str());
			}
			else if (index.row() == 5)
			{
				TriggerControlType triggertype = TriggerManager::GetInstance()->GetTriggerControlType();
				return QString(triggertype == TIME ? "Time" : triggertype == LIMIT ? "Limit" : triggertype == EDGE ? "Edge Limit" : "None");
			}
			else if (index.row() == 6)
			{
				return QString::number(TriggerManager::GetInstance()->GetLimit());
			}
			else if (index.row() == 7)
			{
				return QString::number(TriggerManager::GetInstance()->GetPreTrigger(), 'f', 3);
			}
			else if (index.row() == 8)
			{
				return QString::number(TriggerManager::GetInstance()->GetTriggerChannelIndex());
			}
			else if (index.row() == 9)
			{
				MonitorLimitType triggerlimittype = TriggerManager::GetInstance()->GetTriggerLimitType();
				return QString(triggerlimittype == UPPER ? "Upper" : "Lower");
			}
			else if (index.row() == 10)
			{
				MonitorDataType triggerdatatype = TriggerManager::GetInstance()->GetTriggerLimitValueType();
				return QString(triggerdatatype == EUVALUE ? "EU" : "Milivolt");
			}
			else if (index.row() == 11)
			{
				SYSTEM_STATUS status = SystemManager::GetInstance()->GetPostRecordStatus();
				return QString(status == STOPPED ? "Stop" : "Monitor");
			}
			else if (index.row() == 12)
			{
				RECORD_TYPE filetype = SystemManager::GetInstance()->GetRecordType();
				return QString(filetype == RAWFS ? "Rawfs" : filetype == LDSF_FILE ? "LDSF" : "Both");
			}
		}
	}

	return QVariant();
}

bool SystemModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				DeviceRecordSetting::GetInstance()->SetRecordName(value.toString().toStdString());
			}
			else if (index.row() == 1)
			{
				DeviceRecordSetting::GetInstance()->SetRecordNumber(value.toInt());
			}
			else if (index.row() == 2)
			{
				DeviceRecordSetting::GetInstance()->SetRecordStartTime((long long)(value.toDouble() * 1000000000));
			}
			else if (index.row() == 3)
			{
				DeviceRecordSetting::GetInstance()->SetRecordDuration((long long)(value.toDouble() * 1000000000));
			}
			else if (index.row() == 4)
			{
				DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString());
			}
			else if (index.row() == 5)
			{
				if (value.toString() == "Edge Limit")
				{
					TriggerManager::GetInstance()->SetTriggerControlType(EDGE);
				}
				else if (value.toString() == "Limit")
				{
					TriggerManager::GetInstance()->SetTriggerControlType(LIMIT);
				}
				else if (value.toString() == "Time")
				{
					TriggerManager::GetInstance()->SetTriggerControlType(TIME);
				}
				else if (value.toString() == "None")
				{
					TriggerManager::GetInstance()->SetTriggerControlType(NONE);
				}
			}
			else if (index.row() == 6)
			{
				TriggerManager::GetInstance()->SetLimit(value.toDouble());
			}
			else if (index.row() == 7)
			{
				TriggerManager::GetInstance()->SetPreTrigger(value.toDouble());
			}
			else if (index.row() == 8)
			{
				TriggerManager::GetInstance()->SetTriggerChannelIndex(value.toInt());
			}
			else if (index.row() == 9)
			{
				if (value.toString() == "Upper")
				{
					TriggerManager::GetInstance()->SetTriggerLimitType(UPPER);
				}
				else
				{
					TriggerManager::GetInstance()->SetTriggerLimitType(LOWER);
				}
			}
			else if (index.row() == 10)
			{
				if (value.toString() == "Milivolt")
				{
					TriggerManager::GetInstance()->SetTriggerLimitValueType(MILLIVOLT);
				}
				else
				{
					TriggerManager::GetInstance()->SetTriggerLimitValueType(EUVALUE);
				}
			}
			else if (index.row() == 11)
			{
				if (value.toString() == "Stop")
				{
					SystemManager::GetInstance()->SetPostRecordStatus(STOPPED);
				}
				else
				{
					SystemManager::GetInstance()->SetPostRecordStatus(MONITORING);
				}
			}
			else if (index.row() == 12)
			{
				if (value.toString() == "Rawfs")
				{
					SystemManager::GetInstance()->SetRecordType(RAWFS);
				}
				else if (value.toString() == "LDSF")
				{
					SystemManager::GetInstance()->SetRecordType(LDSF_FILE);
				}
				else if (value.toString() == "Both")
				{
					SystemManager::GetInstance()->SetRecordType(RAWFS_LDSF);
				}
			}
		}
		return true;
	}
	return false;
}

QVariant SystemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Record Infomation");
			}
		}
		if (orientation == Qt::Vertical)
		{
			if (section == 0)
			{
				return QString("Record Name");
			}
			else if (section == 1)
			{
				return QString("Run Number");
			}
			else if (section == 2)
			{
				return QString("Start Time (Second)");
			}
			else if (section == 3)
			{
				return QString("Duration (Second)");
			}
			else if (section == 4)
			{
				return QString("Path");
			}
			else if (section == 5)
			{
				return QString("Trigger Type");
			}
			else if (section == 6)
			{
				return QString("Limit");
			}
			else if (section == 7)
			{
				return QString("Pre Trigger Time (Second)");
			}
			else if (section == 8)
			{
				return QString("Trigger Channel Index");
			}
			else if (section == 9)
			{
				return QString("Trigger Limit Type");
			}
			else if (section == 10)
			{
				return QString("Trigger Limit Value Type");
			}
			else if (section == 11)
			{
				return QString("Post Record Status");
			}
			else if (section == 12)
			{
				return QString("File type");
			}
		}
	}

	return QVariant();
}
