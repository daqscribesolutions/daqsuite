#include "channelitemdelegate.h"
#include <qcombobox.h>

ChannelItemDelegate::ChannelItemDelegate(QObject *parent)
: QItemDelegate(parent)
{
}

QWidget* ChannelItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Active");
		comboBox->addItem("Inactive");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void ChannelItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void ChannelItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void ChannelItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}
