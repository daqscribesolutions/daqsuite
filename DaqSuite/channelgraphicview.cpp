#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

#include "channelgraphicview.h"
#include "channelrenderdata.h"
#include <QMouseEvent>
#include <QTimer>

ChannelGraphicView::ChannelGraphicView(QWidget *parent, MonitoringChannelModel *m)
	: QGraphicsView(parent)
	, model(m)
	, m_Scene(this)
	, selectedItemIdx(0)
{
	setScene(&m_Scene);
}

ChannelGraphicView::~ChannelGraphicView()
{
}

void ChannelGraphicView::focusOutEvent(QFocusEvent * )
{
	if (selectedItemIdx >= 0 && selectedItemIdx < (int)glViewers.size())
	{
		glViewers[selectedItemIdx]->SetBackGroundColor(Qt::darkGray);
	}
}

void ChannelGraphicView::paintEvent(QPaintEvent *ev)
{
	QPainter p(viewport());
	p.setRenderHints(renderHints());
	m_Scene.render(&p, viewport()->rect());
	QGraphicsView::paintEvent(ev);
}

void ChannelGraphicView::reset()
{
	m_mutex.lock();
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->Reset();
	}
	m_mutex.unlock();
	render();
}

void  ChannelGraphicView::mouseReleaseEvent(QMouseEvent *event)
{
	m_mutex.lock();
	if (selectedItemIdx >= 0 && selectedItemIdx < (int)glViewers.size())
	{
		glViewers[selectedItemIdx]->SetBackGroundColor(Qt::darkGray);
	}
	ChannelViewItem * item = (ChannelViewItem*)itemAt(event->x(), event->y());
	if (item)
	{
		selectedItemIdx = item->GetID();
		glViewers[selectedItemIdx]->SetBackGroundColor(Qt::gray);
	}
	m_mutex.unlock();
	render();
}

void ChannelGraphicView::resizeEvent(QResizeEvent*)
{
	m_Scene.setSceneRect(viewport()->rect());
	int nViews = glViewers.size();
	int height = m_Scene.height() / std::min(8, nViews);
	int column = std::max(1, (int)ceil(nViews / 8.0));
	int width = m_Scene.width() / column;
	m_mutex.lock();
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->SetSize(width, height);
	}
	for (size_t i = 0; i < glViewers.size(); ++i)
	{
		glViewers[i]->setPos(width * (int)floor(i / 8.0), height * (i % 8));
	}
	m_mutex.unlock();
	updatePoint();
}

void ChannelGraphicView::init()
{
	m_mutex.lock();
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		SystemManager::GetInstance()->RemoveDataUpdateListener(channelRenderDatas[i]);
	}

	channelRenderDatas.clear();

	const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
	bool bsnapshot = model->IsSnapshot();
	for (size_t i = 0; i < devices.size(); ++i)
	{
		ChannelRenderData * channelRenderData = new ChannelRenderData(model, this, (int)i);
		channelRenderDatas.push_back(channelRenderData);
		if (!bsnapshot) SystemManager::GetInstance()->AddDataUpdateListener(devices[i], channelRenderData);
		else channelRenderData->StartListening();
	}
	viewers.clear();
	for (int i = 0; i < 50; ++i)
	{
		std::shared_ptr<ChannelViewItem> channelview(new ChannelViewItem(&channelRenderDatas, (int)viewers.size()));
		viewers.push_back(channelview);
	}
	m_mutex.unlock();

	setViewNumber(3);
}

void ChannelGraphicView::updatePoint()
{
	m_mutex.lock();

	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->UpdateEntryData(0);
	}
	m_mutex.unlock();

	render();
}

void ChannelGraphicView::render()
{
	for (size_t i = 0; i < glViewers.size(); ++i)
	{
		glViewers[i]->update();
	}
	m_Scene.update();
	update();
}

int ChannelGraphicView::getSelectedItemID()
{
	return selectedItemIdx;
}

void ChannelGraphicView::setViewNumber(int n)
{
	int nSize = (int)glViewers.size();

	m_mutex.lock();

	for (int i = 0; i < nSize; ++i)
	{
		m_Scene.removeItem(viewers[i].get());
	}
	glViewers.clear();
	for (int i = 0; i < n; ++i)
	{
		glViewers.push_back(viewers[i].get());
		m_Scene.addItem(viewers[i].get());
	}
	m_mutex.unlock();

	resizeEvent(0);
	render();
}

void ChannelGraphicView::setMaxValue(double val)
{
	m_mutex.lock();

	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->MaxValue = val;
	}
	m_mutex.unlock();

	resizeEvent(0);
	render();
}

void ChannelGraphicView::setMinValue(double val)
{
	m_mutex.lock();

	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->MinValue = val;
	}
	m_mutex.unlock();

	resizeEvent(0);
	render();
}

void ChannelGraphicView::setDuration(double val)
{
	m_mutex.lock();

	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->displayDuration = val;
	}
	m_mutex.unlock();

	resizeEvent(0);
	render();
}