#include "testplanmodel.h"
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

TestPlanModel::TestPlanModel(QObject *parent)
	: QAbstractTableModel(parent)
{
	resetData();
}

TestPlanModel::~TestPlanModel()
{
}

Qt::ItemFlags TestPlanModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int TestPlanModel::rowCount(const QModelIndex &) const
{
	return planData.size();
}

int TestPlanModel::columnCount(const QModelIndex &) const
{
	return 2;
}

QVariant TestPlanModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.row() < (int)planData.size())
		{
			if (index.column() == 0)
			{
				return QString(planData[index.row()].name.c_str());
			}
			if (index.column() == 1)
			{
				return QString(planData[index.row()].data.c_str());
			}
		}
	}

	return QVariant();
}

bool TestPlanModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.row() < (int)planData.size())
		{
			if (index.column() == 0)
			{
				planData[index.row()].name = value.toString().toStdString();
			}
			if (index.column() == 1)
			{
				planData[index.row()].data = value.toString().toStdString();
			}
		}
		return true;
	}
	return false;
}

QVariant TestPlanModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Value");
			}
		}
		if (orientation == Qt::Vertical)
		{
			return "Field " + QString::number(section + 1);
		}
	}

	return QVariant();
}

void TestPlanModel::addItem()
{
	planData.push_back(Field());
	dataChanged(QModelIndex(), QModelIndex());
}

void TestPlanModel::removeItem()
{
	planData.pop_back();
	dataChanged(QModelIndex(), QModelIndex());
}

void TestPlanModel::resetData()
{
	planData.clear();
	planData.resize(5);
	planData[0].name = "Test Date";
	planData[1].name = "Test Object";
	planData[2].name = "Test Place";
}
