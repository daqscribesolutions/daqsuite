#ifndef SYSTEMMODEL_H
#define SYSTEMMODEL_H

#include <QAbstractTableModel>

class SystemModel : public QAbstractTableModel
{

	Q_OBJECT

public:

	SystemModel(QObject *parent);

	~SystemModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

};


#endif // SYSTEMMODEL_H
