#ifndef FILECHANNELGRAPHICVIEW_H
#define FILECHANNELGRAPHICVIEW_H

#include "channelgraphicview.h"
#include "zoomchannelviewitem.h"

class FileChannelGraphicView : public ChannelGraphicView
{
	Q_OBJECT

public:

	FileChannelGraphicView(QWidget *parent, MonitoringChannelModel * m);

	~FileChannelGraphicView();

	void init();
	
	float GetBeginTime();
	
	float GetDuration();

	void setDuration(double val);

protected:

	void setMaxValue(double val);

	void setMinValue(double val);

	void resizeEvent(QResizeEvent* ev);

public slots:

	void updateZoom(float begin, float duration);
	
	void render();

private:

	ChannelRenderData * channelRenderData;

	ZoomChannelViewItem * zoomchannelview;

	int height;

};

#endif // FILECHANNELGRAPHICVIEW_H
