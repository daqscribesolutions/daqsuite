#ifndef CLIENTDEVICEMODEL_H
#define CLIENTDEVICEMODEL_H

#include <QAbstractTableModel>

class ClientDeviceModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	ClientDeviceModel(QObject *parent);

	~ClientDeviceModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	
};

#endif // CLIENTDEVICEMODEL_H
