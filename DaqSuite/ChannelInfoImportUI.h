#ifndef CHANNELINFOIMPORTUI_H
#define CHANNELINFOIMPORTUI_H

#include <QWidget>
#include "ui_ChannelInfoImportUI.h"
#include "channelinfomodel.h"

class ChannelInfoImportUI : public QWidget
{
	Q_OBJECT

public:
	ChannelInfoImportUI(QWidget *parent = 0);
	~ChannelInfoImportUI();
public slots:
	void load();
	void save();
	void apply();
	void selectall();
	void unselectall();
signals:
	void dataChanged();
private:
	Ui::ChannelInfoImportUI ui;
	ChannelInfoModel model;
};

#endif // CHANNELINFOIMPORTUI_H
