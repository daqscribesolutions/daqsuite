#ifndef MONITORINGCHANNELITEMDELEGATE_H
#define MONITORINGCHANNELITEMDELEGATE_H

#include <QItemDelegate>
#include <qcombobox.h>

class ColorListEditor : public QComboBox
{
	Q_OBJECT
		Q_PROPERTY(QColor color READ color WRITE setColor USER true)

public:

	ColorListEditor(QWidget *widget = 0);

	QColor color() const;

	void setColor(QColor c);

private:

	void populateList();

};

class MonitoringChannelItemDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	MonitoringChannelItemDelegate(QObject *parent);

	~MonitoringChannelItemDelegate();

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual QString displayText(const QVariant & value, const QLocale & locale) const;

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

};

#endif // MONITORINGCHANNELITEMDELEGATE_H
