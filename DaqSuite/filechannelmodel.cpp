#include "filechannelmodel.h"

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

FileChannelModel::FileChannelModel(QObject *parent)
	: MonitoringChannelModel(parent, false)
	, pDevice(0)
{
	pDevice = new VirtualDevice();
}

FileChannelModel::~FileChannelModel()
{
	if (pDevice) delete pDevice;
}

void FileChannelModel::UpdataChannels(const char* filename)
{
	if (pDevice)
	{
		pDevice->ReadData(filename);

		std::vector<ChannelInfo*> channels = pDevice->GetChannels();
		if (channels.size() != m_Channels.size())
			m_Channels.resize(channels.size());

		m_Channels.resize(channels.size());
		for (size_t j = 0; j < channels.size(); ++j)
		{
			DisplayChannel& channel = m_Channels[j];
			channel.m_DeviceID = 1;
			channel.m_Channel = channels[j];
			channel.m_Color = DisplayChannel::ChannelColors[j % DisplayChannel::ChannelColors.size()];
		}
		m_nDevices = 1;
		emit render();
	}
}

DeviceLib::DeviceData * FileChannelModel::GetDeviceData()
{
	if (pDevice)
	{
		return pDevice->GetDeviceData().front();
	}
	return 0;
}