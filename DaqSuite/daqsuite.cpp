#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;
#include "../GSBoardLib/GSBoardFactory.h"
using namespace GenralStandard;
#include "../GSBoardLib/BlackburnBoardFactory.h"
using namespace Blackburn;
#include "../GSBoardLib/ICSBoardFactory.h"
using namespace ICS;

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>
#include "daqsuite.h"
#include <QNetworkInterface>
#include <qmessagebox.h>
#include <qtimer.h>
#include <qfiledialog.h>
#include <qthread.h>
#include <QCloseEvent>
#include "channelitemdelegate.h"
#include "../DeviceLib/StorageManager.h"
std::string settingpath = "C:\\ProgramData\\DaqScribe";

#ifdef WIN32
#pragma warning(disable : 4996)
#endif

const QString title = "DaqHunter 1.0 - DDR";

DeviceLib::FileLog flog;

class SystemRunner : public QThread
{
public:

	SystemRunner() {}

	virtual void run()
	{
		DeviceLib::SystemManager::GetInstance()->StartMonitor();
	}
};

DaqSuite::DaqSuite(QWidget *parent)
	: QMainWindow(parent)
	, channelModel(parent)
	, channelDelegate(parent)
	, deviceModelDelegate(parent)
	, deviceModel(parent)
	, clientModelDelegate(parent)
	, clientModel(parent)
	, recordModel(parent)
	, recordDelegate(parent)
	, monitoringChannelModel(parent, false)
	, monitoringChannelDelegate(parent)
	, monitoringSnapshotChannelModel(parent, true)
	, monitoringSnapshotChannelDelegate(parent)
	, fileChannelDelegate(parent)
	, m_view(0)
	, m_snapshotView(0)
	, m_FileView(0)
	, m_FileModel(parent)
	, m_currentViewerSecond(0)
	, m_SystemModel(parent)
	, m_PlanModel(parent)
	, m_FileNameModel(parent)
	, m_FileNameModelDelegate(parent)
	, nSnapshot(0)
	, m_thread(new SystemRunner())
	, channelImportUi(0)
{
	DeviceLib::StorageManager::CreatePath(settingpath);
	flog.SetFile(settingpath + "\\log.txt");
	DeviceLib::Logger::GetInstance()->AddLog(&flog);
	GSBoardFactory gsfactory;
	SystemManager::GetInstance()->AddDevices(gsfactory);
	BlackburnBoardFactory bbfactory;
	SystemManager::GetInstance()->AddDevices(bbfactory);
	ICSBoardFactory icsfactory;
	SystemManager::GetInstance()->AddDevices(icsfactory);
	boost::property_tree::ptree pt;
	try
	{
		read_xml(settingpath + "\\DaqSuite.setting", pt);
		ReadTestPlan(pt);
		SystemManager::GetInstance()->ReadSystemInfo(pt);
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	Init();
	ReadUISetting(pt);
	int nviews = m_view->getViewNumber();
	monitoringChannelModel.ReadChannelSetting(pt, nviews);
	m_view->setViewNumber(nviews);
	ui.viewTableNumberSpinBox->setValue(nviews);
	monitoringSnapshotChannelModel.ReadChannelSetting(pt, nviews);
	m_snapshotView->setViewNumber(nviews);
	ui.viewTableNumberSpinBox_2->setValue(nviews);
	channelImportUi = new ChannelInfoImportUI();
	connect(channelImportUi, SIGNAL(dataChanged()), this, SLOT(UpdateModel()));
}

DaqSuite::~DaqSuite()
{
	boost::property_tree::ptree pt;
	WriteTestPlan(pt);
	SystemManager::GetInstance()->WriteSystemInfo(pt);
	WriteUISetting(pt);
	monitoringChannelModel.SaveChannelSetting(pt, m_view->getViewNumber());
	monitoringSnapshotChannelModel.SaveChannelSetting(pt, m_snapshotView->getViewNumber());
	boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
	write_xml(settingpath + "\\DaqSuite.setting", pt, std::locale(), settings);
	SystemManager::GetInstance()->Clear();
	delete m_view;
	delete m_snapshotView;
	delete iTimer;
	delete iviewerTimer;
	delete channelImportUi;
}

void DaqSuite::Init()
{
	ui.setupUi(this);
	UpdateMonitorChannelColumns();
	UpdateMonitorSnapshotChannelColumns();

	channelModel.UpdateChannels();
	monitoringChannelModel.UpdateChannels();
	monitoringSnapshotChannelModel.UpdateChannels();
	ui.planTableView->setModel(&m_PlanModel);
	ui.planTableView->addAction(ui.actionLoadSetting);
	ui.planTableView->addAction(ui.actionSaveSetting);
	ui.planTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	m_view = new ChannelGraphicView(this, &monitoringChannelModel);
	m_view->init();
	monitoringChannelModel.SetView(m_view);

	m_snapshotView = new ChannelGraphicView(this, &monitoringSnapshotChannelModel);
	m_snapshotView->init();
	monitoringSnapshotChannelModel.SetView(m_snapshotView);
	ui.acquisitionChannelInMonitorView_2->setModel(&monitoringSnapshotChannelModel);
	ui.snapshotToolButton->setChecked(TriggerManager::GetInstance()->GetSnapshot());
	connect(ui.snapshotToolButton, SIGNAL(toggled(bool)), this, SLOT(setSnapshot(bool)));

	//m_FileView = new FileChannelGraphicView(this, &m_FileModel);
	//m_FileView->init();
	//m_FileModel.SetView(m_FileView);
	//ui.analysisFrame->layout()->addWidget(m_FileView);

	ui.recordTableView->setModel(&recordModel);
	ui.recordTableView->setItemDelegate(&recordDelegate);
	ui.recordTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	//ui.systemSettingTableView->setModel(&m_SystemModel);
	//ui.systemSettingTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.channelTableView->setModel(&channelModel);
	ui.channelTableView->setItemDelegate(&channelDelegate);
	ui.channelTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui.channelTableView->addAction(ui.actionInsertDataAfter);
	ui.channelTableView->addAction(ui.actionInsertDataBefore);
	ui.channelTableView->addAction(ui.actionRemoveData);
	ui.channelTableView->addAction(ui.actionExportChannelInfo);
	ui.channelTableView->addAction(ui.actionImportChannelInfo);

	ui.acquisitionChannelInMonitorView->setModel(&monitoringChannelModel);
	ui.acquisitionChannelInMonitorView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui.acquisitionChannelInMonitorView->setItemDelegate(&monitoringChannelDelegate);
	//ui.acquisitionChannelInMonitorView->addAction(ui.actionInclude);
	//ui.acquisitionChannelInMonitorView->addAction(ui.actionExclude);
	ui.acquisitionChannelInMonitorView->addAction(ui.actionLoad_display_setting);
	ui.acquisitionChannelInMonitorView->addAction(ui.actionSave_display_setting);

	ui.acquisitionChannelInMonitorView_2->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui.acquisitionChannelInMonitorView_2->setItemDelegate(&monitoringSnapshotChannelDelegate);
	//ui.acquisitionChannelInMonitorView_2->addAction(ui.actionInclude);
	//ui.acquisitionChannelInMonitorView_2->addAction(ui.actionExclude);
	ui.acquisitionChannelInMonitorView_2->addAction(ui.actionLoad_display_setting);
	ui.acquisitionChannelInMonitorView_2->addAction(ui.actionSave_display_setting);

	ui.scopeFrame->layout()->addWidget(m_view);
	ui.snapshotFrame->layout()->addWidget(m_snapshotView);

	ui.tableView->setModel(&deviceModel);
	ui.tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui.tableView->addAction(ui.actionAddClient);
	ui.tableView->addAction(ui.actionAddServer);
	ui.tableView->addAction(ui.actionRemoveDevice);
	ui.tableView->addAction(ui.actionAddVirtualDevice);
	ui.tableView->addAction(ui.actionLoadcalibration);
	ui.tableView->setItemDelegate(&deviceModelDelegate);
	ui.tableView->addAction(ui.actionConvertRawFile);
	ui.clientTableView->setModel(&clientModel);
	ui.clientTableView->setItemDelegate(&clientModelDelegate);
	ui.clientTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.clientTableView->addAction(ui.actionRemoveClient);
	//ui.analysisChannelInMonitorView->setItemDelegate(&monitoringChannelDelegate);
	//ui.analysisChannelInMonitorView->addAction(ui.actionIncludeView);
	//ui.analysisChannelInMonitorView->addAction(ui.actionExcludeView);
	//ui.analysisChannelInMonitorView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	ui.fileNameTableView->setModel(&m_FileNameModel);
	ui.fileNameTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.fileNameTableView->setItemDelegate(&m_FileNameModelDelegate);
	ui.fileNameTableView->addAction(ui.actionInsertNameItem);
	ui.fileNameTableView->addAction(ui.actionAddNameItem);
	ui.fileNameTableView->addAction(ui.actionDeleteNameItem);

	iTimer = new QTimer(this);
	iviewerTimer = new QTimer(this);

	ui.viewTableNumberSpinBox->setValue(m_view->getViewNumber());
	ui.minSpinBox->setValue(-5000);
	ui.maxSpinBox->setValue(5000);
	ui.viewTableNumberSpinBox_2->setValue(m_snapshotView->getViewNumber());
	ui.minSpinBox_2->setValue(-5000);
	ui.maxSpinBox_2->setValue(5000);

	connect(&deviceModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), ui.recordTableView, SLOT(update()));
	connect(m_thread.get(), SIGNAL(finished()), this, SLOT(finished()));

	connect(ui.groupViewSpinBox, SIGNAL(valueChanged(int)), this, SLOT(SetView(int)));
	connect(ui.groupViewSpinBox_2, SIGNAL(valueChanged(int)), this, SLOT(SetSnapview(int)));
	//connect(ui.analysisMinValueSpinBox, SIGNAL(valueChanged(double)), m_FileView, SLOT(setMinValue(double)));
	//connect(ui.analysisMaxValueSpinBox, SIGNAL(valueChanged(double)), m_FileView, SLOT(setMaxValue(double)));
	connect(ui.monitorToolButton, SIGNAL(released()), this, SLOT(Monitoring()));
	connect(ui.startToolButton, SIGNAL(released()), this, SLOT(Recording()));
	//connect(ui.actionArm, SIGNAL(triggered()), this, SLOT(ArmTrigger()));
	connect(iTimer, SIGNAL(timeout()), this, SLOT(UpdateSpeed()));
	connect(iviewerTimer, SIGNAL(timeout()), this, SLOT(GoFrontStep()));
	connect(ui.actionForward, SIGNAL(triggered()), this, SLOT(GoFrontStep()));
	connect(ui.actionBackward, SIGNAL(triggered()), this, SLOT(GoBackStep()));
	connect(ui.actionGotoBegin, SIGNAL(triggered()), this, SLOT(GoFront()));
	connect(ui.actionGotoEnd, SIGNAL(triggered()), this, SLOT(GoBack()));
	connect(ui.actionPlay, SIGNAL(triggered()), this, SLOT(Run()));
	connect(ui.actionLoadcalibration, SIGNAL(triggered()), this, SLOT(LoadCalibration()));
	connect(ui.actionNewProject, SIGNAL(triggered()), this, SLOT(NewProject()));
	//connect(ui.actionInclude, SIGNAL(triggered()), this, SLOT(Include()));
	//connect(ui.actionExclude, SIGNAL(triggered()), this, SLOT(Exclude()));
	//connect(ui.actionIncludeView, SIGNAL(triggered()), this, SLOT(IncludeView()));
	//connect(ui.actionExcludeView, SIGNAL(triggered()), this, SLOT(ExcludeView()));
	connect(&monitoringChannelModel, SIGNAL(render()), m_view, SLOT(render()));
	connect(&monitoringSnapshotChannelModel, SIGNAL(render()), m_snapshotView, SLOT(render()));

	connect(&channelModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), &monitoringChannelModel, SLOT(UpdateChannels()));
	connect(&channelModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), &monitoringSnapshotChannelModel, SLOT(UpdateChannels()));

	//connect(&m_FileModel, SIGNAL(render()), m_FileView, SLOT(render()));
	connect(ui.viewTableNumberSpinBox, SIGNAL(valueChanged(int)), m_view, SLOT(setViewNumber(int)));
	connect(ui.minSpinBox, SIGNAL(valueChanged(double)), m_view, SLOT(setMinValue(double)));
	connect(ui.maxSpinBox, SIGNAL(valueChanged(double)), m_view, SLOT(setMaxValue(double)));
	connect(ui.viewTableNumberSpinBox_2, SIGNAL(valueChanged(int)), m_snapshotView, SLOT(setViewNumber(int)));
	connect(ui.minSpinBox_2, SIGNAL(valueChanged(double)), m_snapshotView, SLOT(setMinValue(double)));
	connect(ui.maxSpinBox_2, SIGNAL(valueChanged(double)), m_snapshotView, SLOT(setMaxValue(double)));

	connect(ui.importChannelButton, SIGNAL(clicked(bool)), this, SLOT(openimportui()));
	connect(ui.actionLoad_Raw_File, SIGNAL(triggered()), this, SLOT(OpenRawFile()));
	connect(ui.actionCloseFile, SIGNAL(triggered()), this, SLOT(CloseFile()));
	connect(ui.actionAddClient, SIGNAL(triggered()), this, SLOT(AddClient()));
	connect(ui.actionAddServer, SIGNAL(triggered()), this, SLOT(AddServer()));
	connect(ui.actionRemoveClient, SIGNAL(triggered()), this, SLOT(RemoveClient()));
	connect(ui.actionRemoveDevice, SIGNAL(triggered()), this, SLOT(RemoveServer()));
	connect(ui.actionAddVirtualDevice, SIGNAL(triggered()), this, SLOT(AddVirtualDevice()));
	connect(ui.actionConnect, SIGNAL(triggered()), this, SLOT(Connect()));
	connect(ui.actionDisconnect, SIGNAL(triggered()), this, SLOT(Disconnect()));
	connect(ui.actionLoadSetting, SIGNAL(triggered()), this, SLOT(LoadSetting()));
	connect(ui.actionSaveSetting, SIGNAL(triggered()), this, SLOT(SaveSetting()));
	connect(ui.durationDoubleSpinBox, SIGNAL(valueChanged(double)), m_view, SLOT(setDuration(double)));
	connect(ui.durationDoubleSpinBox_2, SIGNAL(valueChanged(double)), m_snapshotView, SLOT(setDuration(double)));
	connect(ui.actionAddNameItem, SIGNAL(triggered()), this, SLOT(InsertNameItemAfter()));
	connect(ui.actionInsertNameItem, SIGNAL(triggered()), this, SLOT(InsertNameItemBefore()));
	connect(ui.actionDeleteNameItem, SIGNAL(triggered()), this, SLOT(RemoveNameItem()));
	connect(ui.actionInsertDataBefore, SIGNAL(triggered()), this, SLOT(InsertChannelDataBefore()));
	connect(ui.actionInsertDataAfter, SIGNAL(triggered()), this, SLOT(InsertChannelDataAfter()));
	connect(ui.actionRemoveData, SIGNAL(triggered()), this, SLOT(RemoveChannelDataItem()));
	connect(ui.actionExportChannelInfo, SIGNAL(triggered()), this, SLOT(exportChannelInfo()));
	connect(ui.actionImportChannelInfo, SIGNAL(triggered()), this, SLOT(importChannelInfo()));
	connect(ui.actionConvertRawFile, SIGNAL(triggered()), this, SLOT(ConvertRawFile()));

	connect(ui.actionLoad_display_setting, SIGNAL(triggered()), this, SLOT(LoadDisplaySetting()));
	connect(ui.actionSave_display_setting, SIGNAL(triggered()), this, SLOT(SaveDisplaySetting()));
	connect(ui.voltMaxCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.voltMinCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.voltAveCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.voltRMSCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.euMaxCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.euMinCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.euAveCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));
	connect(ui.euRMSCheckBox, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorChannelColumns()));

	connect(ui.voltMaxCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.voltMinCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.voltAveCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.voltRMSCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.euMaxCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.euMinCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.euAveCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));
	connect(ui.euRMSCheckBox_2, SIGNAL(stateChanged(int)), this, SLOT(UpdateMonitorSnapshotChannelColumns()));

	connect(ui.addFieldButton, SIGNAL(pressed()), this, SLOT(AddField()));
	connect(ui.removeFieldButton, SIGNAL(pressed()), this, SLOT(RemoveField()));

	connect(&deviceModel, SIGNAL(channelChanged()), this, SLOT(updateChannels()));
	auto mgr = TriggerManager::GetInstance();
	auto triggertype = mgr->GetTriggerControlType();
	ui.scheduleGroupBox->setChecked(triggertype == TIME);
	ui.manualGroupBox->setChecked(triggertype == NONE);
	if (triggertype == EDGE)
	{
		ui.channelGroupBox->setChecked(true);
		ui.edgeRisingRadioButton->setChecked(mgr->GetTriggerLimitType() == UPPER);
		ui.edgeFallingRadioButton->setChecked(mgr->GetTriggerLimitType() == LOWER);
	}
	else if (triggertype == LIMIT)
	{
		ui.channelGroupBox->setChecked(true);
		ui.levelUpRadioButton->setChecked(mgr->GetTriggerLimitType() == UPPER);
		ui.levelLowRadioButton->setChecked(mgr->GetTriggerLimitType() == LOWER);
	}
	ui.valueTypeComboBox->addItem("mV");
	ui.valueTypeComboBox->addItem("EU");
	ui.valueTypeComboBox->setCurrentIndex(mgr->GetTriggerLimitValueType() == MILLIVOLT ? 0 : 1);
	ui.channelIndexSpinBox->setValue(mgr->GetTriggerChannelIndex() + 1);
	ui.levelSpinBox->setValue(mgr->GetLimit());
	ui.triggerNumberSpinBox->setValue(DeviceRecordSetting::GetInstance()->GetRecordTimes());
	auto t = mgr->GetTriggerScheduleTime();
	ui.scheduleTimeEdit->setTime(QTime(t.tm_hour - 1, t.tm_min, t.tm_sec));

	updateSampleFileName();
	connect(ui.manualGroupBox, SIGNAL(clicked()), this, SLOT(manualCheck()));
	connect(ui.scheduleGroupBox, SIGNAL(clicked()), this, SLOT(scheduleCheck()));
	connect(ui.channelGroupBox, SIGNAL(clicked()), this, SLOT(channelCheck()));
	connect(ui.scheduleTimeEdit, SIGNAL(editingFinished()), this, SLOT(scheduleTime()));
	connect(ui.channelIndexSpinBox, SIGNAL(valueChanged(int)), this, SLOT(channelIndex()));
	connect(ui.valueTypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged()));
	connect(ui.levelSpinBox, SIGNAL(valueChanged(double)), this, SLOT(levelChanged()));
	connect(ui.edgeFallingRadioButton, SIGNAL(clicked()), this, SLOT(updateTrigger()));
	connect(ui.edgeRisingRadioButton, SIGNAL(clicked()), this, SLOT(updateTrigger()));
	connect(ui.levelLowRadioButton, SIGNAL(clicked()), this, SLOT(updateTrigger()));
	connect(ui.levelUpRadioButton, SIGNAL(clicked()), this, SLOT(updateTrigger()));
	connect(ui.triggerNumberSpinBox, SIGNAL(valueChanged(int)), this, SLOT(recordTimesChanged()));
	connect(&m_FileNameModel, SIGNAL(update()), this, SLOT(updateSampleFileName()));

	connect(ui.maxPointSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onMaximumPointsChanged()));
	connect(ui.refreshRateSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onRefreshRateChanged()));
	UpdateUI();
}

void DaqSuite::AddClient()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		std::string host = "192.168.1.100";
		QModelIndexList indexes = ui.tableView->selectionModel()->selection().indexes();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.at(0);
			const std::shared_ptr<IDevice>& pdevice = SystemManager::GetInstance()->GetDevices().at(index.row());
			if (pdevice)
			{
				SystemManager::GetInstance()->AddClientDevice(host, pdevice, 4000);
				ui.clientTableView->setModel(0);
				ui.clientTableView->setModel(&clientModel);
				ui.statusBar->showMessage("Client is added");
			}
		}
		else
		{
			QMessageBox msgBox(this);
			msgBox.setWindowTitle("Warning!");
			msgBox.setText("No device is selected");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::No);
			msgBox.exec();
		}
	}
}

void DaqSuite::AddServer()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		SystemManager::GetInstance()->AddServerDevice(4000);
		UpdateModel();
		m_view->init();
		m_snapshotView->init();
	}
}

void DaqSuite::AddVirtualDevice()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		SystemManager::GetInstance()->AddVirtualDevice();
		UpdateModel();
		m_view->init();
		m_snapshotView->init();
	}
}

void DaqSuite::RemoveClient()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		QModelIndexList indexes = ui.clientTableView->selectionModel()->selection().indexes();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.at(0);
			std::shared_ptr<ClientDevice> pdevice = SystemManager::GetInstance()->GetClientDevices().at(index.row());
			if (pdevice)
			{
				SystemManager::GetInstance()->RemoveClientDevice(pdevice);
				ui.clientTableView->setModel(0);
				ui.clientTableView->setModel(&clientModel);
				ui.clientTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
				ui.statusBar->showMessage("Client is removed");
			}
		}
		else
		{
			QMessageBox msgBox(this);
			msgBox.setWindowTitle("Warning!");
			msgBox.setText("No device is selected");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::No);
			msgBox.exec();
		}
	}
}

void DaqSuite::RemoveServer()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		QModelIndexList indexes = ui.tableView->selectionModel()->selection().indexes();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.at(0);
			std::shared_ptr<IDevice> pdevice = SystemManager::GetInstance()->GetDevices().at(index.row());
			if (pdevice)
			{
				SystemManager::GetInstance()->RemoveDevice(pdevice);
				UpdateModel();
				m_view->init();
				m_snapshotView->init();
				ui.statusBar->showMessage("Server device is removed");
			}
		}
		else
		{
			QMessageBox msgBox(this);
			msgBox.setWindowTitle("Warning!");
			msgBox.setText("No device is selected");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::No);
			msgBox.exec();
		}
	}
}

void DaqSuite::Monitoring()
{
	if (m_thread->isRunning()) return;
	m_view->setFocus();
	//systemMutex.lock();
	ui.statusLabel->setText("Wait");
	ui.statusLabel->update();
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (SystemManager::GetInstance()->RecordExist())
		{
			QMessageBox msgBox(this);
			msgBox.setWindowTitle("Warning!");
			msgBox.setText("The record files exist. Do you want to overwrite?");
			msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
			msgBox.setDefaultButton(QMessageBox::No);
			if (QMessageBox::Ok != msgBox.exec())
			{
				ui.statusBar->showMessage("Monitoring failed");
				ui.actionMonitoring->toggle();
				ui.statusLabel->setText("Stopped");
				//systemMutex.unlock();
				return;
			}
		}
		m_view->reset();
		m_snapshotView->reset();
		m_thread->start();
		iTimer->start(1000 / ChannelRenderData::REFRESH_RATE);
	}
	else
	{
		if (SystemManager::GetInstance()->GetStatus() == RECORDING)
		{
			SystemManager::GetInstance()->StopRecord();
		}
		SystemManager::GetInstance()->StopMonitor();
		ui.monitorToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/utilities-system-monitor.png")));
		ui.monitorToolButton->setIconSize(QSize(30, 30));
		ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-playback-start.png")));
		ui.startToolButton->setIconSize(QSize(30, 30));
		iTimer->stop();
		ui.statusBar->showMessage("Stopping monitoring ...", 1000);
		ui.statusLabel->setText("Stopped");
		ui.recordspeedlabel->setText("Record Speed 0.00 (M/S)");
		ui.monitorspeedlabel->setText("Monitor Speed 0.00 (M/S)");
		ui.progressBar->setValue(0);
		nSnapshot = 0;
		int ncurrent = DeviceRecordSetting::GetInstance()->GetRecordNumber();
		for (int i = nCurrentRecordNumber; i < ncurrent; ++i)
		{
			DeviceRecordSetting::GetInstance()->SetRecordNumber(i);
			channelModel.SaveDSI();
		}
		DeviceRecordSetting::GetInstance()->SetRecordNumber(ncurrent);
	}
	//systemMutex.unlock();
	UpdateUI();
}

void DaqSuite::finished()
{
	if (SystemManager::GetInstance()->GetStatus() == MONITORING)
	{
		SetMinMaxDuration();
		ui.statusBar->showMessage("Monitoring ...");
		ui.monitorToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-playback-stop.png")));
		ui.monitorToolButton->setIconSize(QSize(30, 30));
		ui.statusLabel->setText("Monitoring");
		nCurrentRecordNumber = DeviceRecordSetting::GetInstance()->GetRecordNumber();
	}
	else
	{
		ui.statusBar->showMessage("Monitoring failed");
		ui.actionMonitoring->toggle();
		ui.statusLabel->setText("Stopped");
		ui.recordspeedlabel->setText("Record Speed 0.00 (M/S)");
		ui.monitorspeedlabel->setText("Monitor Speed 0.00 (M/S)");
		ui.progressBar->setValue(0);
		iTimer->stop();
	}
}

void DaqSuite::Recording()
{
	if (m_thread->isRunning()) return;

	//systemMutex.lock();
	ui.statusLabel->setText("Wait");
	ui.statusLabel->update();
	if (SystemManager::GetInstance()->GetStatus() == MONITORING)
	{
		if (TriggerManager::GetInstance()->GetTriggerControlType() == NONE)
		{
			if (SystemManager::GetInstance()->StartRecord())
			{
				ui.startToolButton->setEnabled(false);
				ui.statusBar->showMessage("Recording ...");
				ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-record.png")));
				ui.startToolButton->setIconSize(QSize(30, 30));
				ui.statusLabel->setText("Recording");
			}
			else
			{
				ui.statusBar->showMessage("Recording failed");
				ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-play.png")));
				ui.startToolButton->setIconSize(QSize(30, 30));
				ui.statusLabel->setText("Stopped");
			}
		}
		else
		{
			ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/edit-clear-history.png")));
			ui.startToolButton->setIconSize(QSize(30, 30));
			SystemManager::GetInstance()->ArmTrigger();
			ui.statusLabel->setText("Armed");
		}
	}
	else
	{
		if (SystemManager::GetInstance()->GetStatus() == RECORDING)
			SystemManager::GetInstance()->StopRecord();
		ui.statusBar->showMessage("Stopping recording ...", 1000);
		ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-play.png")));
		ui.startToolButton->setIconSize(QSize(30, 30));
		ui.statusLabel->setText("Stopped");
	}
	//systemMutex.unlock();
	UpdateUI();
}

void DaqSuite::Run()
{
	if (iviewerTimer->isActive())
	{
		iviewerTimer->stop();
		UpdateModel();
	}
	else
	{
		//if (m_FileView->GetDuration() > 0)
		iviewerTimer->start(100);
	}
	ui.actionPlay->setChecked(iviewerTimer->isActive());
}

void DaqSuite::GoFront()
{
	//float duration = ui.displayDurationSpinBox->value();
	//m_currentViewerSecond = 0;
	//m_FileView->updateZoom(m_currentViewerSecond, duration);
}

void DaqSuite::GoBack()
{
	//float duration = ui.displayDurationSpinBox->value();
	//float speed = ui.speedSpinBox->value();
	//m_currentViewerSecond = m_FileView->GetDuration() - speed;
	//m_FileView->updateZoom(m_currentViewerSecond, duration);
}

void DaqSuite::GoFrontStep()
{
	//float duration = ui.displayDurationSpinBox->value();
	//float speed = ui.speedSpinBox->value();
	//if (m_currentViewerSecond + speed + duration < m_FileView->GetBeginTime() + m_FileView->GetDuration())
	//{
	//	m_currentViewerSecond += speed;
	//	m_FileView->updateZoom(m_currentViewerSecond, duration);
	//}
	//else
	//{
	//	m_currentViewerSecond = m_FileView->GetBeginTime() + m_FileView->GetDuration() - duration;
	//	m_FileView->updateZoom(m_currentViewerSecond, duration);
	//	iviewerTimer->stop();
	//}
}

void DaqSuite::GoBackStep()
{
	//float duration = ui.displayDurationSpinBox->value();
	//float speed = ui.speedSpinBox->value();
	//if (m_currentViewerSecond > m_FileView->GetBeginTime())
	//{
	//	m_currentViewerSecond -= speed;
	//	m_FileView->updateZoom(m_currentViewerSecond, duration);
	//}
	//else
	//{
	//	m_currentViewerSecond = m_FileView->GetBeginTime();
	//	m_FileView->updateZoom(m_currentViewerSecond, duration);
	//}
}

void DaqSuite::OpenRawFile()
{
	std::string path = ::DeviceRecordSetting::GetInstance()->GetRecordPath();
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Raw File"), path.c_str(), tr("Files (*.rawfs)"));
	if (fileName != "")
	{
		m_FileModel.UpdataChannels(fileName.toStdString().c_str());
		//m_FileView->init();
		//m_FileView->setDuration(m_FileModel.GetDeviceData()->GetDurationNanoSecond() / 1000000000.0);
		//ui.analysisChannelInMonitorView->setModel(0);
		//ui.analysisChannelInMonitorView->setModel(&m_FileModel);
	}
}

void DaqSuite::CloseFile()
{
	m_FileModel.clearChannels();
	//m_FileView->update();
	//ui.analysisChannelInMonitorView->setModel(0);
	//ui.analysisChannelInMonitorView->setModel(&m_FileModel);
}

void DaqSuite::updateChannels()
{
	ui.channelTableView->setModel(0);
	channelModel.UpdateChannels();
	ui.channelTableView->setModel(&channelModel);

	ui.acquisitionChannelInMonitorView->setModel(0);
	monitoringChannelModel.UpdateChannels();
	ui.acquisitionChannelInMonitorView->setModel(&monitoringChannelModel);
}

void DaqSuite::UpdateModel()
{
	ui.tableView->setModel(0);
	ui.tableView->setModel(&deviceModel);

	ui.channelTableView->setModel(0);
	channelModel.UpdateChannels();
	ui.channelTableView->setModel(&channelModel);

	ui.acquisitionChannelInMonitorView->setModel(0);
	monitoringChannelModel.UpdateChannels();
	ui.acquisitionChannelInMonitorView->setModel(&monitoringChannelModel);

	ui.clientTableView->setModel(0);
	ui.clientTableView->setModel(&clientModel);

	ui.fileNameTableView->setModel(0);
	ui.fileNameTableView->setModel(&m_FileNameModel);

	ui.planTableView->setModel(0);
	ui.planTableView->setModel(&m_PlanModel);

}

void DaqSuite::UpdateSpeed()
{
	auto mgr = SystemManager::GetInstance();
	auto triggermgr = TriggerManager::GetInstance();
	if (mgr->GetStatus() == READYTOSTOP)
	{
		mgr->StopMonitor();
	}
	if (mgr->GetStatus() == MONITORING)
	{
		double monitoringSpeed = mgr->GetMonitorSpeed() / 1000000;
		double smonitoringSpeed = mgr->GetMonitorSampleSpeed() / 1000000;

		QString q = "Monitoring speed / Sample speed : " + QString::number(monitoringSpeed, 'f', 2)
			+ "/" + QString::number(smonitoringSpeed, 'f', 2) + " MBytes/sec";

		ui.statusBar->showMessage(q);
		int nRow = deviceModel.rowCount() - 1;
		deviceModel.dataChanged(deviceModel.index(0, 4), deviceModel.index(nRow, 7));
		nRow = clientModel.rowCount() - 1;
		clientModel.dataChanged(clientModel.index(0, 4), clientModel.index(nRow, 7));
		nRow = monitoringChannelModel.rowCount() - 1;
		monitoringChannelModel.dataChanged(monitoringChannelModel.index(0, 4), monitoringChannelModel.index(nRow, 7));
		nRow = monitoringSnapshotChannelModel.rowCount() - 1;
		monitoringSnapshotChannelModel.dataChanged(monitoringSnapshotChannelModel.index(0, 4), monitoringSnapshotChannelModel.index(nRow, 7));
		m_view->render();
		ui.actionMonitoring->setChecked(true);
		ui.actionRecord->setChecked(false);
		ui.recordspeedlabel->setText("Record Speed 0.00 (M/S)");
		ui.monitorspeedlabel->setText("Monitor Speed " + QString::number(monitoringSpeed, 'f', 2) + " (M/S)");
		ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/edit-clear-history.png")));
		ui.startToolButton->setIconSize(QSize(30, 30));
		ui.progressBar->setValue(0);
		if (triggermgr->nSnapshot >= nSnapshot)
		{
			nSnapshot = triggermgr->nSnapshot;
			monitoringSnapshotChannelModel.SetSnapshotReady(true);
			m_snapshotView->updatePoint();
			m_snapshotView->render();
		}
		if (triggermgr->IsArmed())
		{
			ui.statusLabel->setText("Armed\nTrigger Count : " + QString::number(nSnapshot));
		}
		else
		{
			ui.statusLabel->setText("Monitoring\nTrigger Count : " + QString::number(nSnapshot));
		}
	}
	else if (mgr->GetStatus() == RECORDING)
	{
		double monitoringSpeed = mgr->GetMonitorSpeed() / 1000000;
		double recordingSpeed = triggermgr->GetRecordSpeed() / 1000000;
		double smonitoringSpeed = mgr->GetMonitorSampleSpeed() / 1000000;
		double srecordingSpeed = triggermgr->GetRecordSampleSpeed() / 1000000;
		QString q = "Monitoring speed / Sample speed : " + QString::number(monitoringSpeed, 'f', 2)
			+ "/" + QString::number(smonitoringSpeed, 'f', 2) + " MBytes/sec";
		if (recordingSpeed > 0)
		{
			q += " Recording speed : "
				+ QString::number(recordingSpeed, 'f', 2) + "/" + QString::number(srecordingSpeed, 'f', 2) + " MBytes/sec";
		}
		ui.statusBar->showMessage(q);
		int nRow = deviceModel.rowCount() - 1;
		deviceModel.dataChanged(deviceModel.index(0, 4), deviceModel.index(nRow, 7));
		nRow = clientModel.rowCount() - 1;
		clientModel.dataChanged(clientModel.index(0, 4), clientModel.index(nRow, 7));
		nRow = monitoringChannelModel.rowCount() - 1;
		monitoringChannelModel.dataChanged(monitoringChannelModel.index(0, 4), monitoringChannelModel.index(nRow, 7));
		nRow = monitoringSnapshotChannelModel.rowCount() - 1;
		monitoringSnapshotChannelModel.dataChanged(monitoringSnapshotChannelModel.index(0, 4), monitoringSnapshotChannelModel.index(nRow, 7));
		m_view->render(); 
		ui.actionMonitoring->setChecked(true);
		ui.actionRecord->setChecked(true);
		ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-record.png")));
		ui.startToolButton->setIconSize(QSize(30, 30));
		ui.statusLabel->setText("Recording\nTrigger Count : " + QString::number(nSnapshot));
		ui.recordspeedlabel->setText("Record Speed " + QString::number(recordingSpeed, 'f', 2) + " (M/S)");
		ui.monitorspeedlabel->setText("Monitor Speed " + QString::number(monitoringSpeed, 'f', 2) + " (M/S)");
		ui.progressBar->setValue(mgr->GetRecordProgres());
	}
	else
	{
		if (m_thread->isRunning())
		{
			ui.progressBar->setValue(mgr->GetProgress());
		}
		else
		{
			iTimer->stop();
			recordModel.dataChanged(QModelIndex(), QModelIndex());
			ui.actionMonitoring->setChecked(false);
			ui.actionRecord->setChecked(false);
			ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/media-playback-start.png")));
			ui.startToolButton->setIconSize(QSize(30, 30));
			ui.monitorToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/utilities-system-monitor.png")));
			ui.monitorToolButton->setIconSize(QSize(30, 30));
			ui.statusBar->showMessage("Stopping recording ...", 1000);
			ui.statusLabel->setText("Stopped");
			ui.recordspeedlabel->setText("Record Speed 0.00 (M/S)");
			ui.monitorspeedlabel->setText("Monitor Speed 0.00 (M/S)");
			ui.progressBar->setValue(0);
			int ncurrent = DeviceRecordSetting::GetInstance()->GetRecordNumber();
			for (int i = nCurrentRecordNumber; i < ncurrent; ++i)
			{
				DeviceRecordSetting::GetInstance()->SetRecordNumber(i);
				channelModel.SaveDSI();
			}
			DeviceRecordSetting::GetInstance()->SetRecordNumber(ncurrent);
		}
	}

	UpdateUI();
}

void DaqSuite::ArmTrigger()
{
	ui.startToolButton->setIcon(QIcon(QStringLiteral(":/DaqSuite/Resources/32x32/actions/edit-clear-history.png")));
	ui.startToolButton->setIconSize(QSize(30, 30));
	SystemManager::GetInstance()->ArmTrigger();
}

void DaqSuite::Connect()
{
	SystemManager::GetInstance()->ConnectServerClient();
	UpdateModel();
}

void DaqSuite::Disconnect()
{
	SystemManager::GetInstance()->DisconnectServerClient();
	UpdateModel();
}

void DaqSuite::NewProject()
{
	QMessageBox msgBox(this);
	msgBox.setWindowTitle("Warning!");
	msgBox.setText("Current setting will be lost. Do you want to overwrite?");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::No);
	if (QMessageBox::Ok != msgBox.exec())
	{
		return;
	}
	m_PlanModel.resetData();
	m_FileNameModel.resetData();
	UpdateModel();
	setWindowTitle(title + " : New Project");
}

void DaqSuite::LoadSetting()
{
	std::string path = DeviceRecordSetting::GetInstance()->GetRecordPath();
	QString fileName = QFileDialog::getOpenFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
	if (fileName != "")
	{
		boost::property_tree::ptree pt;
		read_xml(fileName.toStdString(), pt);
		SystemManager::GetInstance()->ReadSystemInfo(pt);
		UpdateModel();
		ui.recordTableView->setModel(0);
		ui.recordTableView->setModel(&recordModel);
		setWindowTitle(title + " : Project - " + fileName);
		UpdateModel();
		m_view->init();
		m_snapshotView->init();
	}
}

void DaqSuite::SaveSetting()
{
	std::string path = ::DeviceRecordSetting::GetInstance()->GetRecordPath();
	QString fileName = QFileDialog::getSaveFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
	if (fileName != "")
	{
		boost::property_tree::ptree pt;
		SystemManager::GetInstance()->WriteSystemInfo(pt);
		boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
		write_xml(fileName.toStdString(), pt, std::locale(), settings);
		setWindowTitle(title + " : Project - " + fileName);
	}
}

void DaqSuite::LoadCalibration()
{
	if (SystemManager::GetInstance()->GetStatus() != STOPPED)
	{
		return;
	}
	QModelIndexList indexes = ui.tableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		std::string path = ::DeviceRecordSetting::GetInstance()->GetRecordPath();
		QString fileName = QFileDialog::getOpenFileName(this, tr("Calibration File"), path.c_str(), tr("Files (*.cal)"));
		if (fileName != "")
		{
			QModelIndex index = indexes.at(0);
			const std::shared_ptr<IDevice>& pdevice = SystemManager::GetInstance()->GetDevices().at(index.row());
			if (pdevice)
			{
				SystemManager::GetInstance()->LoadCalibration(fileName.toStdString(), pdevice);
			}
		}
		UpdateModel();
	}
	else
	{
		QMessageBox msgBox(this);
		msgBox.setWindowTitle("Warning!");
		msgBox.setText("No device is selected");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::No);
		msgBox.exec();
	}
}

void  DaqSuite::InsertNameItemBefore()
{
	QModelIndexList indexes = ui.fileNameTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		m_FileNameModel.addItem(indexes.front().row() - 1);
		ui.fileNameTableView->setModel(0);
		ui.fileNameTableView->setModel(&m_FileNameModel);
	}
}

void  DaqSuite::InsertNameItemAfter()
{
	QModelIndexList indexes = ui.fileNameTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		m_FileNameModel.addItem(indexes.front().row() + 1);
		ui.fileNameTableView->setModel(0);
		ui.fileNameTableView->setModel(&m_FileNameModel);
	}
}

void  DaqSuite::RemoveNameItem()
{
	QModelIndexList indexes = ui.fileNameTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		m_FileNameModel.removeItem(indexes.front().row());
		ui.fileNameTableView->setModel(0);
		ui.fileNameTableView->setModel(&m_FileNameModel);
	}
}

void DaqSuite::InsertChannelDataBefore()
{
	QModelIndexList indexes = ui.channelTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		SystemManager::GetInstance()->AddChannelUserData(indexes.front().row() - 8);
		ui.channelTableView->setModel(0);
		ui.channelTableView->setModel(&channelModel);
	}
}

void DaqSuite::InsertChannelDataAfter()
{
	QModelIndexList indexes = ui.channelTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		SystemManager::GetInstance()->AddChannelUserData(indexes.front().column() - 7);
		ui.channelTableView->setModel(0);
		ui.channelTableView->setModel(&channelModel);
	}
}

void DaqSuite::RemoveChannelDataItem()
{
	QModelIndexList indexes = ui.channelTableView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		SystemManager::GetInstance()->RemoveChannelUserData(indexes.front().column() - 8);
		ui.channelTableView->setModel(0);
		ui.channelTableView->setModel(&channelModel);
	}
}

void DaqSuite::Include()
{
	QModelIndexList indexes = ui.acquisitionChannelInMonitorView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		for (int i = 0; i < indexes.size(); ++i)
		{
			monitoringChannelModel.Include(indexes[i].row());
		}
	}
}

void DaqSuite::Exclude()
{
	QModelIndexList indexes = ui.acquisitionChannelInMonitorView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		for (int i = 0; i < indexes.size(); ++i)
		{
			monitoringChannelModel.Exclude(indexes[i].row());
		}
	}
}

void DaqSuite::SetView(int val)
{
	QModelIndexList indexes = ui.acquisitionChannelInMonitorView->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		for (int i = 0; i < indexes.size(); ++i)
		{
			monitoringChannelModel.SetView(indexes[i].row(), val);
		}
	}
}

void DaqSuite::SetSnapview(int val)
{
	QModelIndexList indexes = ui.acquisitionChannelInMonitorView_2->selectionModel()->selection().indexes();
	if (indexes.size() > 0)
	{
		for (int i = 0; i < indexes.size(); ++i)
		{
			monitoringSnapshotChannelModel.SetView(indexes[i].row(), val);
		}
	}
}
void DaqSuite::IncludeView()
{
	//QModelIndexList indexes = ui.analysisChannelInMonitorView->selectionModel()->selection().indexes();
	//if (indexes.size() > 0)
	//{
	//	for (int i = 0; i < indexes.size(); ++i)
	//	{
	//		m_FileModel.Include(indexes[i].row());
	//	}
	//}
}

void DaqSuite::ExcludeView()
{
	//QModelIndexList indexes = ui.analysisChannelInMonitorView->selectionModel()->selection().indexes();
	//if (indexes.size() > 0)
	//{
	//	for (int i = 0; i < indexes.size(); ++i)
	//	{
	//		m_FileModel.Exclude(indexes[i].row());
	//	}
	//}
}

void DaqSuite::SetMinMaxDuration()
{
	auto sysmgr = SystemManager::GetInstance();
	double du = sysmgr->GetChannelDuration();
	long s = sysmgr->GetSampleRate();
	double m = 1.0 / s;
	double c = 0.0000000001;
	double Min = 0;
	for (int i = 0; i < 10; ++i, c *= 10)
	{
		if (m < c && Min == 0)
		{
			Min = c;
			break;
		}
	}
	ui.durationDoubleSpinBox->setMaximum(du - fmod(du, Min));
	ui.durationDoubleSpinBox->setMinimum(Min);
	ui.durationDoubleSpinBox->setSingleStep(Min);
	ui.durationDoubleSpinBox->setValue(du);
	ui.maxSpinBox->setValue(sysmgr->GetInputMilivoltage());
	ui.minSpinBox->setValue(-sysmgr->GetInputMilivoltage());
	m_view->setDuration(du);

	ui.maxSpinBox_2->setValue(sysmgr->GetInputMilivoltage());
	ui.minSpinBox_2->setValue(-sysmgr->GetInputMilivoltage());
	m_snapshotView->setDuration(TriggerManager::GetInstance()->GetPreTrigger() + std::min(1.0, DeviceRecordSetting::GetInstance()->GetRecordDuration() / 1000000000.0));
}

void DaqSuite::LoadDisplaySetting()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Display File (display)"), "", tr("Files (*.display)"));
	if (fileName != "")
	{
		int nviews = 0;
		boost::property_tree::ptree pt;
		read_xml(fileName.toStdString(), pt);
		monitoringChannelModel.ReadChannelSetting(pt, nviews);
		m_view->setViewNumber(nviews);
		monitoringSnapshotChannelModel.ReadChannelSetting(pt, nviews);
		m_snapshotView->setViewNumber(nviews);
		ui.viewTableNumberSpinBox->setValue(m_view->getViewNumber());
	}
}

void DaqSuite::SaveDisplaySetting()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Display File (display)"), "", tr("Files (*.display)"));
	if (fileName != "")
	{
		boost::property_tree::ptree pt;
		monitoringChannelModel.SaveChannelSetting(pt, m_view->getViewNumber());
		monitoringSnapshotChannelModel.SaveChannelSetting(pt, m_view->getViewNumber());
		boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
		write_xml(fileName.toStdString(), pt, std::locale(), settings);
	}
}

void DaqSuite::UpdateMonitorChannelColumns()
{
	int nCols = 0;
	if (ui.voltMaxCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(4);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(4);
	}

	if (ui.voltMinCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(5);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(5);
	}

	if (ui.voltAveCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(8);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(8);
	}

	if (ui.voltRMSCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(9);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(9);
	}

	if (ui.euMaxCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(6);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(6);
	}

	if (ui.euMinCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(7);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(7);
	}

	if (ui.euAveCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(10);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(10);
	}
	if (ui.euRMSCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView->showColumn(11);
	}
	else
	{
		ui.acquisitionChannelInMonitorView->hideColumn(11);
	}
	int width = 200 + nCols * 55;
	ui.channelMonitorFrame->setMinimumWidth(width);
	ui.channelMonitorFrame->setMaximumWidth(width);
}

void DaqSuite::AddField()
{
	m_PlanModel.addItem();
	ui.planTableView->setModel(0);
	ui.planTableView->setModel(&m_PlanModel);
}

void DaqSuite::UpdateMonitorSnapshotChannelColumns()
{
	int nCols = 0;
	if (ui.voltMaxCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(4);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(4);
	}

	if (ui.voltMinCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(5);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(5);
	}

	if (ui.voltAveCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(8);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(8);
	}

	if (ui.voltRMSCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(9);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(9);
	}

	if (ui.euMaxCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(6);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(6);
	}

	if (ui.euMinCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(7);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(7);
	}

	if (ui.euAveCheckBox->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(10);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(10);
	}
	if (ui.euRMSCheckBox_2->isChecked())
	{
		++nCols;
		ui.acquisitionChannelInMonitorView_2->showColumn(11);
	}
	else
	{
		ui.acquisitionChannelInMonitorView_2->hideColumn(11);
	}
	int width = 200 + nCols * 55;
	ui.channelMonitorFrame_2->setMinimumWidth(width);
	ui.channelMonitorFrame_2->setMaximumWidth(width);
}

void DaqSuite::RemoveField()
{
	m_PlanModel.removeItem();
	ui.planTableView->setModel(0);
	ui.planTableView->setModel(&m_PlanModel);
}

void DaqSuite::ReadUISetting(const boost::property_tree::ptree& pt)
{
	try
	{
		if (pt.empty()) return;
		ui.showChannelControlButton->setChecked(pt.get<bool>("UI.ChannelView.Visible"));
		ui.systemToolButton->setChecked(pt.get<bool>("UI.Setting.Visible"));
		ui.voltMaxCheckBox->setChecked(pt.get<bool>("UI.ChannelView.VoltMax"));
		ui.voltMinCheckBox->setChecked(pt.get<bool>("UI.ChannelView.VoltMin"));
		ui.voltAveCheckBox->setChecked(pt.get<bool>("UI.ChannelView.VoltAve"));
		ui.voltRMSCheckBox->setChecked(pt.get<bool>("UI.ChannelView.VoltRMS"));
		ui.euMaxCheckBox->setChecked(pt.get<bool>("UI.ChannelView.EUMax"));
		ui.euMinCheckBox->setChecked(pt.get<bool>("UI.ChannelView.EUMin"));
		ui.euAveCheckBox->setChecked(pt.get<bool>("UI.ChannelView.EUAve"));
		ui.euRMSCheckBox->setChecked(pt.get<bool>("UI.ChannelSnapshotView.EURMS"));

		ui.voltMaxCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.VoltMax"));
		ui.voltMinCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.VoltMin"));
		ui.voltAveCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.VoltAve"));
		ui.voltRMSCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.VoltRMS"));
		ui.euMaxCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.EUMax"));
		ui.euMinCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.EUMin"));
		ui.euAveCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.EUAve"));
		ui.euRMSCheckBox_2->setChecked(pt.get<bool>("UI.ChannelSnapshotView.EURMS"));
		if (pt.get<bool>("UI.Screen.Full")) showMaximized();
		else
		{
			setGeometry(pt.get<int>("UI.Screen.X"), pt.get<int>("UI.Screen.Y"), pt.get<int>("UI.Screen.Width"), pt.get<int>("UI.Screen.Height"));
		}
		ChannelRenderData::MAX_POINTS = pt.get<long long>("UI.MaxPoints");
		ChannelRenderData::REFRESH_RATE = pt.get<int>("UI.RefreshRate");
		ui.maxPointSpinBox->setValue(ChannelRenderData::MAX_POINTS);
		ui.refreshRateSpinBox->setValue(ChannelRenderData::REFRESH_RATE);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}

void DaqSuite::WriteUISetting(boost::property_tree::ptree& pt)
{
	try
	{
		pt.add("UI.ChannelView.Visible", ui.showChannelControlButton->isChecked());
		pt.add("UI.Setting.Visible", ui.systemToolButton->isChecked());
		pt.add("UI.ChannelView.VoltMax", ui.voltMaxCheckBox->isChecked());
		pt.add("UI.ChannelView.VoltMin", ui.voltMinCheckBox->isChecked());
		pt.add("UI.ChannelView.VoltAve", ui.voltAveCheckBox->isChecked());
		pt.add("UI.ChannelView.VoltRMS", ui.voltRMSCheckBox->isChecked());
		pt.add("UI.ChannelView.EUMax", ui.euMaxCheckBox->isChecked());
		pt.add("UI.ChannelView.EUMin", ui.euMinCheckBox->isChecked());
		pt.add("UI.ChannelView.EUAve", ui.euAveCheckBox->isChecked());
		pt.add("UI.ChannelView.EURMS", ui.euRMSCheckBox->isChecked());

		pt.add("UI.ChannelSnapshotView.VoltMax", ui.voltMaxCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.VoltMin", ui.voltMinCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.VoltAve", ui.voltAveCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.VoltRMS", ui.voltRMSCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.EUMax", ui.euMaxCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.EUMin", ui.euMinCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.EUAve", ui.euAveCheckBox_2->isChecked());
		pt.add("UI.ChannelSnapshotView.EURMS", ui.euRMSCheckBox_2->isChecked());
		pt.add("UI.Screen.Full", isMaximized());
		auto g = geometry();
		pt.add("UI.Screen.X", g.x());
		pt.add("UI.Screen.Y", g.y());
		pt.add("UI.Screen.Width", g.width());
		pt.add("UI.Screen.Height", g.height());
		pt.add("UI.MaxPoints", ChannelRenderData::MAX_POINTS);
		pt.add("UI.RefreshRate", ChannelRenderData::REFRESH_RATE);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}

void DaqSuite::ReadTestPlan(const boost::property_tree::ptree& pt)
{
	try
	{
		if (pt.empty()) return;
		auto& plan = m_PlanModel.getPlan();
		auto su = pt.get_child_optional("Plan");
		if (su)
		{
			plan.clear();
			for (auto itr = su->begin(); itr != su->end(); ++itr)
			{
				if (itr->first == "Field")
				{
					Field field;
					field.name = itr->second.get<std::string>("Name");
					field.data = itr->second.get<std::string>("Data");
					plan.push_back(field);
				}
			}
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}

void DaqSuite::WriteTestPlan(boost::property_tree::ptree& pt)
{
	try
	{
		auto& plan = m_PlanModel.getPlan();
		for (int i = 0; i < (int)plan.size(); ++i)
		{
			auto& field = plan[i];
			auto& node = pt.add("Plan.Field", "");
			node.put("Name", field.name);
			node.put("Data", field.data);
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}

void DaqSuite::updateTrigger()
{
	auto mgr = TriggerManager::GetInstance();

	if (ui.edgeFallingRadioButton->isChecked())
	{
		mgr->SetTriggerControlType(EDGE);
		mgr->SetTriggerLimitType(LOWER);
	}
	else if (ui.edgeRisingRadioButton->isChecked())
	{
		mgr->SetTriggerControlType(EDGE);
		mgr->SetTriggerLimitType(UPPER);
	}
	else if (ui.levelLowRadioButton->isChecked())
	{
		mgr->SetTriggerControlType(LIMIT);
		mgr->SetTriggerLimitType(LOWER);
	}
	else if (ui.levelUpRadioButton->isChecked())
	{
		mgr->SetTriggerControlType(LIMIT);
		mgr->SetTriggerLimitType(UPPER);
	}
}

void DaqSuite::manualCheck()
{
	ui.manualGroupBox->setChecked(true);
	ui.scheduleGroupBox->setChecked(false);
	ui.channelGroupBox->setChecked(false);
	auto mgr = TriggerManager::GetInstance();
	mgr->SetTriggerControlType(NONE);
}

void DaqSuite::scheduleCheck()
{
	ui.manualGroupBox->setChecked(false);
	ui.scheduleGroupBox->setChecked(true);
	ui.channelGroupBox->setChecked(false);
	auto mgr = TriggerManager::GetInstance();
	mgr->SetTriggerControlType(TIME);
}

void DaqSuite::channelCheck()
{
	ui.manualGroupBox->setChecked(false);
	ui.scheduleGroupBox->setChecked(false);
	ui.channelGroupBox->setChecked(true);
	ui.levelLowRadioButton->setChecked(true);
	auto mgr = TriggerManager::GetInstance();
	mgr->SetTriggerControlType(LIMIT);
	mgr->SetTriggerLimitType(LOWER);
}

void DaqSuite::scheduleTime()
{
	auto mgr = TriggerManager::GetInstance();
	tm t;
	time_t ti;
	time(&ti);
	t = *localtime(&ti);
	t.tm_hour = ui.scheduleTimeEdit->time().hour();
	t.tm_min = ui.scheduleTimeEdit->time().minute();
	t.tm_sec = ui.scheduleTimeEdit->time().second();
	mgr->SetTriggerScheduleTime(t);
}

void DaqSuite::channelIndex()
{
	auto mgr = TriggerManager::GetInstance();
	mgr->SetTriggerChannelIndex(ui.channelIndexSpinBox->value() - 1);
}

void DaqSuite::typeChanged()
{
	auto mgr = TriggerManager::GetInstance();
	mgr->SetTriggerLimitValueType(ui.valueTypeComboBox->currentIndex() == 0 ? MILLIVOLT : EUVALUE);
}

void DaqSuite::levelChanged()
{
	auto mgr = TriggerManager::GetInstance();
	mgr->SetLimit(ui.levelSpinBox->value());
}

void DaqSuite::UpdateUI()
{
	auto status = SystemManager::GetInstance()->GetStatus();
	ui.startToolButton->setEnabled(status == MONITORING);
	ui.manualGroupBox->setEnabled(status == STOPPED);
	ui.scheduleGroupBox->setEnabled(status == STOPPED);
	
	if (ui.channelGroupBox->isChecked())
	{
		ui.triggerChannelTypeFrame->setEnabled(status == STOPPED);
		ui.levelSpinBox->setEnabled(status == STOPPED);
		ui.levelSpinBox->setEnabled(status == STOPPED);
		ui.valueTypeComboBox->setEnabled(status == STOPPED);
		ui.triggerNumberSpinBox->setEnabled(status == STOPPED);
	}
	else
	{
		ui.channelGroupBox->setEnabled(status == STOPPED);
	}
}

void DaqSuite::recordTimesChanged()
{
	DeviceRecordSetting::GetInstance()->SetRecordTimes(ui.triggerNumberSpinBox->value());
}

void DaqSuite::updateSampleFileName()
{
	std::string filename = DeviceRecordSetting::GetInstance()->GetRecordFileName(1);
	filename.pop_back();
	filename += "{id}.{file type}";
	ui.fileSampleNameLabel->setText(filename.c_str());
}

void DaqSuite::setSnapshot(bool bSnapshot)
{
	TriggerManager::GetInstance()->SetSnapshot(bSnapshot);
}

void DaqSuite::exportChannelInfo()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Channel File (CSV)"), "", tr("Files (*.CSV)"));
	if (fileName != "")
	{
		channelModel.SaveUserData(fileName);
	}
}

void DaqSuite::importChannelInfo()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Channel File (CSV)"), "", tr("Files (*.CSV)"));
	if (fileName != "")
	{
		channelModel.LoadUserData(fileName);
	}
}

void DaqSuite::closeEvent(QCloseEvent *event)
{
	if (SystemManager::GetInstance()->GetStatus() != STOPPED
		|| m_thread->isRunning())
	{
		QMessageBox msgBox(this);
		msgBox.setWindowTitle("Warning!");
		msgBox.setText("The system is currently running.\nPlease stop the system and close.");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.exec();
		event->ignore();
	}
	else
	{
		event->accept();
	}
}

void DaqSuite::openimportui()
{
	channelImportUi->show();
}

void DaqSuite::ConvertRawFile()
{
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		std::string host = "192.168.1.100";
		QModelIndexList indexes = ui.tableView->selectionModel()->selection().indexes();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.at(0);
			const std::shared_ptr<IDevice>& pdevice = SystemManager::GetInstance()->GetDevices().at(index.row());
			if (pdevice)
			{
				std::string path = ::DeviceRecordSetting::GetInstance()->GetRecordPath();
				QString fileName = QFileDialog::getOpenFileName(this, tr("Open Raw File"), path.c_str(), tr("Files (*.rawfs)"));
				if (fileName != "")
				{
					pdevice->Initialize();
					DeviceDataRecorder::ConvertToLDSF(fileName.toStdString(), pdevice->GetDeviceDataManager()->GetActiveChannels());
					ui.statusBar->showMessage(fileName + " is saved");
				}
			}
		}
		else
		{
			QMessageBox msgBox(this);
			msgBox.setWindowTitle("Warning!");
			msgBox.setText("No device is selected");
			msgBox.setStandardButtons(QMessageBox::Ok);
			msgBox.setDefaultButton(QMessageBox::No);
			msgBox.exec();
		}
	}
}

void DaqSuite::onMaximumPointsChanged()
{
	ChannelRenderData::MAX_POINTS = ui.maxPointSpinBox->value();
}

void DaqSuite::onRefreshRateChanged()
{
	ChannelRenderData::REFRESH_RATE = ui.refreshRateSpinBox->value();
}