#ifndef CLIENTDEVICEMODELDELEGATE_H
#define CLIENTDEVICEMODELDELEGATE_H

#include <QItemDelegate>

class ClientDeviceModelDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	ClientDeviceModelDelegate(QObject *parent);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

};

#endif // CLIENTDEVICEMODELDELEGATE_H
