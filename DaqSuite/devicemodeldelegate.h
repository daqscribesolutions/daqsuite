#ifndef DEVICEMODELDELEGATE_H
#define DEVICEMODELDELEGATE_H

#include <QItemDelegate>

class DeviceModelDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	
	DeviceModelDelegate(QObject *parent);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		
};

#endif // DEVICEMODELDELEGATE_H
