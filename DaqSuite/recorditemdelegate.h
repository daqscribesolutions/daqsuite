#ifndef RECORDITEMDELEGATE_H
#define RECORDITEMDELEGATE_H

#include <QItemDelegate>

class RecordItemDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	RecordItemDelegate(QObject *parent = 0);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	
public slots:

	void buttonClicked();

};

#endif // RECORDITEMDELEGATE_H
