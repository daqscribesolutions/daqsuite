#ifndef CHANNELGRAPHICVIEW_H
#define CHANNELGRAPHICVIEW_H

#include <QGraphicsView>
#include <QMutex>
#include "channelrenderdata.h"
#include "channelviewitem.h"
#include <memory>

class ChannelGraphicView : public QGraphicsView
{
	Q_OBJECT

public:

	ChannelGraphicView(QWidget *parent, MonitoringChannelModel * m);

	~ChannelGraphicView();

	void paintEvent(QPaintEvent *ev);

	int getSelectedItemID();

	int getViewNumber() { return (int)glViewers.size(); }

	virtual void init();

	void reset();

public slots:

	void updatePoint();

	void render();

	void setViewNumber(int n);

	void setMaxValue(double val);

	void setMinValue(double val);

	void setDuration(double val);

protected:

	void focusOutEvent(QFocusEvent * event);

	void mouseReleaseEvent(QMouseEvent *event);

	void resizeEvent(QResizeEvent* event);

	int selectedItemIdx;
	
	QGraphicsScene m_Scene;

	std::vector<ChannelViewItem*> glViewers;

	std::vector<std::shared_ptr<ChannelViewItem> > viewers;

	MonitoringChannelModel * model;

	std::vector<ChannelRenderData*> channelRenderDatas;

	QMutex m_mutex;

};

#endif // CHANNELGRAPHICVIEW_H
