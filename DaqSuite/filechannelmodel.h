#ifndef FILECHANNELMODEL_H
#define FILECHANNELMODEL_H

#include "monitoringchannelmodel.h"

namespace DeviceLib
{
	class VirtualDevice;
	class DeviceData;
}

class FileChannelModel : public MonitoringChannelModel
{
	Q_OBJECT

public:

	FileChannelModel(QObject *parent);

	~FileChannelModel();

	void UpdataChannels(const char* filename);

	DeviceLib::DeviceData * GetDeviceData();

private:

	DeviceLib::VirtualDevice * pDevice;	

};

#endif // FILECHANNELMODEL_H
