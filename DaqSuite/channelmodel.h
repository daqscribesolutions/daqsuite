#ifndef CHANNELMODEL_H
#define CHANNELMODEL_H

#include <QAbstractTableModel>
#include <qprocess.h>

namespace DeviceLib
{
	class ChannelInfo;
}

class ChannelModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	ChannelModel(QObject *parent);

	~ChannelModel();

	void UpdateChannels();
	
	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	void LoadUserData(QString filename);

	void SaveUserData(QString filename, int idx = -1);

	void SaveDSI();

private:
	QProcess process;

	std::vector<DeviceLib::ChannelInfo*> m_Channels;

};

#endif // CHANNELMODEL_H
