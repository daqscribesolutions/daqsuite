#ifndef FILENAMEMODEL_H
#define FILENAMEMODEL_H

#include <QAbstractTableModel>

class FileNameModel : public QAbstractTableModel
{

	Q_OBJECT

public:

	FileNameModel(QObject *parent);

	~FileNameModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

signals:
	
	void update();

public slots:

	void addItem(int i);

	void removeItem(int i);

	void resetData();

};


#endif // FILENAMEMODEL_H
