#include "devicemodel.h"

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

DeviceModel::DeviceModel(QObject *parent)
: QAbstractTableModel(parent)
{
}

DeviceModel::~DeviceModel()
{
}

Qt::ItemFlags DeviceModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		ServerDevice* pDevice = dynamic_cast<ServerDevice*>(devices[index.row()].get());
		if (pDevice == 0 && (index.column() > 0 && index.column() <= 3) || pDevice && (index.column() == 5 || index.column() == 8))
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int DeviceModel::rowCount(const QModelIndex &) const
{
	return (int)SystemManager::GetInstance()->GetDevices().size();
}

int DeviceModel::columnCount(const QModelIndex &) const
{
	return 8;
}

QVariant DeviceModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[index.row()].get());
		if (pDevice)
		{
			if (index.column() == 0)
			{
				return QString(pDevice->GetDeviceInfo());
			}
			else if (index.column() == 1)
			{
				return QString("%1").arg(pDevice->GetChannels().size());
			}
			else if (index.column() == 2)
			{
				return QString("%1").arg(SystemManager::GetInstance()->GetSampleRate());
			}
			else if (index.column() == 3)
			{
				return QString("%1").arg(SystemManager::GetInstance()->GetInputMilivoltage());
			}
			else if (index.column() == 4)
			{
				return QString("%1").arg(pDevice->GetDeviceDataFormat());
			}
			else if (index.column() == 5)
			{
				ServerDevice* pServerDevice = dynamic_cast<ServerDevice*>(pDevice);
				QString port;
				if (pServerDevice)
				{
					port = QString::number(pServerDevice->GetPort());
				}
				return port;
			}
			else if (index.column() == 6)
			{
				ServerDevice* pServerDevice = dynamic_cast<ServerDevice*>(pDevice);
				QString status;
				if (pServerDevice)
				{
					status = (pServerDevice->IsConnected() ? "Connect" : "Disconnect");
				}
				return status;
			}
			else if (index.column() == 7)
			{
				ServerDevice* pServerDevice = dynamic_cast<ServerDevice*>(pDevice);
				QString status;
				if (pServerDevice)
				{
					status = QString::number(pServerDevice->GetMissingPackets());
				}
				return status;
			}
		}
	}
	return QVariant();
}

bool DeviceModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[index.row()].get());
		if (pDevice)
		{
			if (index.column() == 1)
			{
				pDevice->SetChannelNumber(value.toInt());
				dataChanged(QModelIndex(), QModelIndex());
				channelChanged();
			}
			else if (index.column() == 2)
			{
				SystemManager::GetInstance()->SetSampleRate(value.toInt());
				dataChanged(QModelIndex(), QModelIndex());
			}
			else if (index.column() == 3)
			{
				SystemManager::GetInstance()->SetInputMilivoltage(value.toInt());
				dataChanged(QModelIndex(), QModelIndex());
			}
			else if (index.column() == 4)
			{
			}
			else if (index.column() == 5)
			{
				ServerDevice* pServerDevice = dynamic_cast<ServerDevice*>(pDevice);
				if (pServerDevice)
				{
					pServerDevice->SetPort(value.toInt());
				}
			}
			return true;
		}
	}
	return false;
}

QVariant DeviceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Info");
			}
			else if (section == 1)
			{
				return QString("# Channels");
			}
			else if (section == 2)
			{
				return QString("Sample Rate\n/ Second");
			}
			else if (section == 3)
			{
				return QString("Input voltage(P-P)\n(millivolt)");
			}
			else if (section == 4)
			{
				return QString("Format\n(bytes)");
			}
			else if (section == 5)
			{
				return QString("Port\n(Server)");
			}
			else if (section == 6)
			{
				return QString("Connection\n(Server)");
			}
			else if (section == 7)
			{
				return QString("Missing Packets");
			}
		}
	}

	return QVariant();
}
