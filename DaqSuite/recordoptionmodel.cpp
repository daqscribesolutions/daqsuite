#include "recordoptionmodel.h"
#include <string>
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

RecordOptionModel::RecordOptionModel(QObject *parent)
	: QAbstractTableModel(parent)  
{
}

RecordOptionModel::~RecordOptionModel()
{
}

Qt::ItemFlags RecordOptionModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (index.column() == 0 && index.row() < 8)
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int RecordOptionModel::rowCount(const QModelIndex &) const
{
	return 9;
}

int RecordOptionModel::columnCount(const QModelIndex &) const
{
	return 1;
}

QVariant RecordOptionModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordName().c_str());
			}
			else if (index.row() == 1)
			{
				return (DeviceRecordSetting::GetInstance()->GetRecordNumber());
			}
			else if (index.row() == 2)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordPath().c_str());
			}
			else if (index.row() == 3)
			{
				SYSTEM_STATUS status = SystemManager::GetInstance()->GetPostRecordStatus();
				return QString(status == STOPPED ? "Stop" : "Monitor");
			}
			else if (index.row() == 4)
			{
				RECORD_TYPE filetype = SystemManager::GetInstance()->GetRecordType();
				return QString(filetype == RAWFS ? "Rawfs" : filetype == LDSF_FILE ? "LDSF" : "Both");
			}
			else if (index.row() == 5)
			{
				return (TriggerManager::GetInstance()->GetPreTrigger());
			}
			else if (index.row() == 6)
			{
				return (DeviceRecordSetting::GetInstance()->GetRecordDuration() / 1000000000.0);
			}
			else if (index.row() == 7)
			{
				double capacity, available, free;
				StorageManager::GetSpaceInformation(DeviceRecordSetting::GetInstance()->GetRecordPath(), capacity, available, free);
				return QString::number(available, 'f', 2);
			}
			else if (index.row() == 8)
			{
				return QString::number(SystemManager::GetInstance()->GetRecordDataSize(), 'f', 2);;
			}
			else if (index.row() == 9)
			{
				return QString::number(TriggerManager::GetInstance()->GetPreTrigger(), 'f', 3);
			}
			else if (index.row() == 10)
			{
				return QString::number(TriggerManager::GetInstance()->GetTriggerChannelIndex());
			}
			else if (index.row() == 11)
			{
				MonitorLimitType triggerlimittype = TriggerManager::GetInstance()->GetTriggerLimitType();
				return QString(triggerlimittype == UPPER ? "Upper" : "Lower");
			}
			else if (index.row() == 12)
			{
				MonitorDataType triggerdatatype = TriggerManager::GetInstance()->GetTriggerLimitValueType();
				return QString(triggerdatatype == EUVALUE ? "EU" : "Milivolt");
			}
		}
	}

	return QVariant();
}

bool RecordOptionModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				DeviceRecordSetting::GetInstance()->SetRecordName(value.toString().toStdString());
			}
			else if (index.row() == 1)
			{
				DeviceRecordSetting::GetInstance()->SetRecordNumber(value.toInt());
			}
			else if (index.row() == 2)
			{
				DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString());
				dataChanged(QModelIndex(), QModelIndex());
			}
			else if (index.row() == 3)
			{
				if (value.toString() == "Stop")
				{
					SystemManager::GetInstance()->SetPostRecordStatus(STOPPED);
				}
				else
				{
					SystemManager::GetInstance()->SetPostRecordStatus(MONITORING);
				}
			}
			else if (index.row() == 4)
			{
				if (value.toString() == "Rawfs")
				{
					SystemManager::GetInstance()->SetRecordType(RAWFS);
				}
				else if (value.toString() == "LDSF")
				{
					SystemManager::GetInstance()->SetRecordType(LDSF_FILE);
				}
				else if (value.toString() == "Both")
				{
					SystemManager::GetInstance()->SetRecordType(RAWFS_LDSF);
				}
			}
			else if (index.row() == 5)
			{
				TriggerManager::GetInstance()->SetPreTrigger(value.toDouble());
				dataChanged(QModelIndex(), QModelIndex());
			}
			else if (index.row() == 6)
			{
				DeviceRecordSetting::GetInstance()->SetRecordDuration((long long)(value.toDouble() * 1000000000));
				dataChanged(QModelIndex(), QModelIndex());
			}
			else if (index.row() == 7)
			{
			}
			else if (index.row() == 8)
			{
			}
			else if (index.row() == 9)
			{
				TriggerManager::GetInstance()->SetPreTrigger(value.toDouble());
			}
			else if (index.row() == 10)
			{
				TriggerManager::GetInstance()->SetTriggerChannelIndex(value.toInt());
			}
			else if (index.row() == 11)
			{
				if (value.toString() == "Upper")
				{
					TriggerManager::GetInstance()->SetTriggerLimitType(UPPER);
				}
				else
				{
					TriggerManager::GetInstance()->SetTriggerLimitType(LOWER);
				}
			}
			else if (index.row() == 12)
			{
				if (value.toString() == "Milivolt")
				{
					TriggerManager::GetInstance()->SetTriggerLimitValueType(MILLIVOLT);
				}
				else
				{
					TriggerManager::GetInstance()->SetTriggerLimitValueType(EUVALUE);
				}
			}
		}
		return true;
	}
	return false;
}

QVariant RecordOptionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Record Infomation");
			}
		}
		if (orientation == Qt::Vertical)
		{
			if (section == 0)
			{
				return QString("Project Name");
			}
			else if (section == 1)
			{
				return QString("Run Number");
			}
			else if (section == 2)
			{
				return QString("Path");
			}
			else if (section == 3)
			{
				return QString("Post Record Status");
			}
			else if (section == 4)
			{
				return QString("File type");
			}
			else if (section == 5)
			{
				return QString("Pre Trigger Record Time (Second)");
			}
			else if (section == 6)
			{
				return QString("Post Trigger Record Time (Second)");
			}
			else if (section == 7)
			{
				return QString("Storage Capacity (MB)");
			}
			else if (section == 8)
			{
				return QString("Required Capacity (MB)");
			}
			else if (section == 9)
			{
				return QString("Pre Trigger Time (Second)");
			}
			else if (section == 10)
			{
				return QString("Trigger Channel Index");
			}
			else if (section == 11)
			{
				return QString("Trigger Limit Type");
			}
			else if (section == 12)
			{
				return QString("Trigger Limit Value Type");
			}
		}
	}

	return QVariant();
}
