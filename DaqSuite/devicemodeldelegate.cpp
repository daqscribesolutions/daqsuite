#include "devicemodeldelegate.h"
#include <qcombobox.h>

#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

DeviceModelDelegate::DeviceModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

QWidget* DeviceModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 1)
	{
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[index.row()].get());
		QComboBox* comboBox = new QComboBox(parent);
		int nSelected = 0;
		if (pDevice)
		{
			auto channelnumbers = pDevice->GetAvailableChannelNumbers();
			for (int i = 0; i < (int)channelnumbers.size(); ++i)
			{
				comboBox->addItem(QString::number(channelnumbers[i]));
			}
		}
		comboBox->setCurrentIndex(nSelected);
		return comboBox;
	}
	else if (index.column() == 6)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Connect");
		comboBox->addItem("Disconnect");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else if (index.column() == 3)
	{
		const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
		IADCDevice* pDevice = dynamic_cast<IADCDevice*>(devices[index.row()].get());
		QComboBox* comboBox = new QComboBox(parent);
		int nSelected = 0;
		if (pDevice)
		{
			nSelected = pDevice->GetDeviceInputMilliVoltage();
			const std::vector<int>& voltages = pDevice->GetDeviceAvailableInputMilliVoltages();
			for (int i = 0; i < (int)voltages.size(); ++i)
			{
				comboBox->addItem(QString::number(voltages[i]));
			}
		}
		comboBox->setCurrentIndex(nSelected);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void DeviceModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 1)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else if (index.column() == 6)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else if (index.column() == 3)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void DeviceModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 1)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else if (index.column() == 6)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else if (index.column() == 3)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void DeviceModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}