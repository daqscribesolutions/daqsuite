#ifndef TESTPLANMODEL_H
#define TESTPLANMODEL_H

#include <QAbstractTableModel>
#include <string>

struct Field
{
	std::string name;
	std::string data;
};

class TestPlanModel : public QAbstractTableModel
{

	Q_OBJECT

public:

	TestPlanModel(QObject *parent);

	~TestPlanModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	std::vector<Field>& getPlan() { return planData; }

public slots:
	
	void addItem();
	
	void removeItem();

	void resetData();

private:

	std::vector<Field> planData;

};

#endif // TESTPLANMODEL_H
