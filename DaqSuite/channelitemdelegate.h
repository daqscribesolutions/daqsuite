#ifndef CHANNELITEMDELEGATE_H
#define CHANNELITEMDELEGATE_H

#include <QItemDelegate>

class ChannelItemDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	ChannelItemDelegate(QObject *parent = 0);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

};

#endif // CHANNELITEMDELEGATE_H
