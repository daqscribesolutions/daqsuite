#include "channelrenderdata.h"
#include <fstream>
#include "channelgraphicview.h"

long long ChannelRenderData::MAX_POINTS = 100;
int ChannelRenderData::REFRESH_RATE = 10;

ChannelRenderData::ChannelRenderData(MonitoringChannelModel* m, ChannelGraphicView* v, int id)
	: model(m)
	, nFrame(1)
	, nCurrentFrame(0)
	, MaxValue(5000)
	, MinValue(-5000)
	, bInit(false)
	, bRun(false)
	, beginSecond(0)
	, durationSecond(0)
	, width(600)
	, height(400)
	, XAxisWidth(10)
	, YAxisWidth(100)
	, nXAxis(10)
	, nNumLevels(10)
	, m_ID(id)
	, currentSecond(0)
	, displayDuration(0.01f)
	, view(v)
{
	std::vector<DisplayChannel>& channels = model->GetChannels();
	for (int i = 0; i < (int)channels.size(); ++i)
	{
		if (channels[i].m_DeviceID == m_ID)
			m_channels.push_back(&channels[i]);
	}
	SetAxisX();
}

ChannelRenderData::~ChannelRenderData()
{
}

void ChannelRenderData::Reset()
{
	SetAxisX();
	float halfheight = ChartAreaHeight / 2.0f + 10;
	for (int ci = 0; ci < (int)m_channels.size(); ++ci)
	{
		std::vector<PointData>& points = m_channels[ci]->points;
		for (long long i = 0; i < (long long)points.size(); ++i)
		{
			PointData& p = points[i];
			p.y = (halfheight);
		}
	}
}

void ChannelRenderData::UpdateEntryData(DeviceLib::DeviceData*)
{
	SetAxisX();

	double Range = (MaxValue - MinValue) / 2;
	double midRange = (MaxValue + MinValue) / 2;
	double halfheight = ChartAreaHeight / 2.0f + 10;
	if (m_channels.empty()) return;
	auto ddata = model->IsSnapshot() ? m_channels.front()->m_Channel->GetSnapShotData() : m_channels.front()->m_Channel->GetData();
	if (ddata == 0) return;
	int32_t nmidRange = ddata->GetDataValue(midRange);
	double ratio = (halfheight / ddata->GetDataValue(Range));
	double maxheight = halfheight * 2 - 10;

	beginSecond = m_channels.front()->m_Channel->GetStartNanoSecond() / 1000000000.0f;
	durationSecond = nFrame * m_channels.front()->m_Channel->GetDurationNanoSecond() / 1000000000.0f;

	for (int ci = 0; ci < (int)m_channels.size(); ++ci)
	{
		if (model->IsSnapshot() || m_channels[ci]->m_DisplayViewIndex >= 0)
		{
			DeviceLib::ChannelInfo* channel = const_cast<DeviceLib::ChannelInfo*>(m_channels[ci]->m_Channel);
			DeviceLib::DeviceData * data = model->IsSnapshot() ? channel->GetSnapShotData() : channel->GetData();
			if (data == 0) break;
			long long datasize = std::min(MAX_POINTS, data->GetNumberSamples());
			std::vector<PointData>& pts = m_channels[ci]->points;
			for (int ni = 0; ni < nFrame - 1; ++ni)
			{
				long long nBegin = datasize * ni;
				long long nEnd = datasize * (ni + 1);
				for (long long i = nBegin; i < nEnd; ++i)
				{
					PointData& p = pts[i + datasize];
					PointData& pp = pts[i];
					pp.y = p.y;
				}
			}
			long long nBegin = datasize * (nFrame - 1);
			for (long long i = 0; i < datasize; ++i)
			{
				if (i + nBegin < pts.size())
				{
					PointData& p = pts[i + nBegin];
					int32_t v = data->GetValue(i, ci);
					p.y = std::max(10.0, std::min(maxheight, halfheight - (v - nmidRange) * ratio));
				}
			}
		}
	}
	++nCurrentFrame;
	nCurrentFrame = nCurrentFrame % nFrame;
}

void ChannelRenderData::UpdatePoints(int w, int h)
{
	std::vector<DisplayChannel>& channels = model->GetChannels();
	m_channels.clear();
	for (int i = 0; i < (int)channels.size(); ++i)
	{
		if (channels[i].m_DeviceID == m_ID)
			m_channels.push_back(&channels[i]);
	}
	if (m_channels.empty()) return;
	SetSize(w, h);
	SetAxisX();
	double Range = (MaxValue - MinValue) / 2;
	double midRange = (MaxValue + MinValue) / 2;
	double halfheight = ChartAreaHeight / 2.0f + 10;
	double maxheight = halfheight * 2 - 10;
	bool bsnap = model->IsSnapshot();
	auto ddata = bsnap ? m_channels.front()->m_Channel->GetSnapShotData() : m_channels.front()->m_Channel->GetData();
	int32_t nmidRange = ddata->GetDataValue(midRange);
	double ratio = (halfheight / ddata->GetDataValue(Range));
	
	beginSecond = m_channels.front()->m_Channel->GetStartNanoSecond() / 1000000000.0f;
	durationSecond = nFrame * m_channels.front()->m_Channel->GetDurationNanoSecond() / 1000000000.0f;
	for (int ci = 0; ci < (int)m_channels.size(); ++ci)
	{
		DeviceLib::ChannelInfo* channel = const_cast<DeviceLib::ChannelInfo*>(m_channels[ci]->m_Channel);
		DeviceLib::DeviceData * data = bsnap ? channel->GetSnapShotData() : channel->GetData();
		long long datasize = std::min(MAX_POINTS, data->GetNumberSamples());
		double dstep = (double)MAX_POINTS / data->GetNumberSamples();
		long long nBegin = datasize * (nFrame - 1);
		std::vector<PointData>& pts = m_channels[ci]->points;

		for (long long i = 0; i < datasize; ++i)
		{
			PointData& p = pts[(i *dstep) + nBegin];
			int32_t v = data->GetValue(i, ci);
			p.y = std::max(10.0, std::min(maxheight, halfheight - (v - nmidRange) * ratio));
		}
	}
}

void ChannelRenderData::UpdateExitData(DeviceLib::DeviceData*)
{
}

bool ChannelRenderData::StartListening()
{
	bRun = true;
	SetAxisX();
	nCurrentFrame = 0;
	return bRun;
}

void ChannelRenderData::StopListening()
{
	bRun = false;
}

void ChannelRenderData::SetSize(int w, int h)
{
	float hrate = (float)h / height;
	width = w;
	height = h;
	int minX = width / 80;
	int minY = height / 22;
	nXAxis = std::min(minX, 10);
	nNumLevels = std::min(minY, 10);

	ChartAreaWidth = width - YAxisWidth - 20;
	ChartAreaHeight = height - XAxisWidth - 20;
	
	SetAxisX();

	if (!bRun)
	{
		for (int ci = 0; ci < (int)m_channels.size(); ++ci)
		{
			std::vector<PointData>& points = m_channels[ci]->points;
			for (long long i = 0; i < (long long)points.size(); ++i)
			{
				PointData& p = points[i];
				p.y = p.y * hrate;
			}
		}
	}
}

void ChannelRenderData::SetAxisX()
{
	for (int ci = 0; ci < (int)m_channels.size(); ++ci)
	{
		DeviceLib::ChannelInfo* channel = m_channels[ci]->m_Channel;

		long long nPoints = std::min(MAX_POINTS, channel->GetDataSize()) * nFrame;
		std::vector<PointData>& points = m_channels[ci]->points;
		points.resize(nPoints);
		float step = (float)ChartAreaWidth / nPoints;
		for (long long i = 0; i < nPoints; ++i)
		{
			PointData& p = points[i];
			p.x = (i * step + YAxisWidth + 10);
		}
	}

	lines.resize((nXAxis + nNumLevels + 2) * 2);
	float nStep = (float)ChartAreaWidth / nXAxis;
	for (int i = 0; i < nXAxis + 1; ++i)
	{
		lines[i * 2] = QPoint(nStep * i + YAxisWidth + 10, height - 10);
		lines[i * 2 + 1] = QPoint(nStep * i + YAxisWidth + 10, 10);
	}
	nStep = (float)ChartAreaHeight / nNumLevels;
	for (int i = 0; i < nNumLevels + 1; ++i)
	{
		int nindex = (nXAxis + 1 + i) * 2;
		lines[nindex] = QPoint(YAxisWidth, nStep * i + 10);
		lines[nindex + 1] = QPoint(width - 10, nStep * i + 10);
	}
}