#include "daqsuite.h"
#include <QtWidgets/QApplication>
#include <qtcpsocket.h>

int main(int argc, char *argv[])
{
	QTcpSocket tcp;
	int port = 55555;
	if (argc > 1)
	{
		port = atoi(argv[1]);
	}
	if (tcp.bind(port))
	{
		QApplication a(argc, argv);
		DaqSuite w;
		w.show();
		return a.exec();
	}
	return 0;
}
