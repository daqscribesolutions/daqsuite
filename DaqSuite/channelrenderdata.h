#ifndef CHANNELRENDERDATA_H
#define CHANNELRENDERDATA_H

#include <qpainter.h>
#include "monitoringchannelmodel.h"
#include "../DeviceLib/DeviceDataManager.h"
#include <qmutex.h>

class ChannelGraphicView;

class ChannelRenderData : public DeviceLib::IDeviceDataUpdateListener
{

public:
	static long long MAX_POINTS;
	static int REFRESH_RATE;

	ChannelRenderData(MonitoringChannelModel* m, ChannelGraphicView* v, int id);

	~ChannelRenderData();

	virtual void UpdateEntryData(DeviceLib::DeviceData* data);

	virtual void UpdateExitData(DeviceLib::DeviceData* data);

	virtual bool StartListening();

	virtual void StopListening();

	void UpdatePoints(int w, int h);

	double MaxValue;

	double MinValue;

	bool IsRunning() const { return bRun; }

	void SetSize(int w, int h);

	void SetAxisX();

	void Reset();

	QVector<QPoint> lines;

	MonitoringChannelModel* model;

	int nXAxis;

	int nNumLevels;

	int width, height;

	int ChartAreaWidth, ChartAreaHeight;

	int XAxisWidth, YAxisWidth;

	int nFrame;

	double beginSecond;

	double durationSecond;

	int nCurrentFrame;

	bool bRun;

	bool bInit;

	int m_ID;
	
	double currentSecond;

	double displayDuration;

	std::vector<DisplayChannel*> m_channels;

private:

	ChannelGraphicView* view;

};

#endif // CHANNELRENDERDATA_H
