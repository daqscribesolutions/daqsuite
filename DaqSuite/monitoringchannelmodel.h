#ifndef MONITORINGCHANNELMODEL_H
#define MONITORINGCHANNELMODEL_H

#include <QAbstractTableModel>
#include <qcolor.h>
#include <qpoint.h>
#include <boost/property_tree/ptree.hpp>

namespace DeviceLib
{
    class ChannelInfo;
}

enum DisplayValueType { VOLT, EU };

struct PointData
{
	double x, y;
};

struct DisplayChannel
{
	static QVector<QColor> ChannelColors;

	DisplayChannel(DeviceLib::ChannelInfo* pChannel = 0) 
		: m_Channel(pChannel), m_DisplayViewIndex(-1), m_DisplayValueType(VOLT), m_DeviceID(-1), m_ChannalIndex(0) {}

	QColor m_Color;
	
	DisplayValueType m_DisplayValueType;

	DeviceLib::ChannelInfo* m_Channel;

	int m_DeviceID;

	int m_ChannalIndex;

	int m_DisplayViewIndex;

	std::vector<PointData> points;

	QVector<QPoint> renderPoints;

};

class ChannelGraphicView;

class MonitoringChannelModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	MonitoringChannelModel(QObject *parent, bool bSnapshot);

	~MonitoringChannelModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    std::vector<DisplayChannel>& GetChannels();

	void SetView(ChannelGraphicView* view);

	int GetNumberOfDevices() { return m_nDevices; }

	void Include(int id);

	void Exclude(int id);

	void SetView(int id, int viewid);

	void SaveChannelSetting(boost::property_tree::ptree& pt, int nviews);

	void ReadChannelSetting(const boost::property_tree::ptree& pt, int& nviews);

	bool IsSnapshotReady() { return m_bSnapshotReady; }

	void SetSnapshotReady(bool bReady) { m_bSnapshotReady = bReady; }

	bool IsSnapshot() { return m_bSnapshot; }

signals:

	void render();

public slots:

	void UpdateChannels();

	void clearChannels();

protected:

	bool m_bSnapshot;
	
	bool m_bSnapshotReady;

	std::vector<DisplayChannel> m_Channels;

	ChannelGraphicView* m_view;

	int m_nDevices;

};

#endif // MONITORINGCHANNELMODEL_H
