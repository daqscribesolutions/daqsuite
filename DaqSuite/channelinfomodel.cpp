#include "channelinfomodel.h"
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

ChannelInfoModel::ChannelInfoModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

ChannelInfoModel::~ChannelInfoModel()
{
}

Qt::ItemFlags ChannelInfoModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.column() == 0)
		return flags | Qt::ItemIsUserCheckable;
	return flags;
}

int ChannelInfoModel::rowCount(const QModelIndex &) const
{
	return tableData.size();
}

int ChannelInfoModel::columnCount(const QModelIndex &) const
{
	return tableData.size() > 0 ? tableData.front().size() : 5;
}

QVariant ChannelInfoModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::CheckStateRole)
	{
		if (tableData.size() > index.row())
		{
			auto currentRow = tableData[index.row()];
			if (currentRow.size() > index.column() && index.column() == 0)
			{
				return currentRow[index.column()] == "1" ? Qt::Checked : Qt::Unchecked;
			}
		}
	 }
	 else if (role == Qt::DisplayRole)
	 {
		 if (tableData.size() > index.row())
		 {
			 auto currentRow = tableData[index.row()];
			 if (currentRow.size() > index.column() && index.column() != 0)
			 {
				 return currentRow[index.column()];
			 }
		 }
	 }

	return QVariant();
}

bool ChannelInfoModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::CheckStateRole)
	{
		if (tableData.size() > index.row())
		{
			auto& currentRow = tableData[index.row()];
			if (currentRow.size() > index.column() && index.column() == 0)
			{
				currentRow[index.column()] = (Qt::CheckState)value.toInt() == Qt::Checked ? "1" : "0";
				return true;
			}
		}
	}
	return false;
}

QVariant ChannelInfoModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (tableHeader.size() > section)
			{
				return tableHeader[section];
			}
		}
		else
		{
			return QString::number(section + 1);
		}
	}

	return QVariant();
}