#include "filechannelgraphicview.h"
#include "filechannelmodel.h"

FileChannelGraphicView::FileChannelGraphicView(QWidget *parent, MonitoringChannelModel * m)
	: ChannelGraphicView(parent, m)
	, channelRenderData(0)
	, zoomchannelview(0)
	, height(200)
{
	channelRenderData = new ChannelRenderData(model, this, 1);
	channelRenderData->nFrame = 1;
	channelRenderData->StartListening();
	channelRenderDatas.push_back(channelRenderData);
	setViewNumber(1);
	zoomchannelview = new ZoomChannelViewItem(&channelRenderDatas, (int)glViewers.size() - 1);
	m_Scene.addItem(zoomchannelview);
}

FileChannelGraphicView::~FileChannelGraphicView()
{
	if (channelRenderData) delete channelRenderData;
	if (zoomchannelview) delete zoomchannelview;
}

float FileChannelGraphicView::GetBeginTime()
{
	return channelRenderData ? channelRenderData->beginSecond : 0;
}

float FileChannelGraphicView::GetDuration()
{
	return channelRenderData ? channelRenderData->durationSecond : 0;
}

void FileChannelGraphicView::init()
{
	channelRenderData->UpdatePoints(m_Scene.width(), height);
	render();
}

void FileChannelGraphicView::resizeEvent(QResizeEvent*)
{
	m_Scene.setSceneRect(viewport()->rect());
	init();
	glViewers[0]->setPos(0, 0);
	if (zoomchannelview) zoomchannelview->setPos(0, height);
}

void FileChannelGraphicView::setMaxValue(double val)
{
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->MaxValue = val;
	}
	channelRenderData->UpdatePoints(m_Scene.width(), height);
	render();
}

void FileChannelGraphicView::setMinValue(double val)
{
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->MinValue = val;
	}
	channelRenderData->UpdatePoints(m_Scene.width(), height);
	render();
}

void FileChannelGraphicView::render()
{
	for (size_t i = 0; i < glViewers.size(); ++i)
	{
		glViewers[i]->update();
	}
	if (zoomchannelview) zoomchannelview->update();
	m_Scene.update();
	update();
}

void FileChannelGraphicView::updateZoom(float begin, float duration)
{
	if (zoomchannelview) zoomchannelview->Update(begin, duration);
	render();
}

void FileChannelGraphicView::setDuration(double val)
{
	for (size_t i = 0; i < channelRenderDatas.size(); ++i)
	{
		channelRenderDatas[i]->displayDuration = val;
	}
	resizeEvent(0);
	render();
}