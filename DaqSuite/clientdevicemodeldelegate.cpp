#include "clientdevicemodeldelegate.h"
#include <qcombobox.h>

ClientDeviceModelDelegate::ClientDeviceModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{

}

QWidget* ClientDeviceModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 3)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Connect");
		comboBox->addItem("Disconnect");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void ClientDeviceModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 3)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void ClientDeviceModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 3)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void ClientDeviceModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}
