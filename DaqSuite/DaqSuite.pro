#-------------------------------------------------
#
# Project created by QtCreator 2014-12-08T19:19:28
#
#-------------------------------------------------
QT       += core gui opengl network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets opengl network

TARGET = DaqSuite
TEMPLATE = app

SOURCES += main.cpp\
    channelitemdelegate.cpp \
    channelmodel.cpp \
    daqsuite.cpp \
    devicemodel.cpp \
    monitoringchannelitemdelegate.cpp \
    monitoringchannelmodel.cpp \
    channelgraphicview.cpp \
    channelrenderdata.cpp \
    channelviewitem.cpp \
    clientdevicemodel.cpp \
    clientdevicemodeldelegate.cpp \
    devicemodeldelegate.cpp \
    filechannelgraphicview.cpp \
    filechannelmodel.cpp \
    rawfilerendermodel.cpp \
    recorditemdelegate.cpp \
    recordoptionmodel.cpp

HEADERS  += channelitemdelegate.h \
    channelmodel.h \
    daqsuite.h \
    devicemodel.h \
    monitoringchannelitemdelegate.h \
    monitoringchannelmodel.h \
    resource.h \
    channelgraphicview.h \
    channelrenderdata.h \
    channelviewitem.h \
    clientdevicemodel.h \
    clientdevicemodeldelegate.h \
    devicemodeldelegate.h \
    filechannelgraphicview.h \
    filechannelmodel.h \
    rawfilerendermodel.h \
    recorditemdelegate.h \
    recordoptionmodel.h

RESOURCES += \
    daqsuite.qrc

FORMS += \
    daqsuite.ui

OTHER_FILES += \
    DaqSuite.rc

#win32: LIBS += -L$$PWD/../../../boost_1_56_0/lib32-msvc-12.0/ -lboost_thread-vc120-mt-1_56

INCLUDEPATH += $$PWD/../../../boost_1_56_0
DEPENDPATH += $$PWD/../../../boost_1_56_0

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../boost_1_56_0/lib32-msvc-12.0/boost_thread-vc120-mt-1_56.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../boost_1_56_0/lib32-msvc-12.0/libboost_thread-vc120-mt-1_56.a

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Win32/release/ -lDeviceLib
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Win32/debug/ -lDeviceLib

INCLUDEPATH += $$PWD/../DeviceLib
DEPENDPATH += $$PWD/../DeviceLib


win32: LIBS += -L$$PWD/../../../boost_1_56_0/lib64-msvc-12.0/ -lboost_thread-vc120-mt-1_56

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../../boost_1_56_0/lib64-msvc-12.0/boost_thread-vc120-mt-1_56.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../../boost_1_56_0/lib64-msvc-12.0/libboost_thread-vc120-mt-1_56.a

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../x64/release/ -lDeviceLib
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../x64/debug/ -lDeviceLib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../64bit/release/ -lDeviceLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../64bit/debug/ -lDeviceLib
else:unix: LIBS += -L$$PWD/../64bit/ -lDeviceLib
