#include "recorditemdelegate.h"
#include <qpushbutton.h>
#include <QMouseEvent>
#include <qfiledialog.h>
#include <qcombobox.h>
#include <qspinbox.h>
#include <qpainter.h>
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

RecordItemDelegate::RecordItemDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

QWidget* RecordItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 0 && index.row() == 2)
	{
		QPushButton* button = new QPushButton(parent);
		button->setText("Browse");
		connect(button, SIGNAL(clicked()), this, SLOT(buttonClicked()));
		return button;
	}
	else if (index.column() == 0 && index.row() == 7)
	{
		QSpinBox* spinBox = new QSpinBox(parent);
		spinBox->setMaximum(1000);
		spinBox->setMinimum(1);
		spinBox->setSingleStep(1);
		return spinBox;
	}
	else if (index.column() == 0 && index.row() == 11)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Upper");
		comboBox->addItem("Lower");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else if (index.column() == 0 && index.row() == 9)
	{
		QDoubleSpinBox* spinBox = new QDoubleSpinBox(parent);
		spinBox->setMaximum(2);
		spinBox->setMinimum(0);
		spinBox->setSingleStep(0.1);
		return spinBox;
	}
	else if (index.column() == 0 && index.row() == 12)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Milivolt");
		comboBox->addItem("EU");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else if (index.column() == 0 && index.row() == 3)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Stop");
		comboBox->addItem("Monitor");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else if (index.column() == 0 && index.row() == 4)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Rawfs");
		comboBox->addItem("LDSF");
		comboBox->addItem("Both");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void RecordItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 0 && index.row() == 1 && SystemManager::GetInstance()->RecordExist())
	{
		painter->save();
		QRect q = option.rect;
		painter->setPen(Qt::NoPen);
		painter->fillRect(q, QBrush(Qt::red));
		painter->setBrush(option.palette.highlightedText());
		painter->restore();
		QItemDelegate::paint(painter, option, index);
	}
	else
	{
		QItemDelegate::paint(painter, option, index);
	}
}

void RecordItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 0 && (index.row() == 3 || index.row() == 4))
	{
		QString value = index.model()->data(index, Qt::DisplayRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else if (index.column() == 0 && (index.row() == 5 || index.row() == 6))
	{
		QDoubleSpinBox* spinBox = static_cast<QDoubleSpinBox*>(editor);
		spinBox->setValue(index.model()->data(index, Qt::DisplayRole).toDouble());
	}
	else if (index.column() == 0 && (index.row() == 7))
	{
		QSpinBox* spinBox = static_cast<QSpinBox*>(editor);
		spinBox->setValue(index.model()->data(index, Qt::DisplayRole).toInt());
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void RecordItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 0 && (index.row() == 3 || index.row() == 4))
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else if (index.column() == 0 && (index.row() == 9))
	{
		QDoubleSpinBox* spinBox = static_cast<QDoubleSpinBox*>(editor);
		double value = spinBox->value();
		model->setData(index, value, Qt::EditRole);
	}
	else if (index.column() == 0 && (index.row() == 7))
	{
		QSpinBox* spinBox = static_cast<QSpinBox*>(editor);
		int value = spinBox->value();
		model->setData(index, value, Qt::EditRole);
	}
	else if (index.column() != 0 || index.row() != 2)
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void RecordItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}

void RecordItemDelegate::buttonClicked()
{
	QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Direcctory"), "");
	if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString());
}