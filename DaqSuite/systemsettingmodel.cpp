#include "systemsettingmodel.h"
#include "../DeviceLib/DeviceLib.h"
using namespace DeviceLib;

SystemSettingModel::SystemSettingModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

SystemSettingModel::~SystemSettingModel()
{
}

Qt::ItemFlags SystemSettingModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (SystemManager::GetInstance()->GetStatus() == STOPPED)
	{
		if (index.column() == 0)
			return flags | Qt::ItemIsEditable;
	}
	return flags;
}

int SystemSettingModel::rowCount(const QModelIndex &) const
{
	return 3;
}

int SystemSettingModel::columnCount(const QModelIndex &) const
{
	return 1;
}

QVariant SystemSettingModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				return QString::number(SystemManager::GetInstance()->GetLoadGSFlag());
			}
			else if (index.row() == 1)
			{
				return QString::number(SystemManager::GetInstance()->GetLoadBlackBurnBoardFlag());
			}
			else if (index.row() == 2)
			{
				return QString::number(SystemManager::GetInstance()->GetNetworkBufferSize());
			}
		}
	}

	return QVariant();
}

bool SystemSettingModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				SystemManager::GetInstance()->SetLoadGSFlag(value.toInt() != 0);
			}
			else if (index.row() == 1)
			{
				SystemManager::GetInstance()->SetLoadBlackBurnBoardFlag(value.toInt() != 0);
			}
			else if (index.row() == 2)
			{
				SystemManager::GetInstance()->SetNetworkBufferSize(value.toInt());
			}
		}
		return true;
	}
	return false;
}

QVariant SystemSettingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Infomation");
			}
		}
		if (orientation == Qt::Vertical)
		{
			if (section == 0)
			{
				return QString("Load GS Board");
			}
			else if (section == 1)
			{
				return QString("Load Black Burn Board");
			}
			else if (section == 2)
			{
				return QString("Network Buffer Size");
			}
		}
	}

	return QVariant();
}
