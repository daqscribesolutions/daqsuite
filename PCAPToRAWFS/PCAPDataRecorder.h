#ifndef PCAPDATARECORDER_H
#define PCAPDATARECORDER_H

#include <memory>
#include "../DeviceLib/DataSpeedMonitor.h"
#include "../DeviceLib/DeviceDataRecorder.h"
#include "../NetworkLib/pcap_header.h"
#include <string>

namespace NetworkLib
{
	/**
	 * PCAPDataRecorder provides the funtionality to convert pcap file to rawfs file.
	 */
	class PCAPDataRecorder
	{

	public:
		/**
		 * Full constructor.
		 * @param filename The file name to be saved.
		 * @param pcapfile The pcap file name to be converted.
		 * @param packetsize The buffer size used between server/client ADC devices.
		 */
		PCAPDataRecorder(const std::string& filename, const std::string& pcapfile, int packetsize);

		/// Default destructor.
		~PCAPDataRecorder();

		/**
		 * AddPacket converts packet data to ADC data.
		 * @param header The pcap header.
		 * @param pkt_data The pcap buffer.
		 */
		void AddPacket(const pcap_pkthdr *header, const u_char *pkt_data);


		void Convert();

	private:
		/// pcap filename.
		std::string FileName;

		/// The number of missing packets.
		long long m_MissingPackets;

		/// The number of packets to check missing packets.
		int m_Index;

		/// The number of packets for one device data.
		long m_NumberOfPackets;

		/// The current number of the received packets.using boost::asio::ip::tcp;
		long m_CurrentNumberOfPackets;

		/// The recorder for saving data to rawfs.
		std::unique_ptr<DeviceLib::DeviceDataRecorder> m_Recorder;

		/// The device data to be used to save ADC data.
		std::unique_ptr<DeviceLib::DeviceData> m_Data;

		/// The buffer to be used to resize to ADC device data.
		std::vector<char> Buffer;

		/// The book keeping for current data size.
		int m_nCurrentBufferSize;

		/// The packet size used between client/server ADC devices.
		int PACKET_SIZE;

	};

}
#endif // PCAPDATARECORDER_H
