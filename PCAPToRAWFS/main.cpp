#include "PCAPDataRecorder.h"
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc < 4)
	{
		std::cout << "usage : " << argv[0] << " [pcap file] [rawfs file] [packet size]";
	}
	else
	{
		NetworkLib::PCAPDataRecorder recorder(argv[2], argv[1], atoi(argv[3]));
		recorder.Convert();
	}
	return 0;
}