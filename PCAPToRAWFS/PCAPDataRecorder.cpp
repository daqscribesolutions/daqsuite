#include "../DeviceLib/VirtualDevice.h"
#include "PCAPDataRecorder.h"
#include <algorithm>

namespace NetworkLib
{
	PCAPDataRecorder::PCAPDataRecorder(const std::string& filename, const std::string& pcapfile, int packetsize)
		: m_MissingPackets(0)
		, m_Index(-1)
		, m_NumberOfPackets(0)
		, m_CurrentNumberOfPackets(0)
		, PACKET_SIZE(packetsize - DeviceLib::PACKET_HEADER_SIZE)
		, m_nCurrentBufferSize(0)
		, FileName(pcapfile)
	{
		Buffer.resize(PACKET_SIZE + DeviceLib::PACKET_HEADER_SIZE);
		m_Recorder = std::unique_ptr<DeviceLib::DeviceDataRecorder>(new DeviceLib::DeviceDataRecorder(filename.c_str(), 0, false));
		if (m_Recorder) m_Recorder->Open();
	}

	PCAPDataRecorder::~PCAPDataRecorder()
	{
		if (m_Recorder) m_Recorder->Close();
	}

	void PCAPDataRecorder::AddPacket(const pcap_pkthdr *header, const u_char *pkt_data)
	{
		if (m_Recorder)
		{
			int nDataSize = header->len - 54;
			if (m_nCurrentBufferSize + nDataSize < PACKET_SIZE + DeviceLib::PACKET_HEADER_SIZE)
			{
				memcpy(&Buffer[m_nCurrentBufferSize], pkt_data, nDataSize);
				m_nCurrentBufferSize += nDataSize;
				return;
			}
			int nSize = std::min<int>((PACKET_SIZE + DeviceLib::PACKET_HEADER_SIZE - m_nCurrentBufferSize), nDataSize);
			memcpy(&Buffer[m_nCurrentBufferSize], pkt_data, nSize);
			m_nCurrentBufferSize = 0;
			char* pData = &Buffer[0];
			int sampleRatePerSecond = *((int*)&pData[4]);
			int numberOfChannels = *((int*)&pData[8]);
			DeviceLib::DATA_FORMAT format = (DeviceLib::DATA_FORMAT)*((int*)&pData[16]);
			int idx = *((int*)&pData[20]);
			int duration = *((int*)&pData[0]);
			int InputMilliVoltage = *((int*)&pData[12]);
			if (sampleRatePerSecond < 0 || idx < 0 || duration < 0 || InputMilliVoltage < 0 || InputMilliVoltage > 100000)
				return;
			if (m_Index != -1)
			{
				if (idx != m_Index + 1 && idx != 0)
				{
					++m_MissingPackets;
				}
			}
			m_Index = idx;

			if (!m_Data) m_Data = std::unique_ptr<DeviceLib::DeviceData>(new DeviceLib::DeviceData((*(int*)&pData[0]), duration, sampleRatePerSecond, numberOfChannels, InputMilliVoltage, format));

			long long datasize = m_Data->GetADCCountDataSizeOfNanoSecond(duration);
			m_NumberOfPackets = static_cast<long>(ceil(datasize / (double)PACKET_SIZE));

			long length = (long)PACKET_SIZE;
			long pre_size = (long)idx * PACKET_SIZE;
			long byteSize = (long)m_Data->GetDataByteSize();
			if (length > (byteSize - pre_size))
			{
				length = (byteSize - pre_size);
			}
			if (length > 0)
			{
				memcpy(m_Data->GetADCCountData() + pre_size, &pData[DeviceLib::PACKET_HEADER_SIZE], length);
			}
			if (idx == m_NumberOfPackets - 1)
			{
				m_Recorder->UpdateExitData(m_Data.get());
				m_CurrentNumberOfPackets = m_NumberOfPackets - 1;
			}
		}
	}

	void PCAPDataRecorder::Convert()
	{
		/// pcap file hande.
		pcap_t * filehandle;

		char errbuf[PCAP_ERRBUF_SIZE];
		int nPackets = 0;
		struct pcap_pkthdr *header;
		const u_char *pkt_data;
		int res = 0;

		filehandle = pcap_open_offline(FileName.c_str(), errbuf);
		while ((res = pcap_next_ex(filehandle, &header, &pkt_data)) >= 0)
		{
			AddPacket(header, pkt_data);
		}
	}

}