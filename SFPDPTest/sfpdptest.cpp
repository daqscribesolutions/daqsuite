﻿#include "sfpdptest.h"
#include <string>
#include <ipps.h>

void initsfdp();
void runthread();
void stopthread();
void clean();

std::string logstring;

struct Thread : QThread
{
	Thread() : QThread() {}
	virtual void run()
	{
		runthread();
	}
};

SFPDPTest::SFPDPTest(QWidget *parent)
	: QMainWindow(parent)
	, thread(0)
	, timer()
{
	ui.setupUi(this);
	connect(ui.startButton, SIGNAL(clicked()), this, SLOT(start()));
	connect(ui.stopButton, SIGNAL(clicked()), this, SLOT(stop()));
	connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
}

void SFPDPTest::init()
{
	initsfdp();
	update();
}

SFPDPTest::~SFPDPTest()
{
	if (thread) delete thread;
	thread = 0;
	close();
}

void SFPDPTest::start()
{
	if (thread) delete thread;
	thread = 0;
	thread = new Thread();
	thread->start();
	timer.start(100);
	logstring += "start...\n";
}

void SFPDPTest::stop()
{
	logstring += "finished.\n";
	update();
	thread->quit();
	stopthread();
	timer.stop();
}

void SFPDPTest::update()
{
	ui.textBrowser->setText(logstring.c_str());
	QTextCursor cursor(ui.textBrowser->textCursor());
	cursor.movePosition(QTextCursor::End);
	ui.textBrowser->setTextCursor(cursor);
	ui.textBrowser->update();
}

#include <windows.h>
#include "EK-SFM2 Drv.h"
#include "EK-SFM2_XMC Reg.h"  
#include <iostream>
#include <ctime>

char temp[1023];

#define TEST_PACKET_BNUM	(1024 * 256)
#define PLX_PHYSICAL_BUFFER_SIZE	TEST_PACKET_BNUM * 20


//	SFM Handle
EK_DEVICE_OBJ hDev;

//	panel Handle
static int panelHandle;


//	송신하기위한 버퍼 카운트
int sfmW_DmaWcnt = 0;
int sfmW_DmaRcnt = 0;

//	수신하기위한 버퍼 카운트
int sfmR_DmaWcnt = 0;
int sfmR_DmaRcnt = 0;

DWORD WINAPI BuffRead_Thread(LPVOID param);
DWORD WINAPI ReadDMA_Thread(LPVOID param);

DWORD WINAPI BuffWrite_Thread(LPVOID param);
DWORD WINAPI WriteDMA_Thread(LPVOID param);

//	스레드 런 플레그
static int TS_Run = 0;

void SendStart_CB();
int RegView_CB();

bool bRun = true;
HANDLE hHandles[4] = { 0, 0, 0, 0 };

void stopthread()
{
	bRun = false;
	TS_Run = 0;
	Sleep(500);
	CloseHandle(hHandles[0]);
	CloseHandle(hHandles[1]);
	CloseHandle(hHandles[2]);
	CloseHandle(hHandles[3]);
	hHandles[0] = hHandles[1] = hHandles[2] = hHandles[3] = 0;
}

void initsfdp()
{
	// SFM 핸들 얻기
	EK_OpenDevice(0x10EE, 0x0009, &hDev);
	
	//	DMA 버퍼 잡기 기본 4개 잡힘
	EK_AllocPCIPhyBuffer(&hDev, PLX_PHYSICAL_BUFFER_SIZE, 1);	//2메가씩 잡음 (총 8메가)   
	for (int i = 0; i<4; i++)
	{
		sprintf(temp, "[%d] size : %08x(%d)    phy:%08x   user:%08x\n", i, hDev.PhysBuffer[i].Size, hDev.PhysBuffer[i].Size, (PLX_UINT_PTR)hDev.PhysBuffer[i].PhysicalAddr, (PLX_UINT_PTR)hDev.PhysBuffer[i].UserAddr);
		logstring += temp;
	}

	//	Reset
	EK_SFM_Reset(hDev, 0x800000ff);
	RegView_CB();

}

void runthread()
{
	SendStart_CB();

	while (bRun)
	{
		::Sleep(500);
	}
}

void clean()
{
	//	DMA 버퍼 헤제
	EK_FreePCIPhyBuffer(&hDev);

	//	SFM 닫기
	EK_CloseDevice(&hDev);
}

void SendStart_CB()
{
	//	송신을 위한 버퍼 카운트 초기화
	sfmW_DmaWcnt = 0;
	sfmW_DmaRcnt = 0;

	//	수신을 위한 버퍼 카운트 초기화
	sfmR_DmaWcnt = 0;
	sfmR_DmaRcnt = 0;


	//	수신 버퍼 클리어
	EK_SFM_FIFO_Flush(hDev, 2);
	//EK_SFM_FIFO_Flush(hDev, 4);
	Sleep(10);

	TS_Run = 1;


	hHandles[0] = CreateThread(NULL, 0, BuffRead_Thread, NULL, 0, NULL);   //	PCI 버퍼에서 내부 메모리로 읽는 스레드.
	hHandles[1] = CreateThread(NULL, 0, ReadDMA_Thread, NULL, 0, NULL);	 //	SFM내부 DRAM에서 PCI 버퍼로 DMA 하는 스레드

	Sleep(100);

	//	송신을 위한 스레드 생성 (1번 3번 채널 송신)
	hHandles[2] = CreateThread(NULL, 0, BuffWrite_Thread, NULL, 0, NULL);	 // 내부 메모리에서 PCI 버퍼로 쓰는 스레드   
	hHandles[3] = CreateThread(NULL, 0, WriteDMA_Thread, NULL, 0, NULL);	 // PCI 버퍼에서 SFM으로 DMA 하는 스레드
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//	송신을 위해 PCI 버퍼에 기록 하는 스레드
//	PCI 버퍼[0]을 5등분 하여 사용 1번채널에서 버퍼로 사용
//	PCI 버퍼[2]을 5등분 하여 사용 3번채널에서 버퍼로 사용
DWORD WINAPI BuffWrite_Thread(LPVOID param)
{
	static unsigned int writeData[2][TEST_PACKET_BNUM];
	unsigned int regVal = 0;
	int i, j, ptr;
	int buffOffset = 0;

	// 데이터 생성 - 각 채널별로 10001~102048, 20001~202048
	for (i = 0; i<2; i++)
		for (j = 0; j<TEST_PACKET_BNUM; j++)
			writeData[i][j] = (i + 1) * 10000 + j;


	while (TS_Run)
	{
		if (sfmW_DmaWcnt - sfmW_DmaRcnt < 5)	//overflow 방지 하기 위한 비교
		{
			for (i = 0; i<2; i++)
				for (j = 0; j<TEST_PACKET_BNUM; j++)
					writeData[i][j] = (i + 1) * 10000 + sfmW_DmaWcnt;

			//	PCI 버퍼에 복사 (DMA 송신은 다른 스레드에서)
			ptr = sfmW_DmaWcnt % 5;
			buffOffset = ptr * 4 * TEST_PACKET_BNUM;

			//	1번 채널 버퍼에 기록
			//memcpy((void*)(hDev.PhysBuffer[0].UserAddr + buffOffset), writeData[0], TEST_PACKET_BNUM * sizeof(int));
			int userAddr = (PLX_UINT_PTR)hDev.PhysBuffer[0].UserAddr + buffOffset;
			if (ippStsNoErr != ippsCopy_8u((const Ipp8u*)writeData[0], (Ipp8u*)((int*)userAddr), TEST_PACKET_BNUM * sizeof(int)))
			{
			}

			//	2번 채널 버퍼에 기록
			//memcpy((void*)(hDev.PhysBuffer[2].UserAddr + buffOffset), writeData[1], TEST_PACKET_BNUM * sizeof(int));

			sfmW_DmaWcnt++;
		}
		Sleep(10);
	}

	return 0;
}


//	송신을 위해 PCI 버퍼에서 SFM으로 DMA 하는 스레드
DWORD WINAPI WriteDMA_Thread(LPVOID param)
{
	unsigned int regVal = 0;
	int i, j, ms = 1;
	int ptr, rtn;
	int buffOffset = 0;

	while (TS_Run)
	{
		//	버퍼에 데이터가 차면...
		if (sfmW_DmaWcnt > sfmW_DmaRcnt)
		{
			//	DMA 송신만 함 (PCI 버퍼에 복사는 다른 스레드에서)
			ptr = sfmW_DmaRcnt % 5;
			buffOffset = ptr * TEST_PACKET_BNUM * 4;

			//	채널 1번 DMA 수행
			rtn = EK_SFM_DMA_Write(hDev, 1, 0, buffOffset, TEST_PACKET_BNUM);
			if (rtn < TEST_PACKET_BNUM * 4)
				printf("DMA1 fail\n");

			//	채널 3번 DMA 수행
			//rtn = EK_SFM_DMA_Write(hDev, 3, 2, buffOffset, TEST_PACKET_BNUM);
			//if (rtn < TEST_PACKET_BNUM * 4)
			//	printf("DMA3 fail\n");


			sfmW_DmaRcnt++;
		}
		Sleep(0);
	}
	return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	수신을 위해 SFM에서 PCI 버퍼로 DMA 하는 스레드
DWORD WINAPI ReadDMA_Thread(LPVOID param)
{
	unsigned int readData[2][TEST_PACKET_BNUM];
	unsigned int regVal1 = 0, regVal2 = 0;
	int i, ms = 1;
	int ptr, rtn;
	int buffOffset = 0;


	while (TS_Run)
	{
		//2번 채널에 데이터가 수신되었는지 확인: 리컨값은 8byte 단위 개수임. 즉 리턴값이 2 이면 16byte 수신된것임
		regVal1 = EK_SFM_RegRead(hDev, 2, TGT_RD_AV);

		//4번 채널에 데이터가 수신되었는지 확인: 리컨값은 8byte 단위 개수임. 즉 리턴값이 2 이면 16byte 수신된것임
		//regVal2 = EK_SFM_RegRead(hDev, 4, TGT_RD_AV);


		//	2채널 모두 데이터 수신하면 읽음
		if ((regVal1 * 2 >= TEST_PACKET_BNUM))// && (regVal2 * 2 >= TEST_PACKET_BNUM))	// 8byte 개수이기에 읽으려는 개수에 2배 해서 비교
		{
			ptr = sfmR_DmaWcnt % 5;
			buffOffset = ptr * 4 * TEST_PACKET_BNUM;

			//	채널 2번 DMA 수행
			rtn = EK_SFM_DMA_Read(hDev, 2, 1, buffOffset, TEST_PACKET_BNUM);
			if (rtn < TEST_PACKET_BNUM * 4)
				printf("DMA2 fail\n");


			//	채널 4번 DMA 수행
			//rtn = EK_SFM_DMA_Read(hDev, 4, 3, buffOffset, TEST_PACKET_BNUM);
			//if (rtn < TEST_PACKET_BNUM * 4)
			//	printf("DMA4 fail\n");


			sfmR_DmaWcnt++;
		}

		Sleep(0);
	}
	return 0;
}
//	수신을 위해 PCI 버퍼에서 읽는 스레드
//	PCI 버퍼[1]을 5등분 하여 사용 2번채널에서 버퍼로 사용
//	PCI 버퍼[3]을 5등분 하여 사용 4번채널에서 버퍼로 사용
DWORD WINAPI BuffRead_Thread(LPVOID param)
{
	static unsigned int readData[2][TEST_PACKET_BNUM];
	unsigned int regVal = 0;
	int i, j, ptr;
	int buffOffset = 0;

	double DataSize = 0;
	double t = clock();
	double duration = 0;
	while (TS_Run)
	{
		//버퍼 카운트를 가지고 버퍼에 데이터가 왔는지 확인
		if (sfmR_DmaWcnt > sfmR_DmaRcnt)
		{
			//	PCI 버퍼에서 내부 메모리로 복사
			ptr = sfmR_DmaRcnt % 5;
			buffOffset = ptr * TEST_PACKET_BNUM * 4;

			//	2번 채널 버퍼에서 내부 메모리로 복사
			//memcpy(readData[0], (void*)(hDev.PhysBuffer[1].UserAddr + buffOffset), TEST_PACKET_BNUM * sizeof(int));
			int userAddr = (PLX_UINT_PTR)hDev.PhysBuffer[1].UserAddr + buffOffset;
			if (ippStsNoErr != ippsCopy_8u((const Ipp8u*)userAddr, (Ipp8u*)((int*)readData[0]), TEST_PACKET_BNUM * sizeof(int)))
			{
			}

			//	4번 채널 버퍼에서 내부 메모리로 복사
			//memcpy(readData[1], (void*)(hDev.PhysBuffer[3].UserAddr + buffOffset), TEST_PACKET_BNUM * sizeof(int));
			duration = (clock() - t) / 1000;
			DataSize += TEST_PACKET_BNUM * sizeof(int) / 1000000.0;
			if (sfmR_DmaRcnt % 100 == 0)
			{
				double speed = DataSize / duration;
				//	수신결과 확인
				sprintf(temp, "[ch2] [0]:%d   [2047]:%d  [ch4] [0]:%d [2047]:%d %.2f MBytes\n", readData[0][0], readData[0][2047], readData[1][0], readData[1][2047], speed);
				logstring += temp;
			}

			//if (sfmR_DmaRcnt == 1)
			//	Sleep (0);
			sfmR_DmaRcnt++;
		}
		Sleep(0);
	}

	return 0;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//	레지스터 정보 보기
int RegView_CB()
{
	int i;

	sprintf(temp, "\nFirmware Version    [0x00000060] ................... 0x%08x\n", EK_SFM_RegRead(hDev, 0, BOARD_VERSION));
	logstring += temp;

	sprintf(temp, "\n================================================================\n");
	logstring += temp;
	sprintf(temp, "	Board Control register \n");
	logstring += temp;
	sprintf(temp, "================================================================\n");
	logstring += temp;
	for (i = 0; i<31; i++)
	{
		sprintf(temp, "[%2x] [%s]  %08x\n", gr[i], grs[i], EK_SFM_RegRead(hDev, 0, gr[i]));
		logstring += temp;
	}

	sprintf(temp, "\n================================================================\n");
	logstring += temp;
	sprintf(temp, "	Channel register   #1       #2       #3       #4\n");
	logstring += temp;
	sprintf(temp, "================================================================\n");
	logstring += temp;
	for (i = 0; i<25; i++)
	{
		sprintf(temp, "[%2x] [%s]  %08x %08x %08x %08x \n", cr[i], crs[i],
			EK_SFM_RegRead(hDev, 1, cr[i]), EK_SFM_RegRead(hDev, 2, cr[i]), EK_SFM_RegRead(hDev, 3, cr[i]), EK_SFM_RegRead(hDev, 4, cr[i]));
		logstring += temp;
	}

	return 0;
}