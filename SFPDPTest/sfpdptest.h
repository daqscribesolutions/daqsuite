#ifndef SFPDPTEST_H
#define SFPDPTEST_H

#include <QtWidgets/QMainWindow>
#include "ui_sfpdptest.h"
#include <qthread.h>
#include <qtimer.h>

class SFPDPTest : public QMainWindow
{
	Q_OBJECT

public:

	SFPDPTest(QWidget *parent = 0);

	~SFPDPTest();

	void init();

public slots:

	void start();

	void stop();

	void update();

private:

	QThread* thread;
	QTimer timer;
	Ui::SFPDPTestClass ui;

};

#endif // SFPDPTEST_H
