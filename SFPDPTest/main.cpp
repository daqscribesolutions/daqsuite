#include "sfpdptest.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	SFPDPTest w;
	w.init();
	w.show();
	return a.exec();
}
