#include "networkdevicemodel.h"

#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "pcap_header.h"
#include "SFPDPDevice.h"

NetworkDeviceModel::NetworkDeviceModel(QObject *parent)
	: QAbstractTableModel(parent)
{
	manager = NetworkDeviceManager::GetInstance();
}

NetworkDeviceModel::~NetworkDeviceModel()
{
}

Qt::ItemFlags NetworkDeviceModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.column() != 1 && index.column() <= 8 && !NetworkDeviceManager::GetInstance()->IsRunning())
		return flags | Qt::ItemIsEditable;
	return flags;
}

int NetworkDeviceModel::rowCount(const QModelIndex &) const
{
   return (int)manager->GetRecordDevices().size();
}

int NetworkDeviceModel::columnCount(const QModelIndex &) const
{
	return 14;
}

QVariant NetworkDeviceModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
      const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
		if (index.row() < (int)devices.size())
		{
			bool sfpdp = manager->GetNetworkDeviceSetting() == SFPDP;
			const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			if (index.column() == 0)
			{
				return QString(pDevice->Name.c_str());
			}
			else if (index.column() == 1)
			{
				return QString(pDevice->Address.c_str());
			}
			else if (index.column() == 2)
			{
				return QString(pDevice->GetRunOption() == READ_FROM_DEVICE ? "Down" : pDevice->GetRunOption() == WRITE_TO_DEVICE ? "Up" : "TestUp");
			}
			else if (index.column() == 3)
			{
				return QString(pDevice->GetActiveFlag() ? "On" : "Off");
			}
			else if (index.column() == 4)
			{
				if (sfpdp)
				{
					QString option;
					auto opt = pDevice->GetSFPDPSpeedOption();
					if (opt == 0)
					{
						option = "1 G";
					}
					else if (opt == 1)
					{
						option = "2.1 G";
					}
					else if (opt == 2)
					{
						option = "2.5 G";
					}
					else if (opt == 3)
					{
						option = "3.1 G";
					}
					else if (opt == 4)
					{
						option = "4.25 G";
					}
					return option;
				}
				else
				{
					return QString(pDevice->PCAPFilter.c_str());
				}
			}
			else if (index.column() == 5)
			{
				return QString::number(pDevice->GetRunNumber());
			}
			else if (index.column() == 6 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				return QString::number(pDevice->GetRelayID());
			}
			else if (index.column() == 7 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				return QString::number(pDevice->GetRelayOffset() / 1000000000.0, 'f', 2);
			}
			else if (index.column() == 8 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				return QString::number(pDevice->GetRelayDuration() / 1000000000.0, 'f', 2);
			}
			else if (index.column() == 9 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				return QString::number(pDevice->GetRelayRecordDuration() / 1000000, 'f', 2);
			}
			else if (index.column() == 10)
			{
				return QString::number(pDevice->GetCurrentSpeed() / 1000000.0, 'f', 2) + "/" + QString::number(pDevice->GetCurrentSampleSpeed() / 1000000.0, 'f', 2);
			}
			else if (index.column() == 11)
			{
				return QString::number(pDevice->GetTotalDataSize() / 1000000.0, 'f', 2);
			}
			else if (index.column() == 12)
			{
				return pDevice->IsRecording() ? (pDevice->GetRunOption() == READ_FROM_DEVICE ? "Recording" : "Relaying") : "Stopped";
			}
			else if (index.column() == 13)
			{
				return pDevice->ErrorInfo.c_str();
			}
		}
	}
	return QVariant();
}

bool NetworkDeviceModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
      const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
		if (index.row() < (int)devices.size())
		{
			bool sfpdp = NetworkDeviceManager::GetInstance()->GetNetworkDeviceSetting() == SFPDP;
			const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			if (index.column() == 0)
			{
				pDevice->Name = value.toString().toStdString();
			}
			else if (index.column() == 1)
			{
				pDevice->Address = value.toString().toStdString();
			}
			else if (index.column() == 2)
			{
				pDevice->SetRunOption(value.toString() == "Down" ? READ_FROM_DEVICE : WRITE_TO_DEVICE );
			}
			else if (index.column() == 3)
			{
				pDevice->SetActiveFlag(value.toString() == "On");
			}
			else if (index.column() == 4)
			{
				if (sfpdp)
				{
					pDevice->SetSFPDPSpeedOption((SFPDPSpeed)value.toInt());
				}
				else
				{
					pDevice->PCAPFilter = value.toString().toStdString();
				}
			}
			else if (index.column() == 5)
			{
				pDevice->SetRunNumber(value.toInt());
			}
			else if (index.column() == 6 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				pDevice->SetRelayID(value.toInt());
			}
			else if (index.column() == 7 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				pDevice->SetRelayOffset(value.toDouble() * 1000000000);
			}
			else if (index.column() == 8 && pDevice->GetRunOption() == WRITE_TO_DEVICE)
			{
				pDevice->SetRelayDuration(value.toDouble() * 1000000000);
			}
			return true;
		}
	}
	return false;
}

QVariant NetworkDeviceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			bool sfpdp = manager->GetNetworkDeviceSetting() == SFPDP;
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Information");
			}
			else if (section == 2)
			{
				return QString("Up/Down");
			}
			else if (section == 3)
			{
				return QString("On/Off");
			}
			else if (section == 4)
			{
				if (sfpdp)
				{
					return QString("Link Rate");
				}
				else
				{
					return QString("Filter");
				}
			}
			else if (section == 5)
			{
				return QString("Run Number");
			}
			else if (section == 6)
			{
				return QString("Record ID");
			}
			else if (section == 7)
			{
				return QString("Replay Offset");
			}
			else if (section == 8)
			{
				return QString("Replay Duration");
			}
			else if (section == 9)
			{
				return QString("Record Duration");
			}
			else if (section == 10)
			{
				return QString("Data Speed/Sample Speed\n(MBytes/sec)");
			}
			else if (section == 11)
			{
				return QString("Record Data\n(MBytes)");
			}
			else if (section == 12)
			{
				return QString("Record Status");
			}
			else if (section == 13)
			{
				return QString("Error");
			}
		}
		else
		{
			return QString::number(section + 1);
		}
	}

	return QVariant();
}
