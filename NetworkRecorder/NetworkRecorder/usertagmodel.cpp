#include "usertagmodel.h"
#include "usertagmanager.h"

UserTagModel::UserTagModel(QObject *parent)
   : QAbstractTableModel(parent)
{
}

UserTagModel::~UserTagModel()
{
}

Qt::ItemFlags UserTagModel::flags(const QModelIndex &index) const
{
   Qt::ItemFlags flags = QAbstractTableModel::flags(index);
   return flags | Qt::ItemIsEditable;
}

int UserTagModel::rowCount(const QModelIndex &) const
{
   auto tags = NetworkLib::UserTagManager::GetInstance()->Tags;
   return (int)tags.size();
}

int UserTagModel::columnCount(const QModelIndex &) const
{
   return 2;
}

QVariant UserTagModel::data(const QModelIndex &index, int role) const
{
   if (role == Qt::DisplayRole || role == Qt::EditRole)
   {
      auto tags = NetworkLib::UserTagManager::GetInstance()->Tags;
      if (index.row() < (int)tags.size())
      {
         auto tag = tags[index.row()];
         if (index.column() == 0)
         {
            return tag.Name.c_str();
         }
         else if (index.column() == 1)
         {
            return tag.Description.c_str();
         }
      }
   }
   return QVariant();
}

bool UserTagModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
   if (role == Qt::EditRole)
   {
      auto& tags = NetworkLib::UserTagManager::GetInstance()->Tags;
      if (index.row() < (int)tags.size())
      {
         auto& tag = tags[index.row()];
         if (index.column() == 0)
         {
            tag.Name = value.toString().toStdString();
         }
         else if (index.column() == 1)
         {
            tag.Description = value.toString().toStdString();
         }
         return true;
      }
   }
   return false;
}

QVariant UserTagModel::headerData(int section, Qt::Orientation orientation, int role) const
{
   if (role == Qt::DisplayRole)
   {
      if (orientation == Qt::Horizontal)
      {
         if (section == 0)
         {
            return QString("Name");
         }
         else if (section == 1)
         {
            return QString("Description");
         }
      }
      else
      {
          return QString::number(section + 1);
      }
   }
   return QVariant();
}
