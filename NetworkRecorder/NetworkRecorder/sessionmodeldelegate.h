#ifndef SESSIONMODELDELEGATE_H
#define SESSIONMODELDELEGATE_H

#include <QItemDelegate>

class SessionModelDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	SessionModelDelegate(QObject *parent);

	~SessionModelDelegate();

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

public slots:

	void buttonClickedRecord();

   void buttonClickedReplay();
};

#endif // SESSIONMODELDELEGATE_H
