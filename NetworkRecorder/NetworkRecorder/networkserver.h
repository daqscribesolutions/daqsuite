#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include <iostream>
#include "NetworkDeviceManager.h"
#include "StorageManager.h"
#include "logger.h"
#include <string>
#include <QtNetwork>
#include <QTcpSocket>

class NetworkServer : public QObject
{
	Q_OBJECT

public:

	NetworkServer();

	~NetworkServer();

	void init(short port);

	void reset_message();

	void process_message();

public slots:

	void handle_receive_from();

signals:

	void updateui();

private:

	NetworkLib::NetworkDeviceManager* manager;

	NetworkLib::Logger* logger;
	
	QUdpSocket* udpSocket;

	NetworkLib::FileLog log;

	enum { max_length = 512 };

	char data_[max_length];
	
	int port;

	std::string result;

};

#endif // NETWORKSERVER_H
