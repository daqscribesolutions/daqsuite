#include "sessionmodel.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
#include <QtCore/QTime>

using namespace NetworkLib;

SessionModel::SessionModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

SessionModel::~SessionModel()
{
}

Qt::ItemFlags SessionModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.row() > 1 && index.column() == 1) return flags;
	return flags | Qt::ItemIsEditable;
}

int SessionModel::rowCount(const QModelIndex &) const
{
   return 4;
}

int SessionModel::columnCount(const QModelIndex &) const
{
	return 1;
}

QVariant SessionModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordName().c_str());
			}
			else if (index.row() == 1)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordPath().c_str());
			}
         else if (index.row() == 2)
         {
            return QString::number(DeviceRecordSetting::GetInstance()->GetRecordMaximumSize() / 1000000.0, 'f', 2);
         }
         else if (index.row() == 3)
         {
			 auto t = DeviceRecordSetting::GetInstance()->RecordTriggerTime;
			 return QDateTime(QDate(t.tm_year + 1900, t.tm_mon + 1, t.tm_mday), QTime(t.tm_hour, t.tm_min, t.tm_sec));
         }
         else if (index.row() == 4)
         {
            return QString(DeviceRecordSetting::GetInstance()->GetReplayPath().c_str());
         }
      }
	}
	return QVariant();
}

bool SessionModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				DeviceRecordSetting::GetInstance()->SetRecordName(value.toString().toStdString());
			}
			else if (index.row() == 1)
			{
				DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString());
			}
         else if (index.row() == 2)
         {
            DeviceRecordSetting::GetInstance()->SetRecordMaximumSize((long long)(value.toDouble() * 1000000));
         }
         else if (index.row() == 3)
         {
			 auto& t = DeviceRecordSetting::GetInstance()->RecordTriggerTime;
			 auto ti = value.toDateTime();
			 t.tm_year = ti.date().year() - 1900;
			 t.tm_mon = ti.date().month()-1;
			 t.tm_mday = ti.date().day();
			 t.tm_hour = ti.time().hour();
			 t.tm_min = ti.time().minute();
			 t.tm_sec = ti.time().second();
         }
         else if (index.row() == 4)
         {
            DeviceRecordSetting::GetInstance()->SetReplayPath(value.toString().toStdString());
         }
         return true;
		}
	}
	return false;
}

QVariant SessionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Vertical)
		{
			if (section == 0)
			{
            return QString("Record Session");
			}
			else if (section == 1)
			{
            return QString("Record Path");
			}
         else if (section == 2)
         {
            return QString("Maximum Single File Size (Mb)");
         }
         else if (section == 3)
         {
            return QString("Trigger Time");
         }
         else if (section == 4)
         {
            return QString("Replay Path");
         }
		}
	}

	return QVariant();
}
