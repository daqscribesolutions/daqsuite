#include "settingmodeldelegate.h"
#include <qcombobox.h>

SettingModelDelegate::SettingModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

SettingModelDelegate::~SettingModelDelegate()
{
}

QWidget* SettingModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("1 Gb");
		comboBox->addItem("2.1 Gb");
		comboBox->addItem("2.5 Gb");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void SettingModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void SettingModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		int value = comboBox->currentIndex();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void SettingModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}
