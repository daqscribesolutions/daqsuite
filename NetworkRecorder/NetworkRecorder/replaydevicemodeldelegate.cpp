#include "replaydevicemodeldelegate.h"
#include <qpushbutton.h>
#include <QMouseEvent>
#include <qfiledialog.h>
#include <QComboBox>
#include "NetworkDeviceManager.h"
#include "SFPDPDevice.h"
using namespace NetworkLib;

int nSelectedIdx = 0;
ReplayDeviceModelDelegate::ReplayDeviceModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
	manager = NetworkDeviceManager::GetInstance();
}

QWidget* ReplayDeviceModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
		QPushButton* button = new QPushButton(parent);
		button->setText("Browse");
		nSelectedIdx = index.row();
		connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord()));
		return button;
	}
	else if (index.column() == 3)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("Time(Sec)");
		comboBox->addItem("Time Tag");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void ReplayDeviceModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
	}
	else if (index.column() == 3)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void ReplayDeviceModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.column() == 2)
	{
	}
	else if (index.column() == 3)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		QString value = comboBox->currentText();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void ReplayDeviceModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}

void ReplayDeviceModelDelegate::buttonClickedRecord()
{
	const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetReplayDevices();
	if (nSelectedIdx < (int)devices.size())
	{
		const std::shared_ptr<INetworkDevice>& pDevice = devices[nSelectedIdx];
		QString fileName = QFileDialog::getExistingDirectory(0, tr("Replay Directory"), pDevice->GetRecordFile().c_str());
		if (fileName != "")
		{
			pDevice->SetReplayFile(fileName.toStdString());
		}
	}
}