#include "firmata.h"
#include <fstream>
#include <iomanip>
#include <qsettings.h>

namespace Firmata
{
	FirmataController::FirmataController()
		: QObject()
		, pins(128)
		, settings(new SettingsDialog())
	{
		serial = new QSerialPort(this);
		connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
		readSetting();
	}

	FirmataController::~FirmataController()
	{
		writeSetting();
		delete serial;
		delete settings;
	}

	void FirmataController::setIcon(const QIcon& icon)
	{
		if (settings)
		{
			settings->setWindowIcon(icon);
		}
	}

	void FirmataController::openui()
	{
		SettingsDialog::Settings& p = settings->settings();
		p.name = serial->portName();
		p.baudRate = serial->baudRate();
		p.dataBits = serial->dataBits();
		p.parity = serial->parity();
		p.stopBits = serial->stopBits();
		p.flowControl = serial->flowControl();
		settings->updateUI();
		settings->show();
	}

	void FirmataController::open()
	{
		SettingsDialog::Settings p = settings->settings();
		serial->setPortName(p.name);
		serial->setBaudRate(p.baudRate);
		serial->setDataBits(p.dataBits);
		serial->setParity(p.parity);
		serial->setStopBits(p.stopBits);
		serial->setFlowControl(p.flowControl);
		parse_count = 0;
		parse_command_len = 0;

		if (serial->open(QIODevice::ReadWrite))
		{
			QByteArray buf;
			buf.resize(3);
			buf[0] = (unsigned char)START_SYSEX;
			buf[1] = (unsigned char)REPORT_FIRMWARE;
			buf[2] = (unsigned char)END_SYSEX;
			writeData(buf);
		}
	}

	void FirmataController::writeSetting()
	{
		QSettings setting("DaqSuite", "NetworkRecorder");
		SettingsDialog::Settings& p = settings->settings();
		setting.beginGroup("Firmata");
		setting.setValue("name", p.name);
		setting.setValue("baudRate", (int)p.baudRate);
		setting.setValue("dataBits", (int)p.dataBits);
		setting.setValue("parity", (int)p.parity);
		setting.setValue("stopBits", (int)p.stopBits);
		setting.setValue("flowControl", (int)p.flowControl);
		setting.endGroup();
	}

	void FirmataController::readSetting()
	{
		QSettings setting("DaqSuite", "NetworkRecorder");
		setting.beginGroup("Firmata");
		SettingsDialog::Settings& p = settings->settings();
		p.name = setting.value("name", "com1").toString();
		p.baudRate = (QSerialPort::BaudRate)setting.value("baudRate", QSerialPort::Baud9600).toInt();
		p.dataBits = (QSerialPort::DataBits)setting.value("dataBits", QSerialPort::Data5).toInt();
		p.parity = (QSerialPort::Parity)setting.value("parity", QSerialPort::NoParity).toInt();
		p.stopBits = (QSerialPort::StopBits)setting.value("stopBits", QSerialPort::OneStop).toInt();
		p.flowControl = (QSerialPort::FlowControl)setting.value("flowControl", QSerialPort::NoFlowControl).toInt();
		setting.endGroup();
		serial->setPortName(p.name);
		serial->setBaudRate(p.baudRate);
		serial->setDataBits(p.dataBits);
		serial->setParity(p.parity);
		serial->setStopBits(p.stopBits);
		serial->setFlowControl(p.flowControl);
	}

	void FirmataController::close()
	{
		if (serial->isOpen())
			serial->close();
	}

	void FirmataController::writeData(const QByteArray &data)
	{
		serial->write(data);
	}

	void FirmataController::readData()
	{
		QByteArray data = serial->read(40);
		parse(data);
	}

	void FirmataController::parse(QByteArray data)
	{
		unsigned char *p, *end;
		int len = data.size();
		p = (unsigned char *)data.data();
		end = p + len;
		unsigned char msn = 0;
		for (p = (unsigned char *)data.data(); p < end; ++p)
		{
			msn = *p & 0xF0;
			if (msn == 0xE0 || msn == 0x90 || *p == 0xF9)
			{
				parse_command_len = 3;
				parse_count = 0;
			}
			else if (msn == 0xC0 || msn == 0xD0)
			{
				parse_command_len = 2;
				parse_count = 0;
			}
			else if (*p == START_SYSEX)
			{
				parse_count = 0;
				parse_command_len = 4096;
			}
			else if (*p == END_SYSEX)
			{
				parse_command_len = parse_count + 1;
			}
			else if (*p & 0x80)
			{
				parse_command_len = 1;
				parse_count = 0;
			}
			if (parse_count < 4096)
			{
				parse_buf.push_back(*p);
				++parse_count;
			}
			if (parse_count == parse_command_len)
			{
				on_message();
				parse_buf.clear();
			}
		}
	}

	void FirmataController::on_message(void)
	{
		if (parse_buf.size() == 0) return;

		unsigned char cmd = (parse_buf[0] & 0xF0);
		if (cmd == 0xE0 && parse_count == 3)
		{
			int analog_ch = (parse_buf[0] & 0x0F);
			int analog_val = parse_buf[1] | (parse_buf[2] << 7);
			for (int idx = 0; idx < 128; idx++)
			{
				if (pins[idx].analog_channel == analog_ch)
				{
					pins[idx].value = analog_val;
				}
			}
		}
		else if (cmd == 0x90 && parse_count == 3)
		{
			int port_num = (parse_buf[0] & 0x0F);
			int port_val = parse_buf[1] | (parse_buf[2] << 7);
			int idx = port_num * 8;

			for (int mask = 1; mask & 0xFF; mask <<= 1, ++idx)
			{
				if (pins[idx].mode == MODE_INPUT || pins[idx].mode == MODE_PULLUP)
				{
					unsigned int val = (port_val & mask) ? 1 : 0;
					if (pins[idx].value != val)
					{
						emit on_input_up(idx);
						pins[idx].value = val;
					}
				}
			}
		}
		else if (parse_buf[0] == (unsigned char)START_SYSEX && parse_buf[parse_count - 1] == (unsigned char)END_SYSEX)
		{
			if (parse_buf[1] == (unsigned char)REPORT_FIRMWARE)
			{
				char name[140];
				int len = 0;
				for (int i = 4; i < parse_count - 2; i += 2)
				{
					name[len++] = (parse_buf[i] & 0x7F)
						| ((parse_buf[i + 1] & 0x7F) << 7);
				}
				name[len++] = '-';
				name[len++] = parse_buf[2] + '0';
				name[len++] = '.';
				name[len++] = parse_buf[3] + '0';
				name[len++] = 0;
				firmata_name = name;
				QByteArray buf;
				buf.resize(80);
				len = 0;
				buf[len++] = (unsigned char)START_SYSEX;
				buf[len++] = (unsigned char)ANALOG_MAPPING_QUERY; // read analog to idx # info
				buf[len++] = (unsigned char)END_SYSEX;
				buf[len++] = (unsigned char)START_SYSEX;
				buf[len++] = (unsigned char)CAPABILITY_QUERY; // read capabilities
				buf[len++] = (unsigned char)END_SYSEX;
				for (int i = 0; i < 16; i++)
				{
					buf[len++] = 0xC0 | i;  // report analog
					buf[len++] = 1;
					buf[len++] = 0xD0 | i;  // report digital
					buf[len++] = 1;
				}
				buf.resize(len);
				writeData(buf);
			}
			else if (parse_buf[1] == (unsigned char)CAPABILITY_RESPONSE)
			{
				int idx, i, n;
				for (idx = 0; idx < (int)pins.size(); ++idx)
				{
					pins[idx].supported_modes = 0;
				}
				for (i = 2, n = 0, idx = 0; i < parse_count; ++i)
				{
					if (parse_buf[i] == (unsigned char)127)
					{
						++idx;
						n = 0;
						continue;
					}
					if (n == 0)
					{
						// first byte is supported mode
						pins[idx].supported_modes |= ((unsigned long long)1 << parse_buf[i]);
					}
					n = n ^ 1;
				}
				// send a state query for for every idx with any modes
				for (idx = 0; idx < (int)pins.size(); ++idx)
				{
					QByteArray buf;
					buf.resize(512);
					int len = 0;
					if (pins[idx].supported_modes)
					{
						buf[len++] = (unsigned char)START_SYSEX;
						buf[len++] = (unsigned char)PIN_STATE_QUERY;
						buf[len++] = (unsigned char)idx;
						buf[len++] = (unsigned char)END_SYSEX;
					}
					buf.resize(len);
					writeData(buf);
				}
			}
			else if (parse_buf[1] == (unsigned char)ANALOG_MAPPING_RESPONSE)
			{
				int idx = 0;

				for (int i = 2; i < parse_count - 1 && i < (int)pins.size(); ++i)
				{
					pins[idx].analog_channel = parse_buf[i];
					idx++;
				}
			}
			else if (parse_buf[1] == (unsigned char)PIN_STATE_RESPONSE && parse_count >= 6)
			{
				int idx = parse_buf[2];
				if (idx >= 0 && idx < (int)pins.size())
				{
					pins[idx].mode = parse_buf[3];
					pins[idx].value = parse_buf[4];
					if (parse_count > 6) pins[idx].value |= (parse_buf[5] << 7);
					if (parse_count > 7) pins[idx].value |= (parse_buf[6] << 14);
				}
			}
		}
	}

	void FirmataController::changemode(int idx, PinMode mode)
	{
		if (idx < 0 || idx >= (int)pins.size()) return;
		QByteArray buf;
		buf.resize(3);
		buf[0] = (unsigned char)0xF4;
		buf[1] = idx;
		buf[2] = mode;
		writeData(buf);
		pins[idx].mode = (unsigned char)mode;
	}

	void FirmataController::setoutput(int idx, char val)
	{
		if (idx < 0 || idx >= (int)pins.size()) return;
		auto& p = pins[idx];
		if ((unsigned char)(MODE_INPUT | MODE_OUTPUT | MODE_PULLUP) & p.mode)
		{
			p.value = val;
			int port_num = idx / 8;
			int port_val = 0;
			for (int i = 0; i < 8; ++i)
			{
				int pi = port_num * 8 + i;
				if (pins[pi].mode == (unsigned char)MODE_OUTPUT
					|| pins[pi].mode == (unsigned char)MODE_INPUT
					|| pins[pi].mode == (unsigned char)MODE_PULLUP)
				{
					if (pins[pi].value)
					{
						port_val |= (1 << i);
					}
				}
			}
			QByteArray buf;
			buf.resize(3);
			buf[0] = 0x90 | port_num;
			buf[1] = port_val & 0x7F;
			buf[2] = (port_val >> 7) & 0x7F;
			writeData(buf);
		}
		else if ((unsigned char)MODE_SERVO & p.mode)
		{
			if (idx <= 15 && val <= 16383)
			{
				QByteArray buf;
				buf.resize(3);
				buf[0] = 0xE0 | idx;
				buf[1] = val & 0x7F;
				buf[2] = (val >> 7) & 0x7F;
				writeData(buf);
			}
			else
			{
				QByteArray buf;
				buf.resize(4);
				buf[0] = (unsigned char)0xF0;
				buf[1] = 0x6F;
				buf[2] = idx;
				buf[3] = val & 0x7F;
				if (val > 0x00000080) buf.append((val >> 7) & 0x7F);
				if (val > 0x00004000) buf.append((val >> 14) & 0x7F);
				if (val > 0x00200000) buf.append((val >> 21) & 0x7F);
				if (val > 0x10000000) buf.append((val >> 28) & 0x7F);
				buf.append((unsigned char)0xF7);
				writeData(buf);
			}
		}
	}
}
