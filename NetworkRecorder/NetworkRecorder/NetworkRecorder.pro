TEMPLATE = app
TARGET = NetworkRecorder
DESTDIR = ../build
QT += core widgets gui network serialport

win32:RC_FILE = $${PWD}/NetworkRecorder.rc

win32: DEFINES += _XKEYCHECK_H

DEFINES += QT_DLL _ALLOW_KEYWORD_MACROS QT_WIDGETS_LIB
INCLUDEPATH += . \
    ./../../../../boost_1_59_0

win32:contains(QMAKE_HOST.arch, x86_64) LIBS += -L"./../../../../boost_1_59_0/lib64-msvc-12.0/"
else:win32: LIBS += -L"./../../../../boost_1_59_0/lib32-msvc-12.0/"

win32: INCLUDEPATH += ./../../../ThirdParty/winpcap/Include \
    ./../../../ThirdParty/SFPDP/include

win32:contains(QMAKE_HOST.arch, x86_64) LIBS += -L"./../../../ThirdParty/winpcap/Lib/x64/"
else:win32: LIBS += -L"./../../../ThirdParty/winpcap/Lib/"

win32: LIBS += -lwpcap \
    -lPacket\
    -lWs2_32 \
    -L./../../../ThirdParty/Napatech/lib -llibntapi64 \
    -L./../../../ThirdParty/Napatech/lib -llibntutil64

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib
else:unix: LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib

INCLUDEPATH += $$PWD/../../NetworkLib
DEPENDPATH += $$PWD/../../NetworkLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/NetworkLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/NetworkLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a

include(NetworkRecorder.pri)
