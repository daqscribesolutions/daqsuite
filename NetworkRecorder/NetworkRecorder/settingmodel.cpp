#include "settingmodel.h"
#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "StorageManager.h"

SettingModel::SettingModel(QObject *parent)
	: QAbstractTableModel(parent)
{
	drives = StorageManager::HardDiskInformation();
}

SettingModel::~SettingModel()
{
}

Qt::ItemFlags SettingModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	return flags;
}

int SettingModel::rowCount(const QModelIndex &) const
{
   return (int)drives.size();
}

int SettingModel::columnCount(const QModelIndex &) const
{
	return 4;
}

QVariant SettingModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		int col = index.column();
		int row = index.row();
      if (row >= 0 && row < (int)drives.size())
		{
			auto& drive = drives[row];
			if (col == 0)
			{
				return drive.info.c_str();
			}
			else if (col == 1)
			{
				return QString::number(drive.available / 1000.0, 'f', 1);
			}
			else if (col == 2)
			{
				return QString::number(drive.capacity / 1000.0, 'f', 1);
			}
			else if (col == 3)
			{
				return QString::number(drive.free / 1000.0, 'f', 1);
			}
		}
	}
	return QVariant();
}

bool SettingModel::setData(const QModelIndex &, const QVariant &, int)
{
   return false;
}

QVariant SettingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Drive");
			}
			else if (section == 1)
			{
				return QString("Available Space (GBytes)");
			}
			else if (section == 2)
			{
				return QString("Capacity (GBytes)");
			}
			else if (section == 3)
			{
				return QString("Free Space (GBytes)");
			}
		}
	}

	return QVariant();
}
