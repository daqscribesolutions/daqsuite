#include "sessionmodeldelegate.h"
#include "NetworkDeviceManager.h"
#include "SFPDPDevice.h"
using namespace NetworkLib;
#include "DeviceRecordSetting.h"
#include <QSpinBox>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <QMouseEvent>
#include <qfiledialog.h>
#include <qpainter.h>

SessionModelDelegate::SessionModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
}

SessionModelDelegate::~SessionModelDelegate()
{
}

void SessionModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QItemDelegate::paint(painter, option, index);
}

QWidget* SessionModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.row() == 1 || index.row() == 4)
	{
		QPushButton* button = new QPushButton(parent);
		button->setText("Browse");
		if (index.row() == 1)
			connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord()));
		if (index.row() == 4)
			connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedReplay()));
		return button;
	}
	return QItemDelegate::createEditor(parent, option, index);
}

void SessionModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.row() == 1 || index.row() == 4)
	{
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void SessionModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.row() == 1 || index.row() == 4)
	{
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void SessionModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}

void SessionModelDelegate::buttonClickedRecord()
{
	QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
	if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString());
}

void SessionModelDelegate::buttonClickedReplay()
{
	QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
	if (fileName != "") DeviceRecordSetting::GetInstance()->SetReplayPath(fileName.toStdString());
}
