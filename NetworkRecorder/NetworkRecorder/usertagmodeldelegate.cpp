#include "usertagmodeldelegate.h"
#include <QPushButton>

UserTagModelDelegate::UserTagModelDelegate(QObject *parent)
   : QItemDelegate(parent)
{
}

UserTagModelDelegate::~UserTagModelDelegate()
{
}

void UserTagModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QItemDelegate::paint(painter, option, index);
}

QWidget* UserTagModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   if (index.column() == 2)
   {
      QPushButton* button = new QPushButton(parent);
      button->setText("Add");
      connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord()));
      return button;
   }
   return QItemDelegate::createEditor(parent, option, index);
}

void UserTagModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   if (index.row() == 2)
   {
   }
   else
   {
      QItemDelegate::setEditorData(editor, index);
   }
}

void UserTagModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   if (index.row() == 2)
   {
   }
   else
   {
      QItemDelegate::setModelData(editor, model, index);
   }
}

void UserTagModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
   editor->setGeometry(option.rect);
}

void UserTagModelDelegate::buttonClickedRecord()
{
}

void UserTagModelDelegate::buttonClickedReplay()
{
}
