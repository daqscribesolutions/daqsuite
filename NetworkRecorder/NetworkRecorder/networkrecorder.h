#ifndef NETWORKRECORDER_H
#define NETWORKRECORDER_H

#include <QtWidgets/QMainWindow>
#include "ui_networkrecorder.h"
#include "recordchannelmodel.h"
#include "replaychannelmodel.h"
#include "replaydevicemodeldelegate.h"
#include "settingmodel.h"
#include "settingmodeldelegate.h"
#include "sessionmodel.h"
#include "sessionmodeldelegate.h"
#include "usertagmodel.h"
#include <QTimer>
#include <QProgressBar>
#include <qsettings.h>
#include <memory>

class RemoteUI;
class NetworkServer;

class NetworkRecorder : public QMainWindow
{
	Q_OBJECT

public:

	NetworkRecorder(const std::string& path, QWidget *parent = 0);

	~NetworkRecorder();

	public slots :

	void Monitor();

	void StartRecord();

	void StartRecordAll();

	void StopRecord();

	void UpdateStatus();

	void ResetUi();

	void SetUp();

	void SetDown();

	void Enable();

	void Disable();

	void UpdateSessions();

	void InsertTagBefore();

	void InsertTagAfter();

	void RemoveTag();

	void AddTagToSession();

	void AddResetTagToSession();

	void LoadSetting();

	void SaveSetting();

	void SettingServer();

	void InitServer();

private:

	void LoadUISetting();

	void SaveUISetting();

	Ui::NetworkRecorderClass ui;

	RecordChannelModel recordModel;

	ReplayChannelModel replayModel;

	ReplayDeviceModelDelegate deviceModelDelegate;

	SettingModel settingModel;

	SettingModelDelegate settingModelDelegate;

	SessionModel sessionModel;

	SessionModelDelegate sessionModelDelegate;

	UserTagModel userTagModel;

	std::unique_ptr<QProgressBar> progressbar;

	std::unique_ptr<RemoteUI> remoteUI;

	QTimer *timer;

	QTimer *timeTriggerTimer;

	std::string m_path;

	NetworkServer * server;
};

#endif // NETWORKRECORDER_H
