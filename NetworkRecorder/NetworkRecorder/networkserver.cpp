#include "networkserver.h"
#include "DeviceRecordSetting.h"
using namespace NetworkLib;

NetworkServer::NetworkServer()
	: manager(NetworkDeviceManager::GetInstance())
	, port(0)
	, udpSocket(0)
{
	logger = Logger::GetInstance();
	logger->AddLog(&log);
}

NetworkServer::~NetworkServer()
{
	delete udpSocket;
}

void NetworkServer::init(short port)
{
	if (udpSocket != 0)
	{
		delete udpSocket;
	}
	udpSocket = new QUdpSocket(this);
	udpSocket->bind(QHostAddress::AnyIPv4, port);
	connect(udpSocket, SIGNAL(readyRead()), this, SLOT(handle_receive_from()));
	reset_message();
}

void NetworkServer::reset_message()
{
	memset(data_, 0, max_length);
	result = "";
}

void NetworkServer::process_message()
{
	std::vector<std::string> fields = GetFields(data_, ",");
	reset_message();
	if (fields.size() > 0)
	{
		if (fields[0] == "GET_CHANNEL_LIST")
		{
			result = "GET_CHANNEL_LIST,ACK,RECORD_CH 1 2, REPLAY_CH 1 2";
		}
		else if (fields[0] == "GET_STATUS")
		{
			std::string error = manager->GetErrorInfo();
			if (error != "")
			{
				result = "STATUS,ERROR," + error;
			}
			else if (manager->IsRunning())
			{
				result = "STATUS,BUSY";
			}
			else
			{
				result = "STATUS,IDLE";
			}
		}
		else if (fields[0] == "START_SESSION")
		{
			if (manager->IsRunning())
			{
				result = "START_SESSION,NACK,0";
			}
			else
			{
				manager->Monitor();
				std::string error = manager->GetErrorInfo();
				if (error != "")
				{
					result = "START_SESSION,NACK,1";
				}
				else if (manager->IsRunning())
				{
					result = "START_SESSION,ACK";
				}
				else
				{
					result = "START_SESSION,NACK,2";
				}
			}
		}
		else if (fields[0] == "STOP_SESSION")
		{
			if (!manager->IsRunning())
			{
				result = "STOP_SESSION,NACK,0";
			}
			else
			{
				manager->Stop();
				std::string error = manager->GetErrorInfo();
				if (error == "")
				{
					result = "STOP_SESSION,ACK";
				}
				else
				{
					result = "STOP_SESSION,NACK,2";
				}
			}
		}
		else if (fields[0] == "GET_REMAINING_RECORD_TIME" &&  fields.size() > 1)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						result = "REMAINING_RECORD_TIME,ACK," + std::to_string(pdev->RemainRecordTime() / 1000.0);
					}
					else
					{
						result = "REMAINING_RECORD_TIME,NACK,0";
					}
				}
				else
				{
					result = "REMAINING_RECORD_TIME,NACK,1";
				}
			}
			else
			{
				result = "REMAINING_RECORD_TIME,NACK,2";
			}
		}
		else if (fields[0] == "GET_RECORD_SPEED" &&  fields.size() > 1)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						result = "RECORD_SPEED,ACK," + std::to_string(pdev->RecordSpeed() / 1000000.0);
					}
					else
					{
						result = "RECORD_SPEED,NACK,0";
					}
				}
				else
				{
					result = "RECORD_SPEED,NACK,1";
				}
			}
			else
			{
				result = "RECORD_SPEED,NACK,2";
			}
		}
		else if (fields[0] == "GET_FREE_SPACE" &&  fields.size() > 1)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						double avail = 0, free = 0, capacity = 0;
						std::string p = pdev->GetRecordFile();
						StorageManager::GetSpaceInformation(p.substr(0,3), avail, capacity, free);
						result = "FREE_SPACE,ACK," + std::to_string(free / 1000);
					}
					else
					{
						result = "FREE_SPACE,NACK,0";
					}
				}
				else
				{
					result = "FREE_SPACE,NACK,1";
				}
			}
			else
			{
				result = "FREE_SPACE,NACK,2";
			}
		}
		else if (fields[0] == "START_RECORDING" &&  fields.size() > 1)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						result = "START_RECORDING," + fields[1] + ",NACK,0";
					}
					else
					{
						auto filename = DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName(pdev->GetID(), pdev->GetRunNumber());
						pdev->SetReplayFile(filename);
						pdev->RecordBegin();
						result = "START_RECORDING," + fields[1] + ",ACK," + filename;
					}
				}
				else
				{
					result = "START_RECORDING," + fields[1] + ",NACK,1";
				}
			}
			else
			{
				result = "START_RECORDING," + fields[1] + ",NACK,2";
			}
		}
		else if (fields[0] == "STOP_RECORDING" &&  fields.size() > 1)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						pdev->RecordEnd();
						result = "STOP_RECORDING,ACK";
					}
					else
					{
						result = "STOP_RECORDING,NACK,0";
					}
				}
				else
				{
					result = "STOP_RECORDING,NACK,1";
				}
			}
			else
			{
				result = "STOP_RECORDING,NACK,2";
			}
		}
		else if (fields[0] == "REPLAY_BY_TIME" &&  fields.size() > 5)
		{
			auto& devices = manager->GetReplayDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					if (pdev->IsRecording())
					{
						result = "REPLAY_BY_TIME," + fields[1] + ",NACK,0";
					}
					else
					{
						result = "REPLAY_BY_TIME,ACK";
						pdev->ReplayFlag = SECOND;
						int unit = 1;
						if (fields[2] == "SECOND")
						{
							unit = 1000000;
							long long offset = atof(fields[3].c_str()) * unit;
							long long duration = atof(fields[4].c_str()) * unit;
							pdev->SetReplayFile(fields[5]);
							pdev->SetRelayOffset(offset);
							pdev->SetRelayDuration(duration);
							pdev->RecordBegin();
						}
						else if (fields[2] == "MILLISECOND")
						{
							long long offset = atof(fields[3].c_str()) * unit;
							long long duration = atof(fields[4].c_str()) * unit;
							pdev->SetReplayFile(fields[5]);
							pdev->SetRelayOffset(offset);
							pdev->SetRelayDuration(duration);
							pdev->RecordBegin();
						}
						else if (fields[2] == "TIMETAG")
						{
							pdev->ReplayFlag = TAG;
							pdev->SetReplayFile(fields[5]);
							pdev->SetRelayOffset(std::stoull(fields[3].c_str()));
							pdev->SetRelayDuration(std::stoull(fields[4].c_str()));
							pdev->RecordBegin();
						}
						else
						{
							result = "REPLAY_BY_TIME,NACK,3";
						}
					}
				}
				else
				{
					result = "REPLAY_BY_TIME,NACK,1";
				}
			}
			else
			{
				result = "REPLAY_BY_TIME,NACK,2";
			}
		}
		else if (fields[0] == "STOP_REPLAY" &&  fields.size() > 1)
		{
			auto& devices = manager->GetReplayDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					pdev->RecordEnd();
					result = "STOP_REPLAY,ACK";
				}
				else
				{
					result = "STOP_REPLAY,NACK,1";
				}
			}
			else
			{
				result = "STOP_REPLAY,NACK,2";
			}
		}
		else if ((fields[0] == "SET_EVENT" || fields[0] == "RESET_EVENT") && fields.size() > 3)
		{
			auto& devices = manager->GetRecordDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() >= idx && idx > 0)
			{
				auto pdev = devices[(long long)idx - 1];
				if (manager->IsRunning())
				{
					unsigned long long res = pdev->AddTag(std::stoull(fields[2].c_str()), fields[0] == "SET_EVENT", fields[3]);
					result = fields[0] + ",ACK," + std::to_string(res);
				}
				else
				{
					result = fields[0] + ",NACK,1";
				}
			}
			else
			{
				result = fields[0] + ",NACK,2";
			}
		}
	}
	else
	{
		result = "UNKNOWN_COMMAND";
	}
	result.resize(512, ' ');
}

void NetworkServer::handle_receive_from()
{
	quint16 senderPort;
	QHostAddress address;
	udpSocket->readDatagram(data_, 512, &address, &senderPort);
	process_message();
	logger->Log(result);
	updateui();
	udpSocket->writeDatagram((char*)result.c_str(), 512, address, senderPort);
}