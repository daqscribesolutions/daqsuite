HEADERS += ./networkrecorder.h \
    ./networkdevicemodel.h \
    ./networkdevicemodeldelegate.h \
    ./networkserver.h \
    ./sessionmodel.h \
    ./sessionmodeldelegate.h \
    ./settingmodel.h \
    ./settingmodeldelegate.h \
    ./pcap_header.h \
    ./usertagmodel.h \
    $$PWD/firmata.h \
    $$PWD/RemoteUI.h \
    $$PWD/settingsdialog.h \
    $$PWD/replaychannelmodel.h \
    $$PWD/recordchannelmodel.h
SOURCES += ./main.cpp \
    ./networkrecorder.cpp \
    ./networkdevicemodel.cpp \
    ./networkdevicemodeldelegate.cpp \
    ./networkserver.cpp \
    ./sessionmodel.cpp \
    ./sessionmodeldelegate.cpp \
    ./settingmodel.cpp \
    ./settingmodeldelegate.cpp \
    ./usertagmodel.cpp \
    $$PWD/firmata.cpp \
    $$PWD/RemoteUI.cpp \
    $$PWD/settingsdialog.cpp \
    $$PWD/replaychannelmodel.cpp \
    $$PWD/recordchannelmodel.cpp

FORMS += ./networkrecorder.ui \
    $$PWD/RemoteUI.ui \
    $$PWD/settingsdialog.ui
RESOURCES += networkrecorder.qrc
