#include "networkrecorder.h"
#include "NetworkDeviceManager.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "qfiledialog.h"
#include "qmessagebox.h"
#include "RemoteUI.h"
#include "SFPDPDevice.h"
#include "GESFPDPDevice.h"
#include "StorageManager.h"
#include "usertagmanager.h"
#include "logger.h"
NetworkLib::FileLog flog;
#include "networkserver.h"

struct Monitor : public IPacketMonitor
{
	Monitor(NetworkRecorder* precorder) : m_recorder(std::shared_ptr<NetworkRecorder>(precorder)) {}
	void Update(int)
	{
		if (m_recorder) m_recorder->UpdateStatus();
	}
	std::shared_ptr<NetworkRecorder> m_recorder;
};

NetworkRecorder::NetworkRecorder(const std::string& path, QWidget *parent)
	: QMainWindow(parent)
	, recordModel(this)
	, replayModel(this)
	, deviceModelDelegate(this)
	, settingModel(this)
	, settingModelDelegate(this)
	, sessionModel(this)
	, sessionModelDelegate(this)
	, userTagModel(this)
	, progressbar(new QProgressBar(this))
	, m_path(path)
	, remoteUI(new RemoteUI())
	, server(new NetworkServer())
{
	flog.SetFile(m_path + "\\log.txt");
	NetworkLib::Logger::GetInstance()->AddLog(&flog);
	ui.setupUi(this);
	StorageManager::CreatePath(m_path);
	auto netmgr = NetworkDeviceManager::GetInstance();
	netmgr->Init(m_path + "\\NetworkRecorder.xml");
	timer = new QTimer(this);
	timeTriggerTimer = new QTimer(this);
	ui.recordTableView->setModel(&recordModel);
	ui.recordTableView->resizeColumnsToContents();
	ui.replayTableView->setModel(&replayModel);
	ui.replayTableView->setItemDelegate(&deviceModelDelegate);
	ui.replayTableView->resizeColumnsToContents();
	ui.storageTableView->setModel(&settingModel);
	ui.storageTableView->setItemDelegate(&settingModelDelegate);
	ui.recordingOptionTableView->setModel(&sessionModel);
	ui.recordingOptionTableView->setItemDelegate(&sessionModelDelegate);

	ui.userTagTableView->setModel(&userTagModel);

	ui.statusBar->addPermanentWidget(progressbar.get(), 0);
	progressbar->hide();
	connect(timer, SIGNAL(timeout()), this, SLOT(UpdateStatus()));
	connect(ui.actionRun, SIGNAL(triggered()), this, SLOT(Monitor()));
	connect(ui.actionInsertTagAfter, SIGNAL(triggered()), this, SLOT(InsertTagBefore()));
	connect(ui.actionInsertTagBefore, SIGNAL(triggered()), this, SLOT(InsertTagAfter()));
	connect(ui.actionRemoveTag, SIGNAL(triggered()), this, SLOT(RemoveTag()));
	connect(ui.actionAddTagToSession, SIGNAL(triggered()), this, SLOT(AddTagToSession()));
	connect(ui.actionAddResetTagToRecord, SIGNAL(triggered()), this, SLOT(AddResetTagToSession()));
	connect(ui.actionStartRecord, SIGNAL(triggered()), this, SLOT(StartRecord()));
	connect(ui.actionStopRecord, SIGNAL(triggered()), this, SLOT(StopRecord()));
	connect(ui.actionLoadSetting, SIGNAL(triggered()), this, SLOT(LoadSetting()));
	connect(ui.actionSaveSetting, SIGNAL(triggered()), this, SLOT(SaveSetting()));
	connect(ui.actionRunServer, SIGNAL(triggered()), this, SLOT(SettingServer()));
	connect(timeTriggerTimer, SIGNAL(timeout()), this, SLOT(UpdateSessions()));
	connect(ui.actionTimeTrigger, SIGNAL(triggered()), this, SLOT(StartRecordAll()));
	connect(remoteUI.get(), SIGNAL(done()), this, SLOT(InitServer()));
	connect(server, SIGNAL(updateui()), this, SLOT(UpdateStatus()));
	connect(remoteUI.get(), SIGNAL(recordStarted()), this, SLOT(UpdateStatus()));

	ui.recordTableView->addAction(ui.actionStartRecord);
	ui.recordTableView->addAction(ui.actionStopRecord);
	ui.replayTableView->addAction(ui.actionStartRecord);
	ui.replayTableView->addAction(ui.actionStopRecord);

	ui.userTagTableView->addAction(ui.actionAddTagToSession);
	ui.userTagTableView->addAction(ui.actionInsertTagAfter);
	ui.userTagTableView->addAction(ui.actionInsertTagBefore);
	ui.userTagTableView->addAction(ui.actionRemoveTag);

	LoadUISetting();
	remoteUI->setWindowIcon(windowIcon());
	server->init(NetworkDeviceManager::GetInstance()->ServerPort);
	ResetUi();
}

NetworkRecorder::~NetworkRecorder()
{
	auto mgr = NetworkDeviceManager::GetInstance();
	mgr->Stop();
	mgr->WriteSystemInfo(m_path + "\\NetworkRecorder.xml");
	NetworkDeviceManager::Clear();
	SaveUISetting();
	if (timer) delete timer;
	if (timeTriggerTimer) delete timeTriggerTimer;
}

void NetworkRecorder::ResetUi()
{
	bool bChecked = NetworkDeviceManager::GetInstance()->IsRunning();
	ui.actionAddResetTagToRecord->setEnabled(bChecked);
	ui.actionAddTagToSession->setEnabled(bChecked);
	ui.actionRunServer->setEnabled(!bChecked);
	ui.actionStartRecord->setEnabled(bChecked);
	ui.actionStopRecord->setEnabled(bChecked);
}

void NetworkRecorder::UpdateStatus()
{
	if (NetworkDeviceManager::GetInstance()->IsRunning())
	{
		timer->start(100);
		auto networkmanager = NetworkDeviceManager::GetInstance();
		const std::vector<std::shared_ptr<INetworkDevice>>& devices = networkmanager->GetRecordDevices();
		QString status;
		for (size_t i = 0; i < devices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = devices[i];
			if (pDevice && pDevice->IsRunning())
			{
				break;
			}
		}
		recordModel.dataChanged(QModelIndex(), QModelIndex());
		replayModel.dataChanged(QModelIndex(), QModelIndex());
		if (!ui.actionRun->isChecked()) ui.actionRun->setChecked(true);
	}
	else
	{
		if (ui.actionRun->isChecked()) ui.actionRun->setChecked(false);
		timer->stop();
	}
	ResetUi();
}

void NetworkRecorder::Monitor()
{
	setFocus();
	if (!ui.actionRun->isChecked())
	{
		NetworkDeviceManager::GetInstance()->Stop();
		ResetUi();
		timer->stop();
	}
	else
	{
		auto devices = NetworkDeviceManager::GetInstance()->GetRecordDevices();
		bool activeflag = false;
		for (int i = 0; i < (int)devices.size(); ++i)
		{
			auto dev = devices[i];
			if (dev->GetActiveFlag())
			{
				activeflag = true;
				break;
			}
		}

		auto replaydevices = NetworkDeviceManager::GetInstance()->GetReplayDevices();
		for (int i = 0; i < (int)replaydevices.size(); ++i)
		{
			auto dev = replaydevices[i];
			if (dev->GetActiveFlag())
			{
				activeflag = true;
				break;
			}
		}

		if (activeflag)
		{
			if (!NetworkDeviceManager::GetInstance()->IsRunning())
			{
				NetworkDeviceManager::GetInstance()->SetRunMode(MONITORING);
				NetworkDeviceManager::GetInstance()->Monitor();
				ResetUi();
				timer->start(100);
			}
		}
	}
}

void NetworkRecorder::StartRecord()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx && idx >= 0)
			{
				auto& dev = devices[idx];
				dev->SetReplayFile(DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber()));
				dev->RecordBegin();
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx && idx >= 0)
			{
				replyDevices[idx]->RecordBegin();
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::StartRecordAll()
{
	timeTriggerTimer->start(100);
}

void NetworkRecorder::StopRecord()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->RecordEnd();
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx)
			{
				replyDevices[idx]->RecordEnd();
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::SetUp()
{
	QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	auto& devices = networkmanager->GetRecordDevices();
	for (int i = 0; i < indexes.size(); ++i)
	{
		QModelIndex index = indexes.at(i);
		size_t idx = index.row();
		if (devices.size() > idx)
		{
			devices[idx]->SetRunOption(WRITE_TO_DEVICE);
		}
	}
	int nRow = recordModel.rowCount() - 1;
	recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
}

void NetworkRecorder::SetDown()
{
	QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	auto& devices = networkmanager->GetRecordDevices();
	for (int i = 0; i < indexes.size(); ++i)
	{
		QModelIndex index = indexes.at(i);
		size_t idx = index.row();
		if (devices.size() > idx)
		{
			devices[idx]->SetRunOption(READ_FROM_DEVICE);
		}
	}
	int nRow = recordModel.rowCount() - 1;
	recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
}

void NetworkRecorder::Enable()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->SetActiveFlag(true);
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx)
			{
				replyDevices[idx]->SetActiveFlag(true);
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::Disable()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->SetActiveFlag(false);
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx)
			{
				replyDevices[idx]->SetActiveFlag(false);
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::UpdateSessions()
{
	auto setting = DeviceRecordSetting::GetInstance();
	time_t now;
	time(&now);
	tm n = *localtime(&now);
	double diff = difftime(mktime(&setting->RecordTriggerTime), mktime(&n));
	if (0 >= diff)
	{
		ui.statusBar->showMessage("Record is triggered", 5000);
		timeTriggerTimer->stop();
		ui.actionTimeTrigger->setCheckable(false);
		auto networkmanager = NetworkDeviceManager::GetInstance();
		networkmanager->Record();
		ResetUi();
	}
}

void NetworkRecorder::InsertTagBefore()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
	auto mgr = UserTagManager::GetInstance();
	mgr->Insert(index.row() + 1);
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::InsertTagAfter()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
	auto mgr = UserTagManager::GetInstance();
	mgr->Insert(index.row());
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::RemoveTag()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
	auto mgr = UserTagManager::GetInstance();
	mgr->Remove(index.row());
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::AddResetTagToSession()
{
	QModelIndexList tags = ui.userTagTableView->selectionModel()->selection().indexes();
	if (tags.size() > 0)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto networkmanager = NetworkDeviceManager::GetInstance();
		auto& devices = networkmanager->GetRecordDevices();
		QString mss = " Tagged channel : ";
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->AddTag(tags.front().row(), false);
				mss += QString::number(idx) + " " + QString::number(tags.front().row());
			}
		}
		ui.statusBar->showMessage(mss, 1000);
	}
}

void NetworkRecorder::AddTagToSession()
{
	QModelIndexList tags = ui.userTagTableView->selectionModel()->selection().indexes();
	if (tags.size() > 0)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto networkmanager = NetworkDeviceManager::GetInstance();
		auto& devices = networkmanager->GetRecordDevices();
		QString mss = " Tagged channel : ";
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->AddTag(tags.front().row(), true);
				mss += QString::number(idx) + " " + QString::number(tags.front().row());
			}
		}
		ui.statusBar->showMessage(mss, 1000);
	}
}

void NetworkRecorder::SaveUISetting()
{
	QSettings settings("DaqSuite", "NetworkRecorder");
	settings.beginGroup("UI");
	settings.setValue("size", size());
	settings.setValue("pos", pos());
	settings.endGroup();
}

void NetworkRecorder::LoadUISetting()
{
	QSettings settings("DaqSuite", "NetworkRecorder");
	settings.beginGroup("UI");
	resize(settings.value("size", QSize(800, 600)).toSize());
	move(settings.value("pos", QPoint(200, 200)).toPoint());
	settings.endGroup();
}

void NetworkRecorder::LoadSetting()
{
	std::string path = DeviceRecordSetting::GetInstance()->GetRecordPath();
	QString fileName = QFileDialog::getOpenFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
	if (fileName != "")
	{
		NetworkDeviceManager::GetInstance()->ReadSystemInfo(fileName.toStdString());
		ui.userTagTableView->setModel(0);
		ui.userTagTableView->setModel(&userTagModel);
		ui.recordTableView->setModel(0);
		ui.recordTableView->setModel(&recordModel);
		ui.replayTableView->setModel(0);
		ui.replayTableView->setModel(&replayModel);
		ui.recordingOptionTableView->setModel(0);
		ui.recordingOptionTableView->setModel(&sessionModel);
	}
}

void NetworkRecorder::SaveSetting()
{
	std::string path = DeviceRecordSetting::GetInstance()->GetRecordPath();
	QString fileName = QFileDialog::getSaveFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
	if (fileName != "")
	{
		NetworkDeviceManager::GetInstance()->WriteSystemInfo(fileName.toStdString());
	}
}

void NetworkRecorder::SettingServer()
{
	if (remoteUI)
	{
		remoteUI->getSetting();
		remoteUI->show();
	}
}

void NetworkRecorder::InitServer()
{
	server->init(NetworkDeviceManager::GetInstance()->ServerPort);
}
