#include "replaychannelmodel.h"

#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "pcap_header.h"
#include "SFPDPDevice.h"

ReplayChannelModel::ReplayChannelModel(QObject *parent)
	: QAbstractTableModel(parent)
{
	manager = NetworkDeviceManager::GetInstance();
}

ReplayChannelModel::~ReplayChannelModel()
{
}

Qt::ItemFlags ReplayChannelModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (!NetworkDeviceManager::GetInstance()->IsRunning())
	{
		if (index.column() == 1)
		{
			return flags | Qt::ItemIsUserCheckable;
		}
		else if (index.column() > 0 && index.column() < 6)
		{
			return flags | Qt::ItemIsEditable;
		}
	}
	return flags;
}

int ReplayChannelModel::rowCount(const QModelIndex &) const
{
	return (int)manager->GetReplayDevices().size();
}

int ReplayChannelModel::columnCount(const QModelIndex &) const
{
	return 11;
}

QVariant ReplayChannelModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetReplayDevices();
		if (index.row() < (int)devices.size())
		{
			const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			if (index.column() == 0)
			{
				return QString(pDevice->Name.c_str());
			}
			else if (index.column() == 2)
			{
				return pDevice->GetReplayFile().c_str();
			}
			else if (index.column() == 3)
			{
				return pDevice->ReplayFlag == SECOND ? "Time(Sec)" : "Time Tag";
			}
			else if (index.column() == 4)
			{
				return pDevice->ReplayFlag == SECOND ? QString::number(pDevice->GetRelayOffset() / 1000000.0, 'f', 3)
					: QString::number(pDevice->GetRelayOffset());
			}
			else if (index.column() == 5)
			{
				return pDevice->ReplayFlag == SECOND ? QString::number(pDevice->GetRelayDuration() / 1000000.0, 'f', 3)
					: QString::number(pDevice->GetRelayDuration());
			}
			else if (index.column() == 6)
			{
				return pDevice->ReplayFlag == SECOND ? QString::number(pDevice->GetRelayRecordDuration() / 1000000.0, 'f', 3)
					: QString::number(pDevice->GetRelayRecordDuration());
			}
			else if (index.column() == 7)
			{
				return QString::number(pDevice->GetCurrentSpeed() / 1000000.0, 'f', 2);
			}
			else if (index.column() == 8)
			{
				return QString::number(pDevice->GetTotalDataSize() / 1000000.0, 'f', 2);
			}
			else if (index.column() == 9)
			{
				return pDevice->IsRecording() ? "Replaying" : "Stopped";
			}
			else if (index.column() == 10)
			{
				return pDevice->ErrorInfo.c_str();
			}
		}

	}
	else if (role == Qt::CheckStateRole)
	{
		if (index.column() == 1)
		{
			const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetReplayDevices();
			if (index.row() < (int)devices.size())
			{
				const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
				return (pDevice->GetActiveFlag() ? Qt::Checked : Qt::Unchecked);
			}
		}
	}
	return QVariant();
}

bool ReplayChannelModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetReplayDevices();
		if (index.row() < (int)devices.size())
		{
			const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			if (index.column() == 2)
			{
				pDevice->SetReplayFile(value.toString().toStdString());
			}
			else if (index.column() == 3)
			{
				pDevice->ReplayFlag = value.toString() == "Time(Sec)" ? SECOND : TAG;
				pDevice->SetReplayFile(pDevice->GetReplayFile());
			}
			else if (index.column() == 4)
			{
				pDevice->ReplayFlag == SECOND ? pDevice->SetRelayOffset(value.toDouble() * 1000000)
				: pDevice->SetRelayOffset(value.toULongLong());
			}
			else if (index.column() == 5)
			{
				pDevice->ReplayFlag == SECOND ? pDevice->SetRelayDuration(value.toDouble() * 1000000)
					: pDevice->SetRelayDuration(value.toULongLong());
			}
			return true;
		}
	}
	else if (role == Qt::CheckStateRole)
	{
		if (index.column() == 1)
		{
			const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetReplayDevices();
			if (index.row() < (int)devices.size())
			{
				const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
				pDevice->SetActiveFlag((value.toInt() == Qt::Checked ? true : false));
			}
			return true;
		}
	}
	return false;
}

QVariant ReplayChannelModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Name");
			}
			else if (section == 1)
			{
				return QString("Active");
			}
			else if (section == 2)
			{
				return QString("Path");
			}
			else if (section == 3)
			{
				return QString("Unit Type");
			}
			else if (section == 4)
			{
				return QString("Offset/Start\n(Sec/T-Tag)");
			}
			else if (section == 5)
			{
				return QString("Duration/Stop\n(Sec/T-Tag)");
			}
			else if (section == 6)
			{
				return QString("Record Duration\n(Sec/T-Tag)");
			}
			else if (section == 7)
			{
				return QString("Data Speed\n(MBytes/sec)");
			}
			else if (section == 8)
			{
				return QString("Replay Data\n(MBytes)");
			}
			else if (section == 9)
			{
				return QString("Status");
			}
			else if (section == 10)
			{
				return QString("Messages");
			}
		}
		else
		{
			return QString::number(section + 1);
		}
	}

	return QVariant();
}
