#ifndef SETTINGMODEL_H
#define SETTINGMODEL_H

#include <QAbstractTableModel>
namespace NetworkLib
{
	struct DiskInfo;
}

class SettingModel : public QAbstractTableModel
{
	Q_OBJECT

public:

	SettingModel(QObject *parent);

	~SettingModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;

	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	
private:

	std::vector<NetworkLib::DiskInfo> drives;
};

#endif // SETTINGMODEL_H
