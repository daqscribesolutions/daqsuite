#ifndef REMOTEUI_H
#define REMOTEUI_H

#include <QWidget>
#include "ui_RemoteUI.h"
#include "firmata.h"

class RemoteUI : public QWidget
{
	Q_OBJECT

public:
	RemoteUI(QWidget *parent = 0);
	~RemoteUI();
	void getSetting();
	void closeEvent(QCloseEvent *event);
public slots:
	void run();
	void setting(); 
	void comhandle(int idx);
signals:
	void done();
	void recordStarted();
private:
	Ui::RemoteUI ui;
	Firmata::FirmataController firmctr;
};

#endif // REMOTEUI_H
