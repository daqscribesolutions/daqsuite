
// DSIWriter.cpp : Defines the class behaviors for the application.
//
#pragma warning (disable : 4005)

#include "stdafx.h"
#include "DSIWriter.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
using namespace boost::algorithm;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CDSIWriterApp
BEGIN_MESSAGE_MAP(CDSIWriterApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CDSIWriterApp construction
CDSIWriterApp::CDSIWriterApp()
{
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;
}


// The one and only CDSIWriterApp object
CDSIWriterApp theApp;

#define DSI_FORMAT_TEXT                          _T("DaqScribe PI660DS DSI")
#define DSI_VERSION_TEXT_V1                      _T("V1.0")
#define DSI_VERSION_TEXT_V2                      _T("V2.0")


std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
{
	size_t current;
	size_t next = -1;
	std::vector<std::string> columns;
	int nColumns = 0;
	do
	{
		current = next + 1;
		next = line.find_first_of(delimiters, current);
		std::string field = line.substr(current, next - current);
		trim_right(field);
		trim_left(field);
		columns.push_back(field);
		++nColumns;
	} while (next != std::string::npos);
	return columns;
}

struct ChannelInfo
{
	ChannelInfo() : sensitivity(1), offset(0), eusensitivity(1), euoffset(0) {}
	std::string name, description, euname;
	double sensitivity, offset, eusensitivity, euoffset;
};

bool LoadChannelInfo(std::vector<ChannelInfo>& channels, const std::string& filename)
{
	std::ifstream fin(filename.c_str());
	std::string line;
	channels.clear();
	getline(fin, line);
	while (getline(fin, line))
	{
		std::vector<std::string> items = GetFields(line, ",");
		if (items.size() >= 8)
		{
			ChannelInfo info;
			info.name = items[0];
			info.description = items[1];
			info.sensitivity = stof(items[3]);
			info.offset = stof(items[4]);
			info.eusensitivity = stof(items[5]);
			info.euoffset = stof(items[6]);
			info.euname = items[7];
			channels.push_back(info);
		}
	}
	return channels.size() > 0;
}

bool SaveDSI(std::vector<ChannelInfo>& channels, const std::string& filename, const std::string& plan)
{
	std::string file = filename.substr(0, filename.size() - 3) + "dsi";
	CFile hFile;
	CString text;
	text.Format(_T("%s"), file.c_str());

	if (hFile.Open(text, CFile::modeCreate | CFile::modeWrite) == FALSE)
		return false;

	CArchive ar(&hFile, CArchive::store);
	// Version Info
	text.Format(_T("%s %s"), DSI_FORMAT_TEXT, DSI_VERSION_TEXT_V2);
	ar << text;

	// Test Name
	text.Format(_T("%s"), plan.c_str());
	ar << text;

	// Ch Count
	ar << (int)channels.size();

	// Ch Info
	for (int index = 0; index < (int)channels.size(); index++)
	{
		// Get Channel Object
		auto ch = channels[index];
		// Ch Name : CString
		text.Format(_T("%s"), ch.name.c_str());
		ar << text;

		// Description : CString
		text.Format(_T("%s"), ch.description.c_str());
		ar << text;

		// Location : CString
		text.Format(_T("%s"), ch.description.c_str());
		ar << text;

		// VC Offset : double
		ar << ch.offset;

		// VC Sensitivity : double
		ar << ch.sensitivity;

		// Unit : CString
		text.Format(_T("%s"), ch.euname.c_str());
		ar << text;

		// Full-Scale EU Range : double
		double DoubleValue = 0.0;
		ar << DoubleValue;

		// EU Offset : double
		ar << ch.euoffset;

		// EU Sensitivity : double
		ar << ch.eusensitivity;

		// EU Squared Sensitivity : double
		ar << DoubleValue;

		// EU Cubed Sensitivity : double
		ar << DoubleValue;

		// EU Gain : double
		ar << DoubleValue;

		// Card Type : short
		short ShortValue = 0;
		ar << ShortValue;

		// Gage Type : int
		int IntValue = 0;
		ar << IntValue;
	}

	// Flush the unwritten data
	ar.Flush();

	// Flush the unwritten data into File
	hFile.Flush();

	// Close File
	hFile.Close();

	return true;
}

// CDSIWriterApp initialization
BOOL CDSIWriterApp::InitInstance()
{
	CString file = m_lpCmdLine;
	if (file == _T(""))
	{
		std::cout << "No file to convert. Input is channel infomation csv file.\n";
	}
	else
	{
		std::string input, plan = "test";
		for (int i = 0; i < file.GetLength(); ++i)
		{
			input.push_back((char)file[i]);
		}
		std::vector<std::string> items = GetFields(input, ",");
		std::string filename = input;
		if (items.size() > 1)
		{
			filename = items[0];
			plan = items[1];
		}
		std::vector<ChannelInfo> channels;
		if (LoadChannelInfo(channels, filename))
		{
			SaveDSI(channels, filename, plan);
		}
	}
	return FALSE;
}

