// RemoteRecorder.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "../NetworkLib/NetworkDeviceManager.h"
#include "../DeviceLib/StorageManager.h"
#include "../DeviceLib/logger.h"
using namespace NetworkLib;

#include <string>
#include <boost/asio.hpp>
#include "boost/bind.hpp"

using boost::asio::ip::udp;

class server
{
public:
	server(boost::asio::io_service& io_service, short port)
		: io_service_(io_service),
		socket_(io_service, udp::endpoint(udp::v4(), port))
		, manager(NetworkDeviceManager::GetInstance())
	{
		logger = DeviceLib::Logger::GetInstance();
		logger->AddLog(&log);
		reset_message();
	}

	void get_message()
	{
		socket_.async_receive_from(
			boost::asio::buffer(data_, max_length), sender_endpoint_,
			boost::bind(&server::handle_receive_from, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	struct Tokenizer
	{
		std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
		{
			size_t current;
			size_t next = -1;
			std::vector<std::string> columns;
			int nColumns = 0;
			do
			{
				current = next + 1;
				next = line.find_first_of(delimiters, current);
				std::string field = line.substr(current, next - current);
				columns.push_back(field);
				++nColumns;
			} while (next != std::string::npos);
			return columns;
		}
	};

	void reset_message()
	{
		memset(data_, 0, max_length);
		result = "";
	}

	void process_message()
	{
		Tokenizer tok;
		std::vector<std::string> fields = tok.GetFields(data_, ",");
		reset_message();
		if (fields.size() > 0)
		{
			if (fields[0] == "REMAINING_RECORD_TIME")
			{
				std::string error = manager->GetErrorInfo();
				if (error != "")
				{
					result = "ERROR," + error;
				}
				else if (manager->IsRunning())
				{
					result = "BUSY,";
					std::stringstream str;
					str.setf(std::ios::fixed);
					str << manager->GetRemainingRecordDuration();
					result += str.str();
				}
				else
				{
					result = "IDLE," + manager->GetErrorInfo();
				}
			}
			else if (fields[0] == "GET_STATUS")
			{
				std::string error = manager->GetErrorInfo();
				if (error != "")
				{
					result = "ERROR," + error;
				}
				else if (manager->IsRunning())
				{
					result = "BUSY";
				}
				else
				{
					result = "IDLE";
				}
			}
			else if (fields[0] == "START_RECORDING")
			{
				if (manager->IsRunning())
				{
					result = "BUSY,system is already running";
				}
				else
				{
					manager->SetRunMode(RECORDING);
					manager->Monitor();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else if (fields[0] == "STOP_RECORDING")
			{
				if (manager->IsRunning())
				{
					manager->Stop();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
				else
				{
					result = "IDLE,system is not running";
				}
			}
			else if (fields[0] == "REPLAY_CURRENT_BY_DURATION")
			{
				if (manager->IsRunning())
				{
					manager->Relay();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
				else
				{
					result = "IDLE,system is not running";
				}
			}
			else if (fields[0] == "REPLAY_CURRENT_BY_LENGTH")
			{
				if (manager->IsRunning())
				{
					manager->Relay();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
				else
				{
					result = "IDLE,system is not running";
				}
			}
			else if (fields[0] == "REPLAY_FILE_BY_DURATION")
			{
				if (manager->IsRunning())
				{
					result = "BUSY,system is already running";
				}
				else
				{
					manager->Monitor();
					manager->Run();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else if (fields[0] == "REPLAY_FILE_BY_LENGTH")
			{
				if (manager->IsRunning())
				{
					result = "BUSY,system is already running";
				}
				else
				{
					manager->Monitor();
					manager->Run();
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else if (manager->IsRunning())
					{
						result = "BUSY";
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
		}
		result.resize(512, ' ');
	}

	void handle_receive_from(const boost::system::error_code& error,
		size_t bytes_recvd)
	{
		if (!error && bytes_recvd > 0)
		{
			logger->Log(data_);
			process_message();
			logger->Log(result);
			socket_.async_send_to(
				boost::asio::buffer(result, result.size()), sender_endpoint_,
				boost::bind(&server::handle_send_to, this,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));
		}
	}

	void handle_send_to(const boost::system::error_code& error, size_t bytes_sent)
	{
	}

private:

	boost::asio::io_service& io_service_;

	NetworkDeviceManager* manager;
	DeviceLib::Logger* logger;

	udp::socket socket_;

	udp::endpoint sender_endpoint_;

	DeviceLib::FileLog log;

	enum { max_length = 512 };

	char data_[max_length];
	std::string result;

};

int main(int argc, char* argv[])
{
	try
	{
		std::string path = "C:\\ProgramData\\DaqScribe";
		DeviceLib::StorageManager::CreatePath(path);
		DeviceLib::Logger::GetInstance()->SetLogFile(path + "\\log.txt");
		NetworkDeviceManager::GetInstance()->Init(path + "\\NetworkRecorder.xml");

		if (argc != 2)
		{
			std::cerr << "Usage: RemoteRecorder <port>\n";
			return 1;
		}
		boost::asio::io_service io_service;
		DeviceLib::Logger::GetInstance()->Log("server started.");

		using namespace std;
		server s(io_service, atoi(argv[1]));
		while (true)
		{
			s.get_message();
			io_service.run();
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
		DeviceLib::Logger::GetInstance()->Log(e.what());
	}

	return 0;
}