
#include <iostream>
#include <string>
#include <ctime>

#include "../DeviceLib/DeviceLib.h"

using namespace DeviceLib;

int main(int argc, char* argv[])
{
	if (argc < 5)
	{
		std::cout << "Command: [EXE] [Host IP] [Host port] [Sample rate per second] [Running time in second]\n";
		return 1;
	}

	SystemManager::GetInstance()->AddVirtualDevice();

	const std::vector<std::shared_ptr<IDevice>>& devices = SystemManager::GetInstance()->GetDevices();
	VirtualDevice* pDevice = dynamic_cast<VirtualDevice*>(devices.front().get());
	pDevice->SetSampleRatePerSecond(atoi(argv[3]));
	pDevice->Initialize();
	SystemManager::GetInstance()->AddClientDevice(argv[1], devices.front(), atoi(argv[2]));
	SystemManager::GetInstance()->StartMonitor();

	double d = clock();
	int durataion = atoi(argv[4]) * 1000;
	while (clock() - d < durataion)
	{
	}

	SystemManager::GetInstance()->StopMonitor();

	return 0;
}
