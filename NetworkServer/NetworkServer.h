#pragma once

#include "resource.h"

#include <iostream>
#include "NetworkDeviceManager.h"
#include "StorageManager.h"
#include "logger.h"
#include <string>
#include <boost/asio.hpp>
#include "boost/bind.hpp"

using boost::asio::ip::udp;
class NetworkServer
{

public:

	NetworkServer(boost::asio::io_service& io_service, short port);

	void get_message();

	void reset_message();

	void process_message();

	void handle_receive_from(const boost::system::error_code& error, size_t bytes_recvd);

	void handle_send_to(const boost::system::error_code& error, size_t bytes_sent);

private:

	boost::asio::io_service& io_service_;

	NetworkLib::NetworkDeviceManager* manager;

	NetworkLib::Logger* logger;

	udp::socket socket_;

	udp::endpoint sender_endpoint_;

	enum { max_length = 512 };

	char data_[max_length];

	std::string result;

};