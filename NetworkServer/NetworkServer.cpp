// NetworkServer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "NetworkServer.h"
#include <iostream>

int server_main(int port)
{
	try
	{
		std::string path = "C:\\ProgramData\\DaqScribe";
		NetworkLib::StorageManager::CreatePath(path);
		NetworkLib::NetworkDeviceManager::GetInstance()->Init(path + "\\NetworkRecorder.xml");

		boost::asio::io_service io_service;
		NetworkLib::FileLog flog;
		flog.SetFile(path + "\\server.log");
		NetworkLib::Logger::GetInstance()->AddLog(&flog);
		NetworkLib::Logger::GetInstance()->Log("server started.");

		using namespace std;
		NetworkServer s(io_service, port);
		while (true)
		{
			s.get_message();
			io_service.run();
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
		NetworkLib::Logger::GetInstance()->Log(e.what());
	}

	return 0;
}

int main(int argc, char** argv)
{
	if (argc > 1)
	{
		int port = atoi(argv[1]);
		std::cout << "Using port " << port << "\n";
		server_main(port);
	}
	else
	{
		std::cout << "Wrong argument : ex) NetworkServer [port]\n";
	}
	return 0;
}
using namespace NetworkLib;

NetworkServer::NetworkServer(boost::asio::io_service& io_service, short port)
	: io_service_(io_service)
	, socket_(io_service, udp::endpoint(udp::v4(), port))
	, manager(NetworkDeviceManager::GetInstance())
{
	logger = NetworkLib::Logger::GetInstance();
	reset_message();
}

void NetworkServer::get_message()
{
	socket_.async_receive_from(
		boost::asio::buffer(data_, max_length), sender_endpoint_,
		boost::bind(&NetworkServer::handle_receive_from, this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred));
}

std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
{
	size_t current;
	size_t next = -1;
	std::vector<std::string> columns;
	int nColumns = 0;
	do
	{
		current = next + 1;
		next = line.find_first_of(delimiters, current);
		std::string field = line.substr(current, next - current);
		columns.push_back(field);
		++nColumns;
	} while (next != std::string::npos);
	return columns;
}

void NetworkServer::reset_message()
{
	memset(data_, 0, max_length);
	result = "";
}

void NetworkServer::process_message()
{
	std::vector<std::string> fields = GetFields(data_, ",");
	reset_message();
	if (fields.size() > 0)
	{
		if (fields[0] == "GET_STATUS")
		{
			std::string error = manager->GetErrorInfo();
			if (error != "")
			{
				result = "ERROR," + error;
			}
			else if (manager->IsRunning())
			{
				result = "BUSY";
			}
			else
			{
				result = "IDLE";
			}
		}
		else if (fields[0] == "MONITOR")
		{
			if (manager->IsRunning())
			{
				result = "BUSY,system is already monitoring";
			}
			else
			{
				manager->Monitor();
				std::string error = manager->GetErrorInfo();
				if (error != "")
				{
					result = "ERROR," + error;
				}
				else if (manager->IsRunning())
				{
					result = "BUSY,system is monitoring";
				}
				else
				{
					result = "IDLE,failed to monitor";
				}
			}
		}
		else if (fields[0] == "STOP")
		{
			if (!manager->IsRunning())
			{
				result = "IDLE,system is already stopped";
			}
			else
			{
				manager->Stop();
				std::string error = manager->GetErrorInfo();
				if (error != "")
				{
					result = "ERROR," + error;
				}
				else if (manager->IsRunning())
				{
					result = "BUSY,system is still monitoring";
				}
				else
				{
					result = "IDLE,system is stopped";
				}
			}
		}
		else if (fields[0] == "START_RECORDING" &&  fields.size() > 1)
		{
			auto networkmanager = NetworkDeviceManager::GetInstance();
			auto& devices = networkmanager->GetDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() > idx)
			{
				auto pdev = devices[(long long)idx];
				if (!manager->IsRunning())
				{
					manager->Monitor();
				}
				if (manager->IsRunning())
				{
					if (pdev->GetRunOption() == READ_FROM_DEVICE)
					{
						if (pdev->IsRecording())
						{
							result = "ERROR,channel " + std::to_string((long long)idx) + " is already recording";
						}
						else
						{
							result = "BUSY,channel " + std::to_string((long long)idx) + " is recording";
							pdev->RecordBegin();
						}
					}
					else
					{
						result = "ERROR,channel " + std::to_string((long long)idx) + " is set to relay";
					}
				}
				else
				{
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else
			{
				result = "ERROR,channel " + std::to_string((long long)idx) + " does not exist";
			}
		}
		else if (fields[0] == "STOP_RECORDING" &&  fields.size() > 1)
		{
			auto networkmanager = NetworkDeviceManager::GetInstance();
			auto& devices = networkmanager->GetDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() > idx)
			{
				auto pdev = devices[(long long)idx];
				if (manager->IsRunning())
				{
					if (pdev->GetRunOption() == READ_FROM_DEVICE)
					{
						if (pdev->IsRecording())
						{
							pdev->RecordEnd();
							result = "IDLE,channel " + std::to_string((long long)idx) + " is stopped recording";
						}
						else
						{
							result = "ERROR,channel " + std::to_string((long long)idx) + " is already stopped recording";
						}
					}
					else
					{
						result = "ERROR,channel " + std::to_string((long long)idx) + " is set to relay";
					}
				}
				else
				{
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else
			{
				result = "ERROR,channel " + std::to_string((long long)idx) + " does not exist";
			}
		}
		else if (fields[0] == "START_RELAY" &&  fields.size() > 1)
		{
			auto networkmanager = NetworkDeviceManager::GetInstance();
			auto& devices = networkmanager->GetDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() > idx)
			{
				auto pdev = devices[(long long)idx];
				if (!manager->IsRunning())
				{
					manager->Monitor();
				}
				if (manager->IsRunning())
				{
					if (pdev->GetRunOption() == WRITE_TO_DEVICE)
					{
						if (pdev->IsRecording())
						{
							result = "ERROR,channel " + std::to_string((long long)idx) + " is already relaying";
						}
						else
						{
							result = "BUSY,channel " + std::to_string((long long)idx) + " is relaying";
							pdev->RecordBegin();
						}
					}
					else
					{
						result = "ERROR,channel " + std::to_string((long long)idx) + " is set to record";
					}
				}
				else
				{
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else
			{
				result = "ERROR,channel " + std::to_string((long long)idx) + " does not exist";
			}
		}
		else if (fields[0] == "STOP_RELAY" &&  fields.size() > 1)
		{
			auto networkmanager = NetworkDeviceManager::GetInstance();
			auto& devices = networkmanager->GetDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() > idx)
			{
				auto pdev = devices[(long long)idx];
				if (manager->IsRunning())
				{
					if (pdev->GetRunOption() == WRITE_TO_DEVICE)
					{
						if (pdev->IsRecording())
						{
							pdev->RecordEnd();
							result = "IDLE,channel " + std::to_string((long long)idx) + " is stopped relaying";
						}
						else
						{
							result = "ERROR,channel " + std::to_string((long long)idx) + " is already stopped relaying";
						}
					}
					else
					{
						result = "ERROR,channel " + std::to_string((long long)idx) + " is set to record";
					}
				}
				else
				{
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else
			{
				result = "ERROR,channel " + std::to_string((long long)idx) + " does not exist";
			}
		}
		else if (fields[0] == "TAG_CHANNEL" &&  fields.size() > 3)
		{
			auto networkmanager = NetworkDeviceManager::GetInstance();
			auto& devices = networkmanager->GetDevices();
			size_t idx = atoi(fields[1].c_str());
			if (devices.size() > idx)
			{
				auto pdev = devices[(long long)idx];
				if (manager->IsRunning())
				{
					if (pdev->GetRunOption() == READ_FROM_DEVICE)
					{
						if (pdev->IsRecording())
						{
							pdev->AddTag(atoi(fields[2].c_str()));
							result = "IDLE,user tag is added to channel " + std::to_string((long long)idx);
						}
						else
						{
							result = "ERROR,channel " + std::to_string((long long)idx) + " is stopped recording";
						}
					}
					else
					{
						result = "ERROR,channel " + std::to_string((long long)idx) + " is set to relay";
					}
				}
				else
				{
					std::string error = manager->GetErrorInfo();
					if (error != "")
					{
						result = "ERROR," + error;
					}
					else
					{
						result = "IDLE," + manager->GetErrorInfo();
					}
				}
			}
			else
			{
				result = "ERROR,channel " + std::to_string((long long)idx) + " does not exist";
			}
		}
	}
	else
	{
		result = "Wrong command";
	}
	result.resize(512, ' ');
}

void NetworkServer::handle_receive_from(const boost::system::error_code& error, size_t bytes_recvd)
{
	if (!error && bytes_recvd > 0)
	{
		logger->Log(data_);
		process_message();
		logger->Log(result);
		socket_.async_send_to(
			boost::asio::buffer(result, result.size()), sender_endpoint_,
			boost::bind(&NetworkServer::handle_send_to, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}
}

void NetworkServer::handle_send_to(const boost::system::error_code& error, size_t bytes_sent)
{
}
