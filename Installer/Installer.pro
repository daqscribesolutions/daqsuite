TEMPLATE = aux

INSTALLER = installer

INPUT = $$PWD/config/config.xml $$PWD/packages
DaqHunter.input = INPUT
DaqHunter.output = $$INSTALLER
DaqHunter.commands = C:/Qt/QtIFW2.0.1/bin/binarycreator -c $$PWD/config/config.xml -p $$PWD/packages ${QMAKE_FILE_OUT}
DaqHunter.CONFIG += target_predeps no_link combine

QMAKE_EXTRA_COMPILERS += DaqHunter

DISTFILES += \
    packages/DaqScribe/meta/package.xml \
    ../build

